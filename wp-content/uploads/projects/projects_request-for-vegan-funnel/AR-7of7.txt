Subject: The Amazing Jackfruit

Hi there!

In our last email, we made delicious “chicken” nuggets using 
chickpeas instead of meat. 

Today, we’ll take a look at a newcomer to the Western diet: 
jackfruit.

Jackfruit is native to India and has only recently popped 
up on American plates as a meat substitute because it can 
taste like pulled pork, shredded chicken, or even crab cakes 
if you prepare it correctly. 

You do need to know that jackfruit isn’t high in protein 
(only 2 grams per cup of fruit), so add additional protein 
sources to your diet along with the fruit.

Today, we’ll be making Jackfruit Crab Cakes. These have a 
slightly crunchy outer layer and are perfectly moist on the 
inside, similar to Maryland Crab Cakes.

Ingredients:
• 24 ounces of young jackfruit in water (not brine!)
• 2 TBSP ground flax seed
• 6 TBSP water
• 1 tsp Colman’s mustard powder, mixed well with 1 tsp water
• 1 TBSP Old Bay spice
• 1 tsp Worcestershire sauce (can use vegan variety)
• 2 cloves of garlic, minced (or ½ tsp garlic powder)
• 1 TBSP lemon juice
• 1 tsp ground sea salt
• ½ tsp ground black pepper
• 3 TBSP chives, minced
• ¼ cup cilantro, minced
• ½ cup classic breadcrumbs
• ½ cup Panko breadcrumbs

Drain jackfruit and rinse well. Squeeze and pat dry, 
then chop until it resembles lump crab meat.

Mix ground flax seeds with 6 TBSP water to make “flax egg.” 

Set aside and let sit for 10 minutes.

Put all the dry seasonings, mustard, lemon juice, and 
Worcestershire sauce in a large bowl and whisk together 
until completely combined.

Add the jackfruit, “flax egg,” herbs and breadcrumbs. Combine 
with your hands until the mixture begins to stick together. 

Form into cakes. Depending on how large your cakes are, this 
recipe will make varying numbers of cakes.

Place the cakes on a plate and refrigerate for at least one 
hour.

To Bake:

Preheat oven to 375 degrees. Place cakes on a baking sheet. 

Squeeze lemon juice over the top of the cakes and bake for about 
ten minutes or until golden brown. Flip cakes, squeeze more lemon 
juice over the tops and bake another ten minutes until golden brown 
and firm to the touch.

To Fry:

Cover the bottom of a large skillet with a thin layer of oil 
(2-3 tablespoons) and heat on medium-high until hot. 

Once oil is heated, add the cakes and cook until golden brown 
(about five minutes). Flip cakes and cook until other side is 
golden brown and cake is firm to the touch. 

Happy eating – we’ll see you next time!

{Your Name Here}