Subject: Tempeh Tricks & Tips

Hi there,

In our last email, we talked about how to use 
tofu to create a tasty meal. 

Today, we’ll think about tempeh – what is it and 
how do you cook it?

Tempeh is an Indonesian food made from fermented 
soybeans. This gives it a unique flavor that not 
everyone likes, but it’s especially good for you. 

It’s also less processed than tofu! 

The fermentation happens because of a beneficial 
mold named Rhizopus ogliosporus, which, in later 
stages, can give the tempeh a grayish color. 

If your tempeh has a few gray spots, it’s OK – but 
you can also cut them out if you wish. A limp, gray 
piece of tempeh, however, has gone bad so toss that out.

Tempeh comes in several varieties: regular, multigrain, 
flax, and a few more. 

If you’re new to this product, it’s probably a good 
idea to start with the regular, which is just the 
fermented soy without other ingredients. 

The other products have additional plant ingredients 
fermented along with the soy and affect flavor and 
texture.

To remove the bitter taste that turns some people 
off, you need to steam your tempeh. 

Use a steamer basket and steam for at least 15 
minutes. 

You can wrap it in wet paper towels and microwave 
it for four minutes in a pinch, but that’s not as 
effective as the steamer basket. Tempeh crumbles 
easily, and thus is great for meat dishes like 
chili, ragu, soups, or (today’s recipe) Cajun 
Sloppy Joe’s.

 Ingredients:
• 1 TBSP olive oil
• 1 small onion, diced
• 1 green bell pepper, diced
• 2 garlic cloves, minced
• 1 8-ounce package of tempeh, crumbled
• 1 cup vegetable broth
• ¼ cup tomato paste
• 2 TBSP Worcestershire sauce (can buy this vegan)
• 1 TBSP brown sugar
• ½ TBSP red wine vinegar
• 1 tsp prepared mustard
• 1 tsp smoked paprika
• ½ tsp oregano
• ½ tsp thyme
• ½ tsp black pepper
• ¼ tsp Cayenne pepper
• ¼ tsp salt
• Cayenne pepper hot sauce to taste
• 4 buns for serving

Place oil into medium saucepan or skillet over 
medium heat. Add onion and sauté for five minutes. 

Add bell pepper, garlic, and tempeh. Sauté five 
minutes more. 

Add remaining ingredients (except for the buns, 
of course!). Stir and bring to a simmer. 

Simmer for five minutes until heated throughout. 

Add a bit of water if it starts getting too dry. 

Portion out into buns and serve. Guests can season 
with additional hot sauce if desired.

In our next email, we’ll talk about seitan and wheat 
protein.

Until Then,

{Your Name Here}