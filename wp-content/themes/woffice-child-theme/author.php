<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */
get_header(); 
?>

	<div id="left-content">

		<?php  //GET THEME HEADER CONTENT
		/*if ( is_day() ) :
			$title = sprintf( __( 'Daily Archives: <span>%s</span>', 'woffice' ), get_the_date() );
		elseif ( is_month() ) :
			$title = sprintf( __( 'Monthly Archives: <span>%s</span>', 'woffice' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'woffice' ) ) );
		elseif ( is_year() ) :
			$title = sprintf( __( 'Yearly Archives: <span>%s</span>', 'woffice' ), get_the_date( _x( 'Y', 'yearly archives date format', 'woffice' ) ) );
		elseif ( is_tax() ) :
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$title =  $term->name . __(' Archives','woffice'); 		
		elseif (is_post_type_archive('wiki')) :
			$title = __( 'Wiki Articles', 'woffice' );
		elseif (is_post_type_archive('project')) :
			$title = __( 'Projects', 'woffice' );
		else :
			$title = __( 'Archives 11111', 'woffice' );
		endif;*/
		$post_classes = array('box','content');
		$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
		$title = __($author->display_name."'s profile page is coming soon!", 'woffice');
		woffice_title($title); ?> 	

		<!-- START THE CONTENT CONTAINER -->
		<div id="content-container">

			<!-- START CONTENT -->
			<div id="content">

                <?php
                $blog_layout = 'classic';
                if(get_post_type() == 'post'){

                    $blog_layout = woffice_get_settings_option('blog_layout');
	                $blog_layout = (isset($_GET['blog_masonry'])) ? 'masonry' : $blog_layout;

                    echo ('masonry' === $blog_layout) ? '<div id="directory" class="masonry-layout">' : '';
	                $content_type = ('masonry' === $blog_layout) ? 'content-masonry' : 'content';
                }
                ?>

				<?php /*if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php // We check for the role :
						if ( woffice_is_user_allowed() ) {
							get_template_part( $content_type );
						}
						?>
					<?php endwhile; ?>
				<?php else :*/ ?>
					<?php //get_template_part( 'content', 'none' ); ?>
				<?php //endif; ?>
				<article id="post-<?php the_ID(); ?>" <?php post_class($post_classes); ?>>

					<div class="intern-padding">
						<div class="special-404 text-center">
							<i class="fa fa-meh text-light"></i>
						</div>
						<div class="heading text-center">
							<h2>
								<?php // THE TITLE
								_e( 'Nothing Found', 'woffice' );?>
							</h2>
						</div>
					</div>
					<div class="intern-padding">
						<p class="blog-sum-up text-center">
							<?php 
							_e( $author->display_name."'s profile page is coming soon!", 'woffice' ); 
							?>
						</p>

						<div class="blog-button text-center">
							<a href="/store" class="btn btn-default"><i class="fa fa-arrow-left"></i> <?php _e('Back on the Store','woffice'); ?></a>
						</div>
					</div>
				</article>

                <?php echo ('masonry' === $blog_layout) ? '</div>' : ''; ?>

				<!-- THE NAVIGATION --> 
				<?php woffice_paging_nav(); ?>
			</div>
				
		</div><!-- END #content-container -->
		
		<?php woffice_scroll_top(); ?>

	</div><!-- END #left-content -->

<?php 
get_footer();
