<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */
get_header();
define('LIMIT','15');
?>
<style>
.funnel_type_filter label, .funnel_category_filter label, .funnel_sortby_filter label, .funnel_tag_filter label {
    margin: 0 15px 0 5px;
}
#loading {width: 100%;height: 100%;top: 0;left: 0;position: fixed;display: block;opacity: 0.7;background-color: #fff;z-index: 99;text-align: center;}
#loading-image {position:absolute;
    width:592px; /*image width */
    height:512px; /*image height */
    left:50%; 
    top:50%;
    margin-left:-296px; /*image width/2 */
    margin-top:-256px; /*image height/2 */}
#sort_and_filter{background: #82b440;margin: 0;border: 2px solid #FFFFFF;padding: 0 10px;font-size: 20px;font-weight: 600;cursor:pointer;color:#FFFFFF;}
#filter_div hr{margin-top:0.5rem;margin-bottom:0.5rem;}
.p_inactive{border-radius: 50px;}
.p_active{border-bottom: 0 !important;border-radius: 17px 17px 0 0;}
</style>
	<div id="left-content" class="category_page ">

		<?php  //GET THEME HEADER CONTENT
		$title = "Store";
		woffice_title($title); ?> 	

		<!-- START THE CONTENT CONTAINER -->
		<div id="content-container">

		<div id="content" class=""><!-- START CONTENT -->
			<p id="sort_and_filter" class="p_inactive">Sort & Filter<span style="float:right;" class="icon">+</span></p>
			<div id="filter_div" class="box content" style="display:none;background: #FFFFFF;min-height: 110px;border: 2px solid #FFFFFF;padding: 10px;border-top: none;border-radius: 0 0 17px 17px;">
				<div class="funnel_type_filter">
					<h6>Search by Funnel Type</h6>
					<input type="checkbox" name="funnel_type_filter" id="show_free_funnel" value="free" /><label for="show_free_funnel">Free Funnels</label>
					<input type="checkbox" name="funnel_type_filter" id="show_premium_funnel" value="premium" /><label for="show_premium_funnel">Premium Funnels</label>
					<input type="checkbox" name="funnel_type_filter" id="show_whitelabel_funnel" value="whitelabel" /><label for="show_whitelabel_funnel">Whitelabel Funnels</label>
					<input type="checkbox" name="funnel_type_filter" id="show_exclusive_funnel" value="exclusive" /><label for="show_exclusive_funnel">Exclusive Funnels</label>
				</div>
				<hr/>
				<div class="funnel_category_filter">
					<h6>Search by Category</h6>
					<?php
					$terms = get_terms(array(
						'taxonomy' => 'category',
						'hide_empty' => true,
					));
					// echo '<pre>';
					// print_r($terms);
					// echo '</pre>';
					unset($terms[34]);
					foreach($terms as $t){
						?>
						<input type="checkbox" name="funnel_category_filter" id="show_category_funnel_<?php echo $t->term_id ?>" value="<?php echo $t->term_id ?>" /><label for="show_category_funnel_<?php echo $t->term_id ?>"><?php echo $t->name; ?></label>
						<?php
					}
					?>
				</div>
				<hr/>
				<div class="funnel_sortby_filter">
					<h6>Sort By</h6>
					<input type="radio" name="funnel_sort_filter" id="show_datedesc_funnel" value="DESC" /><label for="show_datedesc_funnel">Date Descending</label>
					<input type="radio" name="funnel_sort_filter" id="show_dateasc_funnel" value="ASC" /><label for="show_dateasc_funnel">Date Ascending</label>
					<input type="radio" name="funnel_sort_filter" id="show_priceasc_funnel" value="PRICE_ASC" /><label for="show_priceasc_funnel">Price Low to High</label>
					<input type="radio" name="funnel_sort_filter" id="show_pricedesc_funnel" value="PRICE_DESC" /><label for="show_pricedesc_funnel">Price High to Low</label>
					<!--<input type="radio" name="funnel_sort_filter" id="show_popularity_funnel" value="popularity" /><label for="show_popularity_funnel">Popularity</label>-->
				</div>
				<hr/>
				<div class="funnel_tag_filter">
					<h6>Search by Tags</h6>
					<?php
					$tags = get_tags(array('hide_empty' => true, 'taxonomy' => 'post_tag'));
					// print_r($tags);
					foreach($tags as $t){
						?>
						<input type="checkbox" name="funnel_tag_filter" id="show_tags_funnel_<?php echo $t->term_id ?>" value="<?php echo $t->term_id ?>" /><label for="show_tags_funnel_<?php echo $t->term_id ?>"><?php echo $t->name; ?></label>
						<?php
					}
					?>
				</div>
			</div>
			
			<div id="store_div" class="box content" style="/* display:inline-block; */display: grid;gap: 10px;grid-template-columns: repeat(auto-fill, minmax(30%, 1fr));grid-template-rows: masonry;">
			<?php /*
			<div id="store_div" class="box content" style="display:inline-block; "> // MASONRY
				global $wpdb;
				$items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items ORDER BY id DESC");
				/*echo '<pre>';
				print_r($items);
				echo '</pre>';*//*
				$i_c = 0;
				foreach($items as $i){
					$pricing = maybe_unserialize($i->pricing);
					$post_id = $i->post_id;
					$funnel = get_post($post_id);
					$count_price = count($pricing);
					if(get_post_meta($post_id,'is_funnel_published',true) == 1){ 
					?>
					<div class="marketplace_item <?php echo $post_id; ?>" >
						<div class="marketplace_item_inner">
							<?php
							$freecolor = "background-color:#f37021;color:#fff;border-radius: 4px 0 0 0;";
							$premiumcolor = "background-color:#446095;color:#fff;";
							$whitelabelcolor = "background-color:#a8519f;color:#fff;";
							$exclusivecolor = "background-color:#f4ea00;color:#000;border-radius: 0 4px 0 0;";
							foreach($pricing as $k => $p){
								if($count_price == 4){
									$width = "25%";
								}else if($count_price == 3){
									$width = "33.33%";
								}else if($count_price == 2){
									$width = "50%";
								}else{
									$width = "100%";
								}
								if($k == 'free'){
									echo '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
								}else if($k == 'premium'){
									echo '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
								}else if($k == 'whitelabel'){
									echo '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
								}else if($k == 'exclusive'){
									echo '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
								}
							}
							echo '<div class="funnel_details">';
								echo '<h5 style="text-align: center;">'.$funnel->post_title.'</h5>';
								echo '<div class="funnel_details_inner">';
									$funnel_img = $wpdb->get_row("SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".$funnel->ID."'");
									$path = wp_get_upload_dir();
									$img_path = $path['baseurl'].'/screenshot';
									$f_img = $img_path .'/'. $funnel_img->image;
									if($funnel_img){
										echo '<div style="width:30%;float:left;"><img src="'.$f_img.'" data="'.$funnel->ID.'"/></div>';						
									}else{
										echo '<div style="width:30%;float:left;"><img src="http://placehold.it/100x100" /></div>';
									}
									echo '<div style="width:70%;float:left;padding: 0 10px;font-size: 12px;">';
										echo '<table>';
											echo '<tr>';
												echo '<td>Created By: <a href="'.get_author_posts_url($funnel->post_author).'">'.get_the_author_meta('first_name',$funnel->post_author).'</a></td>';
											echo '</tr>';
											echo '<tr>';
												$cat = get_the_category($post_id);
												echo '<td>Category: <a href="'.get_category_link($cat[0]).'">'.$cat[0]->name.'</a></td>';
											echo '</tr>';
											echo '<tr>';
												$rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
												$review_count = count($rating);
												$totalreview = 0;
												foreach($rating as $r){
													$totalreview = $totalreview + $r->valueformoney;
												}
												echo '<td>Funnel Rating: '.$totalreview.' Based on ('.$review_count.' Review)</td>';
											echo '</tr>';
											echo '<tr>';
												echo '<td>Activate Users:0</td>';
											echo '</tr>';
											echo '<tr>';
												echo '<td>Affiliate Products:0</td>';
											echo '</tr>';
											echo '<tr>';
												$pm_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$post_id);
												if($pm_count == 0){
													$pt = 'No';
												}else{
													$pt = 'Yes';
												}
												echo '<td>Promo Tools: '.$pt.'</td>';
											echo '</tr>';
										echo '</table>';
									echo '</div>';
								echo '</div>';
								echo '<div class="ratings_div" style="display: inline-block;width: 100%;">';
									$rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
									$review_count = count($rating);
									if($review_count > 0){
										echo '<p style="margin: 0;font-size: 13px;font-weight: 600;">Based on <span style="font-size: 20px;">'.$review_count.'</span> Reviews</p>';
									$valueformoney = 0;
									$welldesigned = 0;
									$qualityofcontent = 0;
									foreach($rating as $r){
										$valueformoney = $valueformoney + $r->valueformoney;
										$welldesigned = $welldesigned + $r->welldesigned;
										$qualityofcontent = $qualityofcontent + $r->qualityofcontent;
									}
									$average_valueformoney = $valueformoney / $review_count;
									$average_welldesigned = $welldesigned / $review_count;
									$average_qualityofcontent = $qualityofcontent / $review_count;
									
									if($average_valueformoney > 0 && $average_valueformoney <= 2){
										//red
										$background_valueformoney = 'ED1C24';
									}else if($average_valueformoney > 2 && $average_valueformoney <= 3.5 ){
										//yellow
										$background_valueformoney = 'ff9e28';
									}else if($average_valueformoney > 3.5 ){
										//green
										$background_valueformoney = '9ec73b';
									}
									
									if($average_welldesigned > 0 && $average_welldesigned <= 2){
										//red
										$background_welldesigned = 'ED1C24';
									}else if($average_welldesigned > 2 && $average_welldesigned <= 3.5 ){
										//yellow
										$background_welldesigned = 'ff9e28';
									}else if($average_welldesigned > 3.5 ){
										//green
										$background_welldesigned = '9ec73b';
									}
									
									if($average_qualityofcontent > 0 && $average_qualityofcontent <= 2){
										//red
										$background_qualityofcontent = 'ED1C24';
									}else if($average_qualityofcontent > 2 && $average_qualityofcontent <= 3.5 ){
										//yellow
										$background_qualityofcontent = 'ff9e28';
									}else if($average_qualityofcontent > 3.5 ){
										//green
										$background_qualityofcontent = '9ec73b';
									}
									
									echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_valueformoney.';color: white;">'.$average_valueformoney.'</span> Value For Money</div>';
									echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_welldesigned.';color: white;">'.$average_welldesigned.'</span>Well Designed</div>';
									echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_qualityofcontent.';color: white;">'.$average_qualityofcontent.'</span>Quality of Content</div>';
									}else{
										echo 'No Review Found..';
									}
								echo '</div>';
								echo '<div class="marketplace_item_desc">';
									echo '<p style="text-align: left;margin: 10px 0;font-size: 15px;">'.$i->description.'</p>';
									echo '<button onclick="unlock_funnel('.$i->post_id.');" type="button" style="background-color:#82b440;padding: 5px 30px;border-radius: 20px;">Unlock this Funnel</button>';
								echo '</div>';
							echo '</div>';
							?>
						</div>
					</div>
					<?php
					$i_c++;
					}
					if($i_c%3==0){echo '<div style="clear:both;"></div>';}
				}*/
				?>
			</div>
			<div id="affiliate_products_model" class="cn-demo-modal cn_demo-modal-width-50"></div>
			<div id="unlock_funnels_modal" class="cn-demo-modal cn_demo-modal-width-50"></div>
			<div id="funnel_activation_limit" class="cn-demo-modal cn_demo-modal-width-50"><h3 style="text-align: center;">Your membership level only allows you to activate 2 free funnels a month, to unlock unlimited free funnels <a href="https://wildfireconcepts.com/funnelmatesplatinum" target="_blank" rel="noopener">click here to upgrade to platinum</a>.</h3></div>
			<div id="page_id"></div>
			<span id="load_funnel" style="display:none;"><img src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" style="" class="loader"></span>
			<div id="msg"></div>				
			<button id="check_ajax" value="blank" style="display:none;"></button>
		</div>
		</div>
	</div>
<script>
jQuery( document ).ready(function($) {
	get_store();
	//var position = $(window).scrollTop(); 
		
    $(window).on('scroll', function() {
		// stop infinte scroll when use sorting
		var cat_filter = $( "input[type=checkbox][name=funnel_category_filter]:checked" ).val();
		var type_filter = $( "input[type=checkbox][name=funnel_type_filter]:checked" ).val();
		var tag_filter = $( "input[type=checkbox][name=funnel_tag_filter]:checked" ).val();
		var sort_filter = $("input[name='funnel_sort_filter']:checked").val();

		var filter = '';
		
		if(cat_filter == undefined && type_filter == undefined && tag_filter == undefined && sort_filter == undefined){
			filter = '';
		}else{
			filter = 'selected';
		}
		
		/*var scroll = $(window).scrollTop();
		var ajax_status = $('#check_ajax').val();
		if(scroll > position) {			
			var h = $('#content-container').height();
			var y_scroll_pos = parseInt(window.pageYOffset) + 280;
			
			if(y_scroll_pos > h && ajax_status == 'stop') {				
				get_store();
			}
		}else {
			
		}
		position = scroll;*/
		
		var ajax_status = $('#check_ajax').val();
		 if ($(window).scrollTop() >= $('#content-container').offset().top + $('#content-container').outerHeight() - window.innerHeight && ajax_status == 'stop' && filter == '') {
			 get_store();
         }
    });
});

function get_store(){
	var limit = <?php echo LIMIT; ?>;
	var page = jQuery('#page').val();
	jQuery('#check_ajax').val("run");
	if(page == 'lastpage'){
		return false;
	}
	
	if(page != null){
		jQuery('#load_funnel').show();
	}else{		
	}
	
	jQuery.ajax({
		type:'POST',
		data: {
			"action" : "get_store_items",
			"limit" : limit,
			"page" : page
		},
		url: "<?php echo admin_url('admin-ajax.php'); ?>",
		success: function( data ){
			jQuery('#load_funnel').hide();
			//alert(data);
			var obj = jQuery.parseJSON(data);
			if(obj.html == ''){
				jQuery('#msg').html("<p id='alertFadeOut'>No more post</p>");
				jQuery('#alertFadeOut').fadeOut(10000, function () {
				  jQuery('#alertFadeOut').text('');
				});
			}
			if(page >= 1){
				jQuery('#store_div').append(obj.html);
			}else if(page == 'lastpage'){
				
			}else{
				jQuery('#store_div').html(obj.html);
			}
			jQuery('#page_id').html(obj.pagenav);
			jQuery('#check_ajax').val("stop");
		}
	});
}
</script>
<div id="loading" style="display:none;">
  <img id="loading-image" src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" alt="Loading..." />
</div>
<?php 
get_footer();