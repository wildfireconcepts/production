<?php
	define('LIMIT','3');
?>
<input type="text" name="search_funnel_fm"/>
<input type="hidden" name="is_search_results" value="0"/>
<?php //print_r(do_shortcode('[fm_mail_list_settings]')); ?>
<?php /*
<label style="margin:10px 10px 10px 0;" for="f_published"><input id="f_published" type="checkbox" name="published" value="1" checked /> Published Funnels</label>
<label style="margin:10px 10px 10px 0;" for="f_unpublished"><input id="f_unpublished" type="checkbox" name="unpublished" value="1" checked /> Unpublished Funnels</label>
<label style="margin:10px 10px 10px 0;" for="f_under_review"><input id="f_under_review" type="checkbox" name="under_review" value="1" checked /> Under Review Funnels</label> */ ?>
<div style="display:flex;"> 
<button id="search_fm" class="btn btn-success">Search</button>
<button style="margin-left:10px;display:none;" id="reset_search_fm" class="btn btn-primary">Reset</button>
<span id="load_funnel" style="display:none;"><img src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" style="" class="loader"></span>
<span id="error_msg"></span>
</div>
<style>
#sortable1, #sortable2, #sortable3 { list-style-type: none; margin: 0; float: left; margin-right: 10px; background: #eee; padding: 5px; width: 143px;text-align:center;}
#sortable1 li, #sortable2 li, #sortable3 li { margin: 5px; padding: 5px; font-size: 1.2em; width: 120px;cursor:pointer;display:inline-block;float:none; }
#sortable2 li.ui-state-highlight{padding: 21px;width: 99%;}
.before_drop{float:left;border: 1px dashed;width:auto !important;font-weight:600;}
.after_drop{box-shadow:0px 0px 5px 0px #425f94;}
ul.macros {
	list-style: none;
    padding: 0;
}
ul.macros li {
    display: inline;
    font-size: 13px !important;
    float: left;
    text-align: center;
    width: 32% !important;
	font-weight:600;
}
ul.macros li span {
    display: block;
	font-weight:normal;
}
.funnel_parent ul.dropfalse textarea {
    min-height: 250px;
}
</style>
<div id="search_data"></div>
<?php

global $wpdb;
$c_id = get_current_user_id();

/*$purchase_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_postmeta.meta_key = '_purchase_info_$c_id' AND wp_posts.post_author not in($c_id) and (wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' OR wp_setc_activated_funnels.type = 'whitelabel') GROUP BY(wp_posts.ID) ORDER by wp_postmeta.meta_value ASC");*/
$purchase_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_postmeta.meta_key = '_purchase_info_$c_id' AND wp_posts.post_author not in($c_id) and (wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' OR wp_setc_activated_funnels.type = 'whitelabel') GROUP BY(wp_posts.ID) ORDER by wp_postmeta.meta_value ASC");
$funnel_id = array();
foreach($purchase_funnels as $i){
	$funnel_id[] = $i->ID; 
	$funnel_statue = $i->active_status; 
}
//print_r($funnel_id);
$created_funnels = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
$ids = $created_funnels->posts;
//print_r($ids);
$myarray = array_merge($funnel_id,$ids);
//print_r($myarray);
$all_funnel_id = implode(', ', $myarray); 
//echo "SELECT * FROM `wp_posts`INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_posts.ID in($all_funnel_id) AND (wp_postmeta.meta_key = '_purchase_info_$c_id' or wp_postmeta.meta_key = '_created_info_$c_id') ORDER by wp_postmeta.meta_value DESC";
$sort_funnel_id = $wpdb->get_results("SELECT * FROM `wp_posts`INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_posts.ID in($all_funnel_id) AND (wp_postmeta.meta_key = '_purchase_info_$c_id' or wp_postmeta.meta_key = '_created_info_$c_id') ORDER by wp_postmeta.meta_value DESC");
$new_funnel_id = array();
foreach($sort_funnel_id as $it){
	$new_funnel_id[] = $it->ID; 
}
//print_r($new_funnel_id);
if($new_funnel_id){
	$wpb_all_query = new WP_Query(array('orderby' => 'post__in', 'post__in'=> $new_funnel_id,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1));
	//$wpb_all_query = new WP_Query(array('post__in'=> $myarray,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1));
}else{
	$wpb_all_query = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
}
// echo '<pre>';
// print_r($wpb_all_query);exit;
// echo '</pre>';
?>
<div id="funnel_item">
	<?php 
	if ( $wpb_all_query->have_posts() ) :
		while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); //echo get_the_ID(); ?>
			<div class="funnel_parent funnel_search_<?php echo get_the_ID(); ?>" style="-webkit-box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);-moz-box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);min-height: 100px;border-radius:5px;padding: 15px;margin:30px 0; display:none; ">
				<?php 
				$funnel_current_user = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id = '".get_the_ID()."'");
				$funnel_type = $funnel_current_user->type;
				$c_user_id = $funnel_current_user->user_id;
				$funnel_user_info = get_userdata($c_user_id);
				$username = $funnel_user_info->user_login;
				$funnel_active_status = $funnel_current_user->active_status;
				$is_funnel_published = get_post_meta(get_the_ID(),'is_funnel_published',true);

				if($funnel_type == 'exclusive' || $funnel_active_status == 2){
					$parent_link = get_the_permalink();
					$parent_title = get_the_title();
					global $post;
					?>
					<div class="funnel_title">
						<h3 style="float:left;"><a href="<?php echo $parent_link; ?>" target="_blank"><?php echo $parent_title; ?></a></h3>
						<label class="switch unpublish_funnel_switch"></label>
						<h6>
							<?php 
							$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
							if($funnel_type == 'lead_magnet_funnel'){
								echo 'Lead Magnet Funnel';
							}else if($funnel_type == 'video_lead_funnel'){
								echo 'Video Lead Funnel';
							}else if($funnel_type == 'sales_funnel'){
								echo 'Sales Funnel';
							}
							$category = get_the_category( get_the_ID() ); 
							if($category[0]->cat_name){
								echo '&nbsp;-&nbsp;';
								echo $category[0]->cat_name;
							}
							?>
						</h6>
						<hr/>
						<div class="funnel_content" style="width: 100%;display: inline-block;">
							<div class="funnel_img" style="width:20%;float:left;">
								<?php
								//echo "SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'";
								$funnel_img = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items`where post_id = '".get_the_ID()."'");
								$funnel_image_id = $funnel_img->image_id;
								$funnel_image = wp_get_attachment_url( $funnel_image_id );
								if($funnel_image){
									?>
									<img src="<?php echo $funnel_image; ?>" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
									<?php
								}else{
									?>
									<img src="<?php echo site_url(); ?>/wp-content/uploads/2021/04/fff.png" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
									<?php
								}
								?>
							</div>
							<div class="funnel_btns" id="funnel_btns_<?php echo get_the_ID(); ?>" style="width:80%;float:left;">
								<h3>This funnel purchased by <?php echo $username; ?></h3>
							</div>
						</div>
					</div>
					<?php
				}else if($is_funnel_published == 'review' || $is_funnel_published == 'feedback_requested' || $is_funnel_published == 'triggered_review' || $is_funnel_published == 'resubmitted'){
					$parent_link = get_the_permalink();
					$parent_title = get_the_title();
					global $post;
					if(is_numeric($post)){
						$post = get_post($post);
					}
					$author_id=$post->post_author;
					if($author_id == $c_id){
						?>
						<h3><a href="<?php echo $parent_link; ?>"><?php echo $parent_title; ?></a> is under review process...</h3>
						<?php 
						if($is_funnel_published == 'feedback_requested'){
							echo "We've found the following items that need further attention. Please resolve these issues and click below to resubmit for review.";
							$requested_feedback = json_decode(get_post_meta(get_the_ID(),'requested_feedback',true));
							if(count($requested_feedback) > 0){
								?>
								<ul>
									<?php if(in_array('incomplete_email_sequence',$requested_feedback)){ ?>
										<li style="color:red;">Incomplete Email Sequence <a target="_BLANK" href="/wiki/fix-incomplete-email-sequence/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('no_download_link',$requested_feedback)){ ?>
										<li style="color:red;">No Download Link <a target="_BLANK" href="/wiki/fix-no-download-link/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('missing_optin_form',$requested_feedback)){ ?>
										<li style="color:red;">Missing Optin Form <a target="_BLANK" href="/wiki/fix-missing-optin-form/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('no_affiliate_links',$requested_feedback)){ ?>
										<li style="color:red;">No Affiliate Links <a target="_BLANK" href="/wiki/fix-no-affiliate-links/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('broken_affiliate_links',$requested_feedback)){ ?>
										<li style="color:red;">Broken Affiliate Link <a target="_BLANK" href="/wiki/fix-broken-affiliate-links/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('page_error',$requested_feedback)){ ?>
										<li style="color:red;">Page Error <a target="_BLANK" href="/wiki/fixing-a-page-error/">[FIX]</a></li>
									<?php } ?>
									<?php if(in_array('add_a_note',$requested_feedback)){ ?>
										<textarea style="height:200px;" disabled><?php echo $requested_feedback[7]; ?></textarea>
									<?php } ?>
								</ul>
								<?php
							}
							/*echo '<textarea style="height:200px;" disabled>'.$requested_feedback.'</textarea>';
							echo '<button class="button btn-info" onclick="improve_funnel('.get_the_ID().');">Improve</button>';*/
							echo '<button class="button btn-info" id="improve_funnel_'.get_the_ID().'" onclick="improve_funnel('.get_the_ID().');">OK! Access Funnel To Make Necessary Changes.</button>';
						}else{
							// if(get_current_user_id() == '1'){
								echo '<button class="button btn-info" id="cancel_funnel_review_'.get_the_ID().'" onclick="cancel_funnel_review('.get_the_ID().');">Cancel Funnel Review To Make Changes & Resubmit Later</button>';
							// }
						}
					}else{
						?>
						<h3 style="text-align: center;border-bottom: 1px solid #DDD;margin-bottom: 15px;padding-bottom: 15px;">The '<?php echo $parent_title; ?>' Funnel Is In Maintenance Mode.</h3>
						<h5>The Funnel Creator Has Been Notified & Should Resolve This Issue Soon.</h5>
						<?php
					}
				}else{
					$parent_link = get_the_permalink();
					$parent_title = get_the_title();
					global $post;
					if(is_numeric($post)){
						$post = get_post($post);
					}
					$author_id=$post->post_author;
					if($author_id == $c_id){
						?>
						<div class="funnel_title">
							<h3 style="float:left;"><a href="<?php echo $parent_link; ?>" target="_blank"><?php echo $parent_title; ?></a></h3>
							<?php 
							if($is_funnel_published == 1){ ?>
								<label class="switch unpublish_funnel_switch">
									<input data="<?php the_ID(); ?>" id="publish_funnel_<?php echo get_the_ID(); ?>"  type="checkbox" name="publish_funnel" onchange="unpublish_funnel(<?php the_ID(); ?>);" checked>
									<span class="slider round"></span>
								</label>
							<?php }else if($is_funnel_published == 'review'){ ?>
								<p>Submitted for review</p>
							<?php }else{
								?>
								<label class="switch publish_funnel_switch">
									<input data="<?php the_ID(); ?>" id="publish_funnel_<?php echo get_the_ID(); ?>" type="checkbox" name="publish_funnel" onchange="publish_funnel(<?php the_ID(); ?>);" >
									<span class="slider round"></span>
								</label>
							<?php } ?>
							<?php /* <input type="checkbox" style="margin-left: 40px;" name="viewascustomer" onchange="funnel_viewascustomer(<?php the_ID(); ?>);" data-funnel_id="<?php the_ID(); ?>" value="1" id="viewascustomer<?php the_ID(); ?>" />	
							<label style="cursor:pointer;" for="viewascustomer<?php the_ID(); ?>">View as Customer</label> Date 5-5-2021 */ ?>
							<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
							<div id="funnel_delete_button_container" style="float:right;">
								<?php if($is_funnel_published != 1){ ?>
									<a data="<?php the_ID(); ?>" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;" class="delete_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
								<?php }else{ ?>
									<?php /*<p id="hide_after_unpublish" style="float:right;">You can't delete published Funnel.</p>*/ ?>
									<a id="dlt_after_unpublish_<?php echo get_the_ID(); ?>" data="<?php the_ID(); ?>" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;display:none;" class="delete_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
								<?php }	?>
							</div>
							<h6>
								<?php 
								$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
								if($funnel_type == 'lead_magnet_funnel'){
									echo 'Lead Magnet Funnel';
								}else if($funnel_type == 'video_lead_funnel'){
									echo 'Video Lead Funnel';
								}else if($funnel_type == 'sales_funnel'){
									echo 'Sales Funnel';
								}
								$category = get_the_category( get_the_ID() ); 
								if($category[0]->cat_name){
									echo '&nbsp;-&nbsp;';
									echo $category[0]->cat_name;
								}
								if(get_post_meta(get_the_ID(),'is_user_improvising',true) == '1'){
									echo '&nbsp;-&nbsp;';
									echo '<a href="javascript:void(0);" style="color: red;text-decoration: none;" class="view_funnel_feedback" data="'.get_the_ID().'">View Funnel Feedback</a>';
									$requested_feedback = json_decode(get_post_meta(get_the_ID(),'requested_feedback',true));
									if(count($requested_feedback) > 0){
										?>
										<div id="viwe_feedback_modal_<?php echo get_the_ID(); ?>" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;">
											<h5>Your funnel "<?php echo get_the_title(get_the_ID()); ?>" has below errors.</h5>
											<ul>
												<?php if(in_array('incomplete_email_sequence',$requested_feedback)){ ?>
													<li style="color:red;">Incomplete Email Sequence <a target="_BLANK" href="/wiki/fix-incomplete-email-sequence/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('no_download_link',$requested_feedback)){ ?>
													<li style="color:red;">No Download Link <a target="_BLANK" href="/wiki/fix-no-download-link/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('missing_optin_form',$requested_feedback)){ ?>
													<li style="color:red;">Missing Optin Form <a target="_BLANK" href="/wiki/fix-missing-optin-form/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('no_affiliate_links',$requested_feedback)){ ?>
													<li style="color:red;">No Affiliate Links <a target="_BLANK" href="/wiki/fix-no-affiliate-links/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('broken_affiliate_links',$requested_feedback)){ ?>
													<li style="color:red;">Broken Affiliate Link <a target="_BLANK" href="/wiki/fix-broken-affiliate-links/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('page_error',$requested_feedback)){ ?>
													<li style="color:red;">Page Error <a target="_BLANK" href="/wiki/fixing-a-page-error/">[FIX]</a></li>
												<?php } ?>
												<?php if(in_array('add_a_note',$requested_feedback)){ ?>
													<textarea style="height:200px;" disabled><?php echo $requested_feedback[7]; ?></textarea>
												<?php } ?>
											</ul>
										</div>
										<?php
									}
								}
								?>
							</h6>
						</div>
						<?php 
					}else{ ?>
						<div class="funnel_title">
							<h3 style="float:left;"><a href="<?php echo $parent_link; ?>" target="_blank"><?php echo $parent_title; ?></a></h3>
							<input type="hidden" name="current_user_id" value="<?php echo $c_id; ?>">
							<?php
							// if($funnel_statue == 0){
								?>
								<label class="switch">
									<input data="<?php the_ID(); ?>" type="checkbox" name="active_funnel" checked>
									<span class="slider round"></span>
								</label>
								<?php
							/*}else{
								?>
								<label class="switch">
									<input data="<?php the_ID(); ?>" type="checkbox" name="active_funnel">
									<span class="slider round"></span>
								</label>
								<?php
							}*/
							?>
							<img class="switch funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/floader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
							<?php /* if($funnel_statue != 0){ ?>
								<a id="dlt_active_funnel"data="<?php the_ID(); ?>" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;" class="delete_inactive_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
							<?php }else{ ?>
								<a id="dlt_inactive_funnel" data="<?php the_ID(); ?>" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right; display:none;" class="delete_inactive_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>
							<?php } Date 4-5-2021 */ ?>
							<h6>
								<?php 
								$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
								if($funnel_type == 'lead_magnet_funnel'){
									echo 'Lead Magnet Funnel';
								}else if($funnel_type == 'video_lead_funnel'){
									echo 'Video Lead Funnel';
								}else if($funnel_type == 'sales_funnel'){
									echo 'Sales Funnel';
								}
								$category = get_the_category( get_the_ID() ); 
								if($category[0]->cat_name){
									echo '&nbsp;-&nbsp;';
									echo $category[0]->cat_name;
								}
								?>
							</h6>
						</div>
						<?php
					}
					?>
					<hr/>
					<div class="funnel_content" style="width: 100%;display: inline-block;">
						<div class="funnel_img" style="width:20%;float:left;">
							<?php
							//echo "SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'";
							$funnel_img = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items`where post_id = '".get_the_ID()."'");
							$funnel_image_id = $funnel_img->image_id;
							$funnel_image = wp_get_attachment_url( $funnel_image_id );
							if($funnel_image){
								?>
								<img src="<?php echo $funnel_image; ?>" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
								<?php
							}else{
								?>
								<img src="<?php echo site_url(); ?>/wp-content/uploads/2021/04/fff.png" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
								<?php
							}
							?>
							<?php /*<a href="javascript:void(0);" class="regenerate_thumbnail" data="<?php echo get_the_ID(); ?>">Regenerate Thumbnail</a>*/ ?>
						</div>
						<div class="funnel_btns" id="funnel_btns_<?php echo get_the_ID(); ?>" style="width:80%;float:left;">
							<?php 
							/*<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">
							<?php if($is_funnel_published != 1 || $author_id == $c_id){ ?>
							<a href="javascript:void(0);" class="edit_funnel_pages" data="<?php echo get_the_ID(); ?>">Edit Pages</a>
							<a href="javascript:void(0);" class="funnel_website_links" data="<?php echo get_the_ID(); ?>">Website Links</a>
							<a href="javascript:void(0);" class="followup_emails" data="<?php echo get_the_ID(); ?>">Followup Emails</a>
							<a href="javascript:void(0);" class="f_integrations_activator" data="<?php echo get_the_ID(); ?>">Integrations</a>
							<?php }else{ ?>
							<?php /*<a href="javascript:void(0);" class="funnel_links" data="<?php echo get_the_ID(); ?>">My Funnel Link</a>* ?>
							<a href="javascript:void(0);" class="download_resource" data="<?php echo get_the_ID(); ?>">Download Resource</a>
							<?php /*<a href="#">Affiliate Requests</a>* ?>
							<a href="javascript:void(0);" class="affiliate_requests" data="<?php echo get_the_ID();?>">Affiliate Requests</a>
							<a href="javascript:void(0);" class="f_integrations_creator" data="<?php echo get_the_ID(); ?>">Integrations</a>
							<?php } ?>
							</div>
							<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">
								<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="add_promotional_tools">Promotional Tools</a>
								<a href="javascript:void(0);">Statistics</a>
								<?php //if(get_post_meta(get_the_ID(),'is_funnel_published',true) != 1){ ?>
								<?php if($author_id == $c_id){ ?>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="btn_marketplace">Marketplace</a>
								<?php }else{ ?>
									<a href="javascript:void();" class="funnel_review" data="<?php echo get_the_ID();?>">Review</a>
								<?php } ?>
								<?php if(current_user_can('administrator') && $author_id == $c_id){ 
								?>
								<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="clone_funnel">Clone Funnel</a>
								<?php
								}  ?>
							</div> */ ?>
							<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">
								<?php if($author_id == $c_id){ ?>
									<a href="javascript:void(0);" class="edit_funnel_pages" data="<?php echo get_the_ID(); ?>">Edit Pages</a>
									<a href="javascript:void(0);" class="funnel_website_links" data="<?php echo get_the_ID(); ?>">Website Links</a>
									<a href="javascript:void(0);" class="followup_emails" data="<?php echo get_the_ID(); ?>">Followup Emails</a>
									<a href="javascript:void(0);" class="f_integrations_activator" data="<?php echo get_the_ID(); ?>">Integrations</a>
								<?php }else{ ?>
									<a href="javascript:void(0);" class="affiliate_requests" data="<?php echo get_the_ID();?>">Promotional Links</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="add_promotional_tools">Funnel Link & Traffic Kit</a>
									<a href="javascript:void(0);" class="download_resource" data="<?php echo get_the_ID(); ?>">Download Resources</a>
								<?php }?>
							</div>
							<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">
								<?php if($author_id == $c_id){ ?>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="add_promotional_tools">Promotional Tools</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="funnel_statistics">Statistics</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="btn_marketplace">Marketplace</a>
									<?php if(current_user_can('funnelmates_pro')){ ?>
										<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="clone_funnel">Clone Funnel</a>
									<?php } ?>
								<?php }else{ ?>
									<a href="javascript:void(0);" class="f_integrations_activator" data="<?php echo get_the_ID(); ?>">Integrations</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="clicks_and_coversions">Clicks & Conversions</a>
									<a href="javascript:void();" class="funnel_review" data="<?php echo get_the_ID();?>">Rate & Review</a>
								<?php }?>
							</div>
						</div>
						<?php
						if($author_id == $c_id && $is_funnel_published == 1){
						?>
						<div class="funnel_content" style="width: 100%;display: inline-block;">
							<hr style="background-color:#435F94; height:5px;">
							<div><h3 style="color:#28A745;"><?php echo $parent_title; ?><span> - Customer View</span></h3></div>
							<div style="width:20%; float:left;">
							<?php
								$funnel_img = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items`where post_id = '".get_the_ID()."'");
								$funnel_image_id = $funnel_img->image_id;
								$funnel_image = wp_get_attachment_url( $funnel_image_id );
								if($funnel_image){
									?>
									<img src="<?php echo $funnel_image; ?>" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
									<?php
								}else{
									?>
									<img src="<?php echo site_url(); ?>/wp-content/uploads/2021/04/fff.png" data="<?php echo get_the_ID(); ?>" class="change_funnel_image" style="width:130px; height:125px;" />
									<?php
								}
							?>
							</div>
							<div style="width:80%; float:left;">
								<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">
									<a href="javascript:void(0);" class="affiliate_requests" data="<?php echo get_the_ID();?>">Promotional Links</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="add_promotional_tools">Funnel Link & Traffic Kit</a>
									<a href="javascript:void(0);" class="download_resource" data="<?php echo get_the_ID(); ?>">Download Resources</a>
								</div>
								<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">
									<a href="javascript:void(0);" class="f_integrations_activator" data="<?php echo get_the_ID(); ?>">Integrations</a>
									<a href="javascript:void(0);" data="<?php echo get_the_ID(); ?>" class="clicks_and_coversions">Clicks & Conversions</a>
									<a href="javascript:void();" class="funnel_review" data="<?php echo get_the_ID();?>">Rate & Review</a>
								</div>
							</div>
						</div>
						<?php
						}
						?>
					</div>
					<div id="promotional_tools_div_<?php echo get_the_ID(); ?>" style="display:none;">
						<?php 
						//if($author_id != $c_id){
							$post_id = get_the_ID(); 
							$a=array("https://fnl1.com", "https://fnl2.com", "https://fnl3.com", "https://fnl5.com", "https://fnl7.com");
							$random_keys=array_rand($a,1);
							$root_url = $a[$random_keys];
							$is_custom_url = get_user_meta(get_current_user_id(),'fm_custom_url',true);
							/*if($is_custom_url){
								$shorten_url = $is_custom_url.'/'.$post_id.'/';
							}else{
								$shorten_url = $root_url.'/f/'.$post_id.'/'.get_current_user_id().'/';
							}*/
							$slug = get_post_field( 'post_name', $post_id );
							?>
							<h3 align="center">YOUR FUNNEL LINK:</h3>
							<p>Here is the link to your personally branded copy of the <strong>'<?php echo get_the_title($post_id); ?>'</strong> funnel.</p>
							<p>Simply copy, paste and share this link or use the promotional tools below to invite people to claim the gift. They'll automatically have it delivered to them and then start receiving followup emails containing your affiliate links for any recommended products.</p>
							<p><strong>Shortened link:</strong> Use for social, email and most forms of sharing.</p>
							<?php
							if($is_custom_url){
								$data =  maybe_unserialize($is_custom_url);
								foreach($data as $d){
									$shorten_url = $d.'/'.$post_id.'/';
									echo '<p><a style="font-size: 21px;font-weight: 600;text-align: center;text-transform: lowercase;" href="'.$shorten_url.'" target="_BLANK">'.$shorten_url.'</a></p>';
								}
							}else{
								$shorten_url = $root_url.'/f/'.$post_id.'/'.get_current_user_id().'/';
								echo '<p><a style="font-size: 21px;font-weight: 600;text-align: center;text-transform: lowercase;" href="'.$shorten_url.'" target="_BLANK">'.$shorten_url.'</a></p>';
							}
							?>
							<?php /*<p><a style="font-size: 21px;font-weight: 600;text-align: center;text-transform: lowercase;" href="<?php echo $shorten_url; ?>" target="_BLANK"><?php echo $shorten_url; ?></a></p> */ ?>
							<p><strong>Raw link:</strong> Use this link if you advertise on platforms that doesn't allow URL Redirection.</p>
							<p><a style="font-size: 21px;font-weight: 600;text-align: center;text-transform: lowercase;" href="https://funnelmates.com/f/<?php echo $post_id; ?>/<?php echo get_current_user_id(); ?>/" target="_BLANK">https://funnelmates.com/f/<?php echo $post_id; ?>/<?php echo get_current_user_id(); ?>/</a></p>
							<h3 align="center">TRAFFIC TOOLKIT:</h3>
							<?php
							$author_id = get_post_field ('post_author', $post_id);
							$display_name = get_the_author_meta( 'display_name' , $author_id ); 
							?>
							<p><strong><?php echo $display_name; ?></strong> has provided a collection of done for you tools to make getting traffic to your funnel link even easier!   Your link is already added, so you can click, share, tweet and post your way to leads and sales even faster.</p>
							<?php
						//}
						if($author_id == $c_id){
							?>
							<div style="display:inline-block;width: 100%;">
								<p style="margin:0;font-size: 20px;font-weight: 600;text-align: center;">Drag and Drop Each Promotional Tool Element Below To Create and Edit</p>
								<p style="margin:0;font-size: 14px;text-align: center;">You can drag more than one of each type, to provide multiple alternatives</p>
								<ul class="droptrue" id="sortable1" style="width:100%;margin-bottom:10px;">
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="email_swipe">Email</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="banner">Banner</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="yt_vid">Videos</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="article">Article</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="fb_ads">Facebook</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="twitter">Twitter</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="linkedin">Linkedin</li>
									<li class="ui-state-default before_drop" post_id="<?php echo get_the_ID(); ?>" data="misc">Misc</li>
								</ul>
								<p>Promotional Tools</p>
								<input type="hidden" name="global_post_id" value="<?php echo get_the_ID(); ?>" />
								<input type="hidden" name="global_user_id" value="<?php echo get_current_user_id(); ?>" />
								<ul class="dropfalse dropfalse_<?php echo get_the_ID(); ?>" id="sortable2" style="width:100%;border:1px solid #425f94;background:#e8f0ff;box-shadow:0px 0px 5px 0px #000 inset;">
									<?php
									$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ");
									//echo "SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ";
									// print_r($data);
									//print_r($get_pt);
									foreach($get_pt as $gpt){
										$data = $gpt->content;
										//print_r($data);
										if($gpt->item_type == 'banner'){
											$ter =  maybe_unserialize($data);							
										}else{
											$ter =  maybe_unserialize(base64_decode($data));	
										}
										$subject = '';
										$fb_data = '';
										$fb_image = '';
										$yt_url = '';
										$video_des = '';
										$content = '';
										if($ter !=''){
											foreach($ter as $k=>$p){
												if($k == "subject"){
													$subject = $p;
												}else if($k == "data"){
													$fb_data = $p;
												}else if($k == "image"){
													$fb_image = $p;
												}else if($k == "url"){
													$yt_url = $p;
												}else if($k == "desc"){
													$video_des = $p;
												}else{
													$content = $p;
												}
											}
										}
										?>
										<?php if($gpt->item_type == 'email_swipe'){ 	?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="email_swipe" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="email_swipe_<?php echo $gpt->id; //echo get_the_ID(); ?>">
													<label>Email Content</label>
													<input type="text" name="email_swipe_content_subject_<?php echo $gpt->id; ?>" placeholder="Email Subject" value="<?php echo $subject; ?>"/>
													<div>
														<ul class="macros">
															<li onclick="add_macro_to_mail('[activator_fname]',<?php echo $gpt->id; ?>,this);">Insert funnel activator's first name</li>
															<li onclick="add_macro_to_mail('[activator_lname]',<?php echo $gpt->id; ?>,this);">Insert funnel activator's last name</li>
															<li onclick="add_macro_to_mail('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]',<?php echo $gpt->id; ?>,this);">Insert squeeze page link</li>
														</ul>
													</div>
													<textarea name="email_swipe_content_<?php echo $gpt->id; ?>"><?php echo stripslashes($content); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php 
										}else if($gpt->item_type == 'banner'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="banner" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="banner_<?php echo $gpt->post_id; ?>">
													<label>Banners</label>
													<input type="hidden" name="current_user_id_banner_<?php echo $gpt->id; ?>" value="<?php echo get_current_user_id(); ?>" />
													<input type="hidden" name="current_post_id_banner_<?php echo $gpt->id; ?>" value="<?php echo $gpt->post_id; ?>" />
													<input type="hidden" value="" class="regular-text" id="process_banner_image_<?php echo $gpt->id; ?>" data="<?php echo $gpt->id; ?>" name="process_banner_image_<?php echo $gpt->id; ?>">
													<button style="width:30%;background:#82b440;" type="button" class="banner_image button">Choose Banner</button>
													<?php
													if($gpt->content){
														$image_url = wp_get_attachment_url( $gpt->content );
														$preview_html = '<a href="'.site_url().'/f/'.$gpt->post_id.'/'.get_current_user_id().'/"><img src="'.$image_url.'" /></a>';
														if($image_url == ''){}
													}
													// echo $gpt->content;
													// print_r($image_url);
													?>
													<img src="<?php echo $image_url ?>" onerror="this.style.display='none'" class="preview banner_image_disp_<?php echo $gpt->id; ?>" style="height:100px;width:100px;" />
													<?php
													if($image_url == ''){
														?>
														<span class="img_err_msg_<?php echo $gpt->id; ?>">There's no image uploaded yet</span>
														<?php
													}		
													?>
													<?php /*<div class="preview_html_<?php echo $gpt->id; ?>">
													<textarea disabled><?php echo $preview_html; ?></textarea>
													</div>*/ ?>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php 
										}else if($gpt->item_type == 'fb_ads'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="fb_ads" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="fb_ads_<?php echo $gpt->post_id; ?>">
													<label>Facebook</label>
													<input type="hidden" name="current_user_id_fb_<?php echo $gpt->id; ?>" value="<?php echo get_current_user_id(); ?>" />
													<input type="hidden" name="current_post_id_fb_<?php echo $gpt->id; ?>" value="<?php echo $gpt->post_id; ?>" />
													<input type="hidden" value="<?php echo $fb_image; ?>" name="process_fb_image_<?php echo $gpt->id; ?>" class="regular-text" id="process_fb_image_<?php echo $gpt->id; ?>" data="<?php echo $gpt->id; ?>" name="process_fb_image_<?php echo $gpt->id; ?>">
													<button style="width:30%;background:#82b440;" type="button" class="fb_image button">Add (Optional) Image</button>
													<?php
													$image_url = wp_get_attachment_url( $fb_image );										
													?>
													<img src="<?php echo $image_url ?>" onerror="this.style.display='none'" class="fb_preview fb_image_disp_<?php echo $gpt->id; ?>" style="height:100px;width:100px;" />
													<?php
													if($image_url == ''){
														?>
														<span class="facebook_img_err_msg_<?php echo $gpt->id; ?>">There's no image uploaded yet</span>
														<?php
													}		
													?>
													<ul class="macros">
														<input type="hidden" name="facebook_hidden_post_id" value="" />
														<li onclick="add_macro_to_tools('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
													</ul>
													<textarea name="facebook_content_<?php echo $gpt->id; ?>"><?php echo stripslashes($fb_data); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php 
										}else if($gpt->item_type == 'yt_vid'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="yt_vid" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="yt_vid_<?php echo $gpt->post_id; ?>">
													<label>Videos</label>
													<input type="text" class="yt_vid_content" name="yt_vid_content_<?php echo $gpt->id; ?>" placeholder="Video URL" value="<?php echo $yt_url; ?>"/>
													<?php /*<input type="text" class="yt_vid_content_desc" name="yt_vid_content_desc_<?php echo $gpt->id; ?>" placeholder="Video Description" value="<?php echo $video_des; ?>"/> */?>
													<label style="margin-top:10px;">Description</label>
													<ul class="macros">
														<li onclick="add_macro_to_tools('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','<?php echo $gpt->id; ?>',this);">Insert squeeze page link</li>
													</ul>
													<textarea name="yt_vid_content_desc_<?php echo $gpt->id; ?>" class="yt_vid_content_desc"><?php echo stripslashes($video_des); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader_<?php echo $gpt->id; ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php
										}else if($gpt->item_type == 'twitter'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="twitter" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="twitter_<?php echo $gpt->post_id; ?>">
													<label>Twitter</label>
													<input type="hidden" name="current_user_id_twitter_<?php echo $gpt->id; ?>" value="<?php echo get_current_user_id(); ?>" />
													<input type="hidden" name="current_post_id_twitter_<?php echo $gpt->id; ?>" value="<?php echo $gpt->post_id; ?>" />
													<input type="hidden" value="<?php echo $fb_image; ?>" class="regular-text" id="process_twitter_image_<?php echo $gpt->id; ?>" data="<?php echo $gpt->id; ?>" name="process_twitter_image_<?php echo $gpt->id; ?>">
													<button style="width:30%;background:#82b440;" type="button" class="twitter_image button">Add (Optional) Image</button>
													<?php
													$image_url = wp_get_attachment_url( $fb_image );
													?>
													<img src="<?php echo $image_url ?>" onerror="this.style.display='none'"  class="preview twitter_image_disp_<?php echo $gpt->id; ?>" style="height:100px;width:100px;" />
													<?php
													if($image_url == ''){
														?>
														<span class="tweet_img_err_msg_<?php echo $gpt->id; ?>">There's no image uploaded yet</span>
														<?php
													}		
													?>
													<ul class="macros">
														<input type="hidden" name="twitter_hidden_post_id" value="" />
														<li onclick="add_macro_to_tools('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
													</ul>
													<textarea name="twitter_content_<?php echo $gpt->id; ?>" class="count_char" ><?php echo stripslashes($fb_data); ?></textarea>
													<p><span class="chars">200</span> Character(s) Remaining</p>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php 
										}else if($gpt->item_type == 'linkedin'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="linkedin" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="linkedin_<?php echo $gpt->post_id; ?>">
													<label>LinkedIn</label>
													<input type="hidden" name="current_user_id_linkedin_<?php echo $gpt->id; ?>" value="<?php echo get_current_user_id(); ?>" />
													<input type="hidden" name="current_post_id_linkedin_<?php echo $gpt->id; ?>" value="<?php echo $gpt->post_id; ?>" />										
													<input type="hidden" value="<?php echo $fb_image; ?>" class="regular-text" id="process_linkedin_image_<?php echo $gpt->id; ?>" data="<?php echo $gpt->id; ?>" name="process_linkedin_image_<?php echo $gpt->id; ?>">
													<button style="width:30%;background:#82b440;" type="button" class="linkedin_image button">Add (Optional) Image</button>
													<?php
													$image_url = wp_get_attachment_url( $fb_image );
													?>
													<img src="<?php echo $image_url ?>" onerror="this.style.display='none'" class="preview linkedin_image_disp_<?php echo $gpt->id; ?>" style="height:100px;width:100px;" />		
													<?php
													if($image_url == ''){
														?>
														<span class="linkedin_img_err_msg_<?php echo $gpt->id; ?>">There's no image uploaded yet</span>
														<?php
													}		
													?>		
													<ul class="macros">
														<input type="hidden" name="twitter_hidden_post_id" value="" />
														<li onclick="add_macro_to_tools('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
													</ul>
													<textarea name="linkedin_content_<?php echo $gpt->id; ?>"><?php echo stripslashes($fb_data); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php 
										}else if($gpt->item_type == 'article'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="article" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="article_<?php echo get_the_ID(); ?>">
													<label>Article</label>
													<div>
														<ul class="macros">
															<li onclick="add_macro_to_article('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','<?php echo $gpt->id; ?>',this);">Insert squeeze page link</li>
														</ul>
													</div>
													<textarea name="article_swipe_content_<?php echo $gpt->id; ?>"><?php echo stripslashes($content); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php	
										}else if($gpt->item_type == 'misc'){ ?>
											<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="<?php echo $gpt->post_id; ?>" data="misc" style="display: list-item;" draggable="false" item_id="<?php echo $gpt->id; ?>">
												<div id="misc_<?php echo get_the_ID(); ?>">
													<label>Misc</label>
													<div>
														<ul class="macros">
															<li onclick="add_macro_to_misc('[funnel_link id=<?php echo $gpt->post_id; ?> value=promo_tools][/funnel_link]','<?php echo $gpt->id; ?>',this);">Insert squeeze page link</li>
														</ul>
													</div>
													<textarea name="misc_swipe_content_<?php echo $gpt->id; ?>"><?php echo stripslashes($content); ?></textarea>
													<div class="action_btns">
														<button class="button btn-update update_<?php echo $gpt->item_type; ?>" type="button">Update</button>
														<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
														<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
													</div>
												</div>
											</li>
											<?php	
										} 
									}
									?>
								</ul>
							</div>
							<div id="email_swipe_<?php echo get_the_ID(); ?>" style="display:none;">
								<div>
									<label>Email Content</label>
									<input type="text" name="email_swipe_content_subject_" placeholder="Email Subject" />
									<ul class="macros">
										<input type="hidden" name="email_hidden_post_id" value="" />
										<li onclick="add_macro_to_mail('[activator_fname]','',this);">Insert funnel activator's first name</li>
										<li onclick="add_macro_to_mail('[activator_lname]','',this);">Insert funnel activator's last name</li>
										<li onclick="add_macro_to_mail('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
									</ul>
									<textarea name="email_swipe_content_" id="email_swipe_content_"></textarea>
									<div class="action_btns">
									<button class="button btn-update update_email_swipe" type="button">Update</button>
									<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
									<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
									</div>
								</div>
							</div>
							<div id="banner_<?php echo get_the_ID(); ?>" style="display:none;">
								<label>Banners</label>
								<input type="hidden" name="current_user_id_banner_" value="<?php echo get_current_user_id(); ?>" />
								<input type="hidden" name="current_post_id_banner_" value="" />
								<input type="hidden" value="" class="regular-text" id="process_banner_image_" name="process_banner_image_">
								<button style="width:30%;background:#82b440;" type="button" class="banner_image button">Choose Banner</button>
								<img src="" class="preview" style="height:100px;width:100px;display:none;" />
							
								<div class="action_btns">
								<button class="button btn-update update_banner" id="<?php echo get_the_ID(); ?>" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="update_banner_funnel_loader_<?php echo get_the_ID(); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="fb_ads_<?php echo get_the_ID(); ?>" style="display:none;">
								<label>Facebook</label>
								<input type="hidden" name="current_user_id_fb_" value="<?php echo get_current_user_id(); ?>" />
								<input type="hidden" name="current_post_id_fb_" value="" />
								<input type="hidden" value="" class="regular-text" id="process_fb_image_" name="process_fb_image_">
								<button style="width:30%;background:#82b440;" type="button" class="fb_image button">Add (Optional) Image</button>
								<img src="" class="fb_preview" style="height:100px;width:100px;display:none;" />
								<ul class="macros">
									<input type="hidden" name="facebook_hidden_post_id" value="" />
									<li onclick="add_macro_to_tools('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								<textarea name="facebook_content_" class="facebook_content"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_fb_ads" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="twitter_<?php echo get_the_ID(); ?>" style="display:none;">
								<label>Twitter</label>
								<input type="hidden" name="current_user_id_twitter_" value="<?php echo get_current_user_id(); ?>" />
								<input type="hidden" name="current_post_id_twitter_" value="" />
								<input type="hidden" value="" class="regular-text" id="process_twitter_image_" name="process_twitter_image_">
								<button style="width:30%;background:#82b440;" type="button" class="twitter_image button">Add (Optional) Image</button>
								<img src="" class="twitter_preview" style="height:100px;width:100px;display:none;" />
								<ul class="macros">
									<input type="hidden" name="twitter_hidden_post_id" value="" />
									<li onclick="add_macro_to_tools('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								<textarea name="twitter_content_" class="count_char"></textarea>					
								<p><span class="chars">200</span> Character(s) Remaining</p>
								<div class="action_btns">
								<button class="button btn-update update_twitter" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="linkedin_<?php echo get_the_ID(); ?>" style="display:none;">
								<label>LinkedIn</label>
								<input type="hidden" name="current_user_id_linkedin_" value="<?php echo get_current_user_id(); ?>" />
								<input type="hidden" name="current_post_id_linkedin_" value="" />
								<input type="hidden" value="" class="regular-text" id="process_linkedin_image_" name="process_linkedin_image_">
								<button style="width:30%;background:#82b440;" type="button" class="linkedin_image button">Add (Optional) Image</button>
								<img src="" class="linkedin_preview" style="height:100px;width:100px;display:none;" />
								<ul class="macros">
									<input type="hidden" name="facebook_hidden_post_id" value="" />
									<li onclick="add_macro_to_tools('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								<textarea name="linkedin_content_" class="twitter_content"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_linkedin" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="yt_vid_<?php echo get_the_ID(); ?>" style="display:none;">
								<label>Videos</label>
								<input type="text" class="yt_vid_content" name="yt_vid_content_" placeholder="Youtube Video URL" />
								<label style="margin-top:10px;">Description</label>
								<ul class="macros">
									<input type="hidden" name="youtube_hidden_post_id" value="" />
									<li onclick="add_macro_to_tools('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								<textarea name="yt_vid_content_desc_" class="yt_vid_content_desc_"></textarea>
								<?php /*<input type="text" class="yt_vid_content_desc" name="yt_vid_content_desc_" placeholder="Video Description" />*/ ?>
								<div class="action_btns">
								<button class="button btn-update update_yt_vid" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="article_<?php echo get_the_ID(); ?>" style="display:none;">
								<div>
								<label>Article</label>
								<ul class="macros">
									<input type="hidden" name="article_hidden_post_id" value="" />
									<li onclick="add_macro_to_article('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								<textarea name="article_swipe_content_"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_article" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
								</div>
							</div>
							<div id="misc_<?php echo get_the_ID(); ?>" style="display:none;">
								<div>
								<label>Misc</label>
								<div>
								<ul class="macros">
									<input type="hidden" name="misc_hidden_post_id" value="" />
									<li onclick="add_macro_to_misc('[funnel_link id=<?php echo get_the_ID(); ?> value=promo_tools][/funnel_link]','',this);">Insert squeeze page link</li>
								</ul>
								</div>
								<textarea name="misc_swipe_content_"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_misc" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="<?php echo get_stylesheet_directory_uri(); ?>/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
								</div>
							</div>
							<?php
						}else{
							?>
							<div style="border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;">
								<?php
								$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ");
								if(count($get_pt) > 0){
									foreach($get_pt as $item){
										if($item->content){
											?>
											<div style="padding: 15px 10px; margin: 10px; font-size:16px; font-weight:700;box-shadow:0px 0px 5px 0px #425f94;">
												<div class="title" style="text-align:center;">
													<?php 
													if($item->item_type == 'yt_vid'){
														echo 'Video';
													}else{
														echo ucfirst(str_replace('_',' ',$item->item_type));
													}
													?>
												</div>
												<div class="promo_content">
													<?php 
													if($item->item_type == 'banner'){
														if($item->content){
															$image_url = wp_get_attachment_url( $item->content );
															$preview_html = '<a href="'.site_url().'/f/'.$item->post_id.'/'.get_current_user_id().'/"><img src="'.$image_url.'" /></a>';
															?>
															<img src="<?php echo $image_url ?>" class="preview banner_image_disp_<?php echo $item->id; ?>" style="text-align:center;"/>
															<div class="preview_html_<?php echo $item->id; ?>">
																<textarea disabled><?php echo $preview_html; ?></textarea>
															</div>
															<?php
														}
													}else if($item->item_type == 'email_swipe'){
														$email = maybe_unserialize(base64_decode($item->content));
														$content = stripslashes($email['content']);
														echo 'Subject : '.$email['subject'];
														echo '<hr>';
														echo apply_filters('the_content', $content);
													}else if($item->item_type == 'article'){
														$article = maybe_unserialize(base64_decode($item->content));
														$content = stripslashes($article['content']);
														echo apply_filters('the_content', $content);
													}else if($item->item_type == 'misc'){
														$misc = maybe_unserialize(base64_decode($item->content));
														$content = stripslashes($misc['content']);
														echo apply_filters('the_content', $content);
													}else if($item->item_type == 'yt_vid'){
														$video = maybe_unserialize(base64_decode($item->content));
														$video_url = generateVideoEmbedUrl($video['url']);
														$desc = stripslashes($video['desc']);
														echo '<div style="text-align:center;"><iframe src="'.$video_url.'" width="540" height="310"></iframe></div>';
														echo "<br>";
														echo '<div style="text-align:center; padding:5px;"><a href="'.$video['url'].'" target="_blank" style="text-decoration:none; cursor:pointer;">'.$video_url.'</a></div>';
														echo '<p class="vc_message_box vc_color-warning">Would you like to download this video to use in your marketing? 
														<a href="https://funnelmates.com/wiki/how-to-download-a-youtube-video/"> Click here to see how to do it.</a></p>';
														echo "<br>";
														/*if($_SERVER["REMOTE_ADDR"]=='219.91.196.168'){
															echo "<button class='download_video' value='".$video['url']."'>Download</button>";
														echo "<br>";
														}*/
														//echo $desc;
														echo apply_filters('the_content', $desc);
													}else if($item->item_type == 'twitter'){
														$data = maybe_unserialize(base64_decode($item->content));
														//$url = $data['url'];
														$url = '[landing_page_url]';			
														$url = apply_filters('the_content', $url);
														$data1 = strip_shortcodes($data['data']);
														$image1 = $data['image'];
														$image_url = wp_get_attachment_url( $image1 ); ?>
														<div style="display:flex;">
															<div style="width:25%; padding:10px;">
																<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
															</div>
															<div style="width:75%; padding:10px;">												
																<div><?php // echo stripslashes($data1); ?></div>
																<div><?php echo apply_filters('the_content', $data1); ?></div>
																<p style="margin-top:10px; text-align:center;"><a style="background-color:#1b95e0; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://twitter.com/intent/tweet?text=<?php echo stripslashes($data1); ?> <?php echo $url; ?>" target="_blank"><i class="fab fa-twitter"></i> Click Here To Tweet On Twitter</a></p>
															</div>									
														</div>
														<?php 
													}else if($item->item_type == 'fb_ads'){
														$data = maybe_unserialize(base64_decode($item->content));
														//$url = $data['url'];
														$url = '[landing_page_url]';			
														$url = apply_filters('the_content', $url);
														$data1 = stripslashes($data['data']);
														$image1 = $data['image'];
														$image_url = wp_get_attachment_url( $image1 ); 
														$author_id = get_post_field( 'post_author',get_the_ID() );
														$fb_url = 'https://funnelmates.com/fb/'. get_the_ID().'/' . $author_id;
														?>
														<div style="display:flex;">
															<div style="width:25%; padding:10px;">
																<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
															</div>
															<div style="width:75%; padding:10px;">
																<div><?php //echo stripslashes($data1); ?></div>
																<div><?php echo apply_filters('the_content', $data1); ?></div>
																<p style="margin-top:10px; text-align:center;"><a style="background-color:#29487d; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://www.facebook.com/sharer.php?u=<?php echo $fb_url; ?>/" target="_blank"><i class="fab fa-facebook"></i> Click Here To Post On Facebook</a></p>
															</div>									
														</div>
														<?php 
													}else if($item->item_type == 'linkedin'){
														$data = maybe_unserialize(base64_decode($item->content));
														//$url = $data['url'];			
														$url = '[landing_page_url]';			
														$url = apply_filters('the_content', $url);		
														$data1 = stripslashes($data['data']);
														$image1 = $data['image'];
														$image_url = wp_get_attachment_url( $image1 ); 									
														$author_id = get_post_field( 'post_author',get_the_ID() );
														$linkedin_url = 'https://funnelmates.com/in/'. get_the_ID().'/' . $author_id;
														?>
														<div style="display:flex;">
															<div style="width:25%; padding:10px;">
																<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
															</div>
															<div style="width:75%; padding:10px;">
																<div><?php echo apply_filters('the_content', $data1); ?></div>
																<p style="margin-top:10px; text-align:center;"><a style="background-color:#0077b5; color:#FFFFFF; padding:5px 15px; border-radius:10px; text-decoration:none;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $linkedin_url; ?>/" target="_blank"><i class="fab fa-linkedin"></i> Click Here To Post On LinkedIn</a></p>
															</div>																		
														</div>																		
														<?php
													} ?>
												</div>
											</div>
											<?php
										}
									}
								}else{
									echo '<div style="text-align: center;border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;min-height: 100px;line-height:100px;font-size: 35px;font-weight: 600;text-transform: uppercase;opacity: .5;">No Promotional Tools Available</div>';
								}
								?>
							</div>
							<?php		
						}
						?>
					</div>
					<div id="creator_promotional_tools_div_<?php echo get_the_ID(); ?>" style="display:none;">
						<div style="border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;">
							<?php
							$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ");
							if(count($get_pt) > 0){
								foreach($get_pt as $item){
									if($item->content){
										?>
										<div style="padding: 15px 10px; margin: 10px; font-size:16px; font-weight:700;box-shadow:0px 0px 5px 0px #425f94;">
											<div class="title" style="text-align:center;">
												<?php 
												if($item->item_type == 'yt_vid'){
													echo 'Video';
												}else{
													echo ucfirst(str_replace('_',' ',$item->item_type));
												}
												?>
											</div>
											<div class="promo_content">
												<?php 
												if($item->item_type == 'banner'){
													if($item->content){
														$image_url = wp_get_attachment_url( $item->content );
														$preview_html = '<a href="'.site_url().'/f/'.$item->post_id.'/'.get_current_user_id().'/"><img src="'.$image_url.'" /></a>';
														?>
														<img src="<?php echo $image_url ?>" class="preview banner_image_disp_<?php echo $item->id; ?>" />
														<div class="preview_html_<?php echo $item->id; ?>">
															<textarea disabled><?php echo $preview_html; ?></textarea>
														</div>
														<?php
													}
												}else if($item->item_type == 'email_swipe'){
													$email = maybe_unserialize($item->content);
													$content = $email['content'];
													echo 'Subject : '.$email['subject'];
													echo '<hr>';
													echo apply_filters('the_content', $content);
												}else if($item->item_type == 'article'){
													$article = maybe_unserialize($item->content);
													$content = $article['content'];
													echo apply_filters('the_content', $content);
												}else if($item->item_type == 'misc'){
													$misc = maybe_unserialize($item->content);
													$content = $misc['content'];
													echo apply_filters('the_content', $content);
												}else if($item->item_type == 'yt_vid'){
													$video = maybe_unserialize($item->content);
													$video_url = generateVideoEmbedUrl($video['url']);
													$desc = $video['desc'];
													//echo $video_url;
													echo '<div style="text-align:center;"><iframe src="'.$video_url.'" width="540" height="310"></iframe></div>';
													echo "<br>";
													/*if($_SERVER["REMOTE_ADDR"]=='219.91.196.168'){
														echo "<button class='download_video' value='".$video['url']."'>Download</button>";
														echo "<br>";
													}*/
													//echo $desc;
													echo apply_filters('the_content', $desc);
												}else if($item->item_type == 'twitter'){
													$data = maybe_unserialize(base64_decode($item->content));
													$url = $data['url'];
													$data1 = strip_shortcodes($data['data']);
													$image1 = $data['image'];
													$image_url = wp_get_attachment_url( $image1 ); ?>
													<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
													<div><a href="<?php echo $url; ?>" target="_BLANK"><?php echo $url; ?></a></div>
													<div><?php // echo stripslashes($data1); ?></div>
													<div><?php echo apply_filters('the_content', $data1); ?></div>
													<p style="margin-top:10px;"><a style="background-color:#1b95e0; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://twitter.com/intent/tweet?text=<?php echo stripslashes($data1); ?> <?php echo $url; ?>" target="_blank"><i class="fab fa-twitter"></i> Tweet</a></p>
													<?php 
												}else if($item->item_type == 'fb_ads'){
													$data = maybe_unserialize(base64_decode($item->content));
													$url = $data['url'];
													$data1 = stripslashes($data['data']);
													$image1 = $data['image'];
													$image_url = wp_get_attachment_url( $image1 ); 
													$author_id = get_post_field( 'post_author',get_the_ID() );
													$fb_url = 'https://funnelmates.com/fb/'. get_the_ID().'/' . $author_id;
													?>
													<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
													<div><a href="<?php echo $url; ?>" target="_BLANK"><?php echo $url; ?></a></div>
													<div><?php //echo stripslashes($data1); ?></div>
													<div><?php echo apply_filters('the_content', $data1); ?></div>
													<p style="margin-top:10px;"><a style="background-color:#29487d; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://www.facebook.com/sharer.php?u=<?php echo $fb_url; ?>/" target="_blank"><i class="fab fa-facebook"></i> Post</a></p>
													<?php 
												}else if($item->item_type == 'linkedin'){
													$data = maybe_unserialize(base64_decode($item->content));
													$url = $data['url'];
													$data1 = stripslashes($data['data']);
													$image1 = $data['image'];
													$image_url = wp_get_attachment_url( $image1 ); 									
													$author_id = get_post_field( 'post_author',get_the_ID() );
													$linkedin_url = 'https://funnelmates.com/in/'. get_the_ID().'/' . $author_id;
													?>
													<a href="<?php echo $url; ?>" target="_BLANK"><img src="<?php echo $image_url ?>" class="preview social_image_disp_<?php echo $item->id; ?>" /></a>
													<div><a href="<?php echo $url; ?>" target="_BLANK"><?php echo $url; ?></a></div>
													<?php /*<div><?php echo $data1; ?></div> */?>
													<div><?php echo apply_filters('the_content', $data1); ?></div>
													<p style="margin-top:10px;"><a style="background-color:#0077b5; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $linkedin_url; ?>/" target="_blank"><i class="fab fa-linkedin"></i> Share</a></p>
													<?php /*<p style="margin-top:10px;"><a style="background-color:#0077b5; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://www.linkedin.com/sharing/share-offsite/?url=<?php echo $linkedin_url; ?>/" target="_blank"><i class="fab fa-linkedin"></i> Share</a></p>*/?>
													<?php 
												} ?>
											</div>
										</div>
										<?php
									}
								}
							}else{
								echo '<div style="text-align: center;border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;min-height: 100px;line-height:100px;font-size: 35px;font-weight: 600;text-transform: uppercase;opacity: .5;">No Promotional Tools Available</div>';
							}
							?>
						</div>
					</div>
					<div id="followup_emails_div_<?php echo get_the_ID(); ?>" style="display:none;">
						<div class="tab_email">
							<button class="tablinks_email active" onclick="openEmail(event, 'immediate_<?php echo get_the_ID(); ?>_tab')">Immediately</button>
							<button class="tablinks_email" onclick="openEmail(event, 'one_day_<?php echo get_the_ID(); ?>_tab')">Day 2</button>
							<button class="tablinks_email" onclick="openEmail(event, 'two_day_<?php echo get_the_ID(); ?>_tab')">Day 3</button>
							<button class="tablinks_email" onclick="openEmail(event, 'three_day_<?php echo get_the_ID(); ?>_tab')">Day 4</button>
							<button class="tablinks_email" onclick="openEmail(event, 'four_day_<?php echo get_the_ID(); ?>_tab')">Day 5</button>
							<button class="tablinks_email" onclick="openEmail(event, 'five_day_<?php echo get_the_ID(); ?>_tab')">Day 8</button>
							<button class="tablinks_email" onclick="openEmail(event, 'six_day_<?php echo get_the_ID(); ?>_tab')">Day 11</button>
							<button class="tablinks_email" onclick="openEmail(event, 'seven_day_<?php echo get_the_ID(); ?>_tab')">Day 15</button>
							<button class="tablinks_email" onclick="openEmail(event, 'fourteen_day_<?php echo get_the_ID(); ?>_tab')">Day 19</button>
							<button class="tablinks_email" onclick="openEmail(event, 'twentyone_day_<?php echo get_the_ID(); ?>_tab')">Day 24</button>
							<button class="tablinks_email" onclick="openEmail(event, 'twentyeight_day_<?php echo get_the_ID(); ?>_tab')">Day 30</button>
						</div>
						<div class="followup_email_container" style="display:inline-block;width:100%;">
							<div style="width:100%;padding:0 2px;display:block;" id="immediate_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Immediately</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='1' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="immediate_subject_<?php echo get_the_ID(); ?>" id="immediate_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_immediate = 'immediate_'.get_the_ID();
								$settings_immediate =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_immediate, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									/* 'tinymce'       => array(
										'toolbar1'      => 'bold,italic,underline,separator,alignleft,aligncenter,alignright,separator,link,unlink,undo,redo',
										'toolbar2'      => '',
										'toolbar3'      => '',
									),*/
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_immediate, $settings_immediate );
								?>
								<button class="btn btn-info" id="btn_immidiate_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_immediate; ?>','immediate_subject_<?php echo get_the_ID(); ?>','btn_immidiate_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,1);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_immediate; ?>');" >View Actual Mail</button>
								<?php } ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="one_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 2</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='2' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after1days_subject_<?php echo get_the_ID(); ?>" id="after1days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after1days = 'after1days_'.get_the_ID();
								$settings_after1days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after1days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									/*'tinymce'       => array(
										'toolbar1'      => 'formatselect,bold,italic,bullist,numlist,blockquote,separator,alignleft,aligncenter,alignright,separator,fullscreen,visualchars',
										'toolbar2'      => 'strikethrough,hr,forecolor,pastetext,removeformat,charmap,outdent,indent,undo,redo',
										'toolbar3'      => '',
									),*/
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after1days, $settings_after1days );
								?>
								<button class="btn btn-info" id="btn_after1days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after1days; ?>','after1days_subject_<?php echo get_the_ID(); ?>','btn_after1days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,2);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after1days; ?>');" >View Actual Mail</button>
								<?php } ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="two_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 3</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='3' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after2days_subject_<?php echo get_the_ID(); ?>" id="after2days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after2days = 'after2days_'.get_the_ID();
								$settings_after2days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after2days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after2days, $settings_after2days );
								?>
								<button class="btn btn-info" id="btn_after2days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after2days; ?>','after2days_subject_<?php echo get_the_ID(); ?>','btn_after2days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,3);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after2days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="three_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 4</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='4' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after3days_subject_<?php echo get_the_ID(); ?>" id="after3days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after3days = 'after3days_'.get_the_ID();
								$settings_after3days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after3days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after3days, $settings_after3days );
								?>
								<button class="btn btn-info" id="btn_after3days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after3days; ?>','after3days_subject_<?php echo get_the_ID(); ?>','btn_after3days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,4);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after3days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="four_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 5</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='5' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after4days_subject_<?php echo get_the_ID(); ?>" id="after4days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after4days = 'after4days_'.get_the_ID();
								$settings_after4days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after4days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after4days, $settings_after4days );
								?>
								<button class="btn btn-info" id="btn_after4days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after4days; ?>','after4days_subject_<?php echo get_the_ID(); ?>','btn_after4days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,5);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after4days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="five_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 8</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='8' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after5days_subject_<?php echo get_the_ID(); ?>" id="after5days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after5days = 'after5days_'.get_the_ID();
								$settings_after5days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after5days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after5days, $settings_after5days );
								?>
								<button class="btn btn-info" id="btn_after5days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after5days; ?>','after5days_subject_<?php echo get_the_ID(); ?>','btn_after5days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,8);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after5days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="six_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 11</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='11' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after6days_subject_<?php echo get_the_ID(); ?>" id="after6days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after6days = 'after6days_'.get_the_ID();
								$settings_after6days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after6days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after6days, $settings_after6days );
								?>
								<button class="btn btn-info" id="btn_after6days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after6days; ?>','after6days_subject_<?php echo get_the_ID(); ?>','btn_after6days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,11);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after6days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="seven_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 15</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='15' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after7days_subject_<?php echo get_the_ID(); ?>" id="after7days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after7days = 'after7days_'.get_the_ID();
								$settings_after7days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after7days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after7days, $settings_after7days );
								?>
								<button class="btn btn-info" id="btn_after7days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after7days; ?>','after7days_subject_<?php echo get_the_ID(); ?>','btn_after7days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,15);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after7days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="fourteen_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 19</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='19' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after14days_subject_<?php echo get_the_ID(); ?>" id="after14days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after14days = 'after14days_'.get_the_ID();
								$settings_after14days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after14days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after14days, $settings_after14days );
								?>
								<button class="btn btn-info" id="btn_after14days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after14days; ?>','after14days_subject_<?php echo get_the_ID(); ?>','btn_after14days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,19);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after14days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="twentyone_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 24</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='24' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after21days_subject_<?php echo get_the_ID(); ?>" id="after21days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after21days = 'after21days_'.get_the_ID();
								$settings_after21days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after21days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after21days, $settings_after21days );
								?>
								<button class="btn btn-info" id="btn_after21days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after21days; ?>','after21days_subject_<?php echo get_the_ID(); ?>','btn_after21days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,24);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after21days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
							<div style="width:100%;padding:0 2px;" id="twentyeight_day_<?php echo get_the_ID(); ?>_tab" class="tabcontent_email">
								<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;padding-top:15px;">Day 30</p>
								<?php
								$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='30' ");
								?>
								<input type="text" placeholder="Enter Subject Line" name="after28days_subject_<?php echo get_the_ID(); ?>" id="after28days_subject_<?php echo get_the_ID(); ?>" style="font-weight:600;margin-top: 0;" value="<?php echo $data->subject; ?>" />
								<?php 
								$editor_id_after28days = 'after28days_'.get_the_ID();
								$settings_after28days =   array(
									'wpautop' => true, // use wpautop?
									'media_buttons' => false, // show insert/upload button(s)
									'textarea_name' => $editor_id_after28days, // set the textarea name to something different, square brackets [] can be used here
									'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
									'tabindex' => '',
									'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
									'editor_class' => '', // add extra class(es) to the editor textarea
									'teeny' => false, // output the minimal editor config used in Press This
									'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
									'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()

									'quicktags' => true, // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									'tinymce' => array(
										'toolbar1'      => 'bold,italic,underline,bullist,numlist,alignleft,aligncenter,alignright,separator,undo,redo,separator,pagelink,downloadlink,affiliatelink,externallink,personalisation,wp_add_media,load_template',
										'toolbar2'      => '',
									)
								);
								wp_editor( $data->content, $editor_id_after28days, $settings_after28days );
								?>
								<button class="btn btn-info" id="btn_after28days_save_email_<?php echo get_the_ID(); ?>" onclick="save_followup_email('<?php echo $editor_id_after28days; ?>','after28days_subject_<?php echo get_the_ID(); ?>','btn_after28days_save_email_<?php echo get_the_ID(); ?>',<?php echo get_the_ID(); ?>,30);">Save Email</button>
								<?php if(current_user_can('administrator')){ ?>
									<button class="view_actual_mail btn btn-success" onclick="show_actual_mail('<?php echo $editor_id_after28days; ?>');" >View Actual Mail</button>
								<?php } ?>
								<?php /*<button class="btn btn-primary">Add Link</button>*/ ?>
							</div>
						</div>
					</div>
					<?php
				}//over else part of exclusive funnel type
				?>
			</div>
			<?php 
		endwhile;
	else : ?>
		<p><?php _e( 'Sorry, no Funnels available.' ); ?></p>
	<?php endif;
	?>
	
	<div id="affiliate_requests_modal_links" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;"></div>
	<div id="funnel_integrations_modal" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;"></div>
	<div id="download_resource_modal" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;"></div>
	<div id="add_email_link_modal" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;"></div>
	<div id="funnel_links_modal" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;"></div>
	<div id="whitelabel_update_ex_link" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;"></div>
	
	<div id="email_list_settings_modal" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;">
		<h2 style="text-align: left;font-family:Droid Serif;font-weight:400;font-style:normal" class="vc_custom_heading">Mail List Settings</h2>
		<input type="hidden" name="email_list_post_id" value="" />
		<input type="hidden" name="publish_type" value="" />
		<?php echo do_shortcode('[fm_mail_list_settings]'); ?>
	</div>
	
	<div id="show_actual_mail_modal" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;"></div>
	<div id="marketplace_modal" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;"></div>
	<div id="funnel_clicksconv_modal" class="cn-demo-modal cn_demo-modal-width-50" style="display:none;"></div>
	<div id="page_id"></div>
	<span id="load_funnel_data" style="display:none;"><img src="https://funnelmates.com/wp-content/uploads/screenshot/loading.gif" style="" class="loader"></span>
	<div id="msg"></div>
	<button id="check_ajax" value="blank" style="display:none;"></button>
<style>
.switch{position: relative;display: inline-block;width: 60px;height: 34px;margin-left:20px;}
.switch input{opacity: 0;width: 0;height: 0;}
.slider{position: absolute;cursor: pointer;top: 0;left: 0;right: 0;bottom: 0;background-color: #ccc;-webkit-transition: .4s;transition: .4s;}
.slider:before{position: absolute;content: "";height: 26px;width: 26px;left: 4px;bottom: 4px;background-color: white;-webkit-transition: .4s;transition: .4s;}
input:checked + .slider {background-color: #425f94;}
input:focus + .slider{box-shadow: 0 0 1px #425f94;}
input:checked + .slider:before{-webkit-transform: translateX(26px);-ms-transform: translateX(26px);transform: translateX(26px);}
.slider.round{border-radius: 34px;}
.slider.round:before{border-radius: 50%;}
.funnel_btns_left a, .funnel_btns_right a{

background: #279dbc;
    color: white !important;
    height: 30px;
    border-radius: 15px;
    display: block;
    text-decoration: none !important;
    text-align: center;
    line-height: 30px;
    font-weight: 600;	
    margin: 10px 0;
	}
button.btn-remove{background:#FF6347;}
button.btn-update{background:#279dbc;}
li.ui-sortable-placeholder{background:#FF6347 !important;visibility:visible !important;width:100%; !important;display:inline-block !important;}
.aff_table{padding: 8px; text-align: left; border-bottom: 1px solid #ddd; word-break: break-all;}
 </style>

<script>
jQuery( document ).ready(function($){
	get_funnel();
	var position = $(window).scrollTop(); 
    $(window).on('scroll', function() {
		/* var scroll = $(window).scrollTop();
		var ajax_status = $('#check_ajax').val();
		var is_search = jQuery('input[name=is_search_results]').val();
		if(scroll > position) {
			var h = $('#content-container').height();
			var y_scroll_pos = window.pageYOffset + 250;
			if(y_scroll_pos > h && ajax_status == 'stop' && ajax_status != 'search' && is_search == '0') {
				get_funnel();
			}
		}else {
			
		}
		position = scroll; */
		var ajax_status = $('#check_ajax').val();
		var is_search = jQuery('input[name=is_search_results]').val();
		 if ($(window).scrollTop() >= $('#content-container').offset().top + $('#content-container').outerHeight() - window.innerHeight && ajax_status == 'stop' && ajax_status != 'search' && is_search == '0') {
			 get_funnel();
         }
    });
})	;

function get_funnel(){
	//alert("1");
	var limit = <?php echo LIMIT; ?>;
	var current_id = <?php echo $c_id; ?>;
	var page = jQuery('#page').val();
	jQuery('#check_ajax').val("run");
	if(page != null){
		jQuery('#load_funnel_data').show();
	}else{		
	}
	
	jQuery.ajax({
		type: 'POST',
		data: {
			"action" : "get_funnel_items",
			"limit" : limit,
			"current_id" : current_id,
			"page" : page
		},
		url: "<?php echo admin_url('admin-ajax.php'); ?>",
		success: function( data ){
			var obj = jQuery.parseJSON(data);
			jQuery('#load_funnel_data').hide();
			if(obj.html == ''){
				jQuery('#msg').html("<p id='alertFadeOut'>No more Funnel</p>");
				jQuery('#alertFadeOut').fadeOut(10000, function () {
				  jQuery('#alertFadeOut').text('');
				});
			}
			if(page != 'lastpage'){
				var html_data = obj.html;
				// jQuery('.funnel_parent').hide();
				for (i=0; i < html_data.length; i++){
					var dt = obj.html[i];
					jQuery('.funnel_search_'+dt).show();
				}
			}else{
				
			}
			/*if(page >= 1){
				jQuery('#funnel_item').append(obj.html);
			}else if(page == 'lastpage'){
				
			}else{
				jQuery('#funnel_item').html(obj.html);
			}*/
			jQuery('#page_id').html(obj.pagenav);
			jQuery('#check_ajax').val("stop");
			reinitiate_sortable(); 
		}
	});
} 
</script>