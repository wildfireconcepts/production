function clicked_lead_magnet_funnel(){
	jQuery('input[name=funnel_type]').val('lead_magnet_funnel');
	jQuery('label[for=landing_page_name]').html('Lead Magnet Funnel Name');
	jQuery('#submit_new_landing').html('Create Lead Magnet Funnel');	
	jQuery('#btn_lead_magnet_funnel button').html('Fetching Templates..');	
	var form_data = new FormData();
	form_data.append('funnel_type', 'lead_magnet_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_lead_magnet_funnel button').html('Click to Create');	
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function clicked_video_lead_funnel(){
	jQuery('input[name=funnel_type]').val('video_lead_funnel');
	jQuery('label[for=landing_page_name]').html('Video Lead Funnel Name');
	jQuery('#submit_new_landing').html('Create Video Lead Funnel');
	jQuery('#btn_video_lead_funnel button').html('Fetching Templates..');	
	/*new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();*/
	var form_data = new FormData();
	form_data.append('funnel_type', 'video_lead_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_video_lead_funnel button').html('Click to Create');
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function clicked_sales_funnel(){
	jQuery('input[name=funnel_type]').val('sales_funnel');
	jQuery('label[for=landing_page_name]').html('Sales Funnel Name');
	jQuery('#submit_new_landing').html('Create Sales Funnel');
	new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onComplete : add_close_to_custombox('#activate_dfy_funnel'),
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();
	return false;
}
function clicked_webinar_funnel(){
	jQuery('input[name=funnel_type]').val('webinar_lead_funnel');
	jQuery('label[for=landing_page_name]').html('Webinar Lead Funnel Name');
	jQuery('#submit_new_landing').html('Create Webinar Lead Funnel');
	jQuery('#btn_webinar_lead_funnel button').html('Fetching Templates..');
	/*new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();*/
	var form_data = new FormData();
	form_data.append('funnel_type', 'webinar_lead_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_webinar_lead_funnel button').html('Click to Create');
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function clicked_review_lead_funnel(){
	jQuery('input[name=funnel_type]').val('review_lead_funnel');
	jQuery('label[for=landing_page_name]').html('Review Lead Funnel Name');
	jQuery('#submit_new_landing').html('Create Review Lead Funnel');
	jQuery('#btn_review_lead_funnel button').html('Fetching Templates..');
	/*new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();*/
	var form_data = new FormData();
	form_data.append('funnel_type', 'review_lead_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_review_lead_funnel button').html('Click to Create');
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function clicked_quiz_lead_funnel(){
	jQuery('input[name=funnel_type]').val('quiz_lead_funnel');
	jQuery('label[for=landing_page_name]').html('Quiz Lead Funnel Name');
	jQuery('#submit_new_landing').html('Create Quiz Lead Funnel');
	jQuery('#btn_quiz_lead_funnel button').html('Fetching Templates..');
	/*new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();*/
	var form_data = new FormData();
	form_data.append('funnel_type', 'quiz_lead_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_quiz_lead_funnel button').html('Click to Create');
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function clicked_social_lead_funnel(){
	jQuery('input[name=funnel_type]').val('social_lead_funnel');
	jQuery('label[for=landing_page_name]').html('Social Lead Funnel Name');
	jQuery('#submit_new_landing').html('Create Social Lead Funnel');
	jQuery('#btn_social_lead_funnel button').html('Fetching Templates..');
	/*new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#activate_dfy_funnel',
			onOpen: close_activate_dfy_funnel()
		}	
	}).open();*/
	var form_data = new FormData();
	form_data.append('funnel_type', 'social_lead_funnel');
	form_data.append('action', 'search_funnels_template');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		context: this,
		success: function(data) {
			// alert(data);
			jQuery('#btn_social_lead_funnel button').html('Click to Create');
			jQuery('#display_results_funnel_types').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#activate_dfy_funnel',
					onComplete : add_close_to_custombox('#activate_dfy_funnel'),
					onOpen: close_activate_dfy_funnel()
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
	return false;
}
function close_activate_dfy_funnel(){
	jQuery('.template_sel_parent').css('display','inline-block');
	jQuery('.frm_funnel_create').css('display','none');
}
function frm_submit_submission(){
	var stext = jQuery('input[name=submit_submission]').val();
	if(stext == 'My Funnel Still Needs A Little Work'){
		Custombox.modal.closeAll();
	}else{
		// var post_id = jQuery(this).attr('data');
		var post_id = jQuery('input[name=submission_post_id]').val();
		/*if(post_id){
			jQuery('input[name=submit_submission]').val('Processing...');
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'publish_public_funnel');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					Custombox.modal.close();
					jQuery('#public_funnel_setup_modal_container').html(data);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#public_funnel_setup',
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}*/
		if(post_id){
			jQuery('input[name=submit_submission]').val('Loading...');
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'check_mail_list_settings');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				context: this,
				type: 'POST',
				success: function(data) {
					if(data == '1'){
						// Custombox.modal.close();
						var form_data = new FormData();
						form_data.append('post_id', post_id);
						form_data.append('action', 'publish_public_funnel');
						jQuery.ajax({
							url: ajaxurl,
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,
							type: 'POST',
							success: function(data) {
								Custombox.modal.close();
								jQuery('#public_funnel_setup_modal_container').html(data);
								new Custombox.modal({
									content: {
										effect: 'fadein',
										target: '#public_funnel_setup',
										onComplete : add_close_to_custombox('#public_funnel_setup'),
									}	
								}).open();
							},
							error: function(error) {
								console.log(error);
							}
						});
					}else{
						jQuery('input[name=email_list_post_id]').val(post_id);
						jQuery('input[name=publish_type]').val('public');
						Custombox.modal.close();
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#email_list_settings_modal',
								onComplete : add_close_to_custombox('#email_list_settings_modal'),
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	}
	return false;
}
/*function pages_creation_modal(){
	new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#pages_creation_modal',
		}	
	}).open();
}*/
/*function submit_new_landing(){
	return false;
}*/
jQuery(document).ready(function(){
	jQuery(document).on('click', '#apply_coupon', function(){
		var coupon_id = jQuery('input[name=apply_coupon_id]').val();
		var coupon_code = jQuery('input[name=insert_coupon_code]').val();
		var funnel_id = jQuery('input[name=coupon_funnel_id]').val();
		if(coupon_id && coupon_code){
			// jQuery(this).prop('disabled',true);
			jQuery(this).html('Applying..');
			var form_data = new FormData();
			form_data.append('coupon_id', coupon_id);
			form_data.append('coupon_code', coupon_code);
			form_data.append('funnel_id', funnel_id);
			form_data.append('action', 'apply_coupon');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '0'){
						Swal.fire({
						  icon: 'error',
						  title: 'Oops...',
						  text: 'Coupon Code is Incorrect..!',
						})
						return false;
					}else{
						jQuery('p#coupon_code_p').html('<strong>'+coupon_code+'</strong> Applied Successfully..<span onclick="unlock_funnel('+funnel_id+');">Remove</span>');
						jQuery('p#coupon_code_p').addClass('active');
						jQuery('.table.tbl_coupon').hide();
						jQuery('#price_div_unlock_funnel').html(data);
						// var obj = jQuery.parseJSON(data);
						/*var i = 0;
						jQuery.each(obj, function (key, val) {
							jQuery('#price_'+funnel_id+'_'+key).html('$'+val);
							jQuery('button#unlock_funnel_'+key+'_'+funnel_id).attr("onclick","unlock_funnel_finalize('"+key+"',"+funnel_id+",'"+val+"','"+coupon_id+"')");
							i = 1;
						});
						if(i > 0){
							jQuery('p#coupon_code_p').html('<strong>'+coupon_code+'</strong> Applied Successfully..<span onclick="unlock_funnel('+funnel_id+');">Remove</span>');
							jQuery('p#coupon_code_p').addClass('active');
							jQuery('.table.tbl_coupon').hide();
						}*/
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#enter_coupon_code', function(){
		jQuery('table.tbl_coupon').slideToggle();
	});
	jQuery(document).on('click', '#reset_search_fm', function(){
		window.location = window.location;
	});
	jQuery(document).on('click', '#search_fm', function(){
		var search_str = jQuery('input[name=search_funnel_fm]').val();
		// var published = jQuery('input[name=published]').is(':checked');
		// var unpublished = jQuery('input[name=unpublished]').is(':checked');
		// var under_review = jQuery('input[name=under_review]').is(':checked');
		if(search_str){
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Searching..');
			var form_data = new FormData();
			form_data.append('search_str', search_str);
			// form_data.append('published', published);
			// form_data.append('unpublished', unpublished);
			// form_data.append('under_review', under_review);
			form_data.append('action', 'search_funnels');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					var obj = jQuery.parseJSON(data);
					// jQuery('#load_funnel_data').hide();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Search');
					jQuery('#reset_search_fm').show();
					jQuery('input[name=is_search_results]').val('1');
					if(obj.html == ''){
						jQuery('#msg').html("<p id='alertFadeOut'>No Funnel Found</p>");
						jQuery('#alertFadeOut').fadeOut(10000, function () {
						  jQuery('#alertFadeOut').text('');
						});
					}else{
						var html_data = obj.html;
						jQuery('.funnel_parent').hide();
						for (i=0; i < html_data.length; i++){
							var dt = obj.html[i];
							jQuery('.funnel_search_'+dt).show();
						}
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('change', 'input#funnel_is_coupon_code', function(){
		if(jQuery('input#funnel_is_coupon_code').is(':checked')){
			jQuery('tbody#coupon_container').addClass('active');
			jQuery("input[name=coupon_code]").prop('required',true);
			jQuery("select[name=coupon_type]").prop('required',true);
			jQuery("input[name=coupon_value]").prop('required',true);
		}else{
			jQuery('tbody#coupon_container').removeClass('active');
			jQuery("input[name=coupon_code]").prop('required',false);
			jQuery("select[name=coupon_type]").prop('required',false);
			jQuery("input[name=coupon_value]").prop('required',false);
		}
	});
	jQuery(document).on('change', '.submission_check', function(){
		if(jQuery('input.submission_check').not(':checked').length == 0){
			jQuery('input[name=submit_submission]').removeClass("button-danger");
			jQuery('input[name=submit_submission]').addClass("button-success");
			jQuery('input[name=submit_submission]').val("Yes! My Funnel Is Ready!");
		}else{
			jQuery('input[name=submit_submission]').removeClass("button-success");
			jQuery('input[name=submit_submission]').addClass("button-danger");
			jQuery('input[name=submit_submission]').val("My Funnel Still Needs A Little Work");
		}
	});
	
	jQuery('#submit_new_landing').on('click', function(){
		var name = jQuery('input[name=landing_page_name]').val();
		var funnel_type = jQuery('input[name=funnel_type]').val();
		var funnel_template_type = jQuery('input[name=funnel_template_type]').val();
		if(name){
			var btnoldname = jQuery(this).html();
			jQuery(this).html("Creating...");
			jQuery(this).prop('disabled', true);
			var form_data = new FormData();
			form_data.append('landing_page_name', name);
			form_data.append('funnel_type', funnel_type);
			form_data.append('funnel_template_type', funnel_template_type);
			form_data.append('action', 'landing_page_create');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					// alert(data);
					var obj = jQuery.parseJSON(data);
					jQuery(this).html(btnoldname);
					jQuery(this).prop('disabled', false);
					jQuery('#activate_dfy_funnel .wpb_wrapper').html(obj.html);
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/*jQuery('.publish_funnel').on('click',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'check_funnel_bundle');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					jQuery('#publish_funnel_modal').html(data);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#publish_funnel_modal',
						}	
					}).open();
					// jQuery('#activate_dfy_funnel .wpb_wrapper').html('<a href="'+data+'">Edit Page with Builder</a>');
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});*/
	jQuery(document).on('click', '#remove_custom_domain', function(){
		var form_data = new FormData();
		form_data.append('action', 'remove_custom_url');
		jQuery(this).html('Removing....');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context: this,
			success: function(data) {
				jQuery('input[name=custom_domain]').val("");
				jQuery(this).html('Remove Custom Domain');
				jQuery(this).css('display','none');
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click', '#save_custom_domain', function(){
		var custom_domain = jQuery('input[name=custom_domain]').val();
		var isvalid = isUrlValid(custom_domain);
		if(isvalid == true){
			var lastChar = custom_domain[custom_domain.length -1];
			if(lastChar == '/'){
				custom_domain = custom_domain.slice(0,-1);
			}
			var form_data = new FormData();
			form_data.append('custom_url', custom_domain);
			form_data.append('action', 'save_custom_url');
			jQuery(this).html('Saving....');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					// alert(data);
					// var obj = jQuery.parseJSON(data);
					// jQuery(this).html(btnoldname);
					// jQuery(this).prop('disabled', false);
					// jQuery('#activate_dfy_funnel .wpb_wrapper').html(obj.html);
					jQuery(this).html('Save Custom Domain');
					if(data == '1'){
						// jQuery('input[name="custom_domain"]').val(custom_domain);
						jQuery('p#custom_domain_error').html('Custom URL Saved..');
						jQuery('p#custom_domain_error').css('color','green');
						jQuery('#remove_custom_domain').show();
						jQuery('p#custom_domain_error').show();
					}else if(data == '0'){
						jQuery('p#custom_domain_error').html('Duplicate Custom URL Found for other User..');
						jQuery('p#custom_domain_error').css('color','#F00');
						jQuery('p#custom_domain_error').show();
					}else if(data == '2'){
						jQuery('p#custom_domain_error').html('You\'re using the same Custom URL..');
						jQuery('p#custom_domain_error').css('color','#F00');
						jQuery('p#custom_domain_error').show();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('p#custom_domain_error').html('Invalid Custom URL. Either you are trying to save invalid URL or you should try it with http:// OR https://');
			jQuery('input[name="custom_domain"]').css('border-color','#F00');
			jQuery('p#custom_domain_error').css('color','#F00');
			jQuery('p#custom_domain_error').show();
		}
	});
	/*jQuery(document).on('keyup','input[name=template_search]', function(e){
		if(e.which == 13) {
			e.preventDefault();
			jQuery('#search_template_clicked').trigger('click');
		}
	})*/;
	jQuery(document).on('click','#withdraw_from_fm', function(){
		new Custombox.modal({
		content: {
				effect: 'fadein',
				target: '#withdraw_money_modal',
				onComplete : add_close_to_custombox('#withdraw_money_modal'),
			}	
		}).open();
	});
	
	jQuery(document).on('keyup', 'input[name=withdraw_amount]', function() {
		  var _this = jQuery(this);
		  var min = parseInt(_this.attr('min')) || 1; // if min attribute is not defined, 1 is default
		  var max = parseInt(_this.attr('max')) || 100; // if max attribute is not defined, 100 is default
		  var val = parseInt(_this.val()) || (min - 1); // if input char is not a number the value will be (min - 1) so first condition will be true
		  if (val < min)
			_this.val(min);
		  if (val > max)
			_this.val(max);
	});
	jQuery(document).on('click','#form_popup', function(){
		var value = jQuery("input[name=withdraw_amount]").val();
		if(value < 50){
			jQuery('.paypal_error').html('<p style="color:orange;">Please enter a minimum 50$</p>');
			return false;
		}else{
			jQuery('.paypal_error').html('');
		}
		jQuery('#payment_price').html(value);
			
		Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
		new Custombox.modal({
		content: {
				effect: 'fadein',
				target: '#form_popup_model',
				onComplete : add_close_to_custombox('#form_popup_model'),
			}	
		}).open();	
	});
	
	jQuery(document).on('click','#send_money', function(){
		var amount = jQuery('#payment_price').html();
		var user_id = jQuery("input[name=recipient_id]").val();
		var email = jQuery("input[name=email]").val();
		var name = jQuery("input[name=fname]").val();
		var address = jQuery("input[name=home_address]").val();
		var city = jQuery("input[name=city]").val();
		var postal_code = jQuery("input[name=postal_code]").val();
		var bank_name = jQuery("input[name=bank_name]").val();
		var bank_address = jQuery("input[name=bank_address]").val();
		var account_name = jQuery("input[name=account_name]").val();
		var account_number = jQuery("input[name=account_number]").val();
		var swift_code = jQuery("input[name=swift_code]").val();
		var ifsc_code = jQuery("input[name=ifsc_code]").val();
		var currency = jQuery("input[name=currency]").val();
		
		var form_data = new FormData();
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(user_id == '' || amount == ''){
			return false;
		}
		if(email == '' || name == '' || address == '' || city == '' || postal_code == '' || bank_name == '' || bank_address == '' || account_name == '' || account_number == '' || swift_code == '' || ifsc_code == '' || currency == ''){
			jQuery('#msg').html('<p style="color:red;">Please Insert Empty Fields Data!</p>');
			return false;
		}else if(!regex.test(email)){
			jQuery('#msg').html('<p style="color:red;">Please Insert Valid Email Address!</p>');
			return false;
        }else{
			jQuery('#msg').html('');
		}
		
		form_data.append('amount',amount);
		form_data.append('user_id',user_id);
		form_data.append('email',email);
		form_data.append('name',name);
		form_data.append('address',address);
		form_data.append('city',city);
		form_data.append('postal_code',postal_code);
		form_data.append('bank_name',bank_name);
		form_data.append('bank_address',bank_address);
		form_data.append('account_name',account_name);
		form_data.append('account_number',account_number);
		form_data.append('swift_code',swift_code);
		form_data.append('ifsc_code',ifsc_code);
		form_data.append('currency',currency);
		form_data.append('action','payment_withdrawal');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				jQuery("input[name=recipient_id]").val("");
				jQuery("input[name=recipient_email]").val("");
				jQuery("input[name=recipient_name]").val("");
				jQuery("textarea[name=recipient_address]").val("");
				jQuery("input[name=recipient_iban]").val("");
				jQuery('#payment_price').html('<p style="color:green; font-size:16px;">Your payment request sent</p>');
				setTimeout(function() {
				  location.reload();
				}, 10000);
			}
		});
	});
	
	jQuery(document).on('click','#howitworks_fm', function(){
		new Custombox.modal({
		content: {
				effect: 'fadein',
				target: '#howitworks_fm_modal',
				onComplete : add_close_to_custombox('#howitworks_fm_modal'),
			}	
		}).open();
	});
	/*jQuery(document).on('click','#search_template_clicked', function(){
		var keyword = jQuery('input[name=template_search]').val();
		var funnel_type = jQuery('input[name=funnel_type]').val();
		if(keyword){
			var form_data = new FormData();
			form_data.append('search_keyword', keyword);
			form_data.append('funnel_type', funnel_type);
			form_data.append('action', 'template_search_by_keyword');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					if(data){
						jQuery('.template_sel_parent').html('<p><a href="javascript:void(0);" class="refresh_template_search">Reset</a></p>'+data);
					}
					// jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Confirm Page with Builder</a>');
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});*/
	/*jQuery(document).on('click', '.refresh_template_search', function(){
		var form_data = new FormData();
		form_data.append('search_keyword', '');
		form_data.append('action', 'template_search_by_keyword');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					jQuery('.template_sel_parent').html(data);
				}
				// jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Confirm Page with Builder</a>');
			},
			error: function(error) {
				console.log(error);
			}
		});
	});*/
	jQuery(document).on('click','.view_funnel_feedback',function(){
		var post_id = jQuery(this).attr('data');
		// alert(post_id);
		if(post_id){
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#viwe_feedback_modal_'+post_id,
					onComplete : add_close_to_custombox('#viwe_feedback_modal_'+post_id),
				}	
			}).open();
		}
	});
	jQuery(document).on('click','#create_confirm_page',function(){
		var post_id = jQuery(this).attr('data');
		// alert(post_id);
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('post_title', 'Confirm');
			form_data.append('action', 'confirm_page_create');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Confirm Page with Builder</a>');
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','#create_thankyou_page',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('post_title', 'Thank You');
			form_data.append('action', 'thankyou_page_create');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Thank You Page with Builder</a>');
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','#create_download_page',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('post_title', 'Download');
			form_data.append('action', 'download_page_create');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','#privatepublish_submit_review',function(){
		var post_id = jQuery(this).attr('data');
		
		
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'check_mail_list_settings');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				context: this,
				type: 'POST',
				success: function(data) {
					if(data == '1'){
						var form_data = new FormData();
						form_data.append('post_parent', post_id);
						form_data.append('action', 'private_submission_finalize');
						jQuery.ajax({
							url: ajaxurl,
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,
							type: 'POST',
							success: function(data) {
								if(data){
									jQuery('#private_submission_modal').html("<h3 style='text-align:center;'>Thank You! Your funnel has been submitted for review. <a style='font-size: 20px;' href='https://funnelmates.com/wiki/funnel-review-process/'>Find out more about our review process here</a></h3>");
								}
							},
							error: function(error) {
								console.log(error);
							}
						});
					}else{
						jQuery('input[name=email_list_post_id]').val(post_id);
						jQuery('input[name=publish_type]').val("private");
						Custombox.modal.close();
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#email_list_settings_modal',
								onComplete : add_close_to_custombox('#email_list_settings_modal'),
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','#privatepublish_instant_activation',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'publish_funnel_finalize');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					if(data){
						window.location = window.location;
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','#publish_funnel_finalize',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'private_submission');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					jQuery('#private_submission_modal').html(data);
					Custombox.modal.close();
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#private_submission_modal',
							onComplete : add_close_to_custombox('#private_submission_modal'),
						}	
					}).open();
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					/*if(data){
						window.location = window.location;
					}*/
				},
				error: function(error) {
					console.log(error);
				}
			});
			/*var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'publish_funnel_finalize');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					if(data){
						window.location = window.location;
					}
				},
				error: function(error) {
					console.log(error);
				}
			});*/
		}
	});
	/*jQuery(document).on('click','.unpublish_funnel',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'unpublish_funnel');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					if(data){
						window.location = window.location;
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});*/
	jQuery(document).on('click', '#save_mls', function(){
		var mls_name = jQuery('input[name=mls_name]').val();
		var mls_from_email = jQuery('input[name=mls_from_email]').val();
		// alert(mls_from_email);
		var mls_from_name = jQuery('input[name=mls_from_name]').val();
		var mls_email_subject = jQuery('input[name=mls_email_subject]').val();
		
		var mls_company = jQuery('input[name=mls_company]').val();
		var mls_state = jQuery('input[name=mls_state]').val();
		var mls_address1 = jQuery('input[name=mls_address1]').val();
		var mls_city = jQuery('input[name=mls_city]').val();
		var mls_address2 = jQuery('input[name=mls_address2]').val();
		var mls_zip = jQuery('input[name=mls_zip]').val();
		// var mls_country = jQuery('input[name=mls_country]').val();
		var mls_country = jQuery('select[name=mls_country] :selected').val();
		var mls_phone = jQuery('input[name=mls_phone]').val();
		var mls_email = jQuery('input[name=mls_email]').val();
		var mls_homepage = jQuery('input[name=mls_homepage]').val();
		/*var mls_subscription_confirmation_email = jQuery("input[name='mls_subscription_confirmation_email']");
		if(mls_subscription_confirmation_email.prop('checked')){
			mls_subscription_confirmation_email = '1';
		}else{
			mls_subscription_confirmation_email = '0';
		}
		var mls_final_welcome_email = jQuery("input[name='mls_final_welcome_email']");
		if(mls_final_welcome_email.prop('checked')){
			mls_final_welcome_email = '1';
		}else{
			mls_final_welcome_email = '0';
		}
		var mls_unsubscribe_notification = jQuery("input[name='mls_unsubscribe_notification']");
		if(mls_unsubscribe_notification.prop('checked')){
			mls_unsubscribe_notification = '1';
		}else{
			mls_unsubscribe_notification = '0';
		}*/
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(mls_name == '' || mls_from_email == '' || !regex.test(mls_from_email) || mls_from_name == '' || mls_company == '' || mls_state == '' || mls_address1 == '' || mls_city == '' || mls_zip == '' || mls_country == '' || mls_country == '0' || mls_phone == '' || mls_email == ''){
			jQuery('.mls_error').html('Required Fields Missing or Invalid');
			jQuery('.mls_error').show();
			if(mls_name == ''){
				jQuery('input[name=mls_name]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_name]').removeClass('mls_input_error');
			}
			if(mls_from_email == '' || !regex.test(mls_from_email)){
				jQuery('input[name=mls_from_email]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_from_email]').removeClass('mls_input_error');
			}
			
			if(mls_from_name == ''){
				jQuery('input[name=mls_from_name]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_from_name]').removeClass('mls_input_error');
			}
			
			if(mls_company == ''){
				jQuery('input[name=mls_company]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_company]').removeClass('mls_input_error');
			}
			
			if(mls_state == ''){
				jQuery('input[name=mls_state]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_state]').removeClass('mls_input_error');
			}
			
			if(mls_address1 == ''){
				jQuery('input[name=mls_address1]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_address1]').removeClass('mls_input_error');
			}
			
			if(mls_city == ''){
				jQuery('input[name=mls_city]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_city]').removeClass('mls_input_error');
			}
			
			if(mls_zip == ''){
				jQuery('input[name=mls_zip]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_zip]').removeClass('mls_input_error');
			}
			
			if(mls_country == '' || mls_country == '0'){
				jQuery('select[name=mls_country]').addClass('mls_input_error');
			}else{
				jQuery('select[name=mls_country]').removeClass('mls_input_error');
			}
			
			if(mls_phone == ''){
				jQuery('input[name=mls_phone]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_phone]').removeClass('mls_input_error');
			}
			
			if(mls_email == ''){
				jQuery('input[name=mls_email]').addClass('mls_input_error');
			}else{
				jQuery('input[name=mls_email]').removeClass('mls_input_error');
			}
			return false;
		}else{
			var form_data = new FormData();
			form_data.append('action', 'save_mail_list_settings');
			form_data.append('mls_name', mls_name);
			form_data.append('mls_from_email', mls_from_email);
			form_data.append('mls_from_name', mls_from_name);
			form_data.append('mls_email_subject', mls_email_subject);
			form_data.append('mls_company', mls_company);
			form_data.append('mls_state', mls_state);
			form_data.append('mls_address1', mls_address1);
			form_data.append('mls_city', mls_city);
			form_data.append('mls_address2', mls_address2);
			form_data.append('mls_zip', mls_zip);
			form_data.append('mls_country', mls_country);
			form_data.append('mls_phone', mls_phone);
			form_data.append('mls_email', mls_email);
			form_data.append('mls_homepage', mls_homepage);
			// form_data.append('mls_subscription_confirmation_email', mls_subscription_confirmation_email);
			// form_data.append('mls_final_welcome_email', mls_final_welcome_email);
			// form_data.append('mls_unsubscribe_notification', mls_unsubscribe_notification);
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Saving....');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					//alert(data);
					if(data){
						var pathname = window.location.pathname;
						if(pathname == '/funnels/' || pathname == '/funnels'){
							var post_id = jQuery('input[name=email_list_post_id]').val();
							var publish_type = jQuery('input[name=publish_type]').val();
							if(post_id){
								if(publish_type == 'public'){
									var form_data = new FormData();
									form_data.append('post_id', post_id);
									form_data.append('action', 'publish_public_funnel');
									jQuery.ajax({
										url: ajaxurl,
										cache: false,
										contentType: false,
										processData: false,
										data: form_data,
										type: 'POST',
										success: function(data) {
											Custombox.modal.close();
											jQuery('#public_funnel_setup_modal_container').html(data);
											new Custombox.modal({
												content: {
													effect: 'fadein',
													target: '#public_funnel_setup',
													onComplete : add_close_to_custombox('#public_funnel_setup'),
												}	
											}).open();
										},
										error: function(error) {
											console.log(error);
										}
									});
								}else if(publish_type == 'private'){
									var form_data = new FormData();
									form_data.append('post_parent', post_id);
									form_data.append('action', 'private_submission_finalize');
									jQuery.ajax({
										url: ajaxurl,
										cache: false,
										contentType: false,
										processData: false,
										data: form_data,
										type: 'POST',
										success: function(data) {
											if(data){
												jQuery('#private_submission_modal').html("<h3 style='text-align:center;'>Thank You! Your funnel has been submitted for review. <a style='font-size: 20px;' href='https://funnelmates.com/wiki/funnel-review-process/'>Find out more about our review process here</a></h3>");
											}
										},
										error: function(error) {
											console.log(error);
										}
									});
								}
							}
						}
						jQuery(this).prop('disabled',false);
						jQuery(this).html('Save');
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery('#save_general_settings').on('click', function(){
		var clickbank_id = jQuery('input[name=clickbank_id]').val();
		var jvzoo_id = jQuery('input[name=jvzoo_id]').val();
		var warriorplus_id = jQuery('input[name=warriorplus_id]').val();
		var paypal_email = jQuery('input[name=paypal_email]').val();
		var paykickstart_id = jQuery('input[name=paykickstart_id]').val();
		/*alert(clickbank_id);
		alert(jvzoo_id);
		alert(warriorplus_id);*/
		/*if(paypal_email == ''){
			jQuery('#withdrawl_errot_msg').html('<span style="color:red;">Enter paypal Email </span>');
			return false;
		}else{
			jQuery('#withdrawl_errot_msg').html('');
		}
		if(clickbank_id == ''){
			jQuery('#withdrawl_errot_msg').html('<span style="color:red;">Enter clickbank ID</span>');
			return false;
		}else{
			jQuery('#withdrawl_errot_msg').html('');
		}
		if(jvzoo_id == ''){
			jQuery('#withdrawl_errot_msg').html('<span style="color:red;">Enter jvzoo ID</span>');
			return false;
		}else{
			jQuery('#withdrawl_errot_msg').html('');
		}
		if(warriorplus_id == ''){
			jQuery('#withdrawl_errot_msg').html('<span style="color:red;">Enter warriorplus ID</span>');
			return false;
		}else{
			jQuery('#withdrawl_errot_msg').html('');
		}*/
		// if(clickbank_id || jvzoo_id || warriorplus_id){
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Saving....');
			var form_data = new FormData();
			form_data.append('action', 'save_general_settings');
			form_data.append('clickbank_id', clickbank_id);
			form_data.append('jvzoo_id', jvzoo_id);
			form_data.append('warriorplus_id', warriorplus_id);
			form_data.append('paypal_email', paypal_email);
			form_data.append('paykickstart_id', paykickstart_id);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					//alert(data);
					if(data){
						jQuery(this).prop('disabled',false);
						jQuery(this).html('Save Settings');
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		// }
	});
	jQuery('.delete_funnel').on('click',function(){
		if (!confirm('Are you sure?')) return false;
		var post_id = jQuery(this).attr('data');
		if(post_id){
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Deleting....');
			var form_data = new FormData();
			form_data.append('action', 'delete_funnel');
			form_data.append('post_id', post_id);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					//alert(data);
					if(data){
						jQuery(this).prop('disabled',false);
						jQuery(this).html('Deleted');
						jQuery('.funnel_search_'+post_id).hide();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#everwebinar_connect', function(){
		var everwebinar_key = jQuery('input[name=everwebinar_key]').val();
		if(everwebinar_key){
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('everwebinar_key', everwebinar_key);
			form_data.append('action', 'connect_everwebinar');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					if(data){
						if(data == '1'){
							jQuery(this).prop('disabled',false);
							jQuery('input[name=everwebinar_key]').prop('disabled',true);
							jQuery(this).attr('id','btn_everwebinar_disconnect');
							jQuery(this).removeClass('btn_green');
							jQuery(this).addClass('btn_red');
							jQuery(this).html('Disconnect');
						}else{
							jQuery(this).removeClass('btn_green');
							jQuery(this).addClass('btn_red');
							jQuery(this).html('Invalid API Key');
						}
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#btn_everwebinar_disconnect', function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_everwebinar');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					var html_form = '<input type="text" name="everwebinar_key" placeholder="EverWebinar API Key" /><button type="button" class="btn_green" id="everwebinar_connect" style="float: none;">Connect</button>';
					jQuery(this).parent().html(html_form);
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click', '#btn_webinar_disconnect', function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_webinar');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					var ruri = 'https://funnelmates.com/settings/#1614337426314-c40234df-9260';
					var html_form = '<form action="https://api.getgo.com/oauth/v2/authorize" method="GET">'+
						'<input type="hidden" name="redirect_uri" value="'+ruri+'" />'+
						'<input type="hidden" name="client_id" value="d92b304a-9344-4cb4-a012-56947c8151ff" />'+
						'<input type="hidden" name="response_type" value="code" />'+
						'<input type="submit" name="submit" value="Authorize" />'+
					'</form>';
					jQuery(this).parent().parent().html(html_form);
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click', '#btn_aweber_disconnect', function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
			form_data.append('action', 'disconnect_aweber');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					if(data){
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).html("Authorize");
						jQuery(this).attr("id","btn_aweber_connect");
						jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
	});
	jQuery(document).on('click', '#btn_aweber_connect', function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Authorizing..');
		var form_data = new FormData();
		form_data.append('action', 'connect_aweber');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					window.location = data;
					// if(data == '1'){
						// jQuery(this).removeClass("btn_green");
						// jQuery(this).removeClass("btn_red");
						// jQuery(this).attr("id","btn_aweber_disconnect");
					// }else{
						// window.location = data;
					// }
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/************************************/
	jQuery(document).on('click','#btn_sendlane_connect',function(){
		var apikey = jQuery('input[name=sendlane_api_key]').val();
		var appihashkey = jQuery('input[name=sendlane_api_hash_key]').val();
		if(apikey && appihashkey){
			jQuery('input[name=sendlane_api_key]').removeClass('blank_field');
			jQuery('input[name=sendlane_api_hash_key]').removeClass('blank_field');
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'connect_sendlane');
			form_data.append('apikey', apikey);
			form_data.append('appihashkey', appihashkey);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					if(data){
						if(data == '1'){
							// 4afd1067caae10e72850b267ec749d23-us4
							jQuery('.hidden_tr_message_sl td').html('Account Connected..');
							jQuery('.hidden_tr_message_sl td').css('color','green');
							jQuery('.hidden_tr_message_sl').show();
							jQuery(this).html('Connected');
							
							///////////
							jQuery('input[name=sendlane_api_key]').val(apikey);
							jQuery('input[name=sendlane_api_hash_key]').val(appihashkey);
							jQuery('input[name=sendlane_api_key]').prop('disabled',true);
							jQuery('input[name=sendlane_api_hash_key]').prop('disabled',true);
							jQuery(this).prop('disabled',false);
							jQuery(this).addClass('btn_red');
							jQuery(this).removeClass('btn_green');
							jQuery(this).html('Disconnect');
							jQuery(this).attr('id','btn_sendlane_disconnect');
							
						}else{
							jQuery('.hidden_tr_message_sl td').html('Error While Connecting..');
							jQuery('.hidden_tr_message_sl td').css('color','red');
							jQuery('.hidden_tr_message_sl').show();
							jQuery(this).prop('disabled',false);
							jQuery(this).html('Connect Again');
						}
						// jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=sendlane_api_key]').focus();
			jQuery('input[name=sendlane_api_key]').addClass('blank_field');
			jQuery('input[name=sendlane_api_hash_key]').addClass('blank_field');
		}
	});
	jQuery(document).on('click','#btn_sendlane_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_sendlane');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					//alert(data);
					//window.location = window.location;
					jQuery('.hidden_tr_message_sl td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_sl td').css('color','red');
					jQuery('input[name=sendlane_api_key]').val('');
					jQuery('input[name=sendlane_api_hash_key]').val('');
					jQuery('input[name=sendlane_api_key]').prop('disabled',false);
					jQuery('input[name=sendlane_api_hash_key]').prop('disabled',false);
					jQuery('input[name=sendlane_api_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_sendlane_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/************************************/
	/*jQuery(document).on('click','#btn_webinar_connect',function(){
		var key = jQuery('input[name=webinar_clientid]').val();
		if(key){
			jQuery('input[name=webinar_clientid]').removeClass('blank_field');
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'connect_webinar');
			form_data.append('client_id', key);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					if(data){
						window.open(data, '_SELF');
						/*if(data == '1'){
							// 4afd1067caae10e72850b267ec749d23-us4
							jQuery('.hidden_tr_message td').html('Account Connected..');
							jQuery('.hidden_tr_message td').css('color','green');
							jQuery('.hidden_tr_message').show();
							jQuery(this).html('Connected');
							
							///////////
							jQuery('input[name=webinar_clientid]').val(key);
							jQuery('input[name=webinar_clientid]').prop('disabled',true);
							jQuery(this).prop('disabled',false);
							jQuery(this).addClass('btn_red');
							jQuery(this).removeClass('btn_green');
							jQuery(this).html('Disconnect');
							jQuery(this).attr('id','btn_webinar_disconnect');
							
						}else{
							jQuery('.hidden_tr_message td').html('Error While Connecting..');
							jQuery('.hidden_tr_message td').css('color','red');
							jQuery('.hidden_tr_message').show();
							jQuery(this).prop('disabled',false);
							jQuery(this).html('Connect Again');
						}*
						// jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=webinar_clientid]').focus();
			jQuery('input[name=webinar_clientid]').addClass('blank_field');
		}
	});*/
	jQuery(document).on('click','#btn_constant_contact_connect',function(){
		var key = "sdaf65465wef3asd21fd2s";
		var form_data = new FormData();
		form_data.append('action', 'connect_constant_contact');
		form_data.append('key', key);
		jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {	
					alert(data);
				},
				error: function(error) {
					console.log(error);
				}
			});
	});
	
	/* SENDIIO start */
	jQuery(document).on('click','#btn_sendiio_connect',function(){
		var apitoken = jQuery('input[name=sendiio_api_token]').val();
		var apikey = jQuery('input[name=sendiio_api_key]').val();
		if(apikey && apitoken){			
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'sendiio_connect');
			form_data.append('key', apikey);
			form_data.append('token', apitoken);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {	
					//alert(data);
					if(data == '1'){
					jQuery('.hidden_tr_message_sendi td').html('Account Connected..');
					jQuery('.hidden_tr_message_sendi td').css('color','green');
					jQuery('.hidden_tr_message_sendi').show();
					jQuery(this).html('Connected');
					
					///////////
					jQuery('input[name=sendiio_api_key]').val(apikey);
					jQuery('input[name=sendiio_api_key]').prop('disabled',true);
					jQuery('input[name=sendiio_api_token]').val(apitoken);
					jQuery('input[name=sendiio_api_token]').prop('disabled',true);
					jQuery(this).prop('disabled',false);
					jQuery(this).addClass('btn_red');
					jQuery(this).removeClass('btn_green');
					jQuery(this).html('Disconnect');
					jQuery(this).attr('id','btn_sendiio_disconnect');
					jQuery('.hidden_tr_message_sendi').fadeOut(10000, function () {
					  jQuery('.hidden_tr_message_sendi').text('');
					});
				}else{					
					jQuery('.hidden_tr_message_sendi td').html('Error While Connecting..');
					jQuery('.hidden_tr_message_sendi td').css('color','red');
					jQuery('.hidden_tr_message_sendi').show();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Connect Again');
					
				}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			if(apitoken == ''){
				jQuery('input[name=sendiio_api_token]').focus();
				jQuery('input[name=sendiio_api_token]').addClass('blank_field');
			}else{
				jQuery('input[name=sendiio_api_key]').focus();
				jQuery('input[name=sendiio_api_key]').addClass('blank_field');				
			}
		}
	})
	
	jQuery(document).on('click','#btn_sendiio_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_sendiio');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data == '1'){
					jQuery('.hidden_tr_message_sendi td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_sendi td').css('color','red');
					jQuery('input[name=sendiio_api_key]').val('');
					jQuery('input[name=sendiio_api_key]').prop('disabled',false);
					jQuery('input[name=sendiio_api_token]').val('');
					jQuery('input[name=sendiio_api_token]').prop('disabled',false);
					jQuery('input[name=sendiio_api_token]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_sendiio_connect');
					jQuery('.hidden_tr_message_sendi').fadeOut(10000, function () {
					  jQuery('.hidden_tr_message_sendi').text('');
					});
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/* SENDIIO over */
	
	/* mailerlite */
	jQuery(document).on('click','#btn_mailerlite_connect',function(){
		var apikey = jQuery('input[name=mailerlite_api_key]').val();
		if(apikey){			
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'mailerlite_connect');
			form_data.append('key', apikey);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {	
					//alert(data);
					if(data == '1'){
					jQuery('.hidden_tr_message_ml td').html('Account Connected..');
					jQuery('.hidden_tr_message_ml td').css('color','green');
					jQuery('.hidden_tr_message_ml').show();
					jQuery(this).html('Connected');
					
					///////////
					jQuery('input[name=mailerlite_api_key]').val(apikey);
					jQuery('input[name=mailerlite_api_key]').prop('disabled',true);
					jQuery(this).prop('disabled',false);
					jQuery(this).addClass('btn_red');
					jQuery(this).removeClass('btn_green');
					jQuery(this).html('Disconnect');
					jQuery(this).attr('id','btn_mailerlite_disconnect');
					
				}else{					
					jQuery('.hidden_tr_message_ml td').html('Error While Connecting..');
					jQuery('.hidden_tr_message_ml td').css('color','red');
					jQuery('.hidden_tr_message_ml').show();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Connect Again');
					
				}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=mailerlite_api_key]').focus();
			jQuery('input[name=mailerlite_api_key]').addClass('blank_field');
		}
	});
	
	jQuery(document).on('click','#btn_mailerlite_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_mailerlite');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					jQuery('.hidden_tr_message_ml td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_ml td').css('color','red');
					jQuery('input[name=mailerlite_api_key]').val('');
					jQuery('input[name=mailerlite_api_key]').prop('disabled',false);
					jQuery('input[name=mailerlite_api_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_mailerlite_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/* mailerlite */
	
	/* drip */
	jQuery(document).on('click','#btn_drip_connect',function(){
		var apikey = jQuery('input[name=drip_api_key]').val();
		if(apikey){			
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'drip_connect');
			form_data.append('key', apikey);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {	
					//alert(data);
					if(data == '1'){
					jQuery('.hidden_tr_message_dp td').html('Account Connected..');
					jQuery('.hidden_tr_message_dp td').css('color','green');
					jQuery('.hidden_tr_message_dp').show();
					jQuery(this).html('Connected');
					
					///////////
					jQuery('input[name=convertkit_api_key]').val(apikey);
					jQuery('input[name=convertkit_api_key]').prop('disabled',true);
					jQuery(this).prop('disabled',false);
					jQuery(this).addClass('btn_red');
					jQuery(this).removeClass('btn_green');
					jQuery(this).html('Disconnect');
					jQuery(this).attr('id','btn_drip_disconnect');
					
				}else{					
					jQuery('.hidden_tr_message_dp td').html('Error While Connecting..');
					jQuery('.hidden_tr_message_dp td').css('color','red');
					jQuery('.hidden_tr_message_dp').show();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Connect Again');
					
				}
			},
			error: function(error) {
				console.log(error);
			}
		 });
	}else{
		jQuery('input[name=drip_api_key]').focus();
		jQuery('input[name=drip_api_key]').addClass('blank_field');
	}		
	});
	jQuery(document).on('click','#btn_drip_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_drip');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					jQuery('.hidden_tr_message_dp td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_dp td').css('color','red');
					jQuery('input[name=drip_api_key]').val('');
					jQuery('input[name=drip_api_key]').prop('disabled',false);
					jQuery('input[name=drip_api_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_drip_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/* over drip */
	
	/* convertkit */
	jQuery(document).on('click','#btn_convertkit_connect',function(){
		var apikey = jQuery('input[name=convertkit_api_key]').val();
		if(apikey){
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'convertkit_connect');
			form_data.append('key', apikey);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {	

				if(data == '1'){
					jQuery('.hidden_tr_message_ck td').html('Account Connected..');
					jQuery('.hidden_tr_message_ck td').css('color','green');
					jQuery('.hidden_tr_message_ck').show();
					jQuery(this).html('Connected');
					
					///////////
					jQuery('input[name=convertkit_api_key]').val(apikey);
					jQuery('input[name=convertkit_api_key]').prop('disabled',true);
					jQuery(this).prop('disabled',false);
					jQuery(this).addClass('btn_red');
					jQuery(this).removeClass('btn_green');
					jQuery(this).html('Disconnect');
					jQuery(this).attr('id','btn_convertkit_disconnect');
					
				}else if(data == '2'){
					jQuery('.hidden_tr_message_ck td').html('You have to create at least one tag in your ConvertKit account');
					jQuery('.hidden_tr_message_ck td').css('color','red');
					jQuery('.hidden_tr_message_ck').show();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Connect Again');
				}else{
					jQuery('.hidden_tr_message_ck td').html('Error While Connecting..');
					jQuery('.hidden_tr_message_ck td').css('color','red');
					jQuery('.hidden_tr_message_ck').show();
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Connect Again');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});			
	}else{
		jQuery('input[name=convertkit_api_key]').focus();
		jQuery('input[name=convertkit_api_key]').addClass('blank_field');
	}
	});
	jQuery(document).on('click','#btn_convertkit_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_convertkit');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					//alert(data);
					//window.location = window.location;
					jQuery('.hidden_tr_message_ck td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_ck td').css('color','red');
					jQuery('input[name=convertkit_api_key]').val('');
					jQuery('input[name=convertkit_api_key]').prop('disabled',false);
					jQuery('input[name=convertkit_api_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_convertkit_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	/* over convert kit */
	jQuery(document).on('click','#btn_mailchimp_connect',function(){
		var key = jQuery('input[name=mailchimp_key]').val();
		if(key){
			jQuery('input[name=mailchimp_key]').removeClass('blank_field');
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'connect_mailchimp');
			form_data.append('key', key);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					if(data){
						if(data == '1'){
							// 4afd1067caae10e72850b267ec749d23-us4
							jQuery('.hidden_tr_message td').html('Account Connected..');
							jQuery('.hidden_tr_message td').css('color','green');
							jQuery('.hidden_tr_message').show();
							jQuery(this).html('Connected');
							
							///////////
							jQuery('input[name=mailchimp_key]').val(key);
							jQuery('input[name=mailchimp_key]').prop('disabled',true);
							jQuery(this).prop('disabled',false);
							jQuery(this).addClass('btn_red');
							jQuery(this).removeClass('btn_green');
							jQuery(this).html('Disconnect');
							jQuery(this).attr('id','btn_mailchimp_disconnect');
							
						}else{
							jQuery('.hidden_tr_message td').html('Error While Connecting..');
							jQuery('.hidden_tr_message td').css('color','red');
							jQuery('.hidden_tr_message').show();
							jQuery(this).prop('disabled',false);
							jQuery(this).html('Connect Again');
						}
						// jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=mailchimp_key]').focus();
			jQuery('input[name=mailchimp_key]').addClass('blank_field');
		}
	});
	jQuery(document).on('click','#btn_mailchimp_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_mailchimp');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					//alert(data);
					//window.location = window.location;
					jQuery('.hidden_tr_message td').html('Account Disconnected..');
					jQuery('.hidden_tr_message td').css('color','red');
					jQuery('input[name=mailchimp_key]').val('');
					jQuery('input[name=mailchimp_key]').prop('disabled',false);
					jQuery('input[name=mailchimp_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_mailchimp_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click','#btn_mailsqaud_connect',function(){
		var key = jQuery('input[name=mailsqaud_key]').val();
		var mailsqaud_login = jQuery('input[name=mailsqaud_login]').val();
		if(key && mailsqaud_login){
			jQuery('input[name=mailsqaud_key]').removeClass('blank_field');
			jQuery('input[name=mailsqaud_login]').removeClass('blank_field');
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'connect_mailsqaud');
			form_data.append('key', key);
			form_data.append('mailsqaud_login', mailsqaud_login);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					if(data){
						if(data == '1'){
							// 4afd1067caae10e72850b267ec749d23-us4
							jQuery('.hidden_tr_message_ms td').html('Account Connected..');
							jQuery('.hidden_tr_message_ms td').css('color','green');
							jQuery('.hidden_tr_message_ms').show();
							jQuery(this).html('Connected');
							
							///////////
							jQuery('input[name=mailsqaud_key]').val(key);
							jQuery('input[name=mailsqaud_login]').val(mailsqaud_login);
							jQuery('input[name=mailsqaud_key]').prop('disabled',true);
							jQuery('input[name=mailsqaud_login]').prop('disabled',true);
							jQuery(this).prop('disabled',false);
							jQuery(this).addClass('btn_red');
							jQuery(this).removeClass('btn_green');
							jQuery(this).html('Disconnect');
							jQuery(this).attr('id','btn_mailsqaud_disconnect');
							
						}else{
							jQuery('.hidden_tr_message_ms td').html('Error While Connecting..');
							jQuery('.hidden_tr_message_ms td').css('color','red');
							jQuery('.hidden_tr_message_ms').show();
							jQuery(this).prop('disabled',false);
							jQuery(this).html('Connect Again');
						}
						// jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=mailsqaud_key]').focus();
			jQuery('input[name=mailsqaud_key]').addClass('blank_field');
			jQuery('input[name=mailsqaud_login]').addClass('blank_field');
		}
	});
	jQuery(document).on('click','#btn_mailsqaud_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_mailsqaud');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					//alert(data);
					//window.location = window.location;
					jQuery('.hidden_tr_message_ms td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_ms td').css('color','red');
					jQuery('input[name=mailsqaud_key]').val('');
					jQuery('input[name=mailsqaud_login]').val('');
					jQuery('input[name=mailsqaud_key]').prop('disabled',false);
					jQuery('input[name=mailsqaud_login]').prop('disabled',false);
					jQuery('input[name=mailsqaud_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_mailsqaud_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click','#btn_getresponse_connect',function(){
		var key = jQuery('input[name=getresponse_key]').val();
		if(key){
			jQuery('input[name=getresponse_key]').removeClass('blank_field');
			jQuery(this).prop('disabled',true);
			jQuery(this).html('Connecting..');
			var form_data = new FormData();
			form_data.append('action', 'connect_getresponse');
			form_data.append('key', key);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context:this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					// alert(data);
					if(data){
						if(data == '1'){
							// 4afd1067caae10e72850b267ec749d23-us4
							jQuery('.hidden_tr_message_gr td').html('Account Connected..');
							jQuery('.hidden_tr_message_gr td').css('color','green');
							jQuery('.hidden_tr_message_gr').show();
							jQuery(this).html('Connected');
							
							///////////
							jQuery('input[name=getresponse_key]').val(key);
							jQuery('input[name=getresponse_key]').prop('disabled',true);
							jQuery(this).prop('disabled',false);
							jQuery(this).addClass('btn_red');
							jQuery(this).removeClass('btn_green');
							jQuery(this).html('Disconnect');
							jQuery(this).attr('id','btn_getresponse_disconnect');
							
						}else{
							jQuery('.hidden_tr_message_gr td').html('Error While Connecting..');
							jQuery('.hidden_tr_message_gr td').css('color','red');
							jQuery('.hidden_tr_message_gr').show();
							jQuery(this).prop('disabled',false);
							jQuery(this).html('Connect Again');
						}
						// jQuery(this).prop('disabled',false);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('input[name=getresponse_key]').focus();
			jQuery('input[name=getresponse_key]').addClass('blank_field');
		}
	});
	jQuery(document).on('click','#btn_getresponse_disconnect',function(){
		jQuery(this).prop('disabled',true);
		jQuery(this).html('Disconnecting..');
		var form_data = new FormData();
		form_data.append('action', 'disconnect_getresponse');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			context:this,
			data: form_data,
			type: 'POST',
			success: function(data) {
				if(data){
					//alert(data);
					//window.location = window.location;
					jQuery('.hidden_tr_message_gr td').html('Account Disconnected..');
					jQuery('.hidden_tr_message_gr td').css('color','red');
					jQuery('input[name=getresponse_key]').val('');
					jQuery('input[name=getresponse_key]').prop('disabled',false);
					jQuery('input[name=getresponse_key]').focus();
					jQuery(this).prop('disabled',false);
					jQuery(this).removeClass('btn_red');
					jQuery(this).addClass('btn_green');
					jQuery(this).html('Connect');
					jQuery(this).attr('id','btn_getresponse_connect');
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('click','.select_funnel_template',function(){
		var template_id = jQuery(this).attr('data');
		if(template_id){
			jQuery('input[name=funnel_template_type]').val(template_id);
			jQuery('.template_sel_parent').css('display','none');
			jQuery('#load_more_div').css('display','none');
			jQuery('#template_search_container').css('display','none');
			jQuery('.frm_funnel_create').css('display','block');
		}
	});
	jQuery(document).on('click','.edit_funnel_pages',function(){
		var post_id = jQuery(this).attr('data');
		// alert(post_id);
		if(post_id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'get_edit_funnel_pages');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					/*if(data){
						window.location = window.location;
					}*/
					if(data){
						jQuery(this).html("Edit Pages");
						jQuery('#edit_funnel_pages_container').html(data);
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#edit_funnel_pages',
								onComplete : add_close_to_custombox('#edit_funnel_pages'),
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click','.btn_add_scripts',function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'get_scripts_funnel_pages');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					// alert(data);
					//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
					/*if(data){
						window.location = window.location;
					}*/
					if(data){
						jQuery(this).html("Add/Edit Scripts");
						Custombox.modal.close();
						jQuery('#funnel_pages_scripts_modal_container').html(data);
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#funnel_pages_scripts_modal',
								onComplete : add_close_to_custombox('#funnel_pages_scripts_modal'),
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.save_update_script', function(){
		var post_id = jQuery('input[name=post_id_scripts]').val();
		var header_scripts = jQuery('textarea[name=header_scripts_ta]').val();
		var footer_scripts = jQuery('textarea[name=footer_scripts_ta]').val();
		var header_scripts_loc = jQuery('select[name=location_page_headerscripts]').val();
		var footer_scripts_loc = jQuery('select[name=location_page_footerscripts]').val();
		// alert(header_scripts_loc);
		// alert(footer_scripts_loc);
		if(header_scripts == ''){
			// alert("g");
			jQuery('textarea[name=header_scripts_ta]').css('border','2px solid red !important;');
		}
		if(footer_scripts == ''){
			jQuery('textarea[name=footer_scripts_ta]').css('border','2px solid red !important;');
		}
		if(post_id && (header_scripts || footer_scripts)){
			jQuery(this).html("Updating...");
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('header_scripts', header_scripts);
			form_data.append('header_scripts_loc', header_scripts_loc);
			form_data.append('footer_scripts', footer_scripts);
			form_data.append('footer_scripts_loc', footer_scripts_loc);
			form_data.append('action', 'save_funnel_scripts');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					if(data){
						jQuery(this).html("Updated..");
						// alert(data);
						/*Custombox.modal.close();
						jQuery('#funnel_pages_scripts_modal_container').html(data);
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#funnel_pages_scripts_modal',
							}	
						}).open();*/
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#publish_public_funnel', function(){
		var post_id = jQuery(this).attr('data');
		/* if(post_id){
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'check_mail_list_settings');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				context: this,
				type: 'POST',
				success: function(data) {
					if(data == '1'){
						// Custombox.modal.close();
						var form_data = new FormData();
						form_data.append('post_id', post_id);
						form_data.append('action', 'publish_public_funnel');
						jQuery.ajax({
							url: ajaxurl,
							cache: false,
							contentType: false,
							processData: false,
							data: form_data,
							type: 'POST',
							success: function(data) {
								Custombox.modal.close();
								jQuery('#public_funnel_setup_modal_container').html(data);
								new Custombox.modal({
									content: {
										effect: 'fadein',
										target: '#public_funnel_setup',
									}	
								}).open();
							},
							error: function(error) {
								console.log(error);
							}
						});
					}else{
						jQuery('input[name=email_list_post_id]').val(post_id);
						Custombox.modal.close();
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#email_list_settings_modal',
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		} */
		var post_id = jQuery(this).attr('data');
		if(post_id){
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('action', 'funnel_submission_confirmation');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					Custombox.modal.close();
					jQuery('#funnel_submission_process').html(data);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_submission_process',
							onComplete : add_close_to_custombox('#funnel_submission_process'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.add_promotional_tools', function(){
		var id = jQuery(this).attr('data');
		if(id){
			if(jQuery(this).hasClass('active')){
				jQuery(this).removeClass('active');
				jQuery('div#promotional_tools_div_'+id).slideUp();
			}else{
				jQuery(this).addClass('active');
				jQuery('div#promotional_tools_div_'+id).slideDown();
			}
		}
	});
	jQuery(document).on('click', '.add_promotional_tools_creator', function(){
		var id = jQuery(this).attr('data');
		if(id){
			if(jQuery(this).hasClass('active')){
				jQuery(this).removeClass('active');
				jQuery('div#creator_promotional_tools_div_'+id).slideUp();
			}else{
				jQuery(this).addClass('active');
				jQuery('div#creator_promotional_tools_div_'+id).slideDown();
			}
		}
	});
	jQuery(document).on('click', '.clicks_and_coversions', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html('Loading...');
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'show_funnel_clicks_and_coversions');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context: this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					jQuery(this).html('Clicks & Conversions');
					jQuery('#funnel_clicksconv_modal').html(data);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_clicksconv_modal',
							onComplete : add_close_to_custombox('#funnel_clicksconv_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.btn_marketplace', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html('Loading...');
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('type', 'marketplace_update');
			form_data.append('action', 'publish_public_funnel');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				context: this,
				data: form_data,
				type: 'POST',
				success: function(data) {
					jQuery(this).html('Marketplace');
					jQuery('#marketplace_modal').html(data);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#marketplace_modal',
							onComplete : add_close_to_custombox('#marketplace_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.remove_promotional_tool', function(){
		if (!confirm('Are you sure?')) return false;
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		// var funnel_id = jQuery(this).parent().parent().attr('id');
		var funnel_id = jQuery(this).closest("li").attr('post_id');
		var funnel_type = jQuery(this).closest("li").attr('data');
		// var result = funnel_id.split("_"); // split use to split string it's create array
		//alert(result[0]);
		/*if(funnel_type == 'email_swipe'){
			var funnel_type = result[0] +'_'+ result[1];
			var funnel_id = result[2];
		}else if(result[0] == 'fb'){
			var funnel_type = result[0] +'_'+ result[1];
			var funnel_id = result[2];
		}else if(result[0] == 'yt'){
			var funnel_type = result[0] +'_'+ result[1];
			var funnel_id = result[2];
		}else{
			var funnel_type = result[0];
			var funnel_id = result[1];
		}*/
		
		if(item_id){
			jQuery(this).next('img').show();
			jQuery(this).prop("disabled", true);
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('funnel_id', funnel_id);
			form_data.append('funnel_type', funnel_type);
			form_data.append('action', 'remove_promotional_tool');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					// alert(data);
					if(data == '1'){
						jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.update_email_swipe', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var e_sub = jQuery('input[name=email_swipe_content_subject_'+item_id+']').val()
			var texta = jQuery('textarea[name=email_swipe_content_'+item_id+']').val()
			// var funnel_id = jQuery(this).parent().parent().attr('id');
			var funnel_id = jQuery(this).closest("li").attr('post_id');
			//alert(funnel_id);
			// var result = funnel_id.split("_"); // split use to split string it's create array
			// var funnel_id = result[2];
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'email_swipe');
			form_data.append('funnel_id', funnel_id);
			form_data.append('data', texta);
			form_data.append('email_subject', e_sub);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.update_yt_vid', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		// var funnel_id = jQuery(this).parent().parent().attr('id');
		var funnel_id = jQuery(this).closest("li").attr('post_id');
		//alert(funnel_id);
		// var result = funnel_id.split("_"); // split use to split string it's create array
		// var funnel_id = result[2];
		if(item_id){
			jQuery('.funnel_loader_' + item_id).show();
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta = jQuery('input[name=yt_vid_content_'+item_id+']').val()
			var texta_desc = jQuery('textarea[name=yt_vid_content_desc_'+item_id+']').val()
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'yt_vid');
			form_data.append('data', texta);
			form_data.append('funnel_id', funnel_id);
			form_data.append('video_desc', texta_desc);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
					jQuery('.funnel_loader_' + item_id).hide();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.update_banner', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		/*var funnel_id = jQuery(this).parent().parent().attr('id');
		var result = funnel_id.split("_"); // split use to split string it's create array
		var funnel_id = result[1]; */
		var funnel_id = this.id; 
		
		if(item_id){
			jQuery('.update_banner_funnel_loader_' + item_id).show();
			//jQuery(this).next('img').show();
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta = jQuery('input[name=process_banner_image_'+item_id+']').val()
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('funnel_id', funnel_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'banner');
			form_data.append('data', texta);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
					jQuery(this).next().next('img').hide();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.update_fb_ads', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		// var funnel_id = jQuery(this).parent().parent().attr('id');
		var funnel_id = jQuery(this).closest("li").attr('post_id');
		// var result = funnel_id.split("_"); // split use to split string it's create array
		// var funnel_id = result[2];
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta_img = jQuery('input[name=process_fb_image_'+item_id+']').val()
			var texta = jQuery('textarea[name=facebook_content_'+item_id+']').val()
			//var landing_url = jQuery('input[name=facebook_landing_url_'+item_id+']').val()
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'fb_ads');
			form_data.append('funnel_id', funnel_id);
			form_data.append('image', texta_img);
			form_data.append('data', texta);
			//form_data.append('url', landing_url);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/******************/
	jQuery(document).on('click', '.update_twitter', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		// var funnel_id = jQuery(this).parent().parent().attr('id');
		var funnel_id = jQuery(this).closest("li").attr('post_id');
		// var result = funnel_id.split("_"); // split use to split string it's create array
		// var funnel_id = result[2];
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta_img = jQuery('input[name=process_twitter_image_'+item_id+']').val()
			//var landing_url = jQuery('input[name=twitter_landing_url_'+item_id+']').val()
			var texta = jQuery('textarea[name=twitter_content_'+item_id+']').val()
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'twitter');
			form_data.append('funnel_id', funnel_id);
			form_data.append('image', texta_img);
			//form_data.append('url', landing_url);
			form_data.append('data', texta);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	var maxLength = 200;
	jQuery(document).on('keyup', '.count_char', function(){
	//jQuery('.count_char').keyup(function() {
		//alert("sdfdsf");
		var textlen = maxLength - jQuery(this).val().length;
		jQuery('.chars').text(textlen);
		if(jQuery(this).val().length > maxLength){
			 var textvalue = jQuery(this).val();
			 var str = textvalue.slice(0, maxLength);
			 jQuery('.count_char').val(str);
		}
	});
	jQuery(document).on('click', '.update_linkedin', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		// var funnel_id = jQuery(this).parent().parent().attr('id');
		var funnel_id = jQuery(this).closest("li").attr('post_id');
		// var result = funnel_id.split("_"); // split use to split string it's create array
		// var funnel_id = result[2];
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta_img = jQuery('input[name=process_linkedin_image_'+item_id+']').val()
			//var landing_url = jQuery('input[name=linkedin_landing_url_'+item_id+']').val()
			var texta = jQuery('textarea[name=linkedin_content_'+item_id+']').val()
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'linkedin');
			form_data.append('funnel_id', funnel_id);
			form_data.append('image', texta_img);
			form_data.append('data', texta);
			//form_data.append('url', landing_url);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/******************/
	if (jQuery('.funnel_image').length == 0) { //set == 0 it's not working with > 0
		if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery(document).on('click', '.funnel_image', function(e){
				e.preventDefault();
				var button = jQuery(this);
				var id = button.prev();
				 var funnel_id = jQuery(this).attr('data');
				 var preview = jQuery('.preview_' + funnel_id);
				 var image_disp = jQuery(preview).addClass("funnel_image_disp_"+funnel_id);
				 wp.media.editor.send.attachment = function(props, attachment) {
					image_disp.attr('src',attachment.url);
					image_disp.show();
					jQuery('.img_err_msg_'+funnel_id).hide();
					id.val(attachment.id);
				};
				wp.media.editor.open(button);
				return false;
			});
		}
	}
	
	if (jQuery('.banner_image').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery(document).on('click', '.banner_image', function(e){
                e.preventDefault();
                var button = jQuery(this);
                var id = button.prev();
                var textid = button.prev().attr('data');
                var image_disp = jQuery('.banner_image_disp_'+textid);
                var html_preview = jQuery('.preview_html_'+textid+' textarea');
                var userid = jQuery('input[name=current_user_id_banner_'+textid+']').val();
				// console.log(button.parent.attr('post_id'));
				if(typeof(userid) == 'undefined'){
					var userid = jQuery('input[name=global_user_id]').val();
				}
                var post_id = jQuery('input[name=current_post_id_banner_'+textid+']').val();
				if(typeof(post_id) == 'undefined'){
					var post_id = jQuery(this).closest("li").attr('post_id');
				}
                wp.media.editor.send.attachment = function(props, attachment) {
					image_disp.attr('src',attachment.url);
					html_preview.html('<a href="https://funnelmates.com/f/'+post_id+'/'+userid+'/"><img src="'+attachment.url+'" /></a>');
					image_disp.show();
					jQuery('.img_err_msg_'+textid).hide();
                    id.val(attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
	if (jQuery('.fb_image').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery(document).on('click', '.fb_image', function(e){
                e.preventDefault();
                var button = jQuery(this);
                var id = button.prev();
                var textid = button.prev().attr('data');
				var post_id = jQuery('input[name=current_post_id_fb_'+textid+']').val();
				if(typeof(post_id) == 'undefined'){
					var post_id = jQuery(this).closest("li").attr('post_id');
				}
				var user_id = jQuery('input[name=current_user_id_fb_'+textid+']').val();
				if(typeof(user_id) == 'undefined'){
					var user_id = jQuery('input[name=global_user_id]').val();
				}
				var textarea_name = jQuery('textarea[name=facebook_content_'+textid+']');
				//var input_name = jQuery('input[name=facebook_landing_url_'+textid+']');
				// alert(textid);
                var image_disp = jQuery('.fb_image_disp_'+textid);
                wp.media.editor.send.attachment = function(props, attachment) {
					image_disp.attr('src',attachment.url);
					//textarea_name.html('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					//input_name.val('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					image_disp.show();
                    id.val(attachment.id);
					jQuery('.facebook_img_err_msg_'+textid).hide();
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
	if (jQuery('.twitter_image').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery(document).on('click', '.twitter_image', function(e){
                e.preventDefault();
                var button = jQuery(this);
                var id = button.prev();
                var textid = button.prev().attr('data');
				var post_id = jQuery('input[name=current_post_id_twitter_'+textid+']').val();
				if(typeof(post_id) == 'undefined'){
					var post_id = jQuery(this).closest("li").attr('post_id');
				}
				var user_id = jQuery('input[name=current_user_id_twitter_'+textid+']').val();
				if(typeof(user_id) == 'undefined'){
					var user_id = jQuery('input[name=global_user_id]').val();
				}
				var textarea_name = jQuery('textarea[name=twitter_content_'+textid+']');
				//var input_name = jQuery('input[name=twitter_landing_url_'+textid+']');

                var image_disp = jQuery('.twitter_image_disp_'+textid);
                wp.media.editor.send.attachment = function(props, attachment) {
					image_disp.attr('src',attachment.url);
					//textarea_name.html('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					//input_name.val('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					image_disp.show();
					jQuery('.tweet_img_err_msg_'+textid).hide();
                    id.val(attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
	if (jQuery('.linkedin_image').length > 0) {
        if ( typeof wp !== 'undefined' && wp.media && wp.media.editor) {
			jQuery(document).on('click', '.linkedin_image', function(e){
                e.preventDefault();
                var button = jQuery(this);
                var id = button.prev();
                var textid = button.prev().attr('data');
				var post_id = jQuery('input[name=current_post_id_linkedin_'+textid+']').val();
				if(typeof(post_id) == 'undefined'){
					var post_id = jQuery(this).closest("li").attr('post_id');
				}
				var user_id = jQuery('input[name=current_user_id_linkedin_'+textid+']').val();
				if(typeof(user_id) == 'undefined'){
					var user_id = jQuery('input[name=global_user_id]').val();
				}
				var textarea_name = jQuery('textarea[name=linkedin_content_'+textid+']');
				//var input_name = jQuery('input[name=linkedin_landing_url_'+textid+']');
				// alert(textid);
                var image_disp = jQuery('.linkedin_image_disp_'+textid);
                wp.media.editor.send.attachment = function(props, attachment) {
					image_disp.attr('src',attachment.url);
					//textarea_name.html('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					//input_name.val('https://funnelmates.com/f/'+post_id+'/'+user_id+'/');
					image_disp.show();
					jQuery('.linkedin_img_err_msg_'+textid).hide();
                    id.val(attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
	/*jQuery(document).on('click', '.followup_emails', function(){
		var post_id = jQuery(this).attr('data');
		if(post_id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_parent', post_id);
			form_data.append('action', 'get_followup_emails');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					if(data){
						jQuery(this).html("Followup Emails");
						jQuery('#followup_emails_container').html(data);
						new Custombox.modal({
							content: {
								effect: 'fadein',
								target: '#followup_emails_modal',
							}	
						}).open();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});*/
	jQuery(document).on('click', '.f_integrations_activator', function(){
		//alert("sadf");
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'funnel_integrations_activator');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery(this).html("Integrations");
					jQuery('#funnel_integrations_modal').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_integrations_modal',
							onComplete : add_close_to_custombox('#funnel_integrations_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}
	});
	jQuery(document).on('click', '.followup_emails', function(){
		var id = jQuery(this).attr('data');
		if(id){
			if(jQuery(this).hasClass('active')){
				jQuery(this).removeClass('active');
				jQuery('div#followup_emails_div_'+id).slideUp();
			}else{
				jQuery(this).addClass('active');
				jQuery('div#followup_emails_div_'+id).slideDown();
			}
		}
	});
	jQuery(document).on('click', '.funnel_links', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'generate_funnel_links');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery(this).html("My Funnel Link");
					jQuery('#funnel_links_modal').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_links_modal',
							onComplete : add_close_to_custombox('#funnel_links_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}
	});
	jQuery(document).on('click', '.delete_external_link', function(){
		if (!confirm('Are you sure?')) return false;
		var id = jQuery(this).attr('data');
		if(id){
			// var linktype = jQuery(this).data('linktype');
			//alert(id);
		    var form_data = new FormData();
			form_data.append('post_id', id);
			// form_data.append('linktype', linktype);
			form_data.append('action', 'delete_external_link_data');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					if(data){
						jQuery('.website_link_row_'+id).remove();					
					}
					// new Custombox.modal({
						// content: {
							// effect: 'fadein',
							// target: '#affiliate_requests_modal',
							// onComplete : add_close_to_custombox('#affiliate_requests_modal'),
						// }	
					// }).open(); 
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}		
	});
	jQuery(document).on('click', '.edit_external_link', function(){
		var id = jQuery(this).attr('data');
		if(id){
			// var linktype = jQuery(this).data('linktype');
			//alert(id);
		    var form_data = new FormData();
			form_data.append('post_id', id);
			// form_data.append('linktype', linktype);
			form_data.append('action', 'edit_external_link_data');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery('#affiliate_requests_modal').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#affiliate_requests_modal',
							onComplete : add_close_to_custombox('#affiliate_requests_modal'),
						}	
					}).open(); 
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}
	})
	
	/* */
	jQuery(document).on('click', '.whitelabel_edit_external_link', function(){
		var id = jQuery(this).attr('data');
		if(id){
		    var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'edit_whitelabel_external_link_data');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery('#whitelabel_update_ex_link').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#whitelabel_update_ex_link',
							onComplete : add_close_to_custombox('#whitelabel_update_ex_link'),
						}	
					}).open(); 
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}
	})
	/* */
	jQuery(document).on('click', '.affiliate_requests', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html('Loading...');
			var form_data = new FormData();
			form_data.append('post_id', id);
			// form_data.append('linktype', linktype);
			form_data.append('action', 'open_affiliate_requests_links');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery(this).html('Promotional Links');
					jQuery('#affiliate_requests_modal_links').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#affiliate_requests_modal_links',
							onComplete : add_close_to_custombox('#affiliate_requests_modal_links'),
						}	
					}).open(); 
				},
				error: function(error) {
					console.log(error);
				}
			}); 
			/*if(jQuery(this).hasClass('active')){
				jQuery(this).removeClass('active');
				jQuery('div#funnel_affiliate_requests_'+id).slideUp();
			}else{
				jQuery(this).addClass('active');
				jQuery('div#funnel_affiliate_requests_'+id).slideDown();
			}*/			
		}
	});
	jQuery(document).on('click', '.funnel_website_links', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'funnel_website_links');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery('#funnel_website_links_modal').html(data);
					jQuery(this).html("Website Links");
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_website_links_modal',
							onComplete : add_close_to_custombox('#funnel_website_links_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('change', 'input[name=affiliate_network]', function(){
		jQuery('label.warriorplus').addClass('halfopacity');
		jQuery('label.warriorplus').removeClass('affnetradiochecked');
		jQuery('div.warriorplus_aff_link_div').hide();
		jQuery('label.clickbank').addClass('halfopacity');
		jQuery('label.clickbank').removeClass('affnetradiochecked');
		jQuery('div.clickbank_aff_link_div').hide();
		jQuery('label.jvzoo').addClass('halfopacity');
		jQuery('label.jvzoo').removeClass('affnetradiochecked');
		jQuery('div.jvzoo_aff_link_div').hide();
		jQuery('label.external').addClass('halfopacity');
		jQuery('label.external').removeClass('affnetradiochecked');
		jQuery('div.external_aff_link_div').hide();
		
		jQuery('label.paykickstart').addClass('halfopacity');
		jQuery('label.paykickstart').removeClass('affnetradiochecked');
		jQuery('div.paykickstart_aff_link_div').hide();
		
		jQuery('label.'+jQuery(this).val()).removeClass('halfopacity');
		jQuery('label.'+jQuery(this).val()).addClass('affnetradiochecked');
		jQuery('div.'+jQuery(this).val()+'_aff_link_div').show();
		jQuery('input[name=affiliate_network_type]').val(jQuery(this).val());
	});
	/*jQuery(document).on('click', '.process_aff_link', function(){
		var post_id = jQuery(this).data('postid');
		var link_type = jQuery('input[name=link_type]').val();
		if(link_type == 'download_link'){
			var download_link = jQuery('input[name=download_link]').val();
			var download_name = jQuery('input[name=download_name]').val();
			if(download_link == ''){
				jQuery('input[name=download_link]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=download_link]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(download_name == ''){
				jQuery('input[name=download_name]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=download_name]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(post_id && download_link !='' && download_name !=''){
				jQuery(this).html('Processing..');
				var form_data = new FormData();
				form_data.append('funnel_id', post_id);
				form_data.append('download_link', download_link);
				form_data.append('download_name', download_name);
				form_data.append('link_type', link_type);
				form_data.append('action', 'submit_website_link');
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).html('Link Processed..');
							setTimeout(function() {   //calls click event after a certain time
							   Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
							}, 5000);
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}else if(link_type == 'affiliate_link'){
			// var affiliate_network_type = jQuery('input[name=affiliate_network_type]').val();
			var internal_name = jQuery('input[name=internal_name]').val();			
			var checked = jQuery('input[name=affiliate_network]:checked').val();
			var notes = '';
			if(checked == 'jvzoo'){
				var affiliate_link = jQuery('input[name=jvz_affiliate_link]').val();		
				var link_identifier = jQuery('input[name=jvz_link_identifier]').val();
				if(affiliate_link == ''){
					jQuery('input[name=jvz_affiliate_link]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=jvz_affiliate_link]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(link_identifier == ''){
					jQuery('input[name=jvz_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=jvz_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
				// var link_place = jQuery('select[name=link_place]').val();
			}else if(checked == 'clickbank'){
				var affiliate_link = jQuery('input[name=cb_vendor_id]').val();		
				var link_identifier = jQuery('input[name=cb_link_identifier]').val();
				if(affiliate_link == ''){
					jQuery('input[name=cb_vendor_id]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=cb_vendor_id]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(link_identifier == ''){
					jQuery('input[name=cb_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=cb_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
			}else if(checked == 'warriorplus'){
				var affiliate_link = '';		
				var link_identifier = '';
			}else if(checked == 'external'){
				var affiliate_link = jQuery('input[name=external_affiliate_link]').val();		
				var link_identifier = jQuery('input[name=external_link_identifier]').val();	
				var notes = jQuery('input[name=notes]').val();
				if(affiliate_link == ''){
					jQuery('input[name=external_affiliate_link]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=external_affiliate_link]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(link_identifier == ''){
					jQuery('input[name=external_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=external_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
			}
			if(typeof(checked) == 'undefined'){
			jQuery('.aff_network_container label').addClass('radioerror');
				setTimeout(function() {
					jQuery('.aff_network_container label').removeClass('radioerror');
				}, 500);
			}
			if(internal_name == ''){
				jQuery('input[name=internal_name]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=internal_name]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(post_id && affiliate_link && typeof(checked) != 'undefined' && internal_name && link_identifier){
				var form_data = new FormData();
				form_data.append('link_identifier', link_identifier);
				form_data.append('action', 'validate_link_identifier');
				jQuery(this).html('Processing..');
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).html('Processing..');
							var form_data = new FormData();
							form_data.append('funnel_id', post_id);
							form_data.append('affiliate_link', affiliate_link);
							form_data.append('affiliate_network', checked);
							form_data.append('internal_name', internal_name);
							form_data.append('link_identifier', link_identifier);
							form_data.append('link_type', link_type);
							form_data.append('notes', notes);
							form_data.append('action', 'submit_website_link');
							jQuery.ajax({
								url: ajaxurl,
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
								type: 'POST',
								context: this,
								success: function(data) {
									if(data == '1'){
										jQuery(this).html('Link Processed..');
										setTimeout(function() {   //calls click event after a certain time
										   Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
										}, 5000);
									}
								},
								error: function(error) {
									console.log(error);
								}
							});
						}else{
							jQuery(this).html('Process Link');
							jQuery('.identifier_error').show();
							setTimeout(function() {
								jQuery('.identifier_error').hide();
							}, 3000);
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
				/////////
			}
		}
	});*/
	jQuery(document).on('change', 'input[name=funnel_pricing_option_premium]', function() {
		if(this.checked) {
			// alert('premium_checked');
			jQuery('.premium_field_container').html("<input type='number' name='premium_price' min='1' style='margin:0;width:100%;' placeholder='Premium Price in $' required/>");
		}else{
			// alert('premium_unchecked');
			jQuery('.premium_field_container').html('');
		}
	});
	jQuery(document).on('change', 'input[name=funnel_pricing_option_premium_whitelabel]', function() {
		if(this.checked) {
			// alert('whitelabel_checked');
			jQuery('.whitelabel_field_container').html("<input type='number' name='whitelabel_price' min='1' style='margin:0;width:100%;' placeholder='Whitelabel Price in $' required/>");
		}else{
			// alert('whitelabel_unchecked');
			jQuery('.whitelabel_field_container').html("");
		}
	});
	jQuery(document).on('change', 'input[name=funnel_pricing_option_premium_exclusive]', function() {
		if(this.checked) {
			// alert('eclusive_checked');
			jQuery('.exclusive_field_container').html("<input type='number' name='exclusive_price' min='1' style='margin:0;width:100%;' placeholder='Exclusive Price in $' required/>");
		}else{
			// alert('eclusive_unchecked');
			jQuery('.exclusive_field_container').html("");
		}
	});
	jQuery(document).on('click', '.submit_review', function() {
		var fid = jQuery('input[name=funnel_id_review]').val();
		if(fid){
			var valueformoney_val = jQuery('input[name=valueformoney_val]').val();
			var welldesigned_val = jQuery('input[name=welldesigned_val]').val();
			var qualityofcontent_val = jQuery('input[name=qualityofcontent_val]').val();
			var funnelreview_text = jQuery('textarea[name=funnelreview_text]').val();
			if(valueformoney_val == 0 || welldesigned_val == 0 || qualityofcontent_val == 0 || funnelreview_text == ''){
				jQuery('p.review_error').html("Please fill required fields");
				jQuery('p.review_error').show();
				return;
			}
			jQuery('p.review_error').hide();
			var insert = 1;
			if(jQuery(this).html() == 'Submit Review'){
				jQuery(this).html("Submitting...");
				var insert = 1;
			}else if(jQuery(this).html() == 'Update Review'){
				jQuery(this).html("Updating...");
				var insert = 0;
			}
			
			var form_data = new FormData();
			form_data.append('funnel_id', fid);
			form_data.append('valueformoney', valueformoney_val);
			form_data.append('welldesigned', welldesigned_val);
			form_data.append('qualityofcontent', qualityofcontent_val);
			form_data.append('funnelreview_text', funnelreview_text);
			form_data.append('insert', insert);
			form_data.append('action', 'submit_funnel_review');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == 1){
						if(insert == 1){
							jQuery(this).html("Review Submitted Successfully..");
						}else if(insert == 0){
							jQuery(this).html("Review Updated Successfully..");
						}
						jQuery(this).addClass("btn-success");
						jQuery(this).removeClass("btn-info");
						setTimeout(function() {
							// alert("here");
							jQuery('.submit_review').addClass("btn-info");
							jQuery('.submit_review').removeClass("btn-success");
							jQuery('.submit_review').html("Update Review");
						}, 2000);
					}else{
						jQuery(this).html("Error");
						jQuery(this).addClass("btn-danger");
						jQuery(this).removeClass("btn-info");
						setTimeout(function() {
							jQuery('.submit_review').addClass("btn-info");
							jQuery('.submit_review').removeClass("btn-danger");
							jQuery('.submit_review').html("Submit Review");
						}, 2000);
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '.funnel_review', function() {
		var id = jQuery(this).attr('data');
		//alert("funnel_review");
		if(id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'open_funnel_review_modal');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery('#funnel_review_modal').html(data);
					jQuery(this).html("Rate & Review");
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#funnel_review_modal',
							onComplete : add_close_to_custombox('#funnel_review_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	jQuery(document).on('change', 'input[name=active_funnel]', function() {
		var funnel_id = jQuery(this).attr("data");
		var c_user_id = jQuery('input[name=current_user_id]').val();		
		var funnel_status = '';
		if(this.checked) {
			var funnel_status = 'active';
		}else{
			var funnel_status = 'inactive';
		}
		var form_data = new FormData();
		form_data.append('funnel_status', funnel_status);
		form_data.append('funnel_id', funnel_id);
		form_data.append('c_user_id', c_user_id);
		form_data.append('action', 'active_funnel_status');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context:this,
			success: function(data) {
				//alert(data);
				if(data == 2){
					//alert("show");
					jQuery('a#dlt_inactive_funnel').show();
					jQuery('a#dlt_active_funnel').show();
				}else{
					//alert("hide");
					jQuery('a#dlt_inactive_funnel').hide();
					jQuery('a#dlt_active_funnel').hide();
				}
			},
		});
	});
	
	jQuery(document).on('click','.delete_inactive_funnel', function(){
		var funnel_id = jQuery(this).attr("data");
		var c_user_id = jQuery('input[name=current_user_id]').val();
		var form_data = new FormData();
		form_data.append('funnel_id',funnel_id);
		form_data.append('c_user_id',c_user_id);
		form_data.append('action','delete_inactive_funnel');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context:this,
			success: function(data) {
				if(data){
					jQuery(this).prop('disabled',false);
					jQuery(this).html('Deleted');
					jQuery('.funnel_search_'+funnel_id).hide();
				}
			},
		});
	});
	jQuery(document).on('mouseenter','.valueformoney', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('selected');
			jQuery(this).prevAll().andSelf().addClass('fas');
		}
	});
	jQuery(document).on('mouseleave','.valueformoney', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('fas');
			var rating = jQuery('input[name=valueformoney_val]').val();
			jQuery('span.valueformoney[data-rating='+rating+']').prevAll().andSelf().addClass('fas selected');
		}
	});
	jQuery(document).on('click','.valueformoney', function() {
	   jQuery(this).prevAll().andSelf().addClass('selected'); // Adds the selected class to just the one you clicked
	   jQuery(this).nextAll().removeClass('fas selected'); // Adds the selected class to just the one you clicked

	   var rating = jQuery(this).data('rating');
	   jQuery('input[name=valueformoney_val]').val(rating); // Set the value of the hidden rating form element
	});
	
	jQuery(document).on('mouseenter','.welldesigned', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('selected');
			jQuery(this).prevAll().andSelf().addClass('fas');
		}
	});
	jQuery(document).on('mouseleave','.welldesigned', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('fas');
			var rating = jQuery('input[name=welldesigned_val]').val();
			jQuery('span.welldesigned[data-rating='+rating+']').prevAll().andSelf().addClass('fas selected');
		}
	});
	jQuery(document).on('click','.welldesigned', function() {
	   jQuery(this).prevAll().andSelf().addClass('selected'); // Adds the selected class to just the one you clicked
	   jQuery(this).nextAll().removeClass('fas selected'); // Adds the selected class to just the one you clicked

	   var rating = jQuery(this).data('rating');
	   jQuery('input[name=welldesigned_val]').val(rating); // Set the value of the hidden rating form element
	});
	
	jQuery(document).on('mouseenter','.qualityofcontent', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('selected');
			jQuery(this).prevAll().andSelf().addClass('fas');
		}
	});
	jQuery(document).on('mouseleave','.qualityofcontent', function() {
		if(jQuery(this).hasClass("selected") == false){
			jQuery(this).prevAll().andSelf().removeClass('fas');
			var rating = jQuery('input[name=qualityofcontent_val]').val();
			jQuery('span.qualityofcontent[data-rating='+rating+']').prevAll().andSelf().addClass('fas selected');
		}
	});
	jQuery(document).on('click','.qualityofcontent', function() {
	   jQuery(this).prevAll().andSelf().addClass('selected'); // Adds the selected class to just the one you clicked
	   jQuery(this).nextAll().removeClass('fas selected'); // Adds the selected class to just the one you clicked

	   var rating = jQuery(this).data('rating');
	   jQuery('input[name=qualityofcontent_val]').val(rating); // Set the value of the hidden rating form element
	});
	
	jQuery(document).on('change','#dropdown', function() {
		  var selectedText = jQuery(this).find("option:selected").text();
          var selectedValue = jQuery(this).val();
		  jQuery('.process_aff_link').show();
		  // alert(selectedValue);
          if(selectedValue == 'email'){
			  jQuery("input[name=link_type]").val("affiliate_link");
			  jQuery('.dropdown_div').hide();
			  jQuery('.linkplace_div').show();
			  jQuery('#external_link_div').hide();
		  }else if(selectedValue == 'edit_external_link'){
			  jQuery('#external_link_div').show();
			  jQuery('.process_aff_link').hide();
			  jQuery('.linkplace_div').hide();
			  jQuery('.dropdown_div').hide();
		  }else{
			  jQuery("input[name=link_type]").val("download_link");
			  jQuery('.dropdown_div').show();
			  jQuery('.linkplace_div').hide();
			  jQuery('#external_link_div').hide();
		  }
	});
	
	jQuery(document).on('click','#search', function() {
		var funnel_q = jQuery('input[name=search_funnel]').val();
		if(jQuery.trim(funnel_q) == ''){
			jQuery('#error_msg').html("<span id='alertFadeOut' style='color:red; font-size:14px; padding:5px 10px;'>Please type some text in search field<span>");
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}else{
			jQuery('#error_msg').html("");
		}
		var form_data = new FormData();
		form_data.append('funnel_q', funnel_q);
		form_data.append('action','search_funnel');
		jQuery('#load_funnel').show();
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				jQuery('.funnel_parent').hide();
				data = jQuery.parseJSON(data);
				jQuery.each(data, function(i, item) {
					jQuery('.funnel_search_'+item.id).show();
				});
				jQuery('#load_funnel').hide();
				jQuery('#check_ajax').val("search");
			},
			error: function(error){
				console.log(error);
			}
		});
	});
	
	jQuery(document).on('click', '.update_whitelabel_external_link', function(){
		var post_id = jQuery(this).data('postid');
		var ex_link = jQuery('input[name=external]').val();
		
		if(post_id != '' && ex_link != ''){
			jQuery(this).html('Processing..');
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('link', ex_link);
			form_data.append('action', 'update_whitelabel_external_link');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery(this).html('Link Processed..');
						setTimeout(function() {   //calls click event after a certain time
						   //Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
						   Custombox.modal.close();  // closeAll() function close all the custombox popup
						}, 5000);
						jQuery('#whitelabel_edit_' + post_id).text('Updated');
						jQuery('#whitelabel_link_' + post_id).text(ex_link);
						jQuery('#whitelabel_edit_' + post_id).removeClass('whitelabel_edit_external_link');
					}else if(data == '2'){
						jQuery('#error_msg_aff').html("that identifier has been taken, please choose again");
						jQuery('#error_msg_aff').fadeOut(10000, function () {
						  jQuery('#error_msg_aff').text('');
						});
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			jQuery('#error_msg_aff').html("<p style='color:red;'>Please add url</p>");
			jQuery('#error_msg_aff').fadeOut(10000, function () {
			  jQuery('#error_msg_aff').text('');
			});
		}
	});
	
	jQuery(document).on('click', '.update_aff_external_link', function(){
		var post_id = jQuery(this).data('postid');
		var linktype = jQuery('input[name=linktype]').val();
		if(linktype == 'warriorplus'){
			var link = jQuery('select[name=wp_offer_update] option:selected').val();
		}else{
			var link = jQuery('input[name=afflink]').val();
		}
		var link_name = jQuery('input[name=affname]').val();
		var edit_notes = jQuery('input[name=edit_notes]').val();
		if(link == ''){
			jQuery('input[name=afflink]').addClass('affiliate_link_error');
			setTimeout(function() {
				jQuery('input[name=afflink]').removeClass('affiliate_link_error');
			}, 1000);
		}
		if(link_name == ''){
			jQuery('input[name=affname]').addClass('affiliate_link_error');
			setTimeout(function() {
				jQuery('input[name=affname]').removeClass('affiliate_link_error');
			}, 1000);
		}
		
		if(link != '' && link_name != ''){
			jQuery(this).html('Processing..');
			var form_data = new FormData();
			form_data.append('post_id', post_id);
			form_data.append('link', link);
			form_data.append('link_name', link_name);
			form_data.append('edit_notes', edit_notes);
			form_data.append('action', 'update_aff_external_link');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery(this).html('Link Processed..');
						setTimeout(function() {   //calls click event after a certain time
						   //Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
						   Custombox.modal.close();  // closeAll() function close all the custombox popup
						}, 5000);
						jQuery('#link_name_' + post_id).html(link_name);
						jQuery('#link_' + post_id).html(link);
					}else if(data == '2'){
						jQuery('#error_msg_aff').html("that identifier has been taken, please choose again");
						jQuery('#error_msg_aff').fadeOut(10000, function () {
						  jQuery('#error_msg_aff').text('');
						});
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	jQuery(document).on('click', '.update_article', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta = jQuery('textarea[name=article_swipe_content_'+item_id+']').val();
			// var funnel_id = jQuery(this).parent().parent().attr('id');
			var funnel_id = jQuery(this).closest("li").attr('post_id');
			//alert(funnel_id);
			// var result = funnel_id.split("_"); // split use to split string it's create array
			// var funnel_id = result[2];
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'article');
			form_data.append('funnel_id', funnel_id);
			form_data.append('data', texta);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		} 
	});
	jQuery(document).on('click', '.update_misc', function(){
		var item_id = jQuery(this).parent().parent().parent().attr('item_id');
		var item_id_live = jQuery(this).parent().parent().attr('item_id');
		if(item_id){
			item_id = item_id;
		}else if(item_id_live){
			item_id = item_id_live;
		}
		if(item_id){
			jQuery(this).next().next('img').show();
			jQuery(this).prop("disabled", true);
			var texta = jQuery('textarea[name=misc_swipe_content_'+item_id+']').val();
			// var funnel_id = jQuery(this).parent().parent().attr('id');
			var funnel_id = jQuery(this).closest("li").attr('post_id');
			//alert(funnel_id);
			// var result = funnel_id.split("_"); // split use to split string it's create array
			// var funnel_id = result[2];
			var form_data = new FormData();
			form_data.append('item_id', item_id);
			form_data.append('action', 'update_promotional_tools');
			form_data.append('item_type', 'misc');
			form_data.append('funnel_id', funnel_id);
			form_data.append('data', texta);
			form_data.append('action_type', 'update_content');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context:this,
				success: function(data) {
					jQuery(this).next().next('img').hide();
					jQuery(this).prop("disabled", false);
					// alert(data);
					// if(data == '1'){
						// jQuery('li.after_drop[item_id="'+item_id+'"]').remove();
					// }
				},
				error: function(error) {
					console.log(error);
				}
			});
		} 
	});
	
	
	jQuery(document).on('click','.clone_funnel', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html("Loading...");
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'clone_box');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery('#clone_funnel_links_modal').html(data);
					jQuery(this).html("Clone Funnel");
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#clone_funnel_links_modal',
							onComplete : add_close_to_custombox('#funnel_website_links_modal'),
						}	
					}).open();
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	jQuery(document).on('click','.create_clone_funnel', function(){
		var main_funnel_id = jQuery(this).attr('data-funnelid');
		var clone_name = jQuery('input[name=clone_funnel_name]').val();
		if(clone_name == ''){
			jQuery('#clone_error_msg').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>Please Enter Name For Funnel<span>");
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}
		
		if(clone_name){
			jQuery(this).html("Creating...");
			var form_data = new FormData();
			form_data.append('funnel_name', clone_name);
			form_data.append('main_funnel', main_funnel_id);
			form_data.append('action', 'clone_funnel');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery(this).html("Created");				
					setTimeout(function() {   //calls click event after a certain time
					   //Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
					   Custombox.modal.close();  // closeAll() function close all the custombox popup
					   window.location = window.location;
					}, 5000);		
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	jQuery(document).on('click','.dlt_tracking', function(){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to Delete",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes'
		}).then((result) => {
		  if (result.isConfirmed) {
			var tracking_id = jQuery(this).attr('data');
			var tracking_post_id = jQuery('.track_post_id').val();
			jQuery(".dlt_track_" + tracking_id).css({"color":"red"});
			var form_data = new FormData();
			form_data.append('tracking', tracking_id);
			form_data.append('tracking_post_id', tracking_post_id);
			form_data.append('action', 'delete_tracking_id');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery('.dlt_track_'+ tracking_id).parents('tr').fadeOut();	
						setTimeout(function() {   //calls click event after a certain time					   
						   Custombox.modal.close();  // closeAll() function close all the custombox popup
						}, 5000);
					}else{
						jQuery(".dlt_track_" + tracking_id).css({"color":"#000000"});
					}					
				},
			});
		 }
	  })
	});
	
	
	
	jQuery(document).on('click','.refresh_tracking', function(){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "You want to Refresh",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes'
		}).then((result) => {
			var tracking_id = jQuery(this).attr('data');
			var tracking_post_id = jQuery('.track_post_id').val();
			jQuery(".refresh_track_" + tracking_id).css({"color":"#FFFFFF"});
			var form_data = new FormData();
			form_data.append('tracking', tracking_id);
			form_data.append('tracking_post_id', tracking_post_id);
			form_data.append('action', 'refresh_tracking_id');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					if(data == 1){
						jQuery(".refresh_track_" + tracking_id).css({"color":"#000000"});
						jQuery(".rs_view_" + tracking_id).html("0"); 
						jQuery(".rs_email_" + tracking_id).html("0"); 
						jQuery(".rs_per_" + tracking_id).html("0%"); 				
						setTimeout(function() {   //calls click event after a certain time					   
						   Custombox.modal.close();  // closeAll() function close all the custombox popup
						}, 5000);	
					}else{
						jQuery(".refresh_track_" + tracking_id).css({"color":"#000000"});
					}					
				},
			});
		})
	});
	
});
/* over document.ready function */
function funnel_viewascustomer(post_id){
	if(post_id){
		if(jQuery('#viewascustomer'+post_id).is(':checked')){
			var checked = 1;
		}else{
			var checked = 0;
		}
		jQuery('#viewascustomer'+post_id).attr("disabled", true);
		var form_data = new FormData();
		form_data.append('funnel_id', post_id);
		form_data.append('checked', checked);
		form_data.append('action', 'funnel_viewascustomer');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				jQuery('#viewascustomer'+post_id).attr("disabled", false);
				jQuery('#funnel_btns_'+post_id).html(data);
				// alert(data);
				//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
				/*if(data){
					window.location = window.location;
				}*/
				/*if(data){
					jQuery('p#hide_after_unpublish').hide();
					jQuery('a#dlt_after_unpublish').show();
				}*/
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function unpublish_funnel(post_id){
	if(post_id){
		var form_data = new FormData();
		form_data.append('post_parent', post_id);
		form_data.append('action', 'unpublish_funnel');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				// alert(data);
				//jQuery('#publish_funnel_modal').html('<a href="'+data+'">Edit Download Page with Builder</a>');
				/*if(data){
					window.location = window.location;
				}*/
				if(data){
					jQuery('p#hide_after_unpublish').hide();
					jQuery('a#dlt_after_unpublish_' + post_id).show();
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function publish_funnel(post_id){
	/*alert("1");
	exit;*/
	Custombox.modal.closeAll();
	if(post_id){
		// jQuery('.publish_funnel_switch').hide();
		// jQuery('.funnel_loader').show();
		jQuery('input#publish_funnel_'+post_id).attr("disabled", true);
		// jQuery('input#publish_funnel_'+post_id+':checked + .slider').css("background-color", "#ccc");
		var form_data = new FormData();
		form_data.append('post_id', post_id);
		form_data.append('action', 'check_funnel_bundle');
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				// alert(data);
				jQuery('#publish_funnel_modal').html(data);
				jQuery('.funnel_error_note').fadeOut(10000, function () {
				  jQuery('.funnel_error_note').text('');
				});
				new Custombox.modal({
					content: {
						effect: 'fadein',
						target: '#publish_funnel_modal',
						onClose: function() {
							var form_data = new FormData();
							form_data.append('post_id', post_id);
							form_data.append('action', 'check_if_funnel_published');
							jQuery.ajax({
								url: ajaxurl,
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
								type: 'POST',
								success: function(data) {
									if(data == '0'){
										//jQuery('input[name=publish_funnel]').prop("checked", false);
										jQuery('#publish_funnel_' + post_id).prop("checked", false);
										jQuery('input#publish_funnel_'+post_id).attr("disabled", false);
									}
								},
								error: function(error) {
									console.log(error);
								}
							});
						},
					}	
				}).open();
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function public_funnel_settings_submit(){	
	var x = jQuery('form#form_public_funnel_settings').serializeArray(),dataObj = {};
	// console.log(x);
	var newArray = x.filter(function(v){return v!==''});
	jQuery.each(newArray, function(i, field){
		dataObj[field.name] = field.value;
	});
	var update_type = dataObj['funnel_update_type'];	
	var funnel_name = dataObj['funnel_name'];
	var post_id = dataObj['post_id'];
	var funnel_description = dataObj['funnel_description'];
	var funnel_category = dataObj['funnel_category'];
	var funnel_tags = dataObj['funnel_tags'];
	var funnel_image = dataObj['process_funnel_image_'];
	var funnel_pricing_option_free = dataObj['funnel_pricing_option_free'];
	var funnel_pricing_option_premium = dataObj['funnel_pricing_option_premium'];
	var funnel_pricing_option_premium_whitelabel = dataObj['funnel_pricing_option_premium_whitelabel'];
	var funnel_pricing_option_premium_exclusive = dataObj['funnel_pricing_option_premium_exclusive'];
	var funnel_is_coupon_code = dataObj['funnel_is_coupon_code'];
	var already_coupon = dataObj['already_coupon'];
	var aff_profit_system = dataObj['aff_profit_system'];
	
	var enable_coupon_premium = dataObj['enable_coupon_premium'];
	if(enable_coupon_premium == '1' && typeof(enable_coupon_premium) != 'undefined'){
		enable_coupon_premium = '1';
	}else{
		enable_coupon_premium = '0';
	}
	var enable_coupon_whitelabel = dataObj['enable_coupon_whitelabel'];
	if(enable_coupon_whitelabel == '1' && typeof(enable_coupon_whitelabel) != 'undefined'){
		enable_coupon_whitelabel = '1';
	}else{
		enable_coupon_whitelabel = '0';
	}
	var enable_coupon_exclusive = dataObj['enable_coupon_exclusive'];
	if(enable_coupon_exclusive == '1' && typeof(enable_coupon_exclusive) != 'undefined'){
		enable_coupon_exclusive = '1';
	}else{
		enable_coupon_exclusive = '0';
	}
	
	
	// alert(funnel_pricing_option_free);
	// alert(funnel_pricing_option_premium);
	// alert(funnel_pricing_option_premium_whitelabel);
	// alert(funnel_pricing_option_premium_exclusive);
	var pricing = [];
	var premium_price = '';
	var whitelabel_price = '';
	var exclusive_price = '';
	var typecount = 0;
	if(aff_profit_system == '1' && typeof(aff_profit_system) != 'undefined'){
		aff_profit_system = '1';
	}else{
		aff_profit_system = '0';
	}
	if(funnel_pricing_option_free == 'free' && typeof(funnel_pricing_option_free) != 'undefined'){
		pricing.push(funnel_pricing_option_free);
		typecount++;
	}
	if(funnel_pricing_option_premium == 'premium' && typeof(funnel_pricing_option_premium) != 'undefined'){
		pricing.push(funnel_pricing_option_premium);
		premium_price = dataObj['premium_price'];
		typecount++;
	}
	if(funnel_pricing_option_premium_whitelabel == 'premium_whitelabel' && typeof(funnel_pricing_option_premium_whitelabel) != 'undefined'){
		pricing.push(funnel_pricing_option_premium_whitelabel);
		whitelabel_price = dataObj['whitelabel_price'];
		typecount++;
	}
	if(funnel_pricing_option_premium_exclusive == 'premium_exclusive' && typeof(funnel_pricing_option_premium_exclusive) != 'undefined'){
		pricing.push(funnel_pricing_option_premium_exclusive);
		exclusive_price = dataObj['exclusive_price'];
		typecount++;
	}
	if(typecount == 0){
		// alert(typecount);
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Select atleast one type to publish the funnel.',
		})
		return false;
	}
	
	if(funnel_image == ''){
		Swal.fire({
		  icon: 'error',
		  title: 'Oops...',
		  text: 'Select Image for Funnel.',
		})
		return false;
	}
	/*alert(pricing);
	exit;*/
	var form_data = new FormData();
	form_data.append('action', 'submit_public_funnel_settings');
	form_data.append('post_id', post_id);
	form_data.append('funnel_name', funnel_name);
	form_data.append('funnel_description', funnel_description);
	form_data.append('funnel_category', funnel_category);
	form_data.append('funnel_tags', funnel_tags);
	form_data.append('funnel_image', funnel_image);
	form_data.append('pricing', pricing);
	form_data.append('premium_price', premium_price);
	form_data.append('whitelabel_price', whitelabel_price);
	form_data.append('exclusive_price', exclusive_price);
	form_data.append('aff_profit_system', aff_profit_system);
	if(funnel_is_coupon_code == "coupon_code" && typeof(funnel_is_coupon_code) != 'undefined'){
		form_data.append('is_coupon', "1");
		if(already_coupon != '' && typeof(funnel_is_coupon_code) != 'undefined'){
			form_data.append('coupon_action', "update");
			form_data.append('coupon_id', already_coupon);
		}
		form_data.append('coupon_code', dataObj['coupon_code']);
		form_data.append('coupon_type', dataObj['coupon_type']);
		form_data.append('coupon_value', dataObj['coupon_value']);
		form_data.append('enable_coupon_premium', enable_coupon_premium);
		form_data.append('enable_coupon_whitelabel', enable_coupon_whitelabel);
		form_data.append('enable_coupon_exclusive', enable_coupon_exclusive);
	}else if(already_coupon != '' && typeof(already_coupon) != 'undefined'){
		form_data.append('is_coupon', "1");
		form_data.append('coupon_action', "remove");
		form_data.append('coupon_id', already_coupon);
	}
	if(update_type == 'update'){
		Swal.fire({
		  title: 'Are you sure?',
		  text: "Changing marketplace settings will trigger a review, pausing sales of your funnel until approved.  Please allow 1-3 business days.",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Proceed!'
		}).then((result) => {
		  if (result.isConfirmed) {
			jQuery('form#form_public_funnel_settings input[type=submit]').prop("disabled",true);
			jQuery('form#form_public_funnel_settings input[type=submit]').val("Loading...");
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				success: function(data) {
					//alert(data);
					if(data){
						jQuery('#marketplace_modal').html("<h3 style='text-align:center;'>Thank You! Your funnel has been submitted for review. <a style='font-size: 20px;' href='https://funnelmates.com/wiki/funnel-review-process/'>Find out more about our review process here</a></h3>");
						// Custombox.modal.closeAll();
						// window.location = window.location;
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		  }
		})
	}else if(update_type == 'new'){
		jQuery('form#form_public_funnel_settings input[type=submit]').prop("disabled",true);
		jQuery('form#form_public_funnel_settings input[type=submit]').val("Loading...");
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			success: function(data) {
				//alert(data);
				if(data){
					jQuery('#public_funnel_setup_modal_container').html("<h3 style='text-align:center;'>Thank You! Your funnel has been submitted for review. <a style='font-size: 20px;' href='https://funnelmates.com/wiki/funnel-review-process/'>Find out more about our review process here</a></h3>");
					// Custombox.modal.closeAll();
					// window.location = window.location;
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}	
	return false;
}

// function test(){
	// alert("uio");
// }
jQuery(function() {
    reinitiate_sortable();
});
function reinitiate_sortable(){
	jQuery("ul.droptrue").sortable({
        connectWith: "ul",
        revert: false,
		helper: function (e, li) {
			copyHelper = li.clone().insertAfter(li);
			return li.clone();
		},
    });

    jQuery("ul.dropfalse").sortable({
		//alert("dfadsf");
        connectWith: "ul",
        revert: true,
        receive: function(event, ui) {
			//alert("fdfd");
            var formula = [];
            ui.item.attr('draggable', "false").removeClass('ui-state-default').removeClass('before_drop').addClass('ui-state-highlight').addClass('after_drop');
			var post_id = ui.item.attr("post_id");
			var div_id = ui.item.attr("data");
			//alert(div_id);
			if(post_id){
				ui.item.html('<img src="https://funnelmates.com/wp-content/themes/woffice-child-theme/images/funnel_loader.gif" style="opacity: 0.2;">');
				var form_data = new FormData();
				form_data.append('action', 'update_promotional_tools');
				form_data.append('item_type', div_id);
				form_data.append('post_id', post_id);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data){
							ui.item.html(jQuery('#'+div_id+"_"+post_id).html());
							if(div_id == 'email_swipe'){
								//alert(data);
								jQuery(ui.item).find( "textarea" ).attr("name","email_swipe_content_"+data);
								jQuery(ui.item).find( "input[name=email_swipe_content_subject_]" ).attr("name","email_swipe_content_subject_"+data);								
								// jQuery(ui.item).find( "input[name=email_hidden_post_id]" ).val(post_id);
								// jQuery("input[name=email_hidden_post_id]").val(post_id);
							}else if(div_id == 'yt_vid'){
								jQuery(ui.item).find( ".yt_vid_content" ).attr("name","yt_vid_content_"+data);
								//jQuery(ui.item).find( ".yt_vid_content_desc" ).attr("name","yt_vid_content_desc_"+data);
								jQuery(ui.item).find( "textarea" ).attr("name","yt_vid_content_desc_"+data);
							}else if(div_id == 'banner'){
								jQuery(ui.item).find( "#process_banner_image_" ).attr("name","process_banner_image_"+data);
								jQuery(ui.item).find( "#process_banner_image_" ).attr("id","process_banner_image_"+data);
								jQuery(ui.item).find( "#process_banner_image_"+ data ).attr("data",data);
								// jQuery("img.preview").removeAttr('class');
								// jQuery(ui.item).find( "img" ).addClass("preview");
								jQuery(ui.item).find( "img.preview" ).attr("src","");
								jQuery(ui.item).find( "img.preview" ).addClass("banner_image_disp_"+data);
							}else if(div_id == 'fb_ads'){						
								jQuery(ui.item).find( "#process_fb_image_" ).attr("name","process_fb_image_"+data);
								jQuery(ui.item).find( "#process_fb_image_" ).attr("id","process_fb_image_"+data);
								jQuery(ui.item).find( "#process_fb_image_" + data ).attr("data",data);
								// jQuery("img.fb_preview").removeAttr('class');
								// jQuery(ui.item).find( "img" ).addClass("fb_preview");
								jQuery(ui.item).find( "img.fb_preview" ).attr("src","");
								jQuery(ui.item).find( "img.fb_preview" ).addClass("fb_image_disp_"+data);
								
								jQuery(ui.item).find( "textarea" ).attr("name","facebook_content_"+data);								
								/*jQuery(ui.item).find( "#facebook_landing_url_" ).attr("id","facebook_landing_url_"+data);
								jQuery(ui.item).find( "#facebook_landing_url_"+data ).attr("name","facebook_landing_url_"+data);*/
							}else if(div_id == 'twitter'){
								jQuery(ui.item).find( "#process_twitter_image_" ).attr("name","process_twitter_image_"+data);
								jQuery(ui.item).find( "#process_twitter_image_" ).attr("id","process_twitter_image_"+data);
								jQuery(ui.item).find( "#process_twitter_image_" + data ).attr("data",data);
								// jQuery("img.fb_preview").removeAttr('class');
								// jQuery(ui.item).find( "img" ).addClass("fb_preview");
								jQuery(ui.item).find( "img.twitter_preview" ).attr("src","");
								jQuery(ui.item).find( "img.twitter_preview" ).addClass("twitter_image_disp_"+data);
								
								jQuery(ui.item).find( "textarea" ).attr("name","twitter_content_"+data);
								/*jQuery(ui.item).find( "#twitter_landing_url_" ).attr("id","twitter_landing_url_"+data);
								jQuery(ui.item).find( "#twitter_landing_url_"+data ).attr("name","twitter_landing_url_"+data);*/
							}else if(div_id == 'linkedin'){
								jQuery(ui.item).find( "#process_linkedin_image_" ).attr("name","process_linkedin_image_"+data);
								jQuery(ui.item).find( "#process_linkedin_image_" ).attr("id","process_linkedin_image_"+data);
								jQuery(ui.item).find( "#process_linkedin_image_" + data ).attr("data",data);
								// jQuery("img.fb_preview").removeAttr('class');
								// jQuery(ui.item).find( "img" ).addClass("fb_preview");
								jQuery(ui.item).find( "img.linkedin_preview" ).attr("src","");
								jQuery(ui.item).find( "img.linkedin_preview" ).addClass("linkedin_image_disp_"+data);
								
								jQuery(ui.item).find( "textarea" ).attr("name","linkedin_content_"+data);
								/*jQuery(ui.item).find( "#linkedin_landing_url_" ).attr("id","linkedin_landing_url_"+data);
								jQuery(ui.item).find( "#linkedin_landing_url_"+data ).attr("name","linkedin_landing_url_"+data);*/
							}else if(div_id == 'article'){
								jQuery(ui.item).find( "textarea" ).attr("name","article_swipe_content_"+data);
								// jQuery(ui.item).find( "input[name=article_hidden_post_id]" ).val(post_id);
								// jQuery("input[name=article_hidden_post_id]").val(post_id);
							}else if(div_id == 'misc'){
								jQuery(ui.item).find( "textarea" ).attr("name","misc_swipe_content_"+data);
								// jQuery(ui.item).find( "input[name=misc_hidden_item_id]" ).val(data);
								// jQuery(ui.item).find( "input[name=misc_hidden_post_id]" ).val(post_id);
								// jQuery("input[name=misc_hidden_item_id]").val(data);
								// jQuery("input[name=misc_hidden_post_id]").val(post_id);
							}
							ui.item.attr('item_id',data);
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
        },
		stop: function(event, ui) {
			// alert(post_id);
			var post_id = ui.item.attr("post_id");
			var imageids_arr = [];
			jQuery('ul.dropfalse_'+post_id+' li').each(function(){
			   var item_id = jQuery(this).attr('item_id');
			   imageids_arr.push(item_id);
			});
			var form_data = new FormData();
			form_data.append('action', 'update_promotional_tools_position');
			form_data.append('imageids', imageids_arr);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
    });
    jQuery("#sortable1, #sortable2").disableSelection();
} 
function affiliate_products(post_id){
	var form_data = new FormData();
	form_data.append('action', 'affiliate_products');
	form_data.append('post_id', post_id);
		jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		success: function(data) {
			// alert(data);			
			jQuery('#affiliate_products_model').html(data);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#affiliate_products_model',
					onComplete : add_close_to_custombox('#affiliate_products_model'),
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
}
function unlock_funnel(post_id){
	// alert(post_id);
	var form_data = new FormData();
	form_data.append('action', 'unlock_funnel');
	form_data.append('post_id', post_id);
	jQuery('#unlock_funnel_'+post_id).html('Processing...');
	jQuery('#unlock_funnel_'+post_id).prop('disabled',true);
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		type: 'POST',
		success: function(data) {
			// alert(data);
			Custombox.modal.closeAll();
			jQuery('#unlock_funnel_'+post_id).html('Unlock this Funnel');
			jQuery('#unlock_funnels_modal').html(data);
			jQuery('#unlock_funnel_'+post_id).prop('disabled',false);
			new Custombox.modal({
				content: {
					effect: 'fadein',
					target: '#unlock_funnels_modal',
					onComplete : add_close_to_custombox('#unlock_funnels_modal'),
				}	
			}).open();
		},
		error: function(error) {
			console.log(error);
		}
	});
}
function unlock_funnel_finalize(type,post_id,price,is_coupon = ''){
	//alert(type);
	//alert(post_id);
	//alert(price);
	
	var form_data = new FormData();
	form_data.append('action', 'unlock_funnel_finalize');
	form_data.append('type', type);
	form_data.append('post_id', post_id);
	form_data.append('price', price);
	form_data.append('is_coupon', is_coupon);
	jQuery('#unlock_funnel_'+type+'_'+post_id).prop('disabled',true);
	jQuery('#unlock_funnel_'+type+'_'+post_id).html('Checking..');
	jQuery.ajax({
		url: ajaxurl,
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,
		context: this,
		type: 'POST',
		success: function(data) {			
			jQuery('#unlock_funnel_'+type+'_'+post_id).html(data);
			setTimeout("Custombox.modal.closeAll()", 3000);
		},
		error: function(error) {
			console.log(error);
		}
	});
}
function add_macro_to_mail(macro,fid,elem){
	// var text = document.getElementsByName('email_swipe_content_'+fid)[0];
	var text= elem.parentElement.parentElement.nextSibling.nextSibling;
	if(text == null){
		text= elem.parentElement.nextSibling.nextSibling;
	}
	console.log(text);
	var result = macro.split('][');
	if(result.length > 1){
		var start = result[0]+']';
		var end = '['+result[1];
	}else{
		var start = macro;
		var end = '';
	}
	// if(fid == ''){
		// fid = jQuery('input[name=email_hidden_post_id]').val();
	// }
	// var txt = jQuery.trim(macro);
    // var box = jQuery('textarea[name=email_swipe_content_'+fid+']');
	insertAtCursor(text,start,end);
}
function insertAtCursor(myField, myValueBefore, myValueAfter) {
    if (document.selection) {
        myField.focus();
        document.selection.createRange().text = myValueBefore + document.selection.createRange().text + myValueAfter;
    } else if (myField.selectionStart || myField.selectionStart == '0') {
        var startPos = myField.selectionStart;
        var endPos = myField.selectionEnd;
        myField.value = myField.value.substring(0, startPos)+ myValueBefore+ myField.value.substring(startPos, endPos)+ myValueAfter+ myField.value.substring(endPos, myField.value.length);
		
		myField.focus();
		myField.selectionStart = startPos + myValueBefore.length;
		myField.selectionEnd = endPos + myValueBefore.length;
    } 
}

function add_macro_to_tools(macro,fid,elem){
	var text= elem.parentElement.nextSibling.nextSibling;
	if(text == null){
		text = elem.parentElement.nextSibling.nextSibling;
	}
	var result = macro.split('][');
	var start = result[0]+']';
	var end = '['+result[1];
    insertAtCursor(text,start,end);
}
function add_macro_to_article(macro,fid,elem){
	var text= elem.parentElement.parentElement.nextSibling.nextSibling;
	if(text == null){
		text= elem.parentElement.nextSibling.nextSibling;
	}
	var result = macro.split('][');
	var start = result[0]+']';
	var end = '['+result[1];
    insertAtCursor(text,start,end);
}
function add_macro_to_misc(macro,fid,elem){
	var text= elem.parentElement.parentElement.nextSibling.nextSibling;
	if(text == null){
		text= elem.parentElement.nextSibling.nextSibling;
	}
	var result = macro.split('][');
	var start = result[0]+']';
	var end = '['+result[1];
	insertAtCursor(text,start,end);
}

function openEmail(evt, emailName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent_email");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks_email");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(emailName).style.display = "block";
  evt.currentTarget.className += " active";
}
function findUrls(text){
    var source = (text || '').toString();
    var urlArray = [];
    var url;
    var matchArray;
    // Regular expression to find FTP, HTTP(S) and email URLs.
    var regexToken = /(((ftp|https?):\/\/)[\-\w@:%_\+.~#?,&\/\/=]+)|((mailto:)?[_.\w-]+@([\w][\w\-]+\.)+[a-zA-Z]{2,3})/g;

    // Iterate through any URLs in the text.
    while( (matchArray = regexToken.exec( source )) !== null ){
        var token = matchArray[0];
        urlArray.push( token );
    }
    return urlArray;
}
function show_actual_mail(editor){
	var content = myCustomGetContent(editor);
	if(content){
		jQuery(this).html("Loading...");
		jQuery(this).prop('disabled', true);
		var form_data = new FormData();
		form_data.append('action', 'show_followup_email');
		form_data.append('content', content);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				if(data){
					jQuery('#show_actual_mail_modal').html(data);
					jQuery(this).html("View Actual Mail");
					jQuery(this).prop('disabled', false);
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#show_actual_mail_modal',
							onComplete : add_close_to_custombox('#show_actual_mail_modal'),
						}	
					}).open();
				}
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function save_followup_email(editor,subject,button_id,post_id,type){
	var content = myCustomGetContent(editor);//tinymce.get(editor).getContent({format:'raw'});
	// alert(content);
	var contentlinks = findUrls(content);
	var subject = jQuery('input[name='+subject+']').val();
	// alert(subject);
	var checklink = false;
	if(contentlinks.length === 0){
		checklink = true;
	}else{
		Swal.fire({
			title: '<strong>Email Contains <u>Links</u></strong>',
			icon: 'error',
			html: '<p>Your email contains links to external websites that aren\'t set up properly in FunnelMates.</p>'+
					'<p>To add your link, click on the external link icon <img src="https://funnelmates.com/wp-content/themes/woffice-child-theme/adminjs/buttons/images/btn_external.png" />.</p>'+
					'<p><a href="https://funnelmates.com/wiki/adding-external-links/" target="_BLANK">Read more about external links here.</a></p>',
			focusConfirm: true,
			confirmButtonText: 'Go Back!',
			confirmButtonColor : '#f27474'
		});
		checklink = false;
	}
	if(content && checklink == true){
		jQuery('#'+button_id).html('Saving..');
		var form_data = new FormData();
		form_data.append('action', 'save_followup_email');
		form_data.append('content', content);
		form_data.append('subject', subject);
		form_data.append('post_id', post_id);
		form_data.append('type', type);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				// jQuery('#unlock_funnel_'+type+'_'+post_id).html(data);
				// setTimeout("Custombox.modal.closeAll()", 3000);
				jQuery('#'+button_id).html('Save Email');
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function myCustomGetContent( id ) {
	// Check if TinyMCE is defined or not.
	if( typeof tinymce != "undefined" ) {
		var editor = tinymce.get( id );
		// Check if TinyMCE is initialized properly or not.
		if( editor && editor instanceof tinymce.Editor ) {
			return editor.getContent();
		} else {
			// Fallback
			// If TinyMCE is not initialized then directly set the value in textarea.
			// TinyMCE will take up this value when it gets initialized.
			return jQuery( '#'+id ).val();
		}
	}
	return '';
}
function check_followup_email_template(fe_id){
	if(fe_id){
		var form_data = new FormData();
		form_data.append('action', 'get_followup_email');
		form_data.append('id', fe_id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				// jQuery('#unlock_funnel_'+type+'_'+post_id).html(data);
				// setTimeout("Custombox.modal.closeAll()", 3000);
				// jQuery('#'+button_id).html('Save Email');
				jQuery('#followup_email_template').html(data);
				new Custombox.modal({
					content: {
						effect: 'fadein',
						target: '#followup_email_template',
						onComplete : add_close_to_custombox('#followup_email_template'),
					}	
				}).open();
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function add_website_email_link(funnel_id){
	if(funnel_id){
		var form_data = new FormData();
		form_data.append('action', 'email_link_modal');
		form_data.append('id', funnel_id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				jQuery('#add_email_link_modal').html(data);
				new Custombox.modal({
					content: {
						effect: 'fadein',
						target: '#add_email_link_modal',
						onComplete : add_close_to_custombox('#add_email_link_modal'),
					}	
				}).open();
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function add_link_to_followup_email(linkid,funnelid){
	if(linkid && funnelid){
		alert(linkid);
		alert(funnelid);
	}
}

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}
function improve_funnel(post_id){
	if(post_id){
		// alert();
		jQuery('#improve_funnel_'+post_id).html("Loading...");
		var form_data = new FormData();
		form_data.append('action', 'user_improvising_funnel');
		form_data.append('post_id', post_id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				window.location = window.location;
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function cancel_funnel_review(post_id){
	if(post_id){
		// alert();
		jQuery('#cancel_funnel_review_'+post_id).html("Loading...");
		var form_data = new FormData();
		form_data.append('action', 'cancel_funnel_review');
		form_data.append('post_id', post_id);
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			context: this,
			type: 'POST',
			success: function(data) {			
				window.location = window.location;
			},
			error: function(error) {
				console.log(error);
			}
		});
	}
}
function add_close_to_custombox(id){
	jQuery(id).prepend('<a href="javascript:Custombox.modal.close();" style="position: absolute;right: 6px;top: 6px;color: red !important;font-weight: 600;"><img style="width:25px;" src="https://funnelmates.com/wp-content/uploads/2021/04/1200px-Flat_cross_icon.svg.png" /></a>');
}
function funnel_active_limit(){
	Custombox.modal.close();
	new Custombox.modal({
		content: {
			effect: 'fadein',
			target: '#funnel_activation_limit',
			onComplete : add_close_to_custombox('#funnel_activation_limit'),
		}	
	}).open();
}