jQuery(document).ready(function(){
	var max_fields = 5;
	var wrapper = jQuery(".custom_domain_container");
	var add_button = jQuery(".btn_add_custom_domain");
	
	jQuery(document).on('click', '.btn_add_custom_domain', function(e){
		e.preventDefault();
		var x = jQuery(".array_count").val();	
	
		if(x == 0){
			x = 1;
		}
		if (x < max_fields) {
			x++;
			jQuery(wrapper).append('<div style="margin-top: 15px;" class="input-container"><input placeholder="Enter Custom Domain" style="margin: 0;width: 90%;" type="text" name="custom_domain[]"/><span class="save_custom_domain" style="padding: 10px;color: green;font-size: 20px;"><i class="fa fa-check"></i></span><span class="delete_custom_domain" style="padding: 10px;color: red;font-size: 20px;"><i class="fa fa-times"></i></span><span class="btn_add_custom_domain" style="padding: 10px;color: green;font-size: 20px; display:none;" id="plus_'+ x +'"><i class="fa fa-plus"></i></span></div>'); //add input box
			jQuery('.btn_add_custom_domain').hide();
			jQuery('.array_count').val(x);
		} else {
			alert('You Reached the limits');
		}
	});
	jQuery(wrapper).on("click", ".save_custom_domain", function(e){
		e.preventDefault();
		var x = jQuery(".array_count").val();	
		if(x == 0){
			x = 1;
		}
		var custom_domain_elem = jQuery(this).siblings('input');
		var custom_domain = custom_domain_elem.val();
		var isvalid = isUrlValid(custom_domain);
		if(isvalid == true){
			var lastChar = custom_domain[custom_domain.length -1];
			if(lastChar == '/'){
				custom_domain = custom_domain.slice(0,-1);
			}
			var form_data = new FormData();
			form_data.append('custom_url', custom_domain);
			form_data.append('action', 'save_custom_url');
			jQuery(this).html('Saving....');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					//alert(data);
					jQuery(this).html('Save Custom Domain');
					if(data == '1'){
						custom_domain_elem.css("background-color",'#BBFFBB');
						setTimeout(function(){ (custom_domain_elem).css("background-color",'transparent') },2000)
						jQuery(this).remove();
						jQuery('#plus_' + x).show();
					}else if(data == '0'){
						custom_domain_elem.attr('placeholder','Duplicate Custom URL Found for other User');
						custom_domain_elem.css("background-color",'#FFBBBB');						
						setTimeout(function(){ (custom_domain_elem).css("background-color",'transparent') },2000)
						jQuery(this).html('<span class="save_custom_domain" style="padding: 10px;color: green;font-size: 20px;"><i class="fa fa-check"></i></span>');
					}else if(data == '2'){
						custom_domain_elem.css("background-color",'#FFBBBB');
						jQuery('#custom_error').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>This Domain name alrady used</span>")
						jQuery('#alertFadeOut').fadeOut(10000, function () {
						  jQuery('#alertFadeOut').text('');
						});
						setTimeout(function(){ (custom_domain_elem).css("background-color",'transparent') },2000)
						jQuery(this).html('<span class="save_custom_domain" style="padding: 10px;color: green;font-size: 20px;"><i class="fa fa-check"></i></span>');
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}else{
			custom_domain_elem.attr('placeholder','Invalid Custom URL. Either you are trying to save invalid URL or you should try it with http:// OR https://');
			custom_domain_elem.css("background-color",'#FFBBBB');
			/*jQuery('p#custom_domain_error').html('Invalid Custom URL. Either you are trying to save invalid URL or you should try it with http:// OR https://');
			jQuery('input[name="custom_domain"]').css('border-color','#F00');
			jQuery('p#custom_domain_error').css('color','#F00');
			jQuery('p#custom_domain_error').show();*/
			setTimeout(function(){ (custom_domain_elem).css("background-color",'transparent') },2000)
		}
	});
	jQuery(wrapper).on("click", ".delete_custom_domain", function(e) {
		e.preventDefault();
		var del_id = jQuery(this).val();
		jQuery(this).css('color','#000000');
		var custom_domain_elem = jQuery(this).siblings('input');
		var custom_domain = custom_domain_elem.val();
		var form_data = new FormData();
		form_data.append('delete_custom_domain', custom_domain);
		form_data.append('action', 'delete_custom_domain');
		jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery(this).parent('div').remove();
					location.reload();
				},
				error: function(error) {
					console.log(error);
				}
			});
		/*jQuery(this).parent('div').remove();
		x--;*/
	})
	jQuery(document).on('click', '.download_resource', function(){
		var id = jQuery(this).attr('data');
		if(id){
			jQuery(this).html('Loading...');
			var form_data = new FormData();
			form_data.append('post_id', id);
			form_data.append('action', 'open_download_resource');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery(this).html('Download Resources');
					jQuery('#download_resource_modal').html(data);					
					new Custombox.modal({
						content: {
							effect: 'fadein',
							target: '#download_resource_modal',
						}	
					}).open(); 
				},
				error: function(error) {
					console.log(error);
				}
			}); 
		}
	});
	jQuery(document).on('click', '#save_zapier_webhook', function(){
		var zapier_webhook = jQuery("input[name=zapier_webhook]").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		if(zapier_webhook == ''){
			jQuery("input[name=zapier_webhook]").addClass('blank_field');
		}else{
			if(jQuery("input[name=zapier_webhook]").hasClass('blank_field'))
				jQuery("input[name=zapier_webhook]").removeClass('blank_field');
			
			if(post_id && zapier_webhook){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'zapier');
				form_data.append('list', zapier_webhook);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('input[name=zapier_webhook]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Zapier Webhook");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_zapier_webhook");
						}else{
							jQuery('input[name=zapier_webhook]').prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#remove_zapier_webhook', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'zapier');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('input[name=zapier_webhook]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Zapier Webhook");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_zapier_webhook");
						jQuery('input[name=zapier_webhook]').val('').focus();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#everwebinar_save_webinar', function(){
		var selected_webinar = jQuery("select[name=everwebinar_id] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		if(selected_webinar == '0'){
			jQuery("select[name=everwebinar_id]").addClass('blank_field');
		}else{
			if(jQuery("select[name=everwebinar_id]").hasClass('blank_field'))
				jQuery("select[name=everwebinar_id]").removeClass('blank_field');
			
			if(post_id && selected_webinar){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'everwebinar');
				form_data.append('list', selected_webinar);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=everwebinar_id]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Webinar");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","everwebinar_remove_webinar");
						}else{
							jQuery('select[name=everwebinar_id]').prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#everwebinar_remove_webinar', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'everwebinar');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=everwebinar_id]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Webinar");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","everwebinar_save_webinar");
						jQuery("select[name=everwebinar_id]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#gotowebinar_save_webinar', function(){
		var selected_webinar = jQuery("select[name=webinar_id] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		if(selected_webinar == '0'){
			jQuery("select[name=webinar_id]").addClass('blank_field');
		}else{
			if(jQuery("select[name=webinar_id]").hasClass('blank_field'))
				jQuery("select[name=webinar_id]").removeClass('blank_field');
			
			if(post_id && selected_webinar){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'webinar');
				form_data.append('list', selected_webinar);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=webinar_id]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Webinar");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","gotowebinar_remove_webinar");
						}else{
							jQuery('select[name=webinar_id]').prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#gotowebinar_remove_webinar', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'webinar');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=webinar_id]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Webinar");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","gotowebinar_save_webinar");
						jQuery("select[name=webinar_id]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#aweber_save_list', function(){
		var selected_aweber = jQuery("select[name=aweber_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		if(selected_aweber == '0'){
			jQuery("select[name=aweber_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=aweber_listid]").hasClass('blank_field'))
				jQuery("select[name=aweber_listid]").removeClass('blank_field');
			
			if(post_id && selected_aweber){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'aweber');
				form_data.append('list', selected_aweber);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=aweber_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","aweber_remove_list");
						}else{
							jQuery('select[name=aweber_listid]').prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#aweber_remove_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'aweber');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=aweber_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Aweber List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","aweber_save_list");
						jQuery("select[name=aweber_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#getresponse_save_list', function(){
		var selected_gr = jQuery("select[name=gr_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		if(selected_gr == '0'){
			jQuery("select[name=gr_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=gr_listid]").hasClass('blank_field'))
				jQuery("select[name=gr_listid]").removeClass('blank_field');
			
			if(post_id && selected_gr){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'get_response');
				form_data.append('list', selected_gr);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=gr_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","getresponse_remove_list");
						}else{
							jQuery('select[name=gr_listid]').prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#getresponse_remove_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'get_response');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=gr_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save GetResponse List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","getresponse_save_list");
						jQuery("select[name=gr_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	jQuery(document).on('click', '#mailchimp_save_list', function(){
		var selected_mc = jQuery("select[name=mc_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		if(selected_mc == '0'){
			jQuery("select[name=mc_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=mc_listid]").hasClass('blank_field'))
				jQuery("select[name=mc_listid]").removeClass('blank_field');
			
			if(post_id && selected_mc){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'mailchimp');
				form_data.append('list', selected_mc);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=mc_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","mailchimp_remove_list");
						}else{
							jQuery('select[name=mc_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#mailchimp_remove_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'mailchimp');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=mc_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save MailChimp List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","mailchimp_save_list");
						jQuery("select[name=mc_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	jQuery(document).on('click', '#sendlane_save_list', function(){
		var selected_sl = jQuery("select[name=sendlane_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		if(selected_sl == '0'){
			jQuery("select[name=sendlane_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=sendlane_listid]").hasClass('blank_field'))
				jQuery("select[name=sendlane_listid]").removeClass('blank_field');
			
			if(post_id && selected_sl){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'sendlane');
				form_data.append('list', selected_sl);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=sendlane_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","sendlane_remove_list");
						}else{
							jQuery('select[name=sendlane_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '#sendlane_remove_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'sendlane');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=sendlane_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save SendLane List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","sendlane_save_list");
						jQuery("select[name=sendlane_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	
	/* convertkit */
	jQuery(document).on('click', '#save_convertkit_list', function(){
		var selected_ck = jQuery("select[name=convertkit_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		
		if(selected_ck == '0'){
			jQuery("select[name=convertkit_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=convertkit_listid]").hasClass('blank_field'))
				jQuery("select[name=convertkit_listid]").removeClass('blank_field');
			
			if(post_id && selected_ck){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'convertkit');
				form_data.append('list', selected_ck);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=convertkit_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_convertkit_list");
						}else{
							jQuery('select[name=convertkit_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				}); 
			}
		}
	});
	
	jQuery(document).on('click', '#remove_convertkit_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'convertkit');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=convertkit_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save ConvertKit List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_convertkit_list");
						jQuery("select[name=convertkit_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/* over convertkit */
	
	/* Drip */
	jQuery(document).on('click', '#save_drip_list', function(){
		var selected_dp = jQuery("select[name=drip_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		
		if(selected_dp == '0'){
			jQuery("select[name=drip_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=drip_listid]").hasClass('blank_field'))
				jQuery("select[name=drip_listid]").removeClass('blank_field');
			
			if(post_id && selected_dp){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'drip');
				form_data.append('list', selected_dp);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=drip_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_drip_list");
						}else{
							jQuery('select[name=drip_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				}); 
			}
		}
	});
	
	jQuery(document).on('click', '#remove_drip_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'drip');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=drip_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Drip List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_drip_list");
						jQuery("select[name=drip_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/* over Drip */
	
	/* Mailerlite */
	jQuery(document).on('click', '#save_mailerlite_list', function(){
		var selected_dp = jQuery("select[name=mailerlite_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		
		if(selected_dp == '0'){
			jQuery("select[name=mailerlite_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=mailerlite_listid]").hasClass('blank_field'))
				jQuery("select[name=mailerlite_listid]").removeClass('blank_field');
			
			if(post_id && selected_dp){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'mailerlite');
				form_data.append('list', selected_dp);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=mailerlite_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_mailerlite_list");
						}else{
							jQuery('select[name=mailerlite_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				}); 
			}
		}
	});
	
	jQuery(document).on('click', '#remove_mailerlite_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'mailerlite');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=mailerlite_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Mailerlite List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_mailerlite_list");
						jQuery("select[name=mailerlite_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/* over mailerlite */
	
	/* start MailSqaud */
	jQuery(document).on('click', '#save_mailsqaud_list', function(){
		var selected_dp = jQuery("select[name=mailsqaud_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		if(selected_dp == '0'){
			jQuery("select[name=mailsqaud_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=mailsqaud_listid]").hasClass('blank_field'))
				jQuery("select[name=mailsqaud_listid]").removeClass('blank_field');
			
			if(post_id && selected_dp){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'mailsqaud');
				form_data.append('list', selected_dp);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=mailsqaud_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_mailsqaud_list");
						}else{
							jQuery('select[name=mailsqaud_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				}); 
			}
		}
	});
	jQuery(document).on('click', '#remove_mailsqaud_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'mailsqaud');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=mailsqaud_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save MailSqaud List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_mailsqaud_list");
						jQuery("select[name=mailsqaud_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/* end MailSqaud */
	/* start Sendiio */
	jQuery(document).on('click', '#save_sendiio_list', function(){
		var selected_dp = jQuery("select[name=sendiio_listid] option:selected").val();
		var post_id = jQuery("input[name=int_post_id]").val();
		
		
		if(selected_dp == '0'){
			jQuery("select[name=sendiio_listid]").addClass('blank_field');
		}else{
			if(jQuery("select[name=sendiio_listid]").hasClass('blank_field'))
				jQuery("select[name=sendiio_listid]").removeClass('blank_field');
			
			if(post_id && selected_dp){
				var form_data = new FormData();
				form_data.append('action', 'save_funnel_integration');
				form_data.append('type', 'sendiio');
				form_data.append('list', selected_dp);
				form_data.append('post_id', post_id);
				jQuery(this).html("Saving...");
				jQuery(this).prop('disabled', true);
				jQuery('select[name=sendiio_listid]').prop('disabled', true);
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Remove Selection");
							jQuery(this).removeClass("btn_green");
							jQuery(this).addClass("btn_red");
							jQuery(this).attr("id","remove_sendiio_list");
						}else{
							jQuery('select[name=sendiio_listid]').prop('disabled', false);
							jQuery(this).prop('disabled', false);
							jQuery(this).html("Connect Again");
						}
					},
					error: function(error) {
						console.log(error);
					}
				}); 
			}
		}
	});
	
	jQuery(document).on('click', '#remove_sendiio_list', function(){
		var post_id = jQuery("input[name=int_post_id]").val();
		if(post_id){
			var form_data = new FormData();
			form_data.append('action', 'remove_funnel_integration');
			form_data.append('type', 'sendiio');
			form_data.append('post_id', post_id);
			jQuery(this).html("Removing...");
			jQuery(this).prop('disabled', true);
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					if(data == '1'){
						jQuery('select[name=sendiio_listid]').prop('disabled', false);
						jQuery(this).prop('disabled', false);
						jQuery(this).html("Save Sendiio List");
						jQuery(this).removeClass("btn_red");
						jQuery(this).addClass("btn_green");
						jQuery(this).attr("id","save_sendiio_list");
						jQuery("select[name=sendiio_listid]").val(0).change();
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	});
	/* over Sendiio */
	
	jQuery(document).on('change', 'input:checkbox[name=alt_page]', function(){
		if(jQuery(this).is(':checked')){
			// alert('1');
			jQuery('input[name=alt_page_text]').show();
		}else{
			// alert('0');
			jQuery('input[name=alt_page_text]').hide();
		}
	});
	jQuery(document).on('change', 'input:checkbox[name=funnel_type_filter]', function(){
		var checkboxValues_ft = [];
		jQuery('input:checkbox[name=funnel_type_filter]:checked').each(function(){
		   checkboxValues_ft.push(jQuery(this).val());
		});
		var checkboxValues_ct = [];
		jQuery('input:checkbox[name=funnel_category_filter]:checked').each(function(){
		   checkboxValues_ct.push(jQuery(this).val());
		});
		var checkboxValues_sort = jQuery('input[name="funnel_sort_filter"]:checked').val();
		if(typeof(checkboxValues_sort) == 'undefined'){
			checkboxValues_sort = 'DESC';
		}
		var checkboxValues_tg = [];
		jQuery('input:checkbox[name=funnel_tag_filter]:checked').each(function(){
		   checkboxValues_tg.push(jQuery(this).val());
		});
		var form_data = new FormData();
		form_data.append('filter_funnel_type', checkboxValues_ft);
		form_data.append('filter_category', checkboxValues_ct);
		form_data.append('filter_tags', checkboxValues_tg);
		form_data.append('filter_sortby', checkboxValues_sort);
		form_data.append('action', 'get_filtered_store_items');
		jQuery('#loading').show();
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context: this,
			success: function(data) {
				jQuery('#loading').hide();
				jQuery('#store_div').html(data);
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('change', 'input:checkbox[name=funnel_category_filter]', function(){
		var checkboxValues_ct = [];
		jQuery('input:checkbox[name=funnel_category_filter]:checked').each(function(){
		   checkboxValues_ct.push(jQuery(this).val());
		});
		var checkboxValues_ft = [];
		jQuery('input:checkbox[name=funnel_type_filter]:checked').each(function(){
		   checkboxValues_ft.push(jQuery(this).val());
		});
		var checkboxValues_sort = jQuery('input[name="funnel_sort_filter"]:checked').val();
		if(typeof(checkboxValues_sort) == 'undefined'){
			checkboxValues_sort = 'DESC';
		}
		var checkboxValues_tg = [];
		jQuery('input:checkbox[name=funnel_tag_filter]:checked').each(function(){
		   checkboxValues_tg.push(jQuery(this).val());
		});
		var form_data = new FormData();
		form_data.append('filter_funnel_type', checkboxValues_ft);
		form_data.append('filter_category', checkboxValues_ct);
		form_data.append('filter_tags', checkboxValues_tg);
		form_data.append('filter_sortby', checkboxValues_sort);
		form_data.append('action', 'get_filtered_store_items');
		jQuery('#loading').show();
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context: this,
			success: function(data) {
				jQuery('#loading').hide();
				jQuery('#store_div').html(data);
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	jQuery(document).on('change', 'input:checkbox[name=funnel_tag_filter]', function(){
		var checkboxValues_ct = [];
		jQuery('input:checkbox[name=funnel_category_filter]:checked').each(function(){
		   checkboxValues_ct.push(jQuery(this).val());
		});
		var checkboxValues_ft = [];
		jQuery('input:checkbox[name=funnel_type_filter]:checked').each(function(){
		   checkboxValues_ft.push(jQuery(this).val());
		});
		var checkboxValues_sort = jQuery('input[name="funnel_sort_filter"]:checked').val();
		if(typeof(checkboxValues_sort) == 'undefined'){
			checkboxValues_sort = 'DESC';
		}
		var checkboxValues_tg = [];
		jQuery('input:checkbox[name=funnel_tag_filter]:checked').each(function(){
		   checkboxValues_tg.push(jQuery(this).val());
		});
		var form_data = new FormData();
		form_data.append('filter_funnel_type', checkboxValues_ft);
		form_data.append('filter_category', checkboxValues_ct);
		form_data.append('filter_tags', checkboxValues_tg);
		form_data.append('filter_sortby', checkboxValues_sort);
		form_data.append('action', 'get_filtered_store_items');
		jQuery('#loading').show();
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context: this,
			success: function(data) {
				jQuery('#loading').hide();
				jQuery('#store_div').html(data);
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	
	jQuery(document).on('change', 'input[name=funnel_sort_filter]', function(){
		var checkboxValues_ft = [];
		jQuery('input:checkbox[name=funnel_sort_filter]:checked').each(function(){
		   checkboxValues_ft.push(jQuery(this).val());
		});
		var checkboxValues_ct = [];
		jQuery('input:checkbox[name=funnel_category_filter]:checked').each(function(){
		   checkboxValues_ct.push(jQuery(this).val());
		});
		var checkboxValues_sort = jQuery('input[name="funnel_sort_filter"]:checked').val();
		if(typeof(checkboxValues_sort) == 'undefined'){
			checkboxValues_sort = 'DESC';
		}
		var checkboxValues_tg = [];
		jQuery('input:checkbox[name=funnel_tag_filter]:checked').each(function(){
		   checkboxValues_tg.push(jQuery(this).val());
		});
		var form_data = new FormData();
		form_data.append('filter_funnel_type', checkboxValues_ft);
		form_data.append('filter_category', checkboxValues_ct);
		form_data.append('filter_tags', checkboxValues_tg);
		form_data.append('filter_sortby', checkboxValues_sort);
		form_data.append('action', 'get_filtered_store_items');
		jQuery('#loading').show();
		jQuery.ajax({
			url: ajaxurl,
			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			type: 'POST',
			context: this,
			success: function(data) {
				jQuery('#loading').hide();
				jQuery('#store_div').html(data);
			},
			error: function(error) {
				console.log(error);
			}
		});
	});
	
	jQuery(document).on('click', '#sort_and_filter', function(){
		if(jQuery(this).hasClass("p_active")){
			jQuery(this).removeClass("p_active");
			jQuery(this).addClass("p_inactive");
			jQuery(this).children('.icon').html("+");
			jQuery('#filter_div').slideUp();
		}else{
			jQuery(this).addClass("p_active");
			jQuery(this).removeClass("p_inactive");
			jQuery(this).children('.icon').html("-");
			jQuery('#filter_div').slideDown();
		}
	});
	jQuery(document).on('click', 'button#withdraw_funds_fund', function(){		
		var pemail = jQuery('select[name=paypal_email] option:selected').val();
		var pamount = jQuery('input[name=withdraw_amount]').val();
		var withdraw_balance = jQuery("input[name=withdraw_available_balance]").val();
	    
		if(parseInt(pamount) > parseInt(withdraw_balance)){
			jQuery('.paypal_error').html('<p style="color:red; font-size:20px;">Your Available withdrawal amount is ' + withdraw_balance + '$</p>');
			return false;
		}
		
		var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		// alert(max_withdraw.amount);
		if(pemail == '' || pamount == ''){
			jQuery('.paypal_error').html("Fill proper info");
			jQuery('.paypal_error').show();
		}else if(!regex.test(pemail)){
			jQuery('.paypal_error').html("Enter valid email");
			jQuery('.paypal_error').show();
		}else if(parseInt(max_withdraw.amount) < parseInt(pamount)){		
			jQuery('.paypal_error').html("Requested amount not available in your account");
			jQuery('.paypal_error').show();
		}else if(pamount < 50){
			jQuery('.paypal_error').html("You can not withdraw less than 50.00");
			jQuery('.paypal_error').show();
		}else{
			if(pemail && pamount){
				jQuery('.paypal_error').hide();
				jQuery(this).html('Processing..');
				jQuery(this).prop('disabled', true);
				var form_data = new FormData();
				form_data.append('paypal_amount', pamount);
				form_data.append('paypal_email', pemail);
				form_data.append('action', 'paypal_withdrwal_request');
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).html('Request successfully received..');
							window.location = window.location;
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}
	});
	jQuery(document).on('click', '.process_aff_link', function(){
		var post_id = jQuery(this).data('postid');
		var link_type = jQuery('input[name=link_type]').val();
		if(link_type == 'download_link'){
			var download_link = jQuery('input[name=download_link]').val();
			var download_name = jQuery('input[name=download_name]').val();
			if(download_link == ''){
				jQuery('input[name=download_link]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=download_link]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(download_name == ''){
				jQuery('input[name=download_name]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=download_name]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(post_id && download_link !='' && download_name !=''){
				jQuery(this).html('Processing..');
				var form_data = new FormData();
				form_data.append('funnel_id', post_id);
				form_data.append('download_link', download_link);
				form_data.append('download_name', download_name);
				form_data.append('link_type', link_type);
				form_data.append('action', 'submit_website_link');
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data == '1'){
							jQuery(this).html('Link Processed..');
							setTimeout(function() {   //calls click event after a certain time
							   Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
							}, 5000);
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
			}
		}else if(link_type == 'affiliate_link'){
			// var affiliate_network_type = jQuery('input[name=affiliate_network_type]').val();
			var internal_name = jQuery('input[name=internal_name]').val();			
			var checked = jQuery('input[name=affiliate_network]:checked').val();
			var notes = '';
			if(checked == 'jvzoo'){
				var affiliate_link = jQuery('input[name=jvz_affiliate_link]').val();		
				var link_identifier = jQuery('input[name=jvz_link_identifier]').val();
				// alert(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)+(jvzoo|jvz6+)\.[a-z]{3}?(\/ac|\/affiliate+)+(\/[0-9]{6}|\/affiliateinfo)+(\/[0-9]{6}|\/index)+(\/[0-9]{6}|\/)?$/igm.test(affiliate_link));
				if(affiliate_link == ''){
					jQuery('input[name=jvz_affiliate_link]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=jvz_affiliate_link]').removeClass('affiliate_link_error');
					}, 500);
				}else if(/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)+(jvzoo|jvz1|jvz2|jvz3|jvz4|jvz5|jvz6+)\.[a-z]{3}?(\/ac|\/affiliate+)+(\/[0-9]{7}|\/affiliateinfo)+(\/[0-9]{6}|\/index)+(\/[0-9]{3,9}|\/)?$/igm.test(affiliate_link) == false){
					jQuery('input[name=jvz_affiliate_link]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=jvz_affiliate_link]').removeClass('affiliate_link_error');
					}, 500);
					jQuery('.identifier_error').html('System can\'t process the link, Link may not correct.');
					jQuery('.identifier_error').show();
					setTimeout(function() {
						jQuery('.identifier_error').hide();
						jQuery('.identifier_error').html('Identifier Already in Use.');
					}, 3000);
					return false;
				}
				if(link_identifier == ''){
					jQuery('input[name=jvz_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=jvz_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
				// var link_place = jQuery('select[name=link_place]').val();
			}else if(checked == 'clickbank'){
				var affiliate_link = jQuery('input[name=cb_vendor_id]').val();		
				var link_identifier = jQuery('input[name=cb_link_identifier]').val();
				var notes = jQuery('input[name=alt_page_text]').val();
				if(affiliate_link == ''){
					jQuery('input[name=cb_vendor_id]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=cb_vendor_id]').removeClass('affiliate_link_error');
					}, 500);
				}else if(/^[1-9]{0,6}/.test(affiliate_link) == false){
					jQuery('input[name=cb_vendor_id]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=cb_vendor_id]').removeClass('affiliate_link_error');
					}, 500);
					return false;
				}
				if(link_identifier == ''){
					jQuery('input[name=cb_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=cb_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
			}else if(checked == 'warriorplus'){
				var affiliate_link = jQuery('select[name=wp_offer] option:selected').val();		
				var link_identifier = jQuery('input[name=wp_link_identifier]').val();
				// alert(affiliate_link);
				// alert(link_identifier);
				if(affiliate_link == 'Select Product' || typeof(affiliate_link) == 'undefined'){
					jQuery('select[name=wp_offer]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('select[name=wp_offer]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(link_identifier == ''){
					jQuery('input[name=wp_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=wp_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
			}else if(checked == 'paykickstart'){
				var affiliate_link = jQuery('input[name=pks_campaign_id]').val();
				var link_identifier = jQuery('input[name=pks_link_identifier]').val();	
				var notes = jQuery('input[name=notes_pks]').val();
				if(affiliate_link == ''){
					jQuery('input[name=pks_campaign_id]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=pks_campaign_id]').removeClass('affiliate_link_error');
					}, 500);
				}
			}else if(checked == 'external'){
				var affiliate_link = jQuery('input[name=external_affiliate_link]').val();		
				var link_identifier = jQuery('input[name=external_link_identifier]').val();	
				var notes = jQuery('input[name=notes]').val();
				if(affiliate_link == ''){
					jQuery('input[name=external_affiliate_link]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=external_affiliate_link]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(link_identifier == ''){
					jQuery('input[name=external_link_identifier]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=external_link_identifier]').removeClass('affiliate_link_error');
					}, 500);
				}
				if(notes == ''){
					jQuery('input[name=notes]').addClass('affiliate_link_error');
					setTimeout(function() {
						jQuery('input[name=notes]').removeClass('affiliate_link_error');
					}, 500);
					return false;
				}
			}
			if(typeof(checked) == 'undefined'){
			jQuery('.aff_network_container label').addClass('radioerror');
				setTimeout(function() {
					jQuery('.aff_network_container label').removeClass('radioerror');
				}, 500);
			}
			if(internal_name == ''){
				jQuery('input[name=internal_name]').addClass('affiliate_link_error');
				setTimeout(function() {
					jQuery('input[name=internal_name]').removeClass('affiliate_link_error');
				}, 500);
			}
			if(post_id && affiliate_link && typeof(checked) != 'undefined' && internal_name && link_identifier){
				var form_data = new FormData();
				form_data.append('link_identifier', link_identifier);
				form_data.append('action', 'validate_link_identifier');
				jQuery(this).html('Processing..');
				jQuery.ajax({
					url: ajaxurl,
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,
					type: 'POST',
					context: this,
					success: function(data) {
						if(data && data != '0'){
							link_identifier = data;
							jQuery(this).html('Processing..');
							var form_data = new FormData();
							form_data.append('funnel_id', post_id);
							form_data.append('affiliate_link', affiliate_link);
							form_data.append('affiliate_network', checked);
							form_data.append('internal_name', internal_name);
							form_data.append('link_identifier', link_identifier);
							form_data.append('link_type', link_type);
							form_data.append('notes', notes);
							form_data.append('action', 'submit_website_link');
							jQuery.ajax({
								url: ajaxurl,
								cache: false,
								contentType: false,
								processData: false,
								data: form_data,
								type: 'POST',
								context: this,
								success: function(data) {
									if(data == '1'){
										jQuery(this).html('Link Processed..');
										setTimeout(function() {   //calls click event after a certain time
										   Custombox.modal.closeAll();  // closeAll() function close all the custombox popup
										}, 5000);
									}
								},
								error: function(error) {
									console.log(error);
								}
							});
						}else{
							jQuery(this).html('Process Link');
							jQuery('.identifier_error').show();
							setTimeout(function() {
								jQuery('.identifier_error').hide();
							}, 3000);
						}
					},
					error: function(error) {
						console.log(error);
					}
				});
				/////////
			}
		}
	});
	
	jQuery(document).on('click', '.agency_btn_submit', function(){
		var agency_f_name = jQuery('input[name=agency_first_name]').val();
		var agency_l_name = jQuery('input[name=agency_last_name]').val();
		var agency_u_email = jQuery('input[name=agency_user_email]').val();		
		var current_user_id = jQuery('input[name=current_user_id]').val();			
		var author_funnels = [];
		  for (var option of document.getElementById('agency_author_funnels').options) {
			if (option.selected) {
			  author_funnels.push(option.value);
			}
		  }
		var regex_email = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				
		if(agency_f_name == ''){
			jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>Enter First name<span>")
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}
		
		if(agency_l_name == ''){
			jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>Enter Last name<span>")
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}
		
		if(agency_u_email == ''){
			jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>Enter Email<span>")
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}else if(regex_email.test(agency_u_email) == false){
			jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>Invalid Email<span>")
			jQuery('#alertFadeOut').fadeOut(10000, function () {
			  jQuery('#alertFadeOut').text('');
			});
			return false;
		}
		
			
		if(agency_u_email && agency_f_name && agency_l_name){
			jQuery('#load_agency').show();
			var form_data = new FormData();
			form_data.append('user_fname', agency_f_name);
			form_data.append('user_lname', agency_l_name);
			form_data.append('user_email', agency_u_email);
			form_data.append('funnels', author_funnels);
			form_data.append('current_user_id', current_user_id);
			form_data.append('action', 'agency_create_user');
			jQuery.ajax({
				url: ajaxurl,
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'POST',
				context: this,
				success: function(data) {
					jQuery('#load_agency').hide();	
					if(data == 1){
						jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:red; font-size:18px; padding:5px 10px;'>This Email is already used<span>")
						jQuery('#alertFadeOut').fadeOut(10000, function () {
						  jQuery('#alertFadeOut').text('');
						});
					}else if(data == 0){
						jQuery('#agency_error_msj').html("<span id='alertFadeOut' style='color:Green; font-size:18px; padding:5px 10px;'>New Account is Created<span>")
						jQuery('#alertFadeOut').fadeOut(10000, function () {
						  jQuery('#alertFadeOut').text('');
						});
						jQuery('input[name=agency_first_name]').val("");
						jQuery('input[name=agency_last_name]').val("");
						jQuery('input[name=agency_user_email]').val("");		
						jQuery('#agency_author_funnels').prop('selectedIndex',0);
					}else{
						//alert("Done");
					}
				},
				error: function(error) {
					console.log(error);
				}
			});
		}
	})
	
	jQuery('#agency_chek').change(function() {
		if (jQuery(this).prop('checked')) {
			jQuery('#agency_author_funnels').slideToggle();
		}else {
			jQuery('#agency_author_funnels').slideToggle();
		}
	});
});