<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/wp-load.php");
global $wpdb;
$params = explode('/', $_REQUEST['l']);
// print_r($params);exit;
// [0] = post slug
// [1] = member id
// [2..n] = Anything else that was in the query string
// https://funnelmates.com/dl/4563/1/

$cookie_name = "CCID";
$cookie_value = $params[1];
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day;
$args = array(
	'name'      	 => 'confirm',
	'post_type'      => 'landing',
	'posts_per_page' => -1,
	'post_parent'    => $params[0],
	'order'          => 'ASC',
	'orderby'        => 'menu_order'
);
$children = get_posts( $args );
// echo '<pre>';print_r($children);exit;
if($children[0]->ID != $children[0]->post_parent){
	wp_redirect(get_permalink($children[0]->ID));
}else{
	wp_redirect(get_permalink($params[0]));
}
exit;
?>