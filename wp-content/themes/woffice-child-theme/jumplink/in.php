<?php
	require_once($_SERVER["DOCUMENT_ROOT"]."/wp-load.php");
	global $wpdb;
	$params = explode('/', $_REQUEST['l']);
	$id = $params[0];	
	//echo "SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".$id."' AND item_type = 'fb_ads'";
	$get_pt = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".$id."' AND item_type = 'linkedin'");
	$data = maybe_unserialize(base64_decode($get_pt->content));
	$title = get_the_title($id);
	//$url = $data['url'];
	$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$data1 = strip_shortcodes($data['data']);
	$image1 = $data['image'];
	$image_url = wp_get_attachment_url( $image1 );
?>
<html>
<head>
	<meta property="og:type" content="website" />
	<meta name="title" content="<?php echo $title; ?>">
	<meta property="og:title" content="<?php echo $title; ?>">
	<meta property="og:image" content="<?php echo $image_url; ?>">
	<meta property="og:url" content="<?php echo $url; ?>">
	<meta property="og:description" content="<?php echo stripcslashes($data1); ?>">
	<meta property="og:site_name" content="Funnel Mates">
</head>
</html>
<?php
	exit;
?>