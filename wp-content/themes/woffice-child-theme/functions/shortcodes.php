<?php
add_shortcode('default_subject','default_subject');
function default_subject(){
	global $wpdb;
	$post_id = wp_get_post_parent_id(get_the_ID());
	// return "SELECT * FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='creator' ORDER BY id ASC LIMIT 0,1";
	$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='creator' ORDER BY id ASC LIMIT 0,1");
	if($query->name){
		return 'Please Confirm To Access '.$query->name;
	}else{
		return 'Please Confirm To Access Your Download';
	}
}
add_shortcode('transaction_history','transaction_history');
add_shortcode('dashboard_notices','dashboard_notices');
add_shortcode('fm_custom_domain','fm_custom_domain');
function dashboard_notices(){
	global $wpdb;
	// wp_fm_funneledit_data
	/*$results = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_funneledit_data` ORDER BY date DESC LIMIT 0,10");
	foreach($results as $rs){
		$isactivated = $wpdb->get_var("SELECT COUNT(*) FROM `".$wpdb->prefix."setc_activated_funnels` WHERE funnel_id='".$rs->funnel_id."' AND user_id='".get_current_user_id()."' ");
		if($isactivated > 0){
			if($rs->type=='followup_email'){
				if($rs->process == 'update' || $rs->process == 'insert'){
					$get_fe = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".$rs->funnel_id."' AND id='".$rs->relation_id."' ");
					$html .= '<div class="notice-success">
							<p><i class="fa fa-check" aria-hidden="true"></i> Followup Email Updated in Funnel <a target="_BLANK" href="'.get_permalink($get_fe->funnel_id).'">'.get_the_title($get_fe->funnel_id).'</a></p>
							<p style="margin-left: 34px;font-size: 11px;">'.$rs->date.'</p>
							<p style="margin-left: 34px;font-size: 11px;cursor:pointer;" onclick="check_followup_email_template('.$get_fe->id.');">Check Template</p>
						</div>';
				}
			}else{
				if($rs->process == 'update' || $rs->process == 'insert'){
					if($rs->type == "banner"){
						$html .= '<div class="notice-success">
							<p><i class="fa fa-check" aria-hidden="true"></i> Banner Updated in Funnel <a target="_BLANK" href="'.get_permalink($rs->funnel_id).'">'.get_the_title($rs->funnel_id).'</a></p>
							<p style="margin-left: 34px;font-size: 11px;">'.$rs->date.'</p>
						</div>';
					}
					if($rs->type == "email_swipe"){
						$html .= '<div class="notice-success">
							<p><i class="fa fa-check" aria-hidden="true"></i> Email Swipe Updated in Funnel <a target="_BLANK" href="'.get_permalink($rs->funnel_id).'">'.get_the_title($rs->funnel_id).'</a></p>
							<p style="margin-left: 34px;font-size: 11px;">'.$rs->date.'</p>
						</div>';
					}
				}
				if($rs->process == 'delete'){
					if($rs->type == "banner"){
						$html .= '<div class="notice-success">
							<p><i class="fa fa-check" aria-hidden="true"></i> Banner Removed from Funnel <a target="_BLANK" href="'.get_permalink($rs->funnel_id).'">'.get_the_title($rs->funnel_id).'</a></p>
							<p style="margin-left: 34px;font-size: 11px;">'.$rs->date.'</p>
						</div>';
					}
					if($rs->type == "email_swipe"){
						$html .= '<div class="notice-success">
							<p><i class="fa fa-check" aria-hidden="true"></i> Email Swipe Removed from Funnel <a target="_BLANK" href="'.get_permalink($rs->funnel_id).'">'.get_the_title($rs->funnel_id).'</a><br/></p>
							<p style="margin-left: 34px;font-size: 11px;">'.$rs->date.'</p>
						</div>';
					}
				}
			}
		}
	}
	$html .= '<div id="followup_email_template" class="cn-demo-modal cn_demo-modal-width-85" style="display:none;"></div>';*/
	/*$query_notices = $wpdb->get_results("SELECT * FROM `wp_setc_notices` WHERE user_id='".get_current_user_id()."'");
	foreach($query_notices as $notice){
		echo $notice->funnel_id;
	}
	return $html;*/
}
/*function cmp($a, $b){
    $ad = strtotime($a['time']);
    $bd = strtotime($b['time']);
    return ($ad-$bd);
}*/

function transaction_history(){
	require_once(get_stylesheet_directory().'/funds-history.php');
	/*
	global $wpdb;
	$transaction_history = $wpdb->get_results("SELECT * FROM wp_setc_pks_main_balance WHERE user=".get_current_user_id()." ORDER BY id DESC");
	$transaction_history1 = $wpdb->get_results("SELECT * FROM wp_setc_pks_balance WHERE user=".get_current_user_id()." ORDER BY id DESC");
	$transaction_history = array_merge($transaction_history, $transaction_history1);
	usort($transaction_history);
	?>
	<style>
	table#table_transaction td {
		padding: 10px 0px;
		border-bottom: 1px solid #EEE;
	}
	</style>
	<table id="table_transaction">
		<tr>
			<th>ID</th>
			<th>Type</th>
			<th>Description</th>
			<th>Price</th>
			<th>Time</th>
		</tr>
		<?php
		foreach($transaction_history as $th){
			?>
			<tr>
				<td>#<?php echo $th->id; ?></td>
				<?php 
				if($th->funnel_id == '0'){ ?>
					<td style="color:#28d094;">Credited</td>
					<td>Balance Added</td>
				<?php }else{				
					if($th->type == 'D'){ ?>
						<td style="color:#28d094;">Activated</td>
						<td><a href="<?php echo get_permalink($th->funnel_id); ?>"><?php echo 'Activated '.ucfirst($th->funnel_type).' Funnel'; //$th->funnel_id ?></a></td>
					<?php }else if($th->type == 'C'){ ?>
						<td style="color:#ff4961;"><?php echo 'Buy'; ?></td>
						<td><a href="<?php echo get_permalink($th->funnel_id); ?>"><?php echo 'Sell '.ucfirst($th->funnel_type).' Funnel'; ?></a></td>
					<?php }
				}				?>
				<td><?php echo $th->amount; ?></td>
				<td><?php echo $th->time; ?></td>
			</tr>
			<?php
		}
		?>
		<?php /*<tr>
			<td>#32586</td>
			<td >Buy</td>
			<td>Unlock Funnel</td>
			<td>50</td>
			<td>25/09/2020 04.40am</td>
		</tr>
		<tr>
			<td>#32586</td>
			<td style="color:#28d094;">Buy</td>
			<td>Unlock Funnel</td>
			<td>50</td>
			<td>25/09/2020 04.40am</td>
		</tr>
		<tr>
			<td>#32586</td>
			<td style="color:#ff4961;">Sell</td>
			<td>Unlock Funnel</td>
			<td>50</td>
			<td>25/09/2020 04.40am</td>
		</tr>
		<tr>
			<td>#32586</td>
			<td style="color:#6b6f82;">Pending</td>
			<td>Withdrawal</td>
			<td>50</td>
			<td>25/09/2020 04.40am</td>
		</tr>*//*?>
	</table>
	<?php*/
} 
/*
add_shortcode('marketplace_items','marketplace_items');
function marketplace_items(){
	global $wpdb;
	// echo 'here';
	$items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items ORDER BY id DESC");
	/*echo '<pre>';
	print_r($items);
	echo '</pre>';*//*
	$i_c = 0;
	foreach($items as $i){
		$pricing = maybe_unserialize($i->pricing);
		$post_id = $i->post_id;
		$funnel = get_post($post_id);
		$count_price = count($pricing);
		if(get_post_meta($post_id,'is_funnel_published',true) == 1){ 
		?>
		<div class="marketplace_item <?php echo $post_id; ?>" >
			<div class="marketplace_item_inner">
				<?php
				$freecolor = "background-color:#f37021;color:#fff;border-radius: 4px 0 0 0;";
				$premiumcolor = "background-color:#446095;color:#fff;";
				$whitelabelcolor = "background-color:#a8519f;color:#fff;";
				$exclusivecolor = "background-color:#f4ea00;color:#000;border-radius: 0 4px 0 0;";
				foreach($pricing as $k => $p){
					if($count_price == 4){
						$width = "25%";
					}else if($count_price == 3){
						$width = "33.33%";
					}else if($count_price == 2){
						$width = "50%";
					}else{
						$width = "100%";
					}
					if($k == 'free'){
						echo '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
					}else if($k == 'premium'){
						echo '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
					}else if($k == 'whitelabel'){
						echo '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
					}else if($k == 'exclusive'){
						echo '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
					}
				}
				echo '<div class="funnel_details">';
					echo '<h5 style="text-align: center;">'.$funnel->post_title.'</h5>';
					echo '<div class="funnel_details_inner">';
						$funnel_img = $wpdb->get_row("SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".$funnel->ID."'");
						$path = wp_get_upload_dir();
						$img_path = $path['baseurl'].'/screenshot';
						$f_img = $img_path .'/'. $funnel_img->image;
						if($funnel_img){
							echo '<div style="width:30%;float:left;"><img src="'.$f_img.'" data="'.$funnel->ID.'"/></div>';						
						}else{
							echo '<div style="width:30%;float:left;"><img src="http://placehold.it/100x100" /></div>';
						}
						echo '<div style="width:70%;float:left;padding: 0 10px;font-size: 12px;">';
							echo '<table>';
								echo '<tr>';
									echo '<td>Created By: <a href="'.get_author_posts_url($funnel->post_author).'">'.get_the_author_meta('first_name',$funnel->post_author).'</a></td>';
								echo '</tr>';
								echo '<tr>';
									$cat = get_the_category($post_id);
									echo '<td>Category: <a href="'.get_category_link($cat[0]).'">'.$cat[0]->name.'</a></td>';
								echo '</tr>';
								echo '<tr>';
									$rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
									$review_count = count($rating);
									$totalreview = 0;
									foreach($rating as $r){
										$totalreview = $totalreview + $r->valueformoney;
									}
									echo '<td>Funnel Rating: '.$totalreview.' Based on ('.$review_count.' Review)</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Activate Users:0</td>';
								echo '</tr>';
								echo '<tr>';
									echo '<td>Affiliate Products:0</td>';
								echo '</tr>';
								echo '<tr>';
									$pm_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$post_id);
									if($pm_count == 0){
										$pt = 'No';
									}else{
										$pt = 'Yes';
									}
									echo '<td>Promo Tools: '.$pt.'</td>';
								echo '</tr>';
							echo '</table>';
						echo '</div>';
					echo '</div>';
					echo '<div class="ratings_div" style="display: inline-block;width: 100%;">';
						$rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
						$review_count = count($rating);
						if($review_count > 0){
							echo '<p style="margin: 0;font-size: 13px;font-weight: 600;">Based on <span style="font-size: 20px;">'.$review_count.'</span> Reviews</p>';
						$valueformoney = 0;
						$welldesigned = 0;
						$qualityofcontent = 0;
						foreach($rating as $r){
							$valueformoney = $valueformoney + $r->valueformoney;
							$welldesigned = $welldesigned + $r->welldesigned;
							$qualityofcontent = $qualityofcontent + $r->qualityofcontent;
						}
						$average_valueformoney = $valueformoney / $review_count;
						$average_welldesigned = $welldesigned / $review_count;
						$average_qualityofcontent = $qualityofcontent / $review_count;
						
						if($average_valueformoney > 0 && $average_valueformoney <= 2){
							//red
							$background_valueformoney = 'ED1C24';
						}else if($average_valueformoney > 2 && $average_valueformoney <= 3.5 ){
							//yellow
							$background_valueformoney = 'ff9e28';
						}else if($average_valueformoney > 3.5 ){
							//green
							$background_valueformoney = '9ec73b';
						}
						
						if($average_welldesigned > 0 && $average_welldesigned <= 2){
							//red
							$background_welldesigned = 'ED1C24';
						}else if($average_welldesigned > 2 && $average_welldesigned <= 3.5 ){
							//yellow
							$background_welldesigned = 'ff9e28';
						}else if($average_welldesigned > 3.5 ){
							//green
							$background_welldesigned = '9ec73b';
						}
						
						if($average_qualityofcontent > 0 && $average_qualityofcontent <= 2){
							//red
							$background_qualityofcontent = 'ED1C24';
						}else if($average_qualityofcontent > 2 && $average_qualityofcontent <= 3.5 ){
							//yellow
							$background_qualityofcontent = 'ff9e28';
						}else if($average_qualityofcontent > 3.5 ){
							//green
							$background_qualityofcontent = '9ec73b';
						}
						
						echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_valueformoney.';color: white;">'.$average_valueformoney.'</span> Value For Money</div>';
						echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_welldesigned.';color: white;">'.$average_welldesigned.'</span>Well Designed</div>';
						echo '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_qualityofcontent.';color: white;">'.$average_qualityofcontent.'</span>Quality of Content</div>';
						}else{
							echo 'No Review Found..';
						}
					echo '</div>';
					echo '<div class="marketplace_item_desc">';
						echo '<p style="text-align: left;margin: 10px 0;font-size: 15px;">'.$i->description.'</p>';
						echo '<button onclick="unlock_funnel('.$i->post_id.');" type="button" style="background-color:#82b440;padding: 5px 30px;border-radius: 20px;">Unlock this Funnel</button>';
					echo '</div>';
				echo '</div>';
				?>
			</div>
		</div>
		<?php
		$i_c++;
		}
		if($i_c%3==0){echo '<div style="clear:both;"></div>';}
	}
	?>
	<div id="unlock_funnels_modal" class="cn-demo-modal cn_demo-modal-width-50"></div>
	<?php
} Date : 30-12-2020*/


add_shortcode('funds_page','funds_page');
function funds_page(){
	global $wpdb;
	?>
	<div class="funds_container">
		<div class="inner_funds_first">
			<div class="funds_inner_part" style="display: inline-block;width: 100%;">
				<h3 style="font-weight: 900;">Active Balance</h3>
				<div>
					<p style="font-size: 18px;color: #414d6f;margin: 0;">Funnel Mates Credit</p>
					<div class="credit_div" style="">
						<?php
						/*$c_value = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='C' AND user_id='".get_current_user_id()."' ");
						$d_value = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='D' AND user_id='".get_current_user_id()."' ");
						if(!$c_value){
							$c_value = 0;
						}
						if(!$d_value){
							$d_value = 0;
						}
						$value = $c_value - $d_value;*/
						$fm_credits = get_funnelmates_credit(get_current_user_id());
						?>
						<p style="font-size: 27px;font-weight: 500;">$<?php echo number_format($fm_credits,2); ?></p>
					</div>
					<div class="funds_btn" style="">
						<p>
							<a href="/add-funds/" style="background: #FECA35;padding: 5px 5px;color: #000000;border-radius: 40px;font-weight: 600;">Upgrade or Add Funds</a>
						</p>
						<p>
							<a href="/store/" style="background: #82b440;padding: 5px 20px;color: white;border-radius: 40px;">Buy Funnels</a>
						</p>
					</div>
				</div>
				<div>
					<p style="font-size: 18px;color: #414d6f;margin: 0;">Available For Withdrawal</p>
					<div class="available_withdrawal" style="">
						<?php
						$fm_available_withdraw = get_funnelmates_available_withrawal(get_current_user_id());
						?>
						<p style="font-size: 27px;font-weight: 500;">$<?php echo number_format($fm_available_withdraw,2); ?></p>
					</div>
					<div class="how_it_works_btn" style="">
						<p>
							<a id="howitworks_fm" style="cursor:pointer;background: #82b440;padding: 5px 20px;color: white;border-radius: 40px;">How It Works</a>
						</p>
						<p>
							<a id="withdraw_from_fm" style="cursor:pointer;background: #82b440;padding: 5px 20px;color: white;border-radius: 40px;">Withdraw</a>
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="inner_funds_second">
			<div class="funds_inner_part" style="display: inline-block;width: 100%;">
				<h3 style="font-weight: 900;">Sales Statistics</h3>
				<div>
					<p style="text-align:center;font-size: 25px;margin:0;">Leads Generated</p>
					<p style="font-size: 30px;text-align: center;font-weight:600;"><?php echo fm_total_leads(); ?></p>
				</div>
				<div>
					<p style="text-align:center;font-size: 25px;margin:0;">Earnings To Date</p>
					<p style="font-size: 30px;text-align: center;font-weight:600;">$<?php echo number_format(get_funnelmates_earnings(get_current_user_id()),2); ?></p>
				</div>
			</div>
		</div>
		<div class="inner_funds_third">
			<div class="funds_inner_part" style="display: inline-block;width: 100%;">
				<h3 style="font-weight: 900;font-size: 25px;">Funnel Statistics</h3>
				<?php
				$free_funnel = 0;
				$premium_funnel = 0;
				$whitelabel_funnel = 0;
				$exclusive_funnel = 0;
				wp_reset_query();
				$wpb_all_query = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1));
				//print_r($wpb_all_query);
				if($wpb_all_query->have_posts()){
					while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
						if(get_post_meta(get_the_ID(),'is_funnel_published',true) == 1){
						/*echo get_the_ID();
						echo "<br>";*/
						$get_funnel_details = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items` WHERE post_id=".get_the_ID());
						$pricing = maybe_unserialize($get_funnel_details->pricing);
						//print_r($pricing);
						foreach($pricing as $k=>$p){
							if($k == 'free')
								$free_funnel++;
							if($k == 'premium')
								$premium_funnel++;
							if($k == 'whitelabel')
								$whitelabel_funnel++;
							if($k == 'exclusive')
								$exclusive_funnel++;
						}
						}
					endwhile;
				}
				?>
				<div>
					<p style="margin: 0;font-weight: 600;text-align: center;font-size: 20px;">Funnels Published</p>
					<div class="fund_publish" style="">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Free</p>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $free_funnel ?></div>
						</div>
					</div>
					<div class="fund_publish" style="">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Premium</p>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $premium_funnel ?></div>
						</div>
					</div>
					<div class="fund_publish" style="">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Whitelabel</p>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $whitelabel_funnel ?></div>
						</div>
					</div>
					<div class="fund_publish" style="">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Exclusive</p>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $exclusive_funnel ?></div>
						</div>
					</div>
				</div>
				<div>
					<p style="margin: 0;font-weight: 600;text-align: center;font-size: 20px;">Purchases/Activations</p>
					<div class="purchases_activations_funnel">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Free</p>
							<?php
							//$getfreecount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_pks_main_balance` WHERE user='".get_current_user_id()."' AND type='C' AND funnel_type='free'");
							$getfreecount = $wpdb->get_var("SELECT count(DISTINCT(funnel_id)) FROM `wp_setc_activated_funnels` where user_id = '".get_current_user_id()."' and type = 'free'");
							?>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $getfreecount; ?></div>
						</div>
					</div>
					<div class="purchases_activations_funnel">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Premium</p>
							<?php
							//$getpremiumcount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_pks_main_balance` WHERE user='".get_current_user_id()."' AND type='C' AND funnel_type='premium'");
							$getpremiumcount = $wpdb->get_var("SELECT count(DISTINCT(funnel_id)) FROM `wp_setc_activated_funnels` where user_id = '".get_current_user_id()."' and type = 'premium'");
							?>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $getpremiumcount; ?></div>
						</div>
					</div>
					<div class="purchases_activations_funnel">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Whitelabel</p>
							<?php
							//$getwhitelabelcount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_pks_main_balance` WHERE user='".get_current_user_id()."' AND type='C' AND funnel_type='whitelabel'");
							$getwhitelabelcount = $wpdb->get_var("SELECT count(DISTINCT(funnel_id)) FROM `wp_setc_activated_funnels` where user_id = '".get_current_user_id()."' and type = 'whitelabel'");
							?>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $getwhitelabelcount; ?></div>
						</div>
					</div>
					<div class="purchases_activations_funnel">
						<div>
							<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">Exclusive</p>
							<?php
							//$getexclusivecount = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_pks_main_balance` WHERE user='".get_current_user_id()."' AND type='C' AND funnel_type='exclusive'");
							$getexclusivecount = $wpdb->get_var("SELECT count(DISTINCT(funnel_id)) FROM `wp_setc_activated_funnels` where user_id = '".get_current_user_id()."' and type = 'exclusive'");
							?>
							<div style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;"><?php echo $getexclusivecount; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
}
add_shortcode('withdraw_money','withdraw_money');
function withdraw_money(){
	global $wpdb;
	$user = get_user_by('id', get_current_user_id());
	$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
	/* $clear_balance = $wpdb->get_var("SELECT SUM(amount) FROM `wp_setc_pks_main_balance` WHERE type='C' AND user='".get_current_user_id()."' AND time < now() - interval 30 DAY");
	if(!$clear_balance){
		$clear_balance = 0;
	}
	$processing_balance = $wpdb->get_var("SELECT SUM(request_amount) FROM `wp_withdrawal` WHERE request_process=0 AND user_id='".get_current_user_id()."'");
	if(!$processing_balance){
		$processing_balance = 0;
	} */
	$clear_balance = get_funnelmates_available_withrawal(get_current_user_id());
	if($clear_balance > 50){
	?>
		<div style="text-align:center;">
			<h1>Withdraw Money</h1>
			<div>
				<input type="hidden" name="withdraw_available_balance" value="<?php echo $clear_balance; ?>">
				<input style="width:100%;text-align: right;font-size: 40px;font-weight: 900;padding: 3px !important;" min="50.00" max="<?php echo $clear_balance; ?>" value="50.00" step=".01" type="number" name="withdraw_amount" />
				<?php if($general_settings['paypal_email']){ ?>
				<select name="paypal_email" style="width:100%;padding: 3px !important;">
					<option value="<?php echo $general_settings['paypal_email']; ?>"><?php echo $general_settings['paypal_email']; ?></option>
				</select>
				<p class="paypal_error" style="color:red;" style="display:none;"></p>
				<button style="width: 100%;margin-top: 15px;border: none;background: green;color: white;padding: 5px;font-size: 17px;border-radius: 5px;" id="withdraw_funds_fund">Withdraw Funds</button>
				<?php }else{ ?>
					<button onclick="window.open('/settings/','_SELF');" style="width: 100%;margin: 15px 0;border: none;background: green;color: white;padding: 5px;font-size: 17px;border-radius: 5px;" type="button">Please connect Paypal to receive your payment</button>
					<?php /* <p style="margin: 0;font-style: italic;">I don't have Paypal and want payment by other means (slower)</p> */ ?>
					<a id="form_popup" style="margin: 0;font-style: italic; cursor:pointer;">I don't have Paypal and want payment by other means (slower)</a>
				<?php } ?>
			</div>
		</div>
	<?php }else{ ?>
		<div style="text-align:center;">
			<h1>Withdraw Money</h1>
			<h5>You should have at least $50.00 in 'Available Funnel Mates Cash'</h5>
		</div>
	<?php }
}
add_shortcode('howitworks_modal','howitworks_modal');
function howitworks_modal(){
	$user = get_user_by('id', get_current_user_id());
	?>
	<div style="text-align:center;">
		<h2>How it Works</h2>
		<div>
			<p style="font-size: 20px;">Welcome <strong><?php echo $user->display_name; ?></strong>.</p>
			<div style="text-align:left;">
				<p style="font-size: 17px;font-weight: 600;text-decoration: underline;margin: 0;">You currently have:</p>
				<p style="margin: 0;"><span style="font-weight: 600;font-size: 20px;">$<?php echo number_format(get_funnelmates_credit(get_current_user_id()),2); ?></span> FunnelMates Credit</p>
				<p style="margin: 0;"><span style="font-weight: 600;font-size: 20px;">$<?php echo number_format(get_funnelmates_pending_cash(get_current_user_id()),2); ?></span> Pending FunnelMates Cash</p>
				<p style="margin: 0;"><span style="font-weight: 600;font-size: 20px;">$<?php echo number_format(get_funnelmates_available_withrawal(get_current_user_id()),2); ?></span> Available FunnelMates Cash</p>
				<p style="margin: 0;"><span style="font-weight: 600;font-size: 20px;">$<?php echo number_format(get_funnelmates_processing_balance(get_current_user_id()),2); ?></span> Processing FunnelMates Cash</p>
				<p style="margin: 0;"><span style="font-weight: 600;font-size: 20px;">$<?php echo number_format(get_funnelmates_withdrawed_amount(get_current_user_id()),2); ?></span> Withdrawn FunnelMates Cash</p>
			</div>
			
			<div style="text-align: left;margin-top: 20px;">
				<p style="font-size: 17px;font-weight: 600;text-decoration: underline;margin: 0;">FunnelMates Credit:</p>
				<p style="margin: 0;">You can use this credit to purchase new funnels in our <a href="/store/">marketplace</a>. Need more credit?  You can subscribe or purchase on-off amounts <a href="/add-funds/">here</a>.</p>
			</div>
			<div style="text-align: left;margin-top: 20px;">
				<p style="font-size: 17px;font-weight: 600;text-decoration: underline;margin: 0;">Pending FunnelMates Cash:</p>
				<p style="margin: 0;">Money you've earned from sales of your funnels in the last 30 days. <a href="/wiki/making-a-funnel-for-sale-or-profit/">Find out how to make and sell more funnels here.</a></p>
			</div>
			<div style="text-align: left;margin-top: 20px;">
				<p style="font-size: 17px;font-weight: 600;text-decoration: underline;margin: 0;">Available FunnelMates Cash:</p>
				<?php /*<p style="margin: 0;">Money you've earned that is now available for withdrawal. Create more funnels <a href="/builder/">here</a> and list them in the marketplace to earn money faster.</p>*/ ?>
				<p style="margin: 0;">Money you've earned from sales of your funnels, at least 30 days ago that is now available for withdrawal. You need at least $50 available to process a withdrawal.</p>
			</div>
			<div style="text-align: left;margin-top: 20px;">
				<p style="font-size: 17px;font-weight: 600;text-decoration: underline;margin: 0;">Processing FunnelMates Cash:</p>
				<p style="margin: 0;">Money that's on it's way to you!  We process your withdrawal requests within 3-4 business days.</p>
			</div>
		</div>
	</div>
	<?php
}

add_shortcode('form_model','form_model');
function form_model(){
	?>
	<div style="text-align:center;">
		<?php $user_ID = get_current_user_id(); ?>
		<input type="hidden" name="recipient_id" value="<?php echo $user_ID; ?>">
			<div class="half">
				<div class="first_half">
					<input type="text" name="fname" placeholder="Full Name" />
				</div>
				<div class="second_half">
					<input type="email" name="email" placeholder="Email Address" />
				</div>
			</div>
			<input type="text" name="home_address" placeholder="Home Address" />
			<div class="half">
				<div class="first_half">
					<input type="text" name="city" placeholder="City" />
				</div>
				<div class="second_half">
					<input type="text" name="postal_code" placeholder="Postal Code" />
				</div>
			</div>
			<div class="half">
				<div class="first_half">
					<input type="text" name="bank_name" placeholder="Bank Name" />
				</div>
				<div class="second_half">
					<input type="text" name="bank_address" placeholder="Bank Address" />
				</div>
			</div>
			<div class="half">
				<div class="first_half">
					<input type="text" name="account_name" placeholder="Account Name" />
				</div>
				<div class="second_half">
					<input type="text" name="account_number" placeholder="Account Number" />
				</div>
			</div>
			<div class="half">
				<div class="first_half">
					<input type="text" name="swift_code" placeholder="Swift Code" />
				</div>
				<div class="second_half">
					<input type="text" name="ifsc_code" placeholder="IFSC Code" />
				</div>
			</div>
			<input type="text" name="currency" placeholder="Currency" />
			<p id="payment_price" style="font-size:20px; font-weight:600;"></p>
			<div id="msg"></div>
			<button style="background-color:#82b440; color: #FFF; cursor: pointer; padding: 5px 20px; border: none; border-radius: 30px;" id="send_money" type="button">Send</button>
		<?php /*<div class="form-group">
			<input type="email" name="recipient_email" class="form-control" id="recipient_email" aria-describedby="emailHelp" placeholder="Enter recipient email">
		</div>
		<div class="form-group">
			<input type="text" name="recipient_name" class="form-control" id="recipient_name" aria-describedby="emailHelp" placeholder="Enter recipient name">
		</div>
		<div class="form-group">
			<textarea name="recipient_address" class="form-control" id="recipient_address" aria-describedby="emailHelp" placeholder="Enter recipient home address"></textarea>
		</div>
		<div class="form-group">
			<input type="text" name="recipient_iban" class="form-control" id="recipient_iban" aria-describedby="emailHelp" placeholder="Enter recipient IBAN ">
		</div>
		<div class="form-group">
			<button style="background-color:#82b440; color: #FFF; cursor: pointer; padding: 5px 20px; border: none; border-radius: 30px;" id="send_money" type="button">Send</button>
		</div>
		<p id="payment_price" style="font-size:20px; font-weight:600;"></p>*/ ?>
	</div>
	<?php
}
function fm_custom_domain(){
	$custom_domain = get_user_meta(get_current_user_id(),'fm_custom_url',true);
	$data =  maybe_unserialize($custom_domain);	
	$array_count = count($data);
	if($array_count){
		$check_last_field = $array_count - 1; 
	}else{
		$check_last_field = 1; 
	}
	
	/*$html .= '<table>';
		$html .= '<tr>';
			$html .= '<th style="width:25%;">Custom Domain : </th>';
				$html .= '<td>';
					$html .= '<input type="text" value="'.$custom_domain.'" placeholder="Enter Custom Domain" name="custom_domain" />';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '<tr>';
			$html .= '<td colspan="2">';
				$html .= '<button class="btn_green" id="save_custom_domain" type="button">Save Custom Domain</button>';
				if($custom_domain){
					$style="";
				}else{
					$style="display:none;";
				}
				$html .= '<button class="btn_red" id="remove_custom_domain" type="button" style="margin-left: 10px; '.$style.'">Remove Custom Domain</button>';
				$html .= '<p id="custom_domain_error" style="display:none;margin:0;"></p>';
			$html .= '</td>';
		$html .= '</tr>';
	$html .= '</table>';*/
	$html .= '<div class="custom_domain_container">';
		if(empty($data)){
			$html .= '<div style="margin-top: 15px;" class="input-container">
				<input placeholder="Enter Custom Domain" style="margin: 0;width: 90%;" type="text" name="custom_domain[]"/><span class="delete_custom_domain" style="padding: 10px;color: red;font-size: 20px;"><i class="fa fa-times"></i></span><span class="save_custom_domain" style="padding: 10px;color: green;font-size: 20px;"><i class="fa fa-check"></i></span><span class="btn_add_custom_domain" id="plus_'.$check_last_field.'" style="padding: 10px;color: green;font-size: 20px; display:none;"><i class="fa fa-plus"></i></span></div>';
		}
		
		$i = 1;
		foreach($data as $k => $d){
		$html .='<div style="margin-top: 15px;" class="input-container custom_field_'.$k.'">
				<input placeholder="Enter Custom Domain" style="margin: 0;width: 90%;" type="text" name="custom_domain[]" value="'.$d.'"/><span class="delete_custom_domain" style="padding: 10px;color: red;font-size: 20px;" value="'.$k.'"><i class="fa fa-times"></i></span>';
				if($array_count == $i){
					$html .= '<span class="btn_add_custom_domain" style="padding: 10px;color: green;font-size: 20px;"><i class="fa fa-plus"></i></span>';
				}
		$html .= '</div>';
		$i++;
		}
	$html .= '</div>';	
	$html .= '<button class="array_count" value="'.$array_count.'" style="display:none;"></button>';
	/*$html .= '<button class="btn_add_custom_domain btn_green">Add New Custom Domain &nbsp; 
		  <span style="font-size:16px; font-weight:bold;">+ </span> 	
		</button> 
		  <button class="array_count" value="'.$array_count.'" style="display:none;"></button><span id="custom_error"></span>';*/
	return $html;
}

add_shortcode('activator_fname','activator_fname');
function activator_fname(){ 
	/*$post_id = get_the_ID(); 
	$author_id = get_post_field( 'post_author', $post_id );			
	return get_the_author_meta( 'first_name', $author_id );*/
	$current_username = get_current_user_id();
	return get_the_author_meta( 'first_name', $current_username );
}

add_shortcode('activator_lname','activator_lname');
function activator_lname(){
	/*$post_id = get_the_ID(); 
	$author_id = get_post_field( 'post_author', $post_id );
	return get_the_author_meta( 'last_name', $author_id );	*/
	$current_username = get_current_user_id();
	return get_the_author_meta( 'last_name', $current_username );
}

add_shortcode('creator_fname','creator_fname');
function creator_fname(){ 
	$post_id = get_the_ID(); 
	$author_id = get_post_field( 'post_author', $post_id );	
	if($author_id){
		return get_the_author_meta( 'first_name', $author_id );
	}else{
		return get_the_author_meta( 'first_name', get_current_user_id() );
	}
}

add_shortcode('creator_lname','creator_lname');
function creator_lname(){
	$post_id = get_the_ID(); 
	$author_id = get_post_field( 'post_author', $post_id );
	if($author_id){
		return get_the_author_meta( 'last_name', $author_id );
	}else{
		return get_the_author_meta( 'last_name', get_current_user_id() );
	}
}

add_shortcode('post_url','post_url');
function post_url(){
	$post_id = get_the_ID(); 
	return get_permalink($post_id);	
}
function funnelmates_link_direct_shortcode( $atts = array(), $content = null ) {
	$a = shortcode_atts( array(
        'id' => '0',
		'value' => '',
    ), $atts );
	
	if(esc_attr($a['id']) == '0'){
		$id = get_the_ID();
	}else{
		$id = esc_attr($a['id']);
	}
	
	if(esc_attr($a['value']) == ''){
		$activator_id = '';
	}else{
		$activator_id = get_current_user_id();
	}
	
	global $wpdb;
	if($id && $id != '0'){
		if($activator_id && $activator_id !='0'){
			if($content){
				return '<a href="https://funnelmates.com/f/'.$id.'/'.$activator_id.'/">' . $content . '</a>';
			}else{
				return '<a href="https://funnelmates.com/f/'.$id.'/'.$activator_id.'/">https://funnelmates.com/f/'.$id.'/'.$activator_id.'/</a>';
			}
		}else{
			if($content){
				return '<a href="https://funnelmates.com/f/'.$id.'/{SUBSCRIBER_CCID}/">' . $content . '</a>';
			}else{
				return '<a href="https://funnelmates.com/f/'.$id.'/{SUBSCRIBER_CCID}/">https://funnelmates.com/f/'.$id.'/{SUBSCRIBER_CCID}/</a>';
			}
		}
	}
}
add_shortcode( 'funnel_link', 'funnelmates_link_direct_shortcode' );
// add_shortcode( 'funnelmates_link', 'funnelmates_link_followup' );
// function funnelmates_link_followup( $atts = array(), $content = null ){
	// return 'here';
	/*$a = shortcode_atts( array(
        'id' => '0',
    ), $atts );
	if(esc_attr($a['id']) == '0'){
		$id = get_the_ID();
	}else{
		$id = esc_attr($a['id']);
	}
	global $wpdb;
	if($id && $id != '0'){
		if($content){
			return '<a href="https://funnelmates.com/f/'.$id.'/'.get_current_user_id().'/">' . $content . '</a>';
		}else{
			return '<a href="https://funnelmates.com/f/'.$id.'/'.get_current_user_id().'/">https://funnelmates.com/f/'.$id.'/'.get_current_user_id().'/</a>';
		}
	}*/
// }
add_shortcode( 'fm_mail_list_settings', 'funnelmates_fm_mail_list_settings_shortcode' );
function funnelmates_fm_mail_list_settings_shortcode(){
	// return '';
	$countries = array('Choose','Afghanistan','Albania','Algeria','American Samoa','Andorra','Angola','Anguilla','Antigua','Argentina','Armenia','Aruba','Australia','Austria','Azerbaijan','Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia','Bosnia and Herzegovina','Botswana','Brazil','British Indian Ocean Territory','British Virgin Islands','Brunei','Bulgaria','Burkina Faso','Burma Myanmar','Burundi','Cambodia','Cameroon','Canada','Cape Verde','Cayman Islands','Central African Republic','Chad','Chile','China','Colombia','Comoros','Cook Islands','Costa Rica','Côte d\'Ivoire','Croatia','Cuba','Cyprus','Czech Republic','Democratic Republic of Congo','Denmark','Djibouti','Dominica','Dominican Republic','Ecuador','Egypt','El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia','Falkland Islands','Faroe Islands','Federated States of Micronesia','Fiji','Finland','France','French Guiana','French Polynesia','Gabon','Georgia','Germany','Ghana','Gibraltar','Greece','Greenland','Grenada','Guadeloupe','Guam','Guatemala','Guinea','Guinea-Bissau','Guyana','Haiti','Honduras','Hong Kong','Hungary','Iceland','India','Indonesia','Iran','Iraq','Ireland','Israel','Italy','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Kosovo','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein','Lithuania','Luxembourg','Macau','Macedonia','Madagascar','Malawi','Malaysia','Maldives','Mali','Malta','Marshall Islands','Martinique','Mauritania','Mauritius','Mayotte','Mexico','Moldova','Monaco','Mongolia','Montenegro','Montserrat','Morocco','Mozambique','Namibia','Nauru','Nepal','Netherlands','Netherlands Antilles','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island','North Korea','Northern Mariana Islands','Norway','Oman','Pakistan','Palau','Palestine','Panama','Papua New Guinea','Paraguay','Peru','Philippines','Poland','Portugal','Puerto Rico','Qatar','Republic of the Congo','Réunion','Romania','Russia','Rwanda','Saint Barthélemy','Saint Helena','Saint Kitts and Nevis','Saint Martin','Saint Pierre and Miquelon','Saint Vincent and the Grenadines','Samoa','San Marino','São Tomé and Príncipe','Saudi Arabia','Senegal','Serbia','Seychelles','Sierra Leone','Singapore','Slovakia','Slovenia','Solomon Islands','Somalia','South Africa','South Korea','Spain','Sri Lanka','St. Lucia','Sudan','Suriname','Swaziland','Sweden','Switzerland','Syria','Taiwan','Tajikistan','Tanzania','Thailand','The Bahamas','The Gambia','Timor-Leste','Togo','Tokelau','Tonga','Trinidad and Tobago','Tunisia','Turkey','Turkmenistan','Turks and Caicos Islands','Tuvalu','Uganda','Ukraine','United Arab Emirates','United Kingdom','United States','Uruguay','US Virgin Islands','Uzbekistan','Vanuatu','Vatican City','Venezuela','Vietnam','Wallis and Futuna','Yemen','Zambia','Zimbabwe');
	$curuser = wp_get_current_user();
	$_mail_settings = get_user_meta(get_current_user_id(),'_mail_settings',true);
	// return $_mail_settings;
	if($_mail_settings){
		$msce_checked = '';
		$mfwe_checked = '';
		$mun_checked = '';
	}else{
		$msce_checked = 'checked';
		$mfwe_checked = 'checked';
		$mun_checked = 'checked';
	}
	if($_mail_settings['mls_from_name']){
		$name = $_mail_settings['mls_from_name'];
	}else{
		$name = $curuser->user_firstname.' '.$curuser->user_lastname;
	}
	
	$mls_from_email = ($_mail_settings['mls_from_email'])?$_mail_settings['mls_from_email']:$curuser->user_email;
	if($_mail_settings['mls_address1']){
		$_mail_settings_mls_name = ($_mail_settings['mls_name'])?$_mail_settings['mls_name']:'';
		$_mail_settings_mls_email_subject = ($_mail_settings['mls_email_subject'])?$_mail_settings['mls_email_subject']:'';
		$_mail_settings_mls_company = ($_mail_settings['mls_company'])?$_mail_settings['mls_company']:'';
		$_mail_settings_mls_state = ($_mail_settings['mls_state'])?$_mail_settings['mls_state']:'';
		$_mail_settings_mls_address1 = ($_mail_settings['mls_address1'])?$_mail_settings['mls_address1']:'';
		$_mail_settings_mls_city = ($_mail_settings['mls_city'])?$_mail_settings['mls_city']:'';
		$_mail_settings_mls_address2 = ($_mail_settings['mls_address2'])?$_mail_settings['mls_address2']:'';
		$_mail_settings_mls_zip = ($_mail_settings['mls_zip'])?$_mail_settings['mls_zip']:'';
		$_mail_settings_mls_country = ($_mail_settings['mls_country'])?$_mail_settings['mls_country']:'';
		$_mail_settings_mls_phone = ($_mail_settings['mls_phone'])?$_mail_settings['mls_phone']:'';
		$_mail_settings_mls_homepage = ($_mail_settings['mls_homepage'])?$_mail_settings['mls_homepage']:'';
	}
	$html = '<div class="mail_list_settings_cls">';
		$html .= '<div class="mls_identity">';
			$html .= '<table class="tbl_mls" style="width:100%;">';
				$html .= '<tr>';
					$html .= '<th colspan="2"><h5>Identity</h5></th>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_name">Name <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_name" id="mls_name" value="'.$_mail_settings_mls_name.'" />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_from_email">Default From email address <span class="req">*</span></label>';
						$html .= '<input type="email" name="mls_from_email" id="mls_from_email" value="'.$mls_from_email.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_from_name">Default From name <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_from_name" id="mls_from_name" value="'.$name.'" />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_email_subject">Default email subject</label>';
						$html .= '<input type="text" name="mls_email_subject" id="mls_email_subject" value="'.$_mail_settings_mls_email_subject.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<th colspan="2"><h5>Contact information</h5></th>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_company">Company / Organization <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_company" id="mls_company" value="'.$_mail_settings_mls_company.'" />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_state">State / Province / Region <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_state" id="mls_state" value="'.$_mail_settings_mls_state.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_address1">Address 1 <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_address1" id="mls_address1" value="'.$_mail_settings_mls_address1.'" />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_city">City <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_city" id="mls_city" value="'.$_mail_settings_mls_city.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_address2">Address 2</label>';
						$html .= '<input type="text" name="mls_address2" id="mls_address2" value="'.$_mail_settings_mls_address2.'" />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_zip">Zip / Postal code <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_zip" id="mls_zip" value="'.$_mail_settings_mls_zip.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_country">Country <span class="req">*</span></label>';
						// $html .= '<input type="text" name="mls_country" id="mls_country" value="'.$_mail_settings['mls_country'].'" />';
						
						$html .= '<select name="mls_country">';
							foreach($countries as $k=>$cn){
								if($_mail_settings_mls_country == $k){
									$selected_c = "selected";
								}else{
									$selected_c = "";
								}
								$html .= '<option value="'.$k.'" '.$selected_c.'>'.$cn.'</option>';
							}
						$html .= '</select>';
						
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_phone">Phone <span class="req">*</span></label>';
						$html .= '<input type="text" name="mls_phone" id="mls_phone" value="'.$_mail_settings_mls_phone.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td>';
						$html .= '<label for="mls_email">Email <span class="req">*</span></label>';
						$html .= '<input type="email" name="mls_email" id="mls_email" value="'.$curuser->user_email.'" disabled />';
					$html .= '</td>';
					$html .= '<td>';
						$html .= '<label for="mls_homepage">Home page</label>';
						$html .= '<input type="text" name="mls_homepage" id="mls_homepage" value="'.$_mail_settings_mls_homepage.'" />';
					$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td colspan="2">';
					$html .= '<p class="mls_error"></p>';
					$html .= '<button class="btn_green btn" id="save_mls" type="button">Save</button>';
					$html .= '</td>';
				$html .= '</tr>';
			$html .= '</table>';
		$html .= '</div>';
	$html .= '</div>';
	return $html;
}

add_shortcode( 'funnel_agency', 'funnel_agency' );
function funnel_agency(){
	require_once(get_stylesheet_directory().'/funnel-agency.php');
}

add_shortcode( 'landing_page_url', 'landing_page_url' );
function landing_page_url($atts = array(), $content = null){
	
	$a = shortcode_atts( array(
        'id' => '0'
    ), $atts );
	
	if(esc_attr($a['id']) == '0'){
		$id = get_the_ID();
	}else{
		$id = esc_attr($a['id']);
	}	
	$activator_id = get_current_user_id();
	return 'https://funnelmates.com/f/'.$id.'/'.$activator_id; 
}