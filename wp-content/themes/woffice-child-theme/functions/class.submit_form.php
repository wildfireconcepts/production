<?php
class FM_Submit_Landing {
	function __construct() {
		$this->wpdb = $GLOBALS['wpdb'];
	}
	public function fm_subscription($post_id,$data,$listuid){
		$apiendpoint = ACELLE_DOMAIN.'/api/v1/subscribers?list_uid='.$listuid; //Acelle V4.0
		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$response = $this->curl_post($apiendpoint,http_build_query($data),$headers);
		return $response;
	}
	public function category_subscription($post_id,$data){
		global $wpdb;
		$post_category = get_the_category($post_id);
		$profit_system = $wpdb->get_var("SELECT profit_system FROM `wp_fm_marketplace_items` WHERE post_id='$post_id'");
		if(get_post_meta($post_id,'is_funnel_public',true) == '1' && $profit_system == '1'){
			foreach($post_category as $pc){
				$cat_id = $pc->term_id;
				$cat_list_uid = get_field('acelle_list_uid', 'category_'.$cat_id);
				if($cat_list_uid){
					// $category_list_uid = ACELLE_DOMAIN.'/api/v1/lists/'.$cat_list_uid.'/subscribers/store?';
					$category_list_uid = ACELLE_DOMAIN.'/api/v1/subscribers?list_uid='.$cat_list_uid; //Acelle V4.0
					$headers = array();
					$headers[] = 'Accept: application/json';
					$headers[] = 'Content-Type: application/x-www-form-urlencoded';
					
					$response = $this->curl_post($category_list_uid,http_build_query($data),$headers);
					
					return $response;
				}
			}
		}else{
			return false;
		}
	}
	public function gotowebinar($post_id,$CCID,$data){
		global $wpdb;
		$webinar_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='webinar'");
		$webinar_ar_integration = $webinar_ar_integration->list;
		if($webinar_ar_integration){
			$gotowebinar = $this->refresh_gotowebinar($CCID);
			$organizer_key = $gotowebinar->organizer_key;
			$url_webinar = "https://api.getgo.com/G2W/rest/v2/organizers/{$organizer_key}/webinars/{$webinar_ar_integration}/registrants";
			$param_array = array(
				"firstName" => "FunnelMates",
				"lastName" => "User",
				"email" => $data,
				"source" => 'FunnelMates'
			);
			$headers = array();
			$headers[] = 'Authorization: Bearer '.$gotowebinar->access_token;
			$headers[] = 'Accept: application/vnd.citrix.g2wapi-v1.1+json';
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			
			$response = $this->curl_post($url_webinar,json_encode($param_array),$headers);
			
			return $response;
		}
	}
	public function everwebinar($post_id,$CCID,$data){
		global $wpdb;
		$everwebinar_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='everwebinar'");
		$everwebinar_ar_integration = $everwebinar_ar_integration->list;
		if($everwebinar_ar_integration){
			// $everwebinar_apikey = "930b2329-82e6-4170-8a24-eead5ed69c8b";
			$everwebinar_apikey = get_user_meta($CCID,'fm_everwebinar_apikey',true);
			$post_fields = "api_key=$everwebinar_apikey&webinar_id=$everwebinar_ar_integration&first_name=FunnelMates&last_name=User&email=$data&schedule=0";
			$headers = array();
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			
			$response = $this->curl_post('https://api.webinarjam.com/everwebinar/register',$post_fields,$headers);
			return $response;
		}
	}
	public function zapier($email,$CCID){
		global $wpdb;
		$zapier_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='zapier'");
		
		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/json';
		
		$response = $this->curl_post($zapier_ar_integration->list,json_encode(array("email" => $email,"name" => 'FunnelMates User',"affid" => $CCID)),$headers);
		
		return $response;
	}
	public function sendlane($post_id,$email,$CCID){
		global $wpdb;
		$sendlane_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='sendlane'");
		$list_id = $sendlane_ar_integration->list;
		if($list_id){
			$apikey = get_user_meta($CCID,'_ccv3_sendlane_apikey',true);
			$apihashkey = get_user_meta($CCID,'_ccv3_sendlane_apihashkey',true);
			if($apikey && $apihashkey){
				$list_id = $sendlane_ar_integration->list;
				$curl = curl_init();
				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://sendlane.com/api/v1/list-subscriber-add",
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "POST",
				  CURLOPT_POSTFIELDS => array('api' => $apikey,'hash' => $apihashkey,'email' => $email,'list_id' => $list_id/*,'optional
				' => 'tag_ids, tag_names, Custom Field 1, Custom Field 2'*/),
				));

				$response = curl_exec($curl);
				curl_close($curl);
				return $response;
			}
		}else{
			return false;
		}
	}
	public function getresponse($post_id,$CCID,$email){
		global $wpdb;
		$get_response_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='get_response'");
		$list_id = $get_response_ar_integration->list;
		if($list_id){
			$api_key = get_user_meta($CCID,'_ccv3_getresponse_apikey',true);
			$data = array(
				"name" => "FunnelMates User",
				"campaign" => array (
					"campaignId" => $list_id
				),
				"email" => $email,
				"dayOfCycle" => "0"
			);
			$data_string = json_encode($data);
			$headers = array(
				'Content-Type: application/json',
				'X-Auth-Token: api-key '.$api_key,
			);
			
			$response = $this->curl_post('https://api.getresponse.com/v3/contacts',$data_string,$headers);
			
			return $response;
		}
	}
	public function mailchimp($post_id,$CCID,$email){
		global $wpdb;
		$mailchimp_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='mailchimp'");
		$list_id = $mailchimp_ar_integration->list;
		if($list_id){
			$api_key = get_user_meta($CCID,'_ccv3_mailchimp_apikey',true);
			$status = 'subscribed';
			$merge_fields = array('FNAME' => 'FunnelMates','LNAME' => 'User');
			$response = $this->mailchimp_subscriber_status($email,$status,$list_id,$api_key,$merge_fields);
			return $response;
		}
	}
	
	public function drip($post_id,$CCID,$email){
		global $wpdb;
		$drip_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='drip'");
		$list_id = $drip_ar_integration->list;
		if($list_id){
			$api_key = get_user_meta($CCID,'_ccv3_drip_apikey',true);
			$account_id = get_user_meta($CCID,'_ccv3_drip_account_id',true);
			$json = '{
				"subscribers": [{
				  "email": "'.$email.'",
				  "utc_offset": 660,
				  "double_optin": true,
				  "starting_email_index": 0,
				  "reactivate_if_removed": true,
				  "custom_fields": {
					"shirt_size": "Medium"
				  }
				}]
			}';
			// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.getdrip.com/v2/'.$account_id.'/campaigns/'.$list_id.'/subscribers');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			$post = $json;
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . '');

			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'User-Agent: Your App Name (www.yourapp.com)';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);			
			return $result;
		}
	}
	
	public function convertkit($post_id,$CCID,$email){
		global $wpdb;
	
		$convertkit_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='convertkit'");
		$list_id = $convertkit_ar_integration->list;
		
		if($list_id){	
			$api_key = get_user_meta($CCID,'_ccv3_convertkit_apikey',true);
			$tag_id = get_user_meta($CCID,'_ccv3_convertkit_tag_id',true);
			$json = '{
				"api_key": "'.$api_key.'",
				"email": "'.$email.'"
			 }';
			// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.convertkit.com/v3/tags/'.$list_id.'/subscribe');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

			$headers = array();
			$headers[] = 'Content-Type: application/json; charset=utf-8';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			
			//print_r($result);
			//$response = json_decode($result);
			return $result;
		}
	}
	
	public function mailerlite($post_id,$CCID,$email){
		global $wpdb;
	
		$mailerlite_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='mailerlite'");
		$list_id = $mailerlite_ar_integration->list;
		if($list_id){
			$api_key = get_user_meta($CCID,'_ccv3_mailerlite_apikey',true);
			$json = '{
				"email": "'.$email.'"
			 }';
			// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.mailerlite.com/api/v2/groups/'.$list_id.'/subscribers');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

			$headers = array();
			$headers[] = 'Content-Type: application/json';
			$headers[] = 'X-Mailerlite-Apikey: '.$api_key.'';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			
			return $result;
		}
	}
	
	
	public function sendiio($post_id,$CCID,$email){
		global $wpdb;
		$sendiio_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='sendiio'");		
		$list_id = $sendiio_ar_integration->list;		
		if($list_id){
			$curl = curl_init();
			curl_setopt_array($curl, array(
			 CURLOPT_URL => 'https://sendiio.com/api/v1/lists/subscribe/json',
			 CURLOPT_RETURNTRANSFER => true,
			 CURLOPT_ENCODING => '',
			 CURLOPT_MAXREDIRS => 10,
			 CURLOPT_TIMEOUT => 0,
			 CURLOPT_FOLLOWLOCATION => true,
			 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			 CURLOPT_CUSTOMREQUEST => 'POST',
			 CURLOPT_POSTFIELDS => array(
				'email_list_id' => $list_id,
				'email' => $email,
			  ),
			));
						
			$result = curl_exec($curl);				
			curl_close($curl);
			return $result;
		} 
	}
	public function mailsqaud($post_id,$CCID,$email){
		global $wpdb;
		$mailsqaud_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='mailsqaud'");		
		$list_id = $mailsqaud_ar_integration->list;		
		$api_token = get_user_meta($CCID,'_ccv3_mailsqaud_apitoken',true);
		$api_login = get_user_meta($CCID,'_ccv3_mailsqaud_apilogin',true);
		if($list_id){
			$postData['list_id']    = $list_id;
			$postData['email']      = $email;
			// $postData['first_name'] = 'Robert';
			// $postData['last_name']  = 'Daniel';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,'https://mailsqaud.com/API/addSubscriber');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_POST, count($postData));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'APIToken: '.$api_token, 'Login: '.$api_login));
			$data = curl_exec($ch);
			return $data;
			/*if (curl_errno($ch)) {

			   print "Error: " . curl_error($ch);

			} else {

			   // Show me the result
			   print ($data);
			   curl_close($ch);
			}*/
		} 
	}
	
	public function aweber_req($client, $accessToken, $url) {
		$collection = array();
		while (isset($url)) {
			$request = $client->get($url,
				['headers' => ['Authorization' => 'Bearer ' . $accessToken]]
			);
			$body = $request->getBody();
			$page = json_decode($body, true);
			$collection = array_merge($page['entries'], $collection);
			$url = isset($page['next_collection_link']) ? $page['next_collection_link'] : null;
		}
		return $collection;
	}
	public function curl_get_aweber($url,$headers){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		return json_decode($result);
	}
	public function aweber($post_id,$email,$CCID){
		global $wpdb;
		$aweber_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='aweber'");
		// print_r($aweber_ar_integration);exit;
		$list_id = $aweber_ar_integration->list;
		if($list_id){
			require_once(get_stylesheet_directory().'/includes/oauth2/vendor/autoload.php');
			$client = new GuzzleHttp\Client();
			$provider = new League\OAuth2\Client\Provider\GenericProvider([
				'clientId'              	=> 'CGPdJJYONkMmvDYO4P6JCDnNzJtvoYUq',    // The client ID assigned to you by the provider
				'clientSecret'          	=> 'np8YZI3zpI6MjAYQxbZTuKPS0mi2mv5F',    // The client password assigned to you by the provider
				'redirectUri'           	=> 'https://funnelmates.com/settings/',
				'scopes' 					=> $scopes,
				'scopeSeparator' 			=> ' ',
				'urlAuthorize'          	=> 'https://auth.aweber.com/oauth2/authorize',
				'urlAccessToken'        	=> 'https://auth.aweber.com/oauth2/token',
				'urlResourceOwnerDetails' 	=> 'https://api.aweber.com/1.0/accounts'
			]);
			$existingAccessToken = get_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', true);
			if ($existingAccessToken->hasExpired()) {
				$newAccessToken = $provider->getAccessToken('refresh_token', [
					'refresh_token' => $existingAccessToken->getRefreshToken()
				]);
				update_user_meta(get_current_user_id(), 'fm_aweber_access_token', $newAccessToken->getToken());
				update_user_meta(get_current_user_id(), 'fm_aweber_token_refresh', $newAccessToken->getRefreshToken());
				update_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', $newAccessToken);
			}
			$finalAccessToken = get_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', true);
			$resourceowner = get_user_meta(get_current_user_id(), 'fm_aweber_resourceowner', true);
			$accounts = $this->aweber_req($client, $finalAccessToken->getToken(), 'https://api.aweber.com/1.0/accounts');
			$lists_collection_link = $accounts[0]['lists_collection_link'].'/'.$list_id.'/subscribers';
			
			$params = array(
				'ws.op' => 'find',
				'email' => $email
			);
			$findUrl = $lists_collection_link . '?' . http_build_query($params);
			$headers = array();
			$headers[] = 'Authorization: Bearer '.$finalAccessToken->getToken();
			$foundSubscribers = $this->curl_get_aweber($findUrl,$headers);
			$foundSubscribers = $foundSubscribers->entries;
			// echo '<pre>';
			// print_r($foundSubscribers);exit;
			if (isset($foundSubscribers[0]->self_link)) {
				// echo $foundSubscribers[0]->self_link;exit;
				$data = array(
					'custom_fields' => array('affid' => $CCID),
					// 'tags' => array('add' => array('prospect'))
				);
				$subscriberUrl = $foundSubscribers[0]->self_link;
				$subscriberResponse = $client->patch($subscriberUrl, [
						'json' => $data, 
						'headers' => ['Authorization' => 'Bearer ' . $finalAccessToken->getToken()]
					])->getBody();
				$subscriber = json_decode($subscriberResponse, true);
			}else{				
				$data = array(
					'email' => $email,
					'custom_fields' => array('CCID' => $CCID),
					// 'tags' => array('prospect')
				);
				$body = $client->post($lists_collection_link, [
					'json' => $data, 
					'headers' => ['Authorization' => 'Bearer ' . $finalAccessToken->getToken()]
				]);
				$subscriber = json_decode($subscriberResponse, true);
			}
			return $subscriber;
		}
	}
	/*public function aweber($post_id,$email,$CCID){
		global $wpdb;
		$aweber_ar_integration = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$CCID' AND type='aweber'");
		$list_id = $aweber_ar_integration->list;
		if($list_id){
			// require_once(get_stylesheet_directory().'/includes/AWeber-API-PHP-Library-master/aweber_api/aweber.php');
			$accessToken = get_user_meta($CCID,'fm_aweber_access_token',true);
			$accessTokenSecret = get_user_meta($CCID,'fm_aweber_token_secret',true);	
			
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.aweber.com/1.0/accounts');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


			$headers = array();
			$headers[] = 'Authorization: Bearer '.$accessToken;
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			print_r($result);exit;
			$aweber = new AWeberAPI($accessToken, $accessTokenSecret);		
			try {
				$account = $aweber->getAccount($accessToken, $accessTokenSecret);
				print_r($account);exit;
				$account_id = $account->id;

				$list = $account->loadFromUrl("/accounts/{$account_id}/lists/{$list_id}");

				# create a subscriber
				$params = array(
					'email' => $email,
					'name' => $name,
				);

				$subscribers = $list->subscribers;
				$new_subscriber = $subscribers->create($params);

				# success!
				return "A new subscriber was added to the $list->name list!";

			} catch(AWeberAPIException $exc) {
				echo "Type: $exc->type<br>";
				echo "Msg : $exc->message<br>";
				exit(1);
			}
		}
	}*/
	public function mailchimp_subscriber_status( $email, $status, $list_id, $api_key, $merge_fields = array('FNAME' => '','LNAME' => '') ){
		$data = array(
			'apikey'        => $api_key,
				'email_address' => $email,
			'status'        => $status,
			'merge_fields'  => $merge_fields
		);
		$mch_api = curl_init(); // initialize cURL connection
	 
		curl_setopt($mch_api, CURLOPT_URL, 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($data['email_address'])));
		curl_setopt($mch_api, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: Basic '.base64_encode( 'user:'.$api_key )));
		curl_setopt($mch_api, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($mch_api, CURLOPT_RETURNTRANSFER, true); // return the API response
		curl_setopt($mch_api, CURLOPT_CUSTOMREQUEST, 'PUT'); // method PUT
		curl_setopt($mch_api, CURLOPT_TIMEOUT, 10);
		curl_setopt($mch_api, CURLOPT_POST, true);
		curl_setopt($mch_api, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($mch_api, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
	 
		$result = curl_exec($mch_api);
		return $result;
	}
	public function curl_post($url,$post_fields,$headers){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		return $result;
	}
	public function refresh_gotowebinar($user){
		$fm_gotowebinar = get_user_meta($user,'fm_gotowebinar',true);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://api.getgo.com/oauth/v2/token');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=refresh_token&refresh_token=".$fm_gotowebinar->refresh_token);

		$headers = array();
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		$headers[] = 'Authorization: Basic '.base64_encode('d92b304a-9344-4cb4-a012-56947c8151ff:UcRhLAxv81il8dp2s8mxHrSN');
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			return 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$rs = json_decode($result);
		update_user_meta($user,'fm_gotowebinar', $rs);
		return $rs;
	}
}