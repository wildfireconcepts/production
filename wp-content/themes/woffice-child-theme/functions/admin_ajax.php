<?php
add_action('wp_ajax_search_funnels','search_funnels');
add_action('wp_ajax_landing_page_create','landing_page_create');
add_action('wp_ajax_confirm_page_create','confirm_page_create');
add_action('wp_ajax_thankyou_page_create','confirm_page_create');
add_action('wp_ajax_download_page_create','confirm_page_create');
add_action('wp_ajax_save_general_settings','save_general_settings');
add_action('wp_ajax_save_mail_list_settings','save_mail_list_settings');
add_action('wp_ajax_check_mail_list_settings','check_mail_list_settings');
// add_action('wp_ajax_connect_webinar','connect_webinar');
add_action('wp_ajax_disconnect_webinar','disconnect_webinar');
add_action('wp_ajax_connect_everwebinar','connect_everwebinar');
add_action('wp_ajax_disconnect_everwebinar','disconnect_everwebinar');
add_action('wp_ajax_connect_aweber','connect_aweber');
add_action('wp_ajax_disconnect_aweber','disconnect_aweber');
add_action('wp_ajax_connect_sendlane','connect_sendlane');
add_action('wp_ajax_disconnect_sendlane','disconnect_sendlane');
add_action('wp_ajax_connect_mailchimp','connect_mailchimp');
add_action('wp_ajax_disconnect_mailchimp','disconnect_mailchimp');
add_action('wp_ajax_connect_getresponse','connect_getresponse');
add_action('wp_ajax_connect_mailsqaud','connect_mailsqaud');
add_action('wp_ajax_disconnect_getresponse','disconnect_getresponse');
add_action('wp_ajax_disconnect_mailsqaud','disconnect_mailsqaud');
add_action('wp_ajax_connect_constant_contact','connect_constant_contact');
add_action('wp_ajax_convertkit_connect','convertkit_connect');
add_action('wp_ajax_disconnect_convertkit','disconnect_convertkit');
add_action('wp_ajax_drip_connect','drip_connect');
add_action('wp_ajax_disconnect_drip','disconnect_drip');
add_action('wp_ajax_mailerlite_connect','mailerlite_connect');
add_action('wp_ajax_disconnect_mailerlite','disconnect_mailerlite');
add_action('wp_ajax_sendiio_connect','sendiio_connect');
add_action('wp_ajax_disconnect_sendiio','disconnect_sendiio');
add_action('wp_ajax_delete_funnel','delete_funnel');
// add_action('wp_ajax_template_search_by_keyword','template_search_by_keyword');
add_action('wp_ajax_get_edit_funnel_pages','get_edit_funnel_pages');
add_action('wp_ajax_get_scripts_funnel_pages','get_scripts_funnel_pages');
add_action('wp_ajax_save_funnel_scripts','save_funnel_scripts');
add_action('wp_ajax_publish_public_funnel','publish_public_funnel');
add_action('wp_ajax_check_funnel_bundle','check_funnel_bundle');
add_action('wp_ajax_publish_funnel_finalize','publish_funnel_finalize');
add_action('wp_ajax_funnel_submission_confirmation','funnel_submission_confirmation');
add_action('wp_ajax_private_submission','private_submission');
add_action('wp_ajax_private_submission_finalize','private_submission_finalize');
add_action('wp_ajax_unpublish_funnel','unpublish_funnel');
add_action('wp_ajax_check_if_funnel_published','check_if_funnel_published');
add_action('wp_ajax_submit_public_funnel_settings','submit_public_funnel_settings_function');
add_action('wp_ajax_update_promotional_tools','update_promotional_tools');
add_action('wp_ajax_update_promotional_tools_position','update_promotional_tools_position');
add_action('wp_ajax_remove_promotional_tool','remove_promotional_tool');
add_action('wp_ajax_funnel_website_links','funnel_website_links');
add_action('wp_ajax_unlock_funnel','unlock_funnel');
add_action('wp_ajax_unlock_funnel_finalize','unlock_funnel_finalize');
add_action('wp_ajax_payment_withdrawal','payment_withdrawal');
add_action('wp_ajax_paypal_withdrwal_request','paypal_withdrwal_request');
add_action('wp_ajax_withdrawal_search_email','withdrawal_search_email');
add_action('wp_ajax_withdrawal_accept_request','withdrawal_accept_request');
add_action('wp_ajax_withdrawal_reject_request','withdrawal_reject_request');
add_action('wp_ajax_withdrawal_status_tab','withdrawal_status_tab');
add_action('wp_ajax_open_funnel_review_modal','funnel_review_modal');
add_action('wp_ajax_active_funnel_status','active_funnel_status');
add_action('wp_ajax_delete_inactive_funnel','delete_inactive_funnel');
add_action('wp_ajax_save_followup_email','save_followup_email');
add_action('wp_ajax_get_followup_email','get_followup_email');
add_action('wp_ajax_submit_website_link','submit_website_link');
add_action('wp_ajax_email_link_modal','email_link_modal');
add_action('wp_ajax_upload_screenshot','upload_screenshot');
add_action('wp_ajax_funnel_viewascustomer','funnel_viewascustomer');
add_action('wp_ajax_search_funnel','search_funnel');
add_action('wp_ajax_get_category_post','get_category_post');
add_action('wp_ajax_get_author_post','get_author_post');
add_action('wp_ajax_get_store_items','get_store_items');
add_action('wp_ajax_get_funnel_items','get_funnel_items');
add_action('wp_ajax_get_funnel_buy_purchase_data','get_funnel_buy_purchase_data');
add_action('wp_ajax_validate_link_identifier','validate_link_identifier');
add_action('wp_ajax_edit_external_link_data','edit_external_link_data');
add_action('wp_ajax_delete_external_link_data','delete_external_link_data');
add_action('wp_ajax_update_aff_external_link','update_aff_external_link');
add_action('wp_ajax_generate_funnel_links','generate_funnel_links');
add_action('wp_ajax_save_custom_url','save_custom_url');
add_action('wp_ajax_remove_custom_url','remove_custom_url');
add_action('wp_ajax_search_funnels_template','search_funnels_template');
add_action('wp_ajax_open_affiliate_requests_links','open_affiliate_requests_links');
add_action('wp_ajax_download_vid','download_vid');
add_action('wp_ajax_funnel_integrations_activator','funnel_integrations_activator');
add_action('wp_ajax_save_funnel_integration','save_funnel_integration');
add_action('wp_ajax_remove_funnel_integration','remove_funnel_integration');
add_action('wp_ajax_open_download_resource','open_download_resource');
add_action('wp_ajax_show_followup_email','show_followup_email');
add_action('wp_ajax_add_followup_emails_to_mailserver','add_followup_emails_to_mailserver');
add_action('wp_ajax_get_review_data_admin','get_review_data_admin');
add_action('wp_ajax_get_feedback_data_admin','get_feedback_data_admin');
add_action('wp_ajax_get_affiliaterequests_admin','get_affiliaterequests_admin');
add_action('wp_ajax_save_requested_feedback','save_requested_feedback');
add_action('wp_ajax_user_improvising_funnel','user_improvising_funnel');
add_action('wp_ajax_cancel_funnel_review','cancel_funnel_review');
add_action('wp_ajax_approve_funnel_final','approve_funnel_final');
add_action('wp_ajax_get_followupemails_admin','get_followupemails_admin');
add_action('wp_ajax_get_promotionaltools_admin','get_promotionaltools_admin');
add_action('wp_ajax_clone_box','clone_box');
add_action('wp_ajax_clone_funnel','clone_funnel');
add_action('wp_ajax_show_funnel_clicks_and_coversions','show_funnel_clicks_and_coversions');
add_action('wp_ajax_force_review','force_review');
add_action('wp_ajax_delete_tracking_id','delete_tracking_id');
add_action('wp_ajax_refresh_tracking_id','refresh_tracking_id');
add_action('wp_ajax_affiliate_products','affiliate_products');
add_action('wp_ajax_edit_whitelabel_external_link_data','edit_whitelabel_external_link_data');
add_action('wp_ajax_update_whitelabel_external_link','update_whitelabel_external_link');
add_action('wp_ajax_save_free_funnel','save_free_funnel');
add_action('wp_ajax_delete_free_funnel','delete_free_funnel');
add_action('wp_ajax_apply_coupon','apply_coupon');
function search_funnels(){
	global $wpdb;
	$search_str = $_POST['search_str'];
	// $published = $_POST['published'];
	// $unpublished = $_POST['unpublished'];
	// $under_review = $_POST['under_review'];
	$c_id = get_current_user_id();
	$html = array();
	$purchase_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_postmeta.meta_key = '_purchase_info_$c_id' AND wp_posts.post_title LIKE '%$search_str%' AND wp_posts.post_author not in($c_id) and (wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' OR wp_setc_activated_funnels.type = 'whitelabel') GROUP BY(wp_posts.ID) ORDER by wp_postmeta.meta_value ASC");
	$funnel_id = array();
	foreach($purchase_funnels as $i){
		$funnel_id[] = $i->ID; 
		$funnel_statue = $i->active_status; 
	}
	$created_funnels = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
	$ids = $created_funnels->posts;
	$myarray = array_merge($funnel_id,$ids);
	$all_funnel_id = implode(', ', $myarray); 
	$sort_funnel_id = $wpdb->get_results("SELECT * FROM `wp_posts`INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_posts.ID in($all_funnel_id) AND (wp_postmeta.meta_key = '_purchase_info_$c_id' or wp_postmeta.meta_key = '_created_info_$c_id') ORDER by wp_postmeta.meta_value DESC");
	$new_funnel_id = array();
	foreach($sort_funnel_id as $it){
		$new_funnel_id[] = $it->ID; 
	}
	if($new_funnel_id){
		$wpb_all_query = new WP_Query(array('orderby' => 'post__in', 'setc_title'=> $search_str, 'post__in'=> $new_funnel_id,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1));
		//$wpb_all_query = new WP_Query(array('post__in'=> $myarray,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1));
	}else{
		$wpb_all_query = new WP_Query(array('author'=>get_current_user_id(), 'setc_title'=> $search_str, 'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
	}
	if($wpb_all_query->have_posts()){
		while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
			/*$type = get_post_meta(get_the_ID(),'is_funnel_published');
			if($published){
				if($type == '1'){
					$html[] = get_the_ID();
				}
			}
			if($unpublished){
				if($type == '0'){
					$html[] = get_the_ID();
				}
			}
			if($under_review){
				if($type == 'review' || $type == 'feedback_requested' || $type == 'feedback_requested'){
					$html[] = get_the_ID();
				}
			}*/
			$html[] = get_the_ID();
		endwhile;
	}
	$array = array('html'=>$html);
	$json = json_encode($array);
	echo $json;
	exit;
}
// add_action('wp_ajax_get_single_followupemail','get_single_followupemail');
function landing_page_create(){
	global $wpdb;
	// echo ;
	// global $post;
	$funnel_type = $_POST['funnel_type'];
	$funnel_template_type = $_POST['funnel_template_type'];
	$html = '';
	$current_term = get_term_by('id',$funnel_template_type,'fl-builder-template-category');   
	$date = date('Y-m-d H:i:s');
	
	$funnel_main = get_page_by_title($current_term->name,'OBJECT','fl-builder-template');
	$funnel_confirm = get_page_by_title($current_term->name.' Confirm','OBJECT','fl-builder-template');
	$funnel_download = get_page_by_title($current_term->name.' Download','OBJECT','fl-builder-template');
	$funnel_thanks = get_page_by_title($current_term->name.' Thanks','OBJECT','fl-builder-template');

	/*echo '<pre>';
	print_r($mainpage);
	exit;*/
	// echo $current_term->name;exit;
	$postarr = array(
		'post_author' => get_current_user_id(),
		'post_title' => $_POST['landing_page_name'],
		'post_status' => 'publish',
		'post_type' => 'landing',
		'meta_input' => array(
			'funnel_type' => $funnel_type
		)
	);
	$newpost = wp_insert_post($postarr);
	if($newpost){
		$funnel_template_type = $wpdb->query("INSERT INTO `wp_funnel_template_type` (id,funnel_id,template_type,date) VALUES ('','".$newpost."','".$funnel_template_type."','".$date."')");
		update_post_meta($newpost,'bb_preset_template',$funnel_main->ID);
		$postarr_thanks = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Thank You',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$thanks_post = wp_insert_post($postarr_thanks);
		update_post_meta($thanks_post,'bb_preset_template',$funnel_thanks->ID);
		$postarr_confirm = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Confirm',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$confirm_post = wp_insert_post($postarr_confirm);
		update_post_meta($confirm_post,'bb_preset_template',$funnel_confirm->ID);
		$postarr_download = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Download',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$download_post = wp_insert_post($postarr_download);
		update_post_meta($download_post,'bb_preset_template',$funnel_download->ID);
		$link = get_permalink($newpost);
		$thanks_link = get_permalink($thanks_post);
		$confirm_link = get_permalink($confirm_post);
		$download_link = get_permalink($download_post);
		
		
		$html .= '<div class="pages_created_container" style="width:100%;display:inline-block;text-align:center;">';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/landing.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Landing Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/thanks.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$thanks_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Thanks Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/confirm.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$confirm_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Confirm Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/download.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$download_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Download Page</strong></a>';
			$html .= '</div>';
		$html .= '</div>';
		
		
		///////////////////////////////
		/*$current_user = wp_get_current_user();
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://mail.covertcommissions.com/api/v1/lists?');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$list_array = array(
			"api_token" => ACELLE_APIKEY,
			"name" => $_POST['landing_page_name'],
			"from_email" => $current_user->user_email,
			"from_name" => $current_user->display_name,
			"default_subject" => "Welcome to ".$_POST['landing_page_name'],
			"contact[company]" => "WildfireConcepts",
			"contact[state]" => "Adelaide",
			"contact[address_1]" => "Adelaide",
			"contact[address_2]" => "Adelaide",
			"contact[city]" => "Adelaide",
			"contact[zip]" => "80000",
			"contact[phone]" => "123 456 889",
			"contact[country_id]" => "91",
			"contact[email]" => "support@wildfireconcepts.com",
			"contact[url]" => "http://wildfireconcepts.com",
			"subscribe_confirmation" => "1",
			"send_welcome_email" => "1",
			"unsubscribe_notification" => "1",
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($list_array));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$html .= 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		//print_r($result);
		$result = json_decode($result);
		if($result->status == '1' && $result->list_uid){
			$listid = $result->list_uid;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://mail.covertcommissions.com/api/v1/automations/'.$listid.'/api/call?');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "api_token=".ACELLE_APIKEY."&uid=".$listid);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			$headers = array();
			$headers[] = 'Accept: application/json';
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				$html .= 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			$automation_uids = json_decode($result);
			unset($automation_uids->message);
			update_post_meta($newpost,'acelle_automation_uid',maybe_serialize($automation_uids));
			update_post_meta($newpost,'acelle_list_uid',$listid);
		}*/
		/*$defaultquantityarray =  array(
		 'date' => $date,
		 'author_id' => get_current_user_id()
		);*/
		$c_id = get_current_user_id();
		add_post_meta( $newpost, '_created_info_'.$c_id, $date);
		///////////////////////////////
	}
	//echo $html;
	$page = $link.'?fl_builder';
	$array = array('html'=>$html, 'page'=>$page);
	echo json_encode($array);
	exit;
}
function confirm_page_create(){
	// echo ;
	$post_parent = $_POST['post_parent'];
	$post_title = $_POST['post_title'];
	$postarr = array(
		'post_author' => get_current_user_id(),
		'post_title' => $post_title,
		'post_status' => 'publish',
		'post_type' => 'landing',
		'post_parent' => $post_parent
	);
	$newpost = wp_insert_post($postarr);
	//echo $newpost;
	$link = get_permalink($newpost);
	echo $link_beaver = $link.'?fl_builder';
	exit;
}
function publish_funnel_finalize(){
	$post_id = $_POST['post_parent'];
	$update = update_post_meta($post_id,'is_funnel_published','1');
	if($update){
		echo '1';
	}
	exit;
}
function private_submission_finalize(){
	$post_id = $_POST['post_parent'];
	// $funnel = get_post($post_id);
	if(get_post_meta($post_id,'acelle_list_uid',true) == ''){
		
		// $current_user = wp_get_current_user();
		$_mail_settings = get_user_meta(get_current_user_id(),'_mail_settings',true);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, ACELLE_DOMAIN.'/api/v1/lists?');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$list_array = array(
			"api_token" => ACELLE_APIKEY,
			"name" => get_the_title($post_id),
			"from_email" => 'support@funnelmates.com',//$_mail_settings['mls_from_email'],
			"from_name" => $_mail_settings['mls_from_name'],
			"default_subject" => $_mail_settings['mls_email_subject'],
			"contact[company]" => $_mail_settings['mls_company'],
			"contact[state]" => $_mail_settings['mls_state'],
			"contact[address_1]" => $_mail_settings['mls_address1'],
			"contact[address_2]" => $_mail_settings['mls_address2'],
			"contact[city]" => $_mail_settings['mls_city'],
			"contact[zip]" => $_mail_settings['mls_zip'],
			"contact[phone]" => $_mail_settings['mls_phone'],
			"contact[country_id]" => $_mail_settings['mls_country'],
			"contact[email]" => $_mail_settings['mls_email'],
			"contact[url]" => $_mail_settings['mls_homepage'],
			"subscribe_confirmation" => '1',
			"send_welcome_email" => '0',
			"unsubscribe_notification" => '1',
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($list_array));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$html .= 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		//print_r($result);
		$result = json_decode($result);
		if($result->status == '1' && $result->list_uid){
			$listid = $result->list_uid;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, ACELLE_DOMAIN.'/api/v1/automations/'.$listid.'/api/call?');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "api_token=".ACELLE_APIKEY."&uid=".$listid);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			$headers = array();
			$headers[] = 'Accept: application/json';
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				$html .= 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			$automation_uids = json_decode($result);
			unset($automation_uids->message);
			update_post_meta($post_id,'acelle_automation_uid',maybe_serialize($automation_uids));
			update_post_meta($post_id,'acelle_list_uid',$listid);
			
			$pages_uid = $listid.rand(0,99999);
			$pagesc_uid = $listid.rand(0,99999);
			// $maildb = new wpdb("funnelma_mailu","RX.N1d~jwveA","funnelma_mailserver","localhost");
			// $maildb = new wpdb("mailuser","8CcKYRp<~7ZA","mail_server","localhost");
			$maildb = new wpdb("root","Secutv643!!sf##~~6rfa","mail_server","localhost");
			$content = '<!-- Page container --><div class="page-container login-container" style="min-height: 249px;"><!-- Page content --><div class="page-content"><!-- Main content --><div class="content-wrapper"><div class="row"><div class="col-sm-2 col-md-3"></div><div class="col-sm-8 col-md-6"><!-- subscribe form --><h2 class="text-semibold mt-40 text-white">{LIST_NAME}</h2><div class="panel panel-body"><h4>Subscription Confirmed</h4><hr /><p>Your subscription to our list has been confirmed.</p><p>Thank you for subscribing!</p><a href="{CONTACT_URL}" class="btn btn-info bg-teal-800">Continue to our website</a> or <a href="{UPDATE_PROFILE_URL}" class="btn btn-info bg-teal-800">Manage your preferences</a></div><!-- /subscribe form --></div></div></div><!-- /main content --></div><!-- /page content --> <!-- Footer --><div class="footer text-white"><span class="text-white">{CONTACT_NAME}</span>, <a href="{CONTACT_URL}" class="text-white" target="_blank" rel="noopener">{CONTACT_URL}</a></div><!-- /footer --></div><!-- /page container -->';
			$content_confirmation = '<!-- Page container --><div class="page-container login-container" style="min-height: 249px;"><!-- Page content --><div class="page-content"><!-- Main content --><div class="content-wrapper"><div class="row"><div class="col-sm-2 col-md-3"></div><div class="col-sm-8 col-md-6"><!-- subscribe form --><h2 class="text-semibold mt-40 text-white">{LIST_NAME}</h2><div class="panel panel-body"><h4>Please Confirm Subscription</h4><hr />Click the link below to confirm your subscription:<br /> {SUBSCRIBE_CONFIRM_URL}<hr /><p>If you received this email by mistake, simply delete it. You won\'t be subscribed if you don\'t click the confirmation link above.</p><p>For questions about this list, please contact: <br /> <a href="mailto:{CONTACT_EMAIL}">{CONTACT_EMAIL}</a></p></div><!-- /subscribe form --></div></div></div><!-- /main content --></div><!-- /page content --> <!-- Footer --><div class="footer text-white">&copy; 2020. <span class="text-white">{CONTACT_NAME}</span>, <a href="{CONTACT_URL}" class="text-white" target="_blank">{CONTACT_URL}</a></div><!-- /footer --></div><!-- /page container -->';
			$url = 'https://funnelmates.com/c/'.$post_id.'/{SUBSCRIBER_CCID}';
			$ms_list = $maildb->get_row("SELECT * FROM `fmmsqmail_lists` WHERE uid='".$listid."' ");
			$maildb->query("INSERT INTO `fmmsqpages` (`uid`, `layout_id`, `mail_list_id`, `content`, `created_at`, `updated_at`, `subject`, `use_outside_url`, `outside_url`) VALUES ('$pages_uid','7','".$ms_list->id."','$content',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'Thank you','1','$url')");
			
			
			// $post_id = wp_get_post_parent_id(get_the_ID());
			// return "SELECT * FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='creator' ORDER BY id ASC LIMIT 0,1";
			$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='creator' ORDER BY id ASC LIMIT 0,1");
			$subject_def = 'Please Confirm To Access '.$query->name;
			$maildb->query("INSERT INTO `fmmsqpages` (`uid`, `layout_id`, `mail_list_id`, `content`, `created_at`, `updated_at`, `subject`) VALUES ('$pagesc_uid','6','".$ms_list->id."','".addslashes($content_confirmation)."',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'".addslashes($subject_def)."')");
		}
	}
	if(get_post_meta($post_id, 'is_user_improvising', true) == '1'){
		delete_post_meta($post_id, 'is_user_improvising');
		delete_post_meta($post_id, 'requested_feedback');
		$update = update_post_meta($post_id,'is_funnel_published','resubmitted');
		$update = update_post_meta($post_id,'review_date',date('F j, Y'));
	}else{
		$update = update_post_meta($post_id,'is_funnel_published','review');
		$update = update_post_meta($post_id,'review_date',date('F j, Y'));
	}
	// if($update){
		echo '1';
	// }
	exit;
}
function force_review(){
	$post_id = $_POST['post_id'];
	update_post_meta($post_id,'is_funnel_published','review');
	update_post_meta($post_id,'review_date',date('F j, Y'));
	exit;
}
function private_submission(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$html = '<div class="fnl_submission_parent">';
	//if($_SERVER["REMOTE_ADDR"]=='219.91.196.85'){
		$check_whitelabel = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels`where new_funnel_id = $post_id AND user_id= '".get_current_user_id()."'  AND type = 'whitelabel'");
		if($check_whitelabel){
			$html .= "<p>Adding your email sequence on our mail server will be done by our team and can take up to 24hrs to be live.   You'll receive a notification when your funnel is fully activated and ready to start promoting.</p>";
			$html .= "<p>If you'd like to host the emails yourself you can do that without waiting.   Here are the links to add into your autoresponder. </p>";
			
			$html .= '<p>Your confirmation page is: <a target="_BLANK" href="'.get_permalink($post_id).'">'.get_permalink($post_id).'</a></p>';
			$html .= '<p>Your thank you page is: <a target="_BLANK" href="'.get_permalink($post_id).'thank-you">'.get_permalink($post_id).'thank-you</a></p>';
			$html .= '<p>Your download page is: <a target="_BLANK" href="'.get_permalink($post_id).'download">'.get_permalink($post_id).'download</a></p>';
			
			$html .= '<button data="'.$post_id.'" id="privatepublish_instant_activation" type="button" style="width: 49%;margin-right: 1%;" class="btn btn-info">';
				$html .= '<span style="font-weight: 600;font-size: 18px;">Instant Activation</span><br/><span style="font-size: 11px;">(Use my own autoresponder)</span>';
			$html .= '</button>';
			$html .= '<button data="'.$post_id.'" id="privatepublish_submit_review" type="button" style="width: 49%;margin-left: 1%;" class="btn btn-success">';
				$html .= '<span style="font-weight: 600;font-size: 18px;">Submit My Funnel For Activation</span><br/><span style="font-size: 11px;">(Use our autoresponder, allow 24hr turnaround)</span>';
			$html .= '</button>';
		}else{
	//}
		$html .= '<p>To use our email autoresponder software your messages must be sent to admin for approval.</p>';
		$html .= '<p>Emails submitted will take 5-7 days to be live, and can only be updated once every 7 days.  Your funnel will be active when review is complete.</p>';
		$html .= '<p>OR for a faster option, simply connect your own autoresponder on your settings page and deliver the download there.</p>';
		
		$html .= '<p>Your confirmation page is: <a target="_BLANK" href="'.get_permalink($post_id).'">'.get_permalink($post_id).'</a></p>';
		$html .= '<p>Your thank you page is: <a target="_BLANK" href="'.get_permalink($post_id).'thank-you">'.get_permalink($post_id).'thank-you</a></p>';
		$html .= '<p>Your download page is: <a target="_BLANK" href="'.get_permalink($post_id).'download">'.get_permalink($post_id).'download</a></p>';
		
		$html .= '<button data="'.$post_id.'" id="privatepublish_instant_activation" type="button" style="width: 49%;margin-right: 1%;" class="btn btn-info">';
			$html .= '<span style="font-weight: 600;font-size: 18px;">Instant Activation</span><br/><span style="font-size: 11px;">(Use my own autoresponder)</span>';
		$html .= '</button>';
		$html .= '<button data="'.$post_id.'" id="privatepublish_submit_review" type="button" style="width: 49%;margin-left: 1%;" class="btn btn-success">';
			$html .= '<span style="font-weight: 600;font-size: 18px;">Submit My Funnel For Review</span><br/><span style="font-size: 11px;">(Use our autoresponder, allow 5-7 day turnaround)</span>';
		$html .= '</button>';
		}	
	$html .= '</div>';
	echo $html;
	exit;
}
function funnel_submission_confirmation(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	echo '<div class="fnl_submission_parent">';
		echo '<form method="POST" action="" onsubmit="return frm_submit_submission();">';
			echo '<h4>You\'re about to submit a funnel to the marketplace.</h4>';
			echo '<p>To be approved your funnel needs to meet the following criteria, please check each to make sure your funnel is ready!</p>';
			echo '<input type="hidden" name="submission_post_id" value="'.$post_id.'" />';
			
			echo '<p>';
			echo '<input class="submission_check" type="checkbox" name="complete_email_form" value="1" id="complete_email_form" />';
			echo '<label for="complete_email_form">Landing Page is complete and includes email form [<a target="_BLANK" href="'.site_url().'/wiki/making-your-landing-page/">read more</a>]</label>';
			echo '</p>';
			
			echo '<p>';
			echo '<input class="submission_check" type="checkbox" name="complete_dl_link" value="1" id="complete_dl_link" />';
			echo '<label for="complete_dl_link">Download Page contains download link [<a target="_BLANK" href="'.site_url().'/wiki/making-your-download-page/">read more</a>]</label>';
			echo '</p>';
			
			echo '<p>';
			echo '<input class="submission_check" type="checkbox" name="complete_aff_link" value="1" id="complete_aff_link" />';
			echo '<label for="complete_aff_link">Pages contain at least one affiliate link [<a target="_BLANK" href="'.site_url().'/wiki/adding-affiliate-links-to-your-pages/">read more</a>]</label>';
			echo '</p>';
			
			echo '<p>';
			echo '<input class="submission_check" type="checkbox" name="complete_email_seq" value="1" id="complete_email_seq" />';
			echo '<label for="complete_email_seq">Email Sequence is complete [<a target="_BLANK" href="'.site_url().'/wiki/creating-your-automated-email-sequence/">read more</a>]</label>';
			echo '</p>';
			
			echo '<p>';
			echo '<input class="submission_check" type="checkbox" name="complete_promo_tools" value="1" id="complete_promo_tools" />';
			echo '<label for="complete_promo_tools">Promotional Tools are added [<a target="_BLANK" href="'.site_url().'/wiki/creating-promotional-tools/">read more</a>]</label>';
			echo '</p>';
			
			
			//if($_SERVER["REMOTE_ADDR"]=='219.91.196.85'){
			$check_whitelabel = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels`where new_funnel_id = $post_id AND user_id= '".get_current_user_id()."'  AND type = 'whitelabel'");
				if($check_whitelabel){
					echo '<p>';
					echo '<input class="submission_check" type="checkbox" name="complete_promo_tools" value="1" id="complete_promo_tools" />';
					echo "  I've customised at least 20% of the funnel by adding an extra download, editing page design etc. (only visable to people who are activating a whitelabel funnel) ";
					echo '</p>';
				}
			//}
			
			echo '<p>When submitted for review it may take 3-5 days for your funnel to appear in the marketplace.</p>';
			echo '<p>Once approved you can continue to edit the email sequences in your account.  However, any changes made won\'t be reflected in the live sequence until manually reviewed. [<a target="_BLANK" href="'.site_url().'/wiki/updating-your-automated-email-sequence/">read more</a>]</p>';
			
			echo '<input type="submit" name="submit_submission" class="btn button-danger" value="My Funnel Still Needs A Little Work" >';
		echo '</form>';
	echo '</div>';
	exit;
}
function unpublish_funnel(){
	$post_id = $_POST['post_parent'];
	$update = update_post_meta($post_id,'is_funnel_published','0');
	if($update){
		echo '1';
	}
	exit;
}
function check_if_funnel_published(){
	$post_id = $_POST['post_id'];
	$data = get_post_meta($post_id,'is_funnel_published',true);
	if($data == 1){
		echo $data;
	}else{
		echo '0';
	}
	exit;
}
function check_funnel_bundle(){
	global $wpdb;
	$html = '';
	$post_id = $_POST['post_id'];
	$args = array(
		'post_parent' => $post_id,
		'posts_per_page' => -1,
		'post_type' => 'landing',
	);
	$post_title_array = array();
	$the_query = new WP_Query( $args );
	if($the_query->have_posts()){
		while ( $the_query->have_posts() ) : $the_query->the_post();
			$post_title_array[] = get_the_title();

		endwhile;
		//print_r($post_title_array);
		if(count($post_title_array) > 2){
			$check_whitelabel = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels`where new_funnel_id = $post_id AND user_id= '".get_current_user_id()."'  AND type = 'whitelabel'");
			if($check_whitelabel){
				$check_ex_link = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where (funnel_id = ".$post_id." AND aff_network = 'external') AND whitelabel_updated = '0' ");
				if($check_ex_link){
					$html .= '<div>';
					$html .= '<p style="font-size:18px; font-weight:700;">Your funnel contains the following external links.  Edit or approve to complete activation</p>';
					$get_ex_link = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'external'  ");
					if(count($get_ex_link) > 0){
					$html .= '<table style="width:100%;">';
						$html .= '<tr>
									 <th>Name</th>
									 <th>Description</th>
									 <th>Url</th>
								   </tr>';
						foreach($get_ex_link as $links){
							$html .= '<tr>
										<td>'.$links->name.'</td>
										<td>'.$links->notes.'</td>
										<td><span id="whitelabel_link_'.$links->id.'">'.$links->aff_link.'</span></td>';
										if($links->whitelabel_updated == '0'){
											$html .= '<td><span id="whitelabel_edit_'.$links->id.'" style="cursor:pointer;" class="whitelabel_edit_external_link btn btn-success" data="'.$links->id.'">Update</span></td>';
										}else{
											$html .= '<td><span>Updated</span></td>';
										}
									   $html .= '</tr>';	
						}
					$html .= '</table>';
					}
					$html .= '</div>';
					$html .= '<div class="funnel_error_note"><p style="color:red; font-size:18px;">Updation of link is required<p></div>';
					$html .= '<div style="text-align:right; margin:15px 0;"><span class="btn btn-success" onclick="publish_funnel('.$post_id.');">Confirm & Save<span></div>';
				}else{
					$html .= '<h5>Choose Below To Publish Your Funnel!</h5>';
					$html .= '<p>How would you like to publish it?</p>';
					$html .= '<div style="text-align:center;">';
					$html .= '<button class="btn" id="publish_funnel_finalize" data="'.$post_id.'" style="background:#279dbc;font-weight:600;color:white;margin-bottom:10px;">Activate My Whitelabel Funnel</button><br/>';
					$html .= '<button class="btn" id="publish_public_funnel" data="'.$post_id.'" style="background:#279dbc;font-weight:600;color:white;margin-bottom:10px;">List In Marketplace (Requires Atleast 20% Customisation)</button><br/>';
					$html .= '<button class="btn" onclick="Custombox.modal.close();" style="background:#FF6347;font-weight:600;color:white;margin-bottom:10px;">Cancel</button>';
					$html .= '</div>';
				}					
			}else{
			// $html .=  '<h4 style="text-align:center;"><a href="javascript:void(0);" id="publish_funnel_finalize" data="'.$post_id.'">Publish '.get_the_title($post_id).' Funnel</a></h4>';
			$html .= '<h5>Choose Below To Publish Your Funnel!</h5>';
			$html .= '<p>How would you like to publish it?</p>';
			$html .= '<div style="text-align:center;">';
			$html .= '<button class="btn" id="publish_funnel_finalize" data="'.$post_id.'" style="background:#279dbc;font-weight:600;color:white;margin-bottom:10px;">Private (Only I Can Use This Funnel)</button><br/>';
			$html .= '<button class="btn" id="publish_public_funnel" data="'.$post_id.'" style="background:#279dbc;font-weight:600;color:white;margin-bottom:10px;">Public (List In Marketplace)</button><br/>';
			$html .= '<button class="btn" onclick="Custombox.modal.close();" style="background:#FF6347;font-weight:600;color:white;margin-bottom:10px;">Cancel</button>';
			$html .= '</div>';
		   }
		}else{
			if(!in_array('Confirm',$post_title_array))
				$html .=  '<a href="javascript:void(0);" id="create_confirm_page" data="'.$post_id.'">Create Confirm Page</a><br/>';
			if(!in_array('Thank You',$post_title_array))
				$html .=  '<a href="javascript:void(0);" id="create_thankyou_page" data="'.$post_id.'">Create Thank You Page</a><br/>';
			if(!in_array('Download',$post_title_array))
				$html .=  '<a href="javascript:void(0);" id="create_download_page" data="'.$post_id.'">Create Download Page</a><br/>';
		}
	}else{
		$html .=  '<a href="javascript:void(0);" id="create_confirm_page" data="'.$post_id.'">Create Confirm Page</a>';
		$html .=  '<br/><a href="javascript:void(0);" id="create_thankyou_page" data="'.$post_id.'">Create Thank You Page</a>';
		$html .=  '<br/><a href="javascript:void(0);" id="create_download_page" data="'.$post_id.'">Create Download Page</a>';
	}
	echo $html;
	exit;
}
function save_general_settings(){
	$general_settings_array = array();
	$general_settings_array['clickbank_id'] = $_POST['clickbank_id'];
	$general_settings_array['jvzoo_id'] = $_POST['jvzoo_id'];
	if($_POST['warriorplus_id']){
		$general_settings_array['warriorplus_id'] = $_POST['warriorplus_id'];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		// https://warriorplus.com/api/v2/affiliate_requests?&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=100&status=ACTIVE
		curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=".$_POST['warriorplus_id']."&limit=100&status=ACTIVE");
		$result = curl_exec($ch);
		$json_decode_result = json_decode($result);
		if($json_decode_result->error == 'Please try after some time'){
			echo '0';
			exit;
		}else{
			$json_decode_result = maybe_serialize($json_decode_result);
			$products = update_user_meta(get_current_user_id(),'warrior_plus_products',$json_decode_result);
			//$html = '1';
		}
	}
	$general_settings_array['paykickstart_id'] = $_POST['paykickstart_id'];
	$general_settings_array['paypal_email'] = $_POST['paypal_email'];
	$update = update_user_meta(get_current_user_id(),'_ccv3_general_settings',$general_settings_array);
	// if($update){
		echo '1';
	// }
	exit;
}
function check_mail_list_settings(){
	$_mail_settings = get_user_meta(get_current_user_id(),'_mail_settings',true);
	if($_mail_settings['mls_name'] && $_mail_settings['mls_from_email'] && $_mail_settings['mls_from_name'] && $_mail_settings['mls_company'] && $_mail_settings['mls_state'] && $_mail_settings['mls_address1'] && $_mail_settings['mls_city'] && $_mail_settings['mls_zip'] && $_mail_settings['mls_country'] && $_mail_settings['mls_phone'] && $_mail_settings['mls_email']){
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function save_mail_list_settings(){
	$mail_list_settings_array = array();
	$mail_list_settings_array['mls_name'] = $_POST['mls_name'];
	$mail_list_settings_array['mls_from_email'] = $_POST['mls_from_email'];
	$mail_list_settings_array['mls_from_name'] = $_POST['mls_from_name'];
	$mail_list_settings_array['mls_email_subject'] = $_POST['mls_email_subject'];
	$mail_list_settings_array['mls_company'] = $_POST['mls_company'];
	$mail_list_settings_array['mls_state'] = $_POST['mls_state'];
	$mail_list_settings_array['mls_address1'] = $_POST['mls_address1'];
	$mail_list_settings_array['mls_city'] = $_POST['mls_city'];
	$mail_list_settings_array['mls_address2'] = $_POST['mls_address2'];
	$mail_list_settings_array['mls_zip'] = $_POST['mls_zip'];
	$mail_list_settings_array['mls_country'] = $_POST['mls_country'];
	$mail_list_settings_array['mls_phone'] = $_POST['mls_phone'];
	$mail_list_settings_array['mls_email'] = $_POST['mls_email'];
	$mail_list_settings_array['mls_homepage'] = $_POST['mls_homepage'];
	// $mail_list_settings_array['mls_subscription_confirmation_email'] = $_POST['mls_subscription_confirmation_email'];
	// $mail_list_settings_array['mls_final_welcome_email'] = $_POST['mls_final_welcome_email'];
	// $mail_list_settings_array['mls_unsubscribe_notification'] = $_POST['mls_unsubscribe_notification'];
	// print_r($mail_list_settings_array);exit;
	$update = update_user_meta(get_current_user_id(),'_mail_settings',$mail_list_settings_array);
	// if($update){
		echo '1';
	// }
	exit;
}
function delete_funnel($post_id = ''){
	global $wpdb;
	if($post_id){
		$parentid = $post_id;
		$post_author_id = get_post_field( 'post_author', $parentid );
		$dlt_marketplace_item = $wpdb->query("INSERT INTO `wp_setc_deleted_funnel_history` (user_id,purchaser_id,funnel_id,funnel_name) VALUES ('".$post_author_id."','".get_current_user_id()."','".$parentid."','".get_the_title($parentid)."')");
	}else{
		$parentid = $_POST['post_id'];
	}
	$date = date('Y-m-d H:i:s');
	$args = array( 
		'post_parent' => $parentid,
		'post_type' => 'landing'
	);
	$posts = get_posts( $args );
	if (is_array($posts) && count($posts) > 0) {
		foreach($posts as $post){
			wp_delete_post($post->ID, true);
		}
	}
	if(wp_delete_post($parentid, true)){
		//echo "DELETE FROM `wp_fm_marketplace_items` WHERE post_id = $parentid"; exit;
		$dlt_marketplace_item = $wpdb->query("DELETE FROM `wp_fm_marketplace_items` WHERE post_id = $parentid");
		$dlt_promotional_tools = $wpdb->query("DELETE FROM `wp_fm_promotional_tools` WHERE post_id = $parentid");
		$dlt_website_links = $wpdb->query("DELETE FROM `wp_setc_website_links` WHERE funnel_id = $parentid");
		$dlt_followup_emails = $wpdb->query("DELETE FROM `wp_setc_followup_emails` WHERE funnel_id = $parentid");
		
		$check_funnel_type = $wpdb->get_results("SELECT * FROM `wp_setc_activated_funnels`where new_funnel_id = $parentid AND user_id= '".get_current_user_id()."'  AND type = 'whitelabel'");
		if($check_funnel_type){ // if funnel type is whitelabel
			$dlt_activeted_info = $wpdb->query("DELETE FROM `wp_setc_activated_funnels` WHERE new_funnel_id = $parentid AND user_id= '".get_current_user_id()."' AND type = 'whitelabel'");
		}else{ 
			$dlt_activeted_info = $wpdb->query("DELETE FROM `wp_setc_activated_funnels` WHERE funnel_id = $parentid AND user_id= '".get_current_user_id()."'");
		}
	}
	wp_delete_post($parentid, true);
	echo '1';
	exit;
}
function disconnect_webinar(){
	if(delete_user_meta(get_current_user_id(),'fm_gotowebinar')){
		echo '1';
	}
	exit;
}
function connect_everwebinar(){
	$everwebinar_key = $_POST['everwebinar_key'];
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/everwebinar/webinars');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "api_key=$everwebinar_key");

	$headers = array();
	$headers[] = 'Content-Type: application/x-www-form-urlencoded';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$resultw = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	// echo '<pre>';
	$ever_webinars = json_decode($resultw);
	if($ever_webinars->status == 'success'){
		update_user_meta(get_current_user_id(),'fm_everwebinar_apikey',$everwebinar_key);
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function disconnect_everwebinar(){
	if(delete_user_meta(get_current_user_id(),'fm_everwebinar_apikey')){
		echo '1';
	}
	exit;
}
function disconnect_aweber(){
	if(delete_user_meta(get_current_user_id(),'fm_aweber_access_token') && delete_user_meta(get_current_user_id(),'fm_aweber_token_refresh')){
		echo '1';
	}
	exit;
}
function connect_aweber(){
	require_once(get_stylesheet_directory().'/includes/oauth2/vendor/autoload.php');
	$scopes = array(
		'account.read',
		'list.read',
		'list.write',
		'subscriber.read',
		'subscriber.write',
		// 'email.read',
		// 'email.write',
		'subscriber.read-extended',
		// 'landing-page.read'
	);
	$provider = new League\OAuth2\Client\Provider\GenericProvider([
		'clientId'              	=> 'CGPdJJYONkMmvDYO4P6JCDnNzJtvoYUq',    // The client ID assigned to you by the provider
		'clientSecret'          	=> 'np8YZI3zpI6MjAYQxbZTuKPS0mi2mv5F',    // The client password assigned to you by the provider
		'redirectUri'           	=> 'https://funnelmates.com/settings/',
		'scopes' 					=> $scopes,
		'scopeSeparator' 			=> ' ',
		'urlAuthorize'          	=> 'https://auth.aweber.com/oauth2/authorize',
		'urlAccessToken'        	=> 'https://auth.aweber.com/oauth2/token',
		'urlResourceOwnerDetails' 	=> 'https://api.aweber.com/1.0/accounts'
	]);
	$authorizationUrl = $provider->getAuthorizationUrl();
	$_SESSION['oauth2state'] = $provider->getState();
	echo $authorizationUrl;exit;
	/*require_once(get_stylesheet_directory().'/includes/AWeber-API-PHP-Library-master/aweber_api/aweber.php');
	$consumerKey    = "NyEAXPfk0LUSMwR5i6yqOdfIFZlc5efU";
	$consumerSecret = "jiVDFUaulAn3oWnT0Zzu8lumZO8FqqUs";
	$aweber = new AWeberAPI($consumerKey, $consumerSecret);
	// print_r($aweber);exit;
	if (empty($_COOKIE['accessToken'])) {
		if (empty($_GET['oauth_token'])) {
			$callbackUrl = site_url().'/settings/';
			// print_r($aweber->getRequestToken($callbackUrl));exit;
			list($requestToken, $requestTokenSecret) = $aweber->getRequestToken($callbackUrl);
			// print_r($requestTokenSecret);exit;
			setcookie('requestTokenSecret', $requestTokenSecret,'','/');
			// setcookie('callbackUrl', $callbackUrl,'','/');
			echo $aweber->getAuthorizeUrl();
			exit;
		}
	}*/
	exit;
}
function connect_sendlane(){
	$apikey = $_POST['apikey'];
	$appihashkey = $_POST['appihashkey'];
	if($apikey && $appihashkey){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://sendlane.com/api/v1/lists",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('api' => $apikey,'hash' => $appihashkey),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$response = json_decode($response);
		if(count($response->error) <= 0){
			$update_meta_ak = update_user_meta(get_current_user_id(),'_ccv3_sendlane_apikey',$apikey);
			$update_meta_ahk = update_user_meta(get_current_user_id(),'_ccv3_sendlane_apihashkey',$appihashkey);
			echo '1';
		}else{
			echo '0';
		}
	}
	exit;
}
function disconnect_sendlane(){
	$rm_slapi = delete_user_meta(get_current_user_id(),'_ccv3_sendlane_apikey');
	$rm_slapihash = delete_user_meta(get_current_user_id(),'_ccv3_sendlane_apihashkey');
	if($rm_slapi && $rm_slapihash){
		echo '1';
	}
	exit;
}
function connect_mailchimp(){
	$api_key = $_POST['key'];
	if($api_key){
		$data = array('fields' => 'lists',);
		$url = 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/';
		//echo 'here'.function_exists('mailchimp_curl_connect');exit;
		$result = json_decode( mailchimp_curl_connect( $url, 'GET', $api_key, $data) );
		// print_r( $result);exit;
		if( !empty($result->lists) ) {
			$update_meta = update_user_meta(get_current_user_id(),'_ccv3_mailchimp_apikey',$api_key);
			if($update_meta){
				echo '1';
				//hidden_tr_message
			}
			/*echo 'Lists : <select name="cb_options[mailchimp][listid]">';
			foreach( $result->lists as $list ){
				$selected_mailchimp = '';
				if($options['mailchimp']['listid']){
					if($options['mailchimp']['listid'] == $list->id){
						$selected_mailchimp = 'selected';
					}else{
						$selected_mailchimp = '';
					}
				}
				echo '<option value="' . $list->id . '" '.$selected_mailchimp.'>' . $list->name . ' (' . $list->stats->member_count . ')</option>';
			}
			echo '</select>';*/
		} elseif ( is_int( $result->status ) ) {
			//echo '<strong>' . $result->title . ':</strong> ' . $result->detail;
			echo '0';
		}else{
			echo '0';
		}
	}
	// echo $key;
	exit;
}
function disconnect_mailchimp(){
	$dc_mailchimp = delete_user_meta(get_current_user_id(),'_ccv3_mailchimp_apikey');
	if($dc_mailchimp){
		echo '1';
	}
	exit;
}
function connect_mailsqaud(){
	$api_key = $_POST['key'];
	$mailsqaud_login = $_POST['mailsqaud_login'];
	// echo $api_key;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,'https://mailsqaud.com/API/getLists');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'APIToken: '.$api_key, 'Login: '.$mailsqaud_login));
	$data = curl_exec($ch);
	if (curl_errno($ch)) { 
	   echo '0'; 
	} else {
	   $data = json_decode($data);
	   // print_r($data);
	   if($data->status == 'success'){
		   update_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apitoken',$api_key);
		   update_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apilogin',$mailsqaud_login);
		   echo '1';
	   }
	}
	curl_close($ch);
	exit;
}
function disconnect_mailsqaud(){
	$dc_mailsqaud = delete_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apitoken');
	if($dc_mailsqaud){
		echo '1';
	}
	exit;
}
function connect_getresponse(){
	$api_key = $_POST['key'];
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.getresponse.com/v3/accounts');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


	$headers = array();
	$headers[] = 'X-Auth-Token: api-key '.$api_key;
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	
	$response = json_decode($result);
	if($response->accountId){
		
		/*$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.getresponse.com/v3/campaigns');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


		$headers = array();
		$headers[] = 'X-Auth-Token: api-key '.$api_key;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		print_r(json_decode($result));exit;*/
		
		
		/*$data = array(
			"name" => "Avesh Chauhan",
			"campaign" => array (
				"campaignId" => "TRx5s"
			),
			"email" => "setc.avesh@gmail.com",
			"dayOfCycle" => "0"
		);
		$data_string = json_encode($data);
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,"https://api.getresponse.com/v3/contacts");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		$headers = array(
			'Content-Type: application/json',
			'X-Auth-Token: api-key '.$api_key,
		);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec($ch);

		curl_close ($ch);
		
		print_r($server_output);exit;*/
		
		
		
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_getresponse_apikey',$api_key);
		if($update_meta){
			echo '1';
		}else{
			echo '0';
		}
	}else{
		echo '0';
	}
	/*require_once(get_stylesheet_directory().'/includes/getresponse_api/GetResponseAPI3.class.php');
	// echo 'here';exit;
	$api_key = $_POST['key'];
	if($api_key){
		try{
			$api = new \GetResponse($api_key);
			$retCamp = $api->getCampaigns();
			// print_r($retCamp);exit;
			// $htmlauto_gr ='';
			if(count($retCamp) > 0){
				// print_r($retCamp);exit;
				/*$htmlauto_gr .= '<tr><td>List: </td>';
				$htmlauto_gr .= '<td>
				<select name="cb_options[getresponse][listid]">';
				foreach ($retCamp as $list){
					//echo get_option('vl_getresponse_list_id');
					if($options['getresponse']['listid'] == $list->campaignId){
						$selected_autoresponder = 'selected';
					}else{
						$selected_autoresponder = '';
					}
					$htmlauto_gr .= '<option value="'.$list->campaignId.'" '.$selected_autoresponder.'>'.$list->name.'</option>';
				}
				$htmlauto_gr .= '</td></tr>';*
				
				$update_meta = update_user_meta(get_current_user_id(),'_ccv3_getresponse_apikey',$api_key);
				if($update_meta){
					echo '1';
				}
			}else{
				echo '0';
			}
		}catch (Exception $exptn){
			// print_r($exptn->message);
			echo '0';
		}
	}*/
	exit;
}
function disconnect_getresponse(){
	$dc_getresponse = delete_user_meta(get_current_user_id(),'_ccv3_getresponse_apikey');
	if($dc_getresponse){
		echo '1';
	}
	exit;
}

function connect_constant_contact(){
	//$api_key = $_POST['key'];
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.cc.email/v3/lists');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "{\n  \"name\": \"Multiple purchases\",\n  \"favorite\": true,\n  \"description\": \"List of repeat customers\"\n}");

	$headers = array();
	$headers[] = 'Accept: application/json';
	$headers[] = 'Authorization: Bearer {access_token}';
	$headers[] = 'Cache-Control: no-cache';
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	print_r($result);
	exit;
}

function convertkit_connect(){
	$api_key = $_POST['key'];
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.convertkit.com/v3/tags?api_key='.$api_key);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	$response = json_decode($result);
	//print_r($response);	
	foreach($response as $key=>$value){
		foreach ($value as $k => $v){
			$tag_id = $v->id;
		}
	}
	
	
	if($response->tags && $tag_id){
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_convertkit_apikey',$api_key);
		$update_tag_meta = update_user_meta(get_current_user_id(),'_ccv3_convertkit_tag_id',$tag_id);
	}else{
		echo '2';
		exit;
	}
	
	if($update_meta && $update_tag_meta){
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function disconnect_convertkit(){
	$dc_convertkit = delete_user_meta(get_current_user_id(),'_ccv3_convertkit_apikey');
	$dc_convertkit_tag = delete_user_meta(get_current_user_id(),'_ccv3_convertkit_tag_id');
	if($dc_convertkit && $dc_convertkit_tag){
		echo '1';
	}
	exit;
}

function disconnect_drip(){
	$dc_drip_api = delete_user_meta(get_current_user_id(),'_ccv3_drip_apikey');
	$dc_drip_account_id = delete_user_meta(get_current_user_id(),'_ccv3_drip_account_id');
	if($dc_drip_api && $dc_drip_account_id){
		echo '1';
	}
	exit;
}

function disconnect_mailerlite(){
	$dc_mailerlite_api = delete_user_meta(get_current_user_id(),'_ccv3_mailerlite_apikey');	
	if($dc_mailerlite_api){
		echo '1';
	}
	exit;
}

function drip_connect(){
	$api_key = $_POST['key'];
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
		
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.getdrip.com/v2/accounts');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

	curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . '');

	$headers = array();
	$headers[] = 'User-Agent: Your App Name (www.yourapp.com)';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	
	$response = json_decode($result);
	//print_r($response);
	
	if($response->accounts){
		$auth_id = $response->accounts[0]->id;
	}
	
	if($auth_id){
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_drip_apikey',$api_key);
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_drip_account_id',$auth_id);
		echo '1';
	}
	exit;
}

function mailerlite_connect(){
	$api_key = $_POST['key'];
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://api.mailerlite.com/api/v2');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


	$headers = array();
	$headers[] = 'X-Mailerlite-Apikey: '.$api_key.'';
	$headers[] = 'Content-Type: application/json';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	
	$response = json_decode($result);
	
	if($response->account){
		$account_id = $response->account->id;
	}
	
	if($account_id){
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_mailerlite_apikey',$api_key);
		//$update_meta = update_user_meta(get_current_user_id(),'_ccv3_drip_account_id',$auth_id);
		echo '1';
	}
	
	exit;
}

function sendiio_connect(){
	$api_token = $_POST['token'];
	$api_key = $_POST['key'];
	
	// Generated by curl-to-PHP: http://incarnate.github.io/curl-to-php/
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://sendiio.com/api/v1/auth/check');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, 
    http_build_query(array('token' => $api_token, 'secret' => $api_key)));

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	
	$response = json_decode($result);
	$user = $response->data->user_id;
	
	if($user){
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_sendiio_apikey',$api_key);
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_sendiio_apitoken',$api_token);
		$update_meta = update_user_meta(get_current_user_id(),'_ccv3_sendiio_user_id',$user);
		echo '1';
	}
	exit;
}

function disconnect_sendiio(){
	$dc_sendiio_api = delete_user_meta(get_current_user_id(),'_ccv3_sendiio_apikey');	
	$dc_sendiio_token = delete_user_meta(get_current_user_id(),'_ccv3_sendiio_apitoken');	
	$dc_sendiio_user = delete_user_meta(get_current_user_id(),'_ccv3_sendiio_user_id');	
	if($dc_sendiio_api && $dc_sendiio_token && $dc_sendiio_user){
		echo '1';
	}
	exit;
}
/*function template_search_by_keyword(){
	$keyword = $_POST['search_keyword'];
	$funnel_type = $_POST['funnel_type'];
	if($keyword){
		$terms = get_terms( 'fl-builder-template-category', array(
			'hide_empty' => false,
			'name__like' => $keyword
		));
	}else{
		$terms = get_terms( 'fl-builder-template-category', array(
			'hide_empty' => false,
		));
	}
	foreach($terms as $t){
		$args = array(
			'post_type' => 'fl-builder-template',
			'tax_query' => array(
				array(
					'taxonomy' => 'fl-builder-template-category',
					'field' => 'term_id',
					'terms' => $t->term_id
				),
				array(
					'taxonomy' => 'funnel_type',
					'field' => 'slug',
					'terms' => $funnel_type
				)
			)
		);
		$query = new WP_Query( $args );
		 
		if($query->have_posts()){
			while ( $query->have_posts() ) : $query->the_post();
				if(get_the_title() == $t->name){
					$html .= '<div style="width: 20%;float: left;text-align: center;">';
					$html .= '<h4 style="font-size:17px;">'.get_the_title().'</h4>';
					$html .= '<div>'.get_the_post_thumbnail(get_the_ID(),'thumbnail').'</div>';
					$html .= '<button style="margin-top: 0.5rem;color:#FFF;" data="'.$t->term_id.'" type="button" class="btn btn_green select_funnel_template">Select Template</button>';
					$html .= '</div>';
				}
			endwhile;
			wp_reset_postdata();
		}
	}
	echo $html;
	exit;
}*/
function get_edit_funnel_pages(){
	global $wpdb;
	$post_parent = $_POST['post_parent'];
	$child_landing = new WP_Query(array('post_type'=>'landing','post_parent' => $post_parent, 'post_status'=>'publish', 'posts_per_page'=>-1,'orderby' => 'date','order' => 'ASC'));
	if ( $child_landing->have_posts() ) :
		$html .= '<div class="pages_created_container" style="width:100%;display:inline-block;text-align:center;">';
				$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
					$html .= '<img src="'.get_stylesheet_directory_uri().'/images/landing.png'.'" style="width: 170px;" />';
					$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.get_the_permalink($post_parent).'?fl_builder">Edit Your<br/>'.get_the_title($post_parent).'<br/><strong>Landing Page</strong></a>';
				$html .= '</div>';
			while ( $child_landing->have_posts() ) : $child_landing->the_post();
				$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
					if(strtolower(get_the_title()) == 'thank you'){
						$imagename = 'thanks';
					}else{
						$imagename = strtolower(get_the_title());
					}
					$html .= '<img src="'.get_stylesheet_directory_uri().'/images/'.$imagename.'.png" style="width: 170px;" />';
					$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.get_the_permalink().'?fl_builder">Edit Your<br/>'.get_the_title($post_parent).'<br/><strong>'.get_the_title().' Page</strong></a>';
				$html .= '</div>';
			endwhile;
			// $html .= '<a href="javascript:void(0);" data="'.$post_parent.'" style="background: #434d6c;color: white;text-decoration: none;padding: 3px;display: inline-block;margin-top: 5px;border-radius: 5px;width: 48%;font-weight: 600;" class="btn btn_add_scripts">Add/Edit Scripts</a>';
		$html .= '</div>';
		
		/*$html .= '<div class="pages_created_container" style="width:100%;display:inline-block;text-align:center;">';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/landing.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Landing Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/confirm.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$confirm_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Confirm Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/thanks.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$thanks_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Thanks Page</strong></a>';
			$html .= '</div>';
			$html .= '<div class="page_created" style="width:25%;float:left;padding:10px;">';
				$html .= '<img src="'.get_stylesheet_directory_uri().'/images/download.png'.'" style="width: 170px;" />';
				$html .= '<a style="display: block;text-align: center;color: white;background: #82b440;border-radius: 5px;padding: 5px;position: relative;margin-top: -30px;" target="_blank" href="'.$download_link.'?fl_builder">Edit Your<br/>'.$_POST['landing_page_name'].'<br/><strong>Download Page</strong></a>';
			$html .= '</div>';
		$html .= '</div>';*/
		
		wp_reset_postdata();
	else :
		$html .= '<p>'._e( 'Sorry, no pages available.' ).'</p>';
	endif;
	echo $html;
	exit;
}
function get_scripts_funnel_pages(){
	$post_id = $_POST['post_parent'];
	$html = '';
	if($post_id){
		$header_scripts = get_post_meta($post_id,"header_scripts_".get_current_user_id(),true);
		$header_scripts_loc = get_post_meta($post_id,"header_scripts_location_".get_current_user_id(),true);
		$footer_scripts = get_post_meta($post_id,"footer_scripts_".get_current_user_id(),true);
		$footer_scripts_loc = get_post_meta($post_id,"footer_scripts_location_".get_current_user_id(),true);
		$html .= '<div class="scripts_container" style="text-align:center;">';
			$html .= '<h4>'.get_the_title($post_id).' Scripts</h4>';
			$html .= '<input type="hidden" name="post_id_scripts" value="'.$post_id.'" />';
			$html .= '<div class="header_scripts" style="width:50%;float:left;padding:10px;text-align:left;">';
				$html .= '<h5 style="margin:0;">Header Script</h5>';
				$html .= '<select style="width:100%;" name="location_page_headerscripts">';
					$selected_landing_h = $header_scripts_loc=='landing' ? 'selected' : '';
					$selected_confirm_h = $header_scripts_loc=='confirm' ? 'selected' : '';
					$selected_download_h = $header_scripts_loc=='download' ? 'selected' : '';
					$selected_thankyou_h = $header_scripts_loc=='thankyou' ? 'selected' : '';
					$html .= '<option value="landing" '.$selected_landing_h.' >Landing Page</option>';
					$html .= '<option value="confirm" '.$selected_confirm_h.' >Confirm Page</option>';
					$html .= '<option value="download" '.$selected_download_h.' >Download Page</option>';
					$html .= '<option value="thankyou" '.$selected_thankyou_h.' >Thank you Page</option>';
				$html .= '</select>';
				$html .= '<textarea style="margin:0;" name="header_scripts_ta" rows="10">'.$header_scripts.'</textarea>';
			$html .= '</div>';
			$html .= '<div class="footer_scripts" style="width:50%;float:left;padding:10px;text-align:left;">';
				$html .= '<h5 style="margin:0;">Footer Script</h5>';
				$html .= '<select style="width:100%;" name="location_page_footerscripts">';
					$selected_landing_f = $footer_scripts_loc=='landing' ? 'selected' : '';
					$selected_confirm_f = $footer_scripts_loc=='confirm' ? 'selected' : '';
					$selected_download_f = $footer_scripts_loc=='download' ? 'selected' : '';
					$selected_thankyou_f = $footer_scripts_loc=='thankyou' ? 'selected' : '';
					$html .= '<option value="landing" '.$selected_landing_f.' >Landing Page</option>';
					$html .= '<option value="confirm" '.$selected_confirm_f.' >Confirm Page</option>';
					$html .= '<option value="download" '.$selected_download_f.' >Download Page</option>';
					$html .= '<option value="thankyou" '.$selected_thankyou_f.' >Thank you Page</option>';
				$html .= '</select>';
				$html .= '<textarea style="margin:0;" name="footer_scripts_ta" rows="10">'.$footer_scripts.'</textarea>';
			$html .= '</div>';
			$html .= '<button class="btn btn-primary save_update_script" style="width: 50%;">Update Scripts</button>';
		$html .= '</div>';
	}
	echo $html;
	exit;
}
function save_funnel_scripts(){
	$post_id = $_POST['post_id'];
	$header_scripts = $_POST['header_scripts'];
    $footer_scripts = $_POST['footer_scripts'];
    $header_scripts_loc = $_POST['header_scripts_loc'];
    $footer_scripts_loc = $_POST['footer_scripts_loc'];
	$update_hs = update_post_meta($post_id,'header_scripts_'.get_current_user_id(),$header_scripts);
	$update_hsl = update_post_meta($post_id,'header_scripts_location_'.get_current_user_id(),$header_scripts_loc);
	$update_fs = update_post_meta($post_id,'footer_scripts_'.get_current_user_id(),$footer_scripts);
	$update_fsl = update_post_meta($post_id,'footer_scripts_location_'.get_current_user_id(),$footer_scripts_loc);
	echo '1';
	exit;
}
function publish_public_funnel(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$image_error = "this.style.display='none'";
	$profit_system = '';
	$allready_exists_store = $wpdb->get_results("SELECT * FROM `wp_fm_marketplace_items` where post_id = $post_id");
	//print_r($allready_exists_store);
	foreach($allready_exists_store as $data){
		$description = $data->description;
		$pricing = $data->pricing;
		$profit_system = $data->profit_system;
		$funnel_image_id = $data->image_id;
		$funnel_image = wp_get_attachment_url( $funnel_image_id );
	}
	// if($profit_system == ''){
		// $profit_system_checked = "checked";
	// }
	if($profit_system == '0'){
		$profit_system_checked = "";
	}else{
		$profit_system_checked = "checked";
	}
	//$pricing = explode(',',$pricing);
	//print_r($pricing);
	//$pricing = unserialize($pricing);
	$pricing = maybe_unserialize($pricing);
	//$pricing = maybe_unserialize($getrec->pricing);
	foreach($pricing as $k=>$p){
		if($k == 'free'){
			$free = 'checked';
		}else if($k == 'premium'){
			$premium = 'checked';
			$premium_price = $p;
		}else if($k == 'whitelabel'){
			$premium_whitelabel = 'checked';
			$premium_whitelabel_price = $p;
		}else if($k == 'exclusive'){
			$premium_exclusive = 'checked';
			$premium_exclusive_price = $p;
		}
	}
	//exit;
	$html = '';
	if($post_id){
		$post_title = get_the_title($post_id);
		$categories = get_terms( array(
			'taxonomy' => 'category',
			'hide_empty' => false,
		) );
		$posttags = get_the_tags($post_id);
		// $html .= print_r($tags,true);
		$tags = '';
		if ($posttags) {
		  foreach($posttags as $tag) {
			$tags .= $tag->name.', '; 
		  }
		}
		$html .= '<h4>Marketplace Settings</h4>';
		$html .= '<form id="form_public_funnel_settings" onsubmit="return public_funnel_settings_submit();">';
		$html .= '<input type="hidden" name="action" value="submit_public_funnel_settings" />';
		$html .= '<input type="hidden" name="post_id" value="'.$post_id.'" />';
		$html .= '<table style="width:100%;">';
			$html .= '<tr>';
				$html .= '<td>Funnel Name</td>';
				$html .= '<td><input type="text" name="funnel_name" value="'.$post_title.'" required /></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>Funnel Description</td>';
				$html .= '<td><textarea name="funnel_description">'.stripcslashes(stripcslashes(stripcslashes($description))).'</textarea></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>Category</td>';
				$html .= '<td>';
					$html .= '<select name="funnel_category" style="width:100%;" required>';
						$category = get_the_category( $post_id );
						// print_r($category);
						foreach($categories as $ctg){
							if($category[0]->term_id == $ctg->term_id){
								$cat_selected = "selected";
							}else{
								$cat_selected = "";
							}
							$html .= '<option value="'.$ctg->term_id.'" '.$cat_selected.'>'.$ctg->name.'</option>';
						}
					$html .= '</select>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>Tags</td>';
				$html .= '<td><input type="text" value="'.$tags.'" name="funnel_tags" required /></td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td>Funnel Image</td>';
				$html .= '<td>
								<p>
								<input type="hidden" value="'.$funnel_image_id.'" class="regular-text" id="process_funnel_image_" name="process_funnel_image_">
								<button style="width:75%;background:#82b440; padding:10px 0; border:none; color:#FFFFFF; border-radius:10px;" type="button" class="funnel_image button" data="'.$post_id.'">Choose Image (300x300 Recommended)</button>
								<img src="'.$funnel_image.'" onerror="'.$image_error.'" name="funnel_image" class="preview_'.$post_id.'" style="height:100px;width:100px;" /></p>';
				$html .='</td>';
			$html .= '</tr>';
			
			/***************************************/
				$count_external_links = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='external'");
				if($count_external_links < 1){
				$html .= '<tr>';
					$html .= '<td colspan="2"><input type="checkbox" id="funnel_pricing_option_free" name="funnel_pricing_option_free" value="free" '.$free.'/><label for="funnel_pricing_option_free" style="margin-left: 15px;margin-bottom:0;font-weight: 600;">FREE</label><br/><label for="funnel_pricing_option_free" style="margin-bottom: 10px;margin-left: 28px;font-size: 12px;font-style: italic;color: #999;">Share this with our community. You will automatically get a copy of all leads our members generate to the funnel also added to your own autoresponder.</label></td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td colspan="2"><input type="checkbox" id="funnel_pricing_option_premium" name="funnel_pricing_option_premium" value="premium" '.$premium.'/><label for="funnel_pricing_option_premium" style="margin-left: 15px;margin-bottom:0;font-weight: 600;">Premium</label><br/><label for="funnel_pricing_option_premium" style="margin-bottom: 10px;margin-left: 28px;font-size: 12px;font-style: italic;color: #999;">Members can unlock the use of this funnel for your set fee.  Leads will not be shared.</label>';
					if($premium == 'checked'){
						$html .= "<div class='premium_field_container'><input type='number' name='premium_price' min='1' style='margin:0;width:100%;' placeholder='Premium Price in $' value='".$premium_price."' required/></div>";
					}else{
						$html .= '<div class="premium_field_container"></div>';
					}
				$html .= '</td>';
				$html .= '</tr>';
			}else{
				$html .= '<tr>';
					$html .= '<td colspan="2"><p style="color: #FF6347;">* Your funnel contains external links, which can only be edited by customers purchasing either whitelabel or exclusive licensing permissions.</p></td>';
				$html .= '</tr>';
			}
			/***************************************/
			/*************/
			$user = wp_get_current_user();
			if(in_array( 'funnelmates_pro', (array) $user->roles ) || in_array( 'administrator', (array) $user->roles )){
				$html .= '<tr>';
					$html .= '<td colspan="2"><input type="checkbox" id="funnel_pricing_option_premium_whitelabel" name="funnel_pricing_option_premium_whitelabel" value="premium_whitelabel" '.$premium_whitelabel.' /><label for="funnel_pricing_option_premium_whitelabel" style="margin-left: 15px;margin-bottom:0;font-weight: 600;">Premium Whitelabel</label><br/><label for="funnel_pricing_option_premium_whitelabel" style="margin-bottom: 10px;margin-left: 28px;font-size: 12px;font-style: italic;color: #999;">MEMBERS CAN UNLOCK THE USE OF THIS FUNNEL AND HAVE THE ABILITY TO EDIT THE PAGES, EDIT THE LINKS, REBRAND OR RESELL THIS FUNNEL.</label>';
					if($premium_whitelabel == 'checked'){
						$html .= "<div class='whitelabel_field_container'><input type='number' name='whitelabel_price' min='1' style='margin:0;width:100%;' placeholder='Premium Price in $' value='".$premium_whitelabel_price."' required/></div>";
					}else{
						$html .= '<div class="whitelabel_field_container"></div>';
					}
				$html .= '</td>';
				$html .= '</tr>';
				$html .= '<tr>';
					$html .= '<td colspan="2"><input type="checkbox" id="funnel_pricing_option_premium_exclusive" name="funnel_pricing_option_premium_exclusive" value="premium_exclusive" '.$premium_exclusive.'/><label for="funnel_pricing_option_premium_exclusive" style="margin-left: 15px;margin-bottom:0;font-weight: 600;">Premium Exclusive</label><br/><label for="funnel_pricing_option_premium_exclusive" style="margin-bottom: 10px;margin-left: 28px;font-size: 12px;font-style: italic;color: #999;">Members can unlock exclusive rights to this funnel, it will be removed from your account and all editing and publishing permissions will belong to the new owner.</label>';
					if($premium_exclusive == 'checked'){
						$html .= "<div class='exclusive_field_container'><input type='number' name='exclusive_price' min='1' style='margin:0;width:100%;' placeholder='Premium Price in $' value='".$premium_exclusive_price."' required/></div>";
					}else{
						$html .= '<div class="exclusive_field_container"></div>';
					}
				$html .= '</td>';
				$html .= '</tr>';
			}
			/*************/
			// if(get_current_user_id() == '1'){
				$coupon_code = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_coupon_code` where funnel_id = $post_id");
				if($coupon_code->id){
					$checked_coupon = "checked";
					$tbody_class = 'active';
				}else{
					$checked_coupon = "";
					$tbody_class = '';
				}
				$html .= '<tr class="coupon_code_link">';
					$html .= '<td colspan="2">
						<input type="checkbox" id="funnel_is_coupon_code" name="funnel_is_coupon_code" value="coupon_code" '.$checked_coupon.'/><label for="funnel_is_coupon_code" style="margin-left: 15px;margin-bottom:0;font-weight: 600;">Create Coupon Code</label>
					</td>';
				$html .= '</tr>';		
				$html .= '<tbody class="'.$tbody_class.'" id="coupon_container">';
					$html .= '<tr>';
						$html .= '<td><label for="coupon_code">Coupon Code: </label></td>';
						$html .= '<td>';
							$html .= '<input type="hidden" name="already_coupon" value="'.$coupon_code->id.'" />';
							$html .= '<input id="coupon_code" type="text" name="coupon_code" value="'.$coupon_code->coupon_code.'" />';
						$html .= '</td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td><label for="coupon_type">Coupon Type: </label></td>';
						$price_selected = "";
						$perc_selected = "";
						if($coupon_code->coupon_type == 'price'){
							$price_selected = "selected";
						}else if($coupon_code->coupon_type == 'percentage'){
							$perc_selected = "selected";
						}
						$html .= '<td><select id="coupon_type" name="coupon_type"><option value="price" '.$price_selected.'>$ (Price)</option><option value="percentage" '.$perc_selected.'>% (Percentage)</option></select></td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td><label for="coupon_value">Coupon Value: </label></td>';
						$html .= '<td><input id="coupon_value" type="number" min="1" step="1" name="coupon_value" value="'.$coupon_code->coupon_value.'" /></td>';
					$html .= '</tr>';
					$html .= '<tr>';
						$html .= '<td>Enable coupon code for:</td>';
						$html .= '<td>';
							$valid_for = maybe_unserialize($coupon_code->valid_for);
							$coupon_premium = ($valid_for['premium'] == '1')?'checked':'';
							$coupon_whitelabel = ($valid_for['whitelabel'] == '1')?'checked':'';
							$coupon_exclusive = ($valid_for['exclusive'] == '1')?'checked':'';
							$html .= '<input type="checkbox" value="1" name="enable_coupon_premium" id="enable_coupon_premium" '.$coupon_premium.' /><label style="margin: 10px;" for="enable_coupon_premium">Premium</label>';
							$html .= '<input type="checkbox" value="1" name="enable_coupon_whitelabel" id="enable_coupon_whitelabel" '.$coupon_whitelabel.' /><label style="margin: 10px;" for="enable_coupon_whitelabel">Whitelabel</label>';
							$html .= '<input type="checkbox" value="1" name="enable_coupon_exclusive" id="enable_coupon_exclusive" '.$coupon_exclusive.' /><label style="margin: 10px;" for="enable_coupon_exclusive">Exclusive</label>';
						$html .= '</td>';
					$html .= '</tr>';
				$html .= '</tbody>';
			// }
			
			$html .= '<tr>';
				$html .= '<td colspan="2">';
					$html .= '<input type="checkbox" id="aff_profit_system" name="aff_profit_system" value="1" '.$profit_system_checked.'/><label for="aff_profit_system"><strong>[Recommended] Enable FunnelMates automated affiliate profits system.</strong> When activated our team researches, reviews and writes niche targeted promotions (max 3 per week). All promotions are automatically sent with the funnel buyer\'s affiliate links for the ultimate in hands free affiliate commissions</label>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr align="center">';
				$html .= '<td colspan="2">';
					if($_POST['type'] == 'marketplace_update'){
						$html .= '<input type="hidden" value="update" name="funnel_update_type" />';
						$html .= '<input class="btn" style="background: #82b440;color: white;font-weight: 600;padding: 5px 70px;" type="submit" value="Update">';
					}else{
						$html .= '<input type="hidden" value="new" name="funnel_update_type" />';
						$html .= '<input class="btn" style="background: #82b440;color: white;font-weight: 600;padding: 5px 70px;" type="submit">';
					}
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';
		$html .= '</form>';
	}
	echo $html;
	exit;
}
function submit_public_funnel_settings_function(){
	global $wpdb;	
	$post_id = $_POST['post_id'];	
	$is_coupon = $_POST['is_coupon'];
	if($is_coupon == '1'){
		$coupon_action = $_POST['coupon_action'];
		$coupon_code = $_POST['coupon_code'];
		$coupon_type = $_POST['coupon_type'];
		$coupon_value = $_POST['coupon_value'];
		$valid_for = array();
		$valid_for['premium'] = $_POST['enable_coupon_premium'];
		$valid_for['whitelabel'] = $_POST['enable_coupon_whitelabel'];
		$valid_for['exclusive'] = $_POST['enable_coupon_exclusive'];
		if($coupon_action == 'update'){
			$coupon_id = $_POST['coupon_id'];
			if($coupon_id){
				$wpdb->query("UPDATE `wp_setc_funnel_coupon_code` SET funnel_id='$post_id', coupon_code='$coupon_code', coupon_type='$coupon_type', coupon_value='$coupon_value', valid_for='".maybe_serialize($valid_for)."' WHERE id='$coupon_id'");
			}
		}else if($coupon_action == 'remove'){
			$coupon_id = $_POST['coupon_id'];
			$wpdb->query("DELETE FROM `wp_setc_funnel_coupon_code` WHERE id='$coupon_id'");
		}else{
			if($coupon_code && $coupon_type && $coupon_value){
				$wpdb->query("INSERT INTO `wp_setc_funnel_coupon_code` (funnel_id,coupon_code,coupon_type,coupon_value,valid_for) VALUES ('$post_id','$coupon_code','$coupon_type','$coupon_value','".maybe_serialize($valid_for)."')");
			}
		}
	}
	$funnel_name = $_POST['funnel_name'];
	$funnel_description = addslashes($_POST['funnel_description']);
	$funnel_category = $_POST['funnel_category'];
	$funnel_tags = $_POST['funnel_tags'];
	$funnel_image = $_POST['funnel_image'];
	$pricing = $_POST['pricing'];
	$premium_price = $_POST['premium_price'];
	$whitelabel_price = $_POST['whitelabel_price'];
	$exclusive_price = $_POST['exclusive_price'];
	$aff_profit_system = $_POST['aff_profit_system'];
	
	
	$data = array(
	  'ID' => $post_id,
	  'post_title'   => $funnel_name,
	);
	wp_update_post( $data ); // update post title and categories
    
	
	wp_set_post_categories($post_id,$funnel_category);
	wp_set_post_tags($post_id,explode(',',$funnel_tags));
	$pricing = explode(',',$pricing);
	$array = array();
	foreach($pricing as $p){
		if($p == 'free'){
			$array['free'] = '0';
		}else if($p == 'premium'){
			$array['premium'] = $premium_price;
		}else if($p == 'premium_whitelabel'){
			$array['whitelabel'] = $whitelabel_price;
		}else if($p == 'premium_exclusive'){
			$array['exclusive'] = $exclusive_price;
		}
	}
	$max_price = max($array);
	///////////////////////////////////////
	
	if(get_post_meta($post_id,'acelle_list_uid',true) == ''){
		
		// $current_user = wp_get_current_user();
		$_mail_settings = get_user_meta(get_current_user_id(),'_mail_settings',true);
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, ACELLE_DOMAIN.'/api/v1/lists?');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$list_array = array(
			"api_token" => ACELLE_APIKEY,
			"name" => $funnel_name,
			"from_email" => 'support@funnelmates.com',//$_mail_settings['mls_from_email'],
			"from_name" => $_mail_settings['mls_from_name'],
			"default_subject" => $_mail_settings['mls_email_subject'],
			"contact[company]" => $_mail_settings['mls_company'],
			"contact[state]" => $_mail_settings['mls_state'],
			"contact[address_1]" => $_mail_settings['mls_address1'],
			"contact[address_2]" => $_mail_settings['mls_address2'],
			"contact[city]" => $_mail_settings['mls_city'],
			"contact[zip]" => $_mail_settings['mls_zip'],
			"contact[phone]" => $_mail_settings['mls_phone'],
			"contact[country_id]" => $_mail_settings['mls_country'],
			"contact[email]" => $_mail_settings['mls_email'],
			"contact[url]" => $_mail_settings['mls_homepage'],
			"subscribe_confirmation" => '1',
			"send_welcome_email" => '0',
			"unsubscribe_notification" => '1',
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($list_array));
		curl_setopt($ch, CURLOPT_POST, 1);

		$headers = array();
		$headers[] = 'Accept: application/json';
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$html .= 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		//print_r($result);
		$result = json_decode($result);
		if($result->status == '1' && $result->list_uid){
			$listid = $result->list_uid;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, ACELLE_DOMAIN.'/api/v1/automations/'.$listid.'/api/call?');
			curl_setopt($ch, CURLOPT_POSTFIELDS, "api_token=".ACELLE_APIKEY."&uid=".$listid);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);

			$headers = array();
			$headers[] = 'Accept: application/json';
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				$html .= 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			$automation_uids = json_decode($result);
			unset($automation_uids->message);
			update_post_meta($post_id,'acelle_automation_uid',maybe_serialize($automation_uids));
			update_post_meta($post_id,'acelle_list_uid',$listid);
			
			$pages_uid = $listid.rand(0,99999);
			$pagesc_uid = $listid.rand(0,99999);
			// $maildb = new wpdb("funnelma_mailu","RX.N1d~jwveA","funnelma_mailserver","localhost");
			// $maildb = new wpdb("mailuser","8CcKYRp<~7ZA","mail_server","localhost");
			$maildb = new wpdb("root","Secutv643!!sf##~~6rfa","mail_server","localhost");
			$content = '<!-- Page container --><div class="page-container login-container" style="min-height: 249px;"><!-- Page content --><div class="page-content"><!-- Main content --><div class="content-wrapper"><div class="row"><div class="col-sm-2 col-md-3"></div><div class="col-sm-8 col-md-6"><!-- subscribe form --><h2 class="text-semibold mt-40 text-white">{LIST_NAME}</h2><div class="panel panel-body"><h4>Subscription Confirmed</h4><hr /><p>Your subscription to our list has been confirmed.</p><p>Thank you for subscribing!</p><a href="{CONTACT_URL}" class="btn btn-info bg-teal-800">Continue to our website</a> or <a href="{UPDATE_PROFILE_URL}" class="btn btn-info bg-teal-800">Manage your preferences</a></div><!-- /subscribe form --></div></div></div><!-- /main content --></div><!-- /page content --> <!-- Footer --><div class="footer text-white"><span class="text-white">{CONTACT_NAME}</span>, <a href="{CONTACT_URL}" class="text-white" target="_blank" rel="noopener">{CONTACT_URL}</a></div><!-- /footer --></div><!-- /page container -->';
			$content_confirmation = '<!-- Page container --><div class="page-container login-container" style="min-height: 249px;"><!-- Page content --><div class="page-content"><!-- Main content --><div class="content-wrapper"><div class="row"><div class="col-sm-2 col-md-3"></div><div class="col-sm-8 col-md-6"><!-- subscribe form --><h2 class="text-semibold mt-40 text-white">{LIST_NAME}</h2><div class="panel panel-body"><h4>Please Confirm Subscription</h4><hr />Click the link below to confirm your subscription:<br /> {SUBSCRIBE_CONFIRM_URL}<hr /><p>If you received this email by mistake, simply delete it. You won\'t be subscribed if you don\'t click the confirmation link above.</p><p>For questions about this list, please contact: <br /> <a href="mailto:{CONTACT_EMAIL}">{CONTACT_EMAIL}</a></p></div><!-- /subscribe form --></div></div></div><!-- /main content --></div><!-- /page content --> <!-- Footer --><div class="footer text-white">&copy; 2020. <span class="text-white">{CONTACT_NAME}</span>, <a href="{CONTACT_URL}" class="text-white" target="_blank">{CONTACT_URL}</a></div><!-- /footer --></div><!-- /page container -->';
			$url = 'https://funnelmates.com/c/'.$post_id.'/{SUBSCRIBER_CCID}';
			$ms_list = $maildb->get_row("SELECT * FROM `fmmsqmail_lists` WHERE uid='".$listid."' ");
			$maildb->query("INSERT INTO `fmmsqpages` (`uid`, `layout_id`, `mail_list_id`, `content`, `created_at`, `updated_at`, `subject`, `use_outside_url`, `outside_url`) VALUES ('$pages_uid','7','".$ms_list->id."','$content',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'Thank you','1','$url')");
			
			
			$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` WHERE funnel_id='$post_id' AND aff_network='creator' ORDER BY id ASC LIMIT 0,1");
			$subject_def = 'Please Confirm To Access '.$query->name;
			$maildb->query("INSERT INTO `fmmsqpages` (`uid`, `layout_id`, `mail_list_id`, `content`, `created_at`, `updated_at`, `subject`) VALUES ('$pagesc_uid','6','".$ms_list->id."','".addslashes($content_confirmation)."',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,'".addslashes($subject_def)."')");
		}
	}
	///////////////////////////////////////
	// print_r($array);
	$allready_exists = $wpdb->query("select * from `wp_fm_marketplace_items` where post_id = $post_id");
	/*print_r($allready_exists);
	exit;*/
	if($allready_exists != ""){
		$update = $wpdb->query("UPDATE `wp_fm_marketplace_items` SET `post_id`='$post_id',`pricing`='".maybe_serialize($array)."',`description`='$funnel_description',`image_id`='$funnel_image', `profit_system`='$aff_profit_system', max_price='$max_price' WHERE post_id = $post_id");
		//$update = $wpdb->query("UPDATE `wp_fm_marketplace_items` SET `post_id`='$post_id',`pricing`='".maybe_serialize($array)."',`description`='$funnel_description', `profit_system`='$aff_profit_system', max_price='$max_price' WHERE post_id = $post_id");
		// $update = update_post_meta($post_id,'is_funnel_published','1');
		/*$update = update_post_meta($post_id,'is_funnel_published','review');
		$update = update_post_meta($post_id,'review_date',date('F j, Y'));*/
		if(get_post_meta($post_id, 'is_user_improvising', true) == '1'){
			delete_post_meta($post_id, 'is_user_improvising');
			delete_post_meta($post_id, 'requested_feedback');
			$update = update_post_meta($post_id,'is_funnel_published','resubmitted');
			$update = update_post_meta($post_id,'review_date',date('F j, Y'));
		}else{
			$update = update_post_meta($post_id,'is_funnel_published','review');
			$update = update_post_meta($post_id,'review_date',date('F j, Y'));
		}
		echo '1';
		exit;
	}else{
		$wpdb->query("INSERT INTO `wp_fm_marketplace_items` (post_id,pricing,description,image_id,profit_system,max_price) VALUES ('$post_id', '".maybe_serialize($array)."','$funnel_description','$funnel_image','$aff_profit_system','$max_price')");
		//$wpdb->query("INSERT INTO `wp_fm_marketplace_items` (post_id,pricing,description,profit_system,max_price) VALUES ('$post_id', '".maybe_serialize($array)."','$funnel_description','$aff_profit_system','$max_price')");
		// $update = update_post_meta($post_id,'is_funnel_published','1');
		/*$update = update_post_meta($post_id,'is_funnel_published','review');
		$update = update_post_meta($post_id,'review_date',date('F j, Y'));*/
		if(get_post_meta($post_id, 'is_user_improvising', true) == '1'){
			delete_post_meta($post_id, 'is_user_improvising');
			delete_post_meta($post_id, 'requested_feedback');
			$update = update_post_meta($post_id,'is_funnel_published','resubmitted');
			$update = update_post_meta($post_id,'review_date',date('F j, Y'));
		}else{
			$update = update_post_meta($post_id,'is_funnel_published','review');
			$update = update_post_meta($post_id,'review_date',date('F j, Y'));
		}
		$update1 = update_post_meta($post_id,'is_funnel_public','1');
	}
	// if($update){
		echo '1';
	// }
	exit;
}
function update_promotional_tools(){
	global $wpdb;
	$action_type = $_POST['action_type'];
	$item_type = $_POST['item_type'];
	$funnel_id = $_POST['funnel_id'];
	$current_user_id = get_current_user_id();
	$date = date('Y-m-d H:i:s');
	if($action_type == 'update_content'){
		$item_id = $_POST['item_id'];
		if($item_id){
			if($item_type == 'email_swipe'){
				$data = array();
				$data['subject'] = $_POST['email_subject'];
				$data['content'] = $_POST['data'];
				$data = base64_encode(maybe_serialize($data));
				 $wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				 //echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='email_swipe'";
				 $wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='email_swipe'");
				 echo '1';
			}else if($item_type == 'yt_vid'){
				$data = array();
				$data['url'] = addslashes($_POST['data']);
				$data['desc'] = $_POST['video_desc'];
				$data = base64_encode(maybe_serialize($data));
				 $wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				 $wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='yt_vid'");
				 echo '1';
			}else if($item_type == 'banner'){
				$data = $_POST['data'];
				/*echo "UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id;
				exit;*/
				 $wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				 $wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'banner'");
				 echo '1';
			}else if($item_type == 'fb_ads'){
				$data = array();
				$data['url'] = $_POST['url'];
				$data['data'] = $_POST['data'];
				$data['image'] = $_POST['image'];
				$data = base64_encode(maybe_serialize($data));
				$wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				// echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'fb_ads'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'fb_ads'");
				echo '1';
			}else if($item_type == 'twitter'){
				$data = array();
				$data['url'] = $_POST['url'];
				//$data['data'] = stripslashes($_POST['data']);
				$data['data'] = $_POST['data'];
				$data['image'] = $_POST['image'];
				/*print_r($data);
				exit;*/
				$data = base64_encode(maybe_serialize($data));
				$wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				// echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'fb_ads'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'twitter'");
				echo '1';
			}else if($item_type == 'linkedin'){
				$data = array();
				$data['url'] = $_POST['url'];
				$data['data'] = $_POST['data'];
				$data['image'] = $_POST['image'];
				$data = base64_encode(maybe_serialize($data));
				$wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				// echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'fb_ads'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type = 'linkedin'");
				echo '1';
			}else if($item_type == 'article'){
				$data = array();
				$data['content'] = $_POST['data'];
				$data = base64_encode(maybe_serialize($data));
				$wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				//echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='email_swipe'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='article'");
				echo '1';
			}else if($item_type == 'misc'){
				$data = array();
				$data['content'] = $_POST['data'];
				$data = base64_encode(maybe_serialize($data));
				$wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET content='$data' WHERE id=".$item_id);
				//echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='email_swipe'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $funnel_id and type='misc'");
				echo '1';
			}
		}
	}else{
		$post_id = $_POST['post_id'];
		$data = $_POST['data'];
		if($post_id){
			$ins_pro_tool = $wpdb->query("INSERT INTO `".$wpdb->prefix."fm_promotional_tools` (item_type,post_id) VALUES ('{$item_type}','{$post_id}')");
			if($ins_pro_tool){
				echo $wpdb->insert_id;
				//echo "SELECT * FROM `wp_fm_funneledit_data` WHERE funnel_id = $post_id AND type = '".$item_type."'";
				$check_duplicate = $wpdb->get_var("SELECT * FROM `wp_fm_funneledit_data` WHERE funnel_id = $post_id AND type = '".$item_type."'");
				if($check_duplicate == ""){
					$wpdb->query("INSERT INTO `wp_fm_funneledit_data`(`user_id`, `funnel_id`, `type`, `date`, `process`) VALUES (".$current_user_id.",".$post_id.",'".$item_type."','".$date."','insert')");
				}else{
					$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'update' WHERE funnel_id = $post_id AND type = '".$item_type."'");
				}
			}
		}
	}
	exit;
}
function remove_promotional_tool(){
	global $wpdb;
	$item_id = $_POST['item_id'];
	$funnel_id = $_POST['funnel_id'];
	$funnel_item_type = $_POST['funnel_type'];
	$current_user_id = get_current_user_id();
	$date = date('Y-m-d H:i:s');
	if($item_id){
		$rem_pro_tool = $wpdb->query("DELETE FROM `".$wpdb->prefix."fm_promotional_tools` WHERE id=$item_id");
		if($rem_pro_tool){
			//echo "SELECT * FROM `wp_fm_funneledit_data` WHERE funnel_id = $funnel_id AND type = '".$funnel_item_type."' AND process = 'delete'";
			$check_duplicate = $wpdb->get_var("SELECT * FROM `wp_fm_funneledit_data` WHERE funnel_id = $funnel_id AND type = '".$funnel_item_type."' AND process = 'delete'");
			if($check_duplicate == ''){
				//echo "UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'delete' WHERE funnel_id = $funnel_id and type ='".$funnel_item_type."'";
				$wpdb->query("UPDATE `wp_fm_funneledit_data` SET date = '$date', process= 'delete' WHERE funnel_id = $funnel_id and type ='".$funnel_item_type."'");
			}
			echo '1';
		}
	}
	exit;
}
function update_promotional_tools_position(){
	global $wpdb;
	$imageids_arr = explode(',',$_POST['imageids']);
	$position = 1;
	foreach($imageids_arr as $id){
	  $wpdb->query("UPDATE `".$wpdb->prefix."fm_promotional_tools` SET position=".$position." WHERE id=".$id);
	  $position ++;
	}
	exit;
}
function funnel_website_links(){
	$post_id = $_POST['post_id'];
	global $wpdb;
	$link = '';
	/*$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." and aff_network = 'creator'");
	if($query != ''){
		$link = $query->aff_link ;
		$name = $query->name;
	}*/
	
	$html = '<style>.halfopacity{opacity:.5;}.affnetradiochecked{font-size:21px;font-weight:600;}label.warriorplus, label.clickbank, label.jvzoo, label.paykickstart, label.external{-webkit-transition: all .5s ease;-moz-transition: all .5s ease;transition: all .5s ease;cursor:pointer;}.radioerror{color: red;font-size: 21px;font-weight: 600;}.affiliate_link_error{background-color: #FF000055 !important;}.wl_instructions p {
    margin: 0;
    font-size: 12px;
    color: #333333;
    text-align: left;
    line-height: 25px;
    font-style: italic;
}.wl_instructions ul {
    text-align: left;
    line-height: 25px;
    color: #333333;font-size: 12px;
}</style>';
	$html .= '<input type="hidden" name="post_id" value="'.$post_id.'" />';
	$html .= '<select style="width:100%;" name="link_place" id="dropdown">
					<option  value="" disabled selected>Add A New Link</option>
					<option value="download">Download Link</option>
					<option value="email">Affiliate Or External Links</option>
					<option value="edit_external_link">Edit My Links</option>
			  </select>';
    $html .= '<div id="external_link_div" style="display:none;">';
		$query = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id);
		$html .= '<table style="width:100%;">
			<tr>
				<th class="aff_table">Name</th>
				<th class="aff_table">Link</th>
				<th class="aff_table">Type</th>
				<th class="aff_table">Edit</th>';
				if(current_user_can('administrator')){
					$html .= '<th class="aff_table">Link Identifier</th>';
				}
			$html .= '</tr>';
			foreach($query as $q){
				$id = $q->id;
				$name = $q->name;
				$identifier = $q->link_identifier;
				$type = $q->aff_network;
				$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
				if($type == 'clickbank'){
					$clickbank_id = $general_settings['clickbank_id'];
					if(empty($clickbank_id)){
						$clickbank_id = 'funnelmate';
					}
					if($q->notes){
						$linkto = 'http://'.$clickbank_id.'.'.$q->aff_link.'.hop.clickbank.net/?cbpage='.$q->notes;
					}else{
						$linkto = 'http://'.$clickbank_id.'.'.$q->aff_link.'.hop.clickbank.net/';
					}
					$link = '<a href="'.$linkto.'" target="_BLANK">'.$linkto.'</a>';
				}else if($type == 'warriorplus'){
					// $warriorplus_id = $general_settings['warriorplus_id'];
					// if(empty($warriorplus_id)){
						// $warriorplus_id = '932cb1277c5faf9aadfb613c52050e6';
					// }
					// $rs = warrior_plus_affiliate_approved($warriorplus_id);
					// $jumplinkRec = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` WHERE `aff_network`='warriorplus' AND `link_identifier` = '{$params[1]}'");
					// print_r($rs);
					// $offer_id = $q->aff_link;
					// foreach($rs->data as $data){
						// if($data->offer->id == $offer_id){
							// $promotion_url = $data->promote_url;
						// }
					// }
					$link = '<a target="_BLANK" href="https://funnelmates.com/wp/'.get_current_user_id().'/'.$q->link_identifier.'/">https://funnelmates.com/wp/'.get_current_user_id().'/'.$q->link_identifier.'/</a>';
				}else if($type == 'paykickstart'){
					$link = '<a target="_BLANK" href="https://funnelmates.com/pks/'.get_current_user_id().'/'.$q->link_identifier.'/">https://funnelmates.com/pks/'.get_current_user_id().'/'.$q->link_identifier.'/</a>';
				}else{
					$link = '<a target="_BLANK" data="'.$type.'" href="'.$q->aff_link.'">'.$q->aff_link.'</a>';
				}
				if($type == 'creator'){
					$type = 'Download';
				}/*else{
					$type = 'External';
				}*/
			$html .= '<tr class="website_link_row_'.$id.'">
					<td class="aff_table" id="link_name_'.$id.'">'.$name.'</td>
					<td class="aff_table '.$type.'_column" id="link_'.$id.'">'.$link.'</td>
					<td class="aff_table" id="identifier_'.$id.'">'.ucfirst($type).'</td>
					<td class="aff_table"><i class="fa fa-edit edit_external_link" data="'.$id.'" data-linktype="'.ucfirst($type).'" style="cursor:pointer;"></i>
					<i class="fa fa-trash delete_external_link" data="'.$id.'" data-linktype="'.ucfirst($type).'" style="cursor:pointer;"></i>
					</td>';
					if(current_user_can('administrator')){
						$html .= '<td class="aff_table">'.$identifier.'</td>';
					}
				$html .= '</tr>';
			}
		$html .= '</table>';
	$html .= '</div>';
	$html .= '<div class="linkplace_div" style="display:none;">';
		$html .= '<div class="aff_network_link" style="text-align: center;">';
			$html .= '<input type="text" name="internal_name" placeholder="Product/Link Name" style="width:100%;" />';
		$html .= '</div>';
		$html .= '<div class="aff_network_container" style="text-align: center;min-height: 50px;line-height: 25px;">';
			$html .= '<p style="margin: 0;text-align: center;color: #888;font-size: 11px;font-style: italic;position: relative;">Select your Link Type</p>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="jvzoo" id="jvzoo" /><label class="halfopacity jvzoo" for="jvzoo">JVZoo</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="clickbank" id="clickbank" /><label class="halfopacity clickbank" for="clickbank">ClickBank</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="warriorplus" id="warriorplus" /><label class="halfopacity warriorplus" for="warriorplus">Warrior Plus</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="paykickstart" id="paykickstart" /><label class="halfopacity paykickstart" for="paykickstart">PayKickStart</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="external" id="external" /><label class="halfopacity external" for="external">External</label>';
			$html .= '<div class="jvzoo_aff_link_div" style="display:none;">';
				$html .= '<input type="text" name="jvz_affiliate_link" placeholder="Affiliate Request Link" style="width:100%;" />';
				$html .= '<div class="wl_instructions">';
					$html .= '<p>The link users visit to request to promote the offer eg. https://www.jvzoo.com/affiliate/affiliateinfo/index/349507</p>';
				$html .= '</div>';
				$html .= '<input type="hidden" name="jvz_link_identifier" value="jvz'.get_current_user_id().$post_id.'" />';
				/*$html .= '<div class="wl_instructions">';
					$html .= '<p>All lower caps, no spaces or special characters</p>';
				$html .= '</div>';*/
			$html .= '</div>';
			$html .= '<div class="clickbank_aff_link_div" style="display:none;">';
				$html .= '<input type="text" name="cb_vendor_id" placeholder="Clickbank Vendor ID" style="width:100%;" />';
				$html .= '<div class="wl_instructions">';
					$html .= '<p>This is the account nickname of the product you\'re promoting</p>';
				$html .= '</div>';
				$html .= '<input type="hidden" name="cb_link_identifier" value="cb'.get_current_user_id().$post_id.'" />';
				/*$html .= '<div class="wl_instructions">';
					$html .= '<p>All lower caps, no spaces or special characters</p>';
				$html .= '</div>';*/
				$html .= '<div class="alt_page_cb" style="text-align: left;margin: 10px 0;">';
				$html .= '<input type="checkbox" name="alt_page" id="alt_page" value="1" /><label style="margin: 0;margin-left: 10px;font-size: 18px;" for="alt_page">alt page</label>';
				$html .= '<input type="text" name="alt_page_text" style="display:none;" id="alt_page_text" placeholder="alt page text" />';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="warriorplus_aff_link_div" style="display:none;">';
				$html .= '<div class="wl_instructions">';
					$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
					$wp_id = $general_settings['warriorplus_id'];
					if(empty($wp_id)){
						$html .= '<a target="_blank" href="/settings/">Please Connect Warrior Plus Account</a>';
					}else{
						/*$ch = curl_init();
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
						curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=$wp_id&limit=50");
						$result = curl_exec($ch);
						// $html .= 'https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=50';
						$json_decode_result = json_decode($result);
						// $html .= print_r($json_decode_result,true);*/
						
						$json_decode_result = get_user_meta(get_current_user_id(),'warrior_plus_products',true);
						// $json_decode_result = base64_decode($json_decode_result);
						// $html . = print_r($json_decode_result,true);
						$json_decode_result = maybe_unserialize($json_decode_result);
						// $html . = print_r($json_decode_result,true);
						$html .= '<select name="wp_offer" style="width:100%;">';
							$html .= '<option valule="0" disabled selected>Select Product</option>';
						foreach($json_decode_result->data as $data){
							$html .= '<option value="'.$data->offer->id.'">'.$data->offer->name.'</option>';
						}
						$html .= '</select>';
						$html .= '<input type="hidden" name="wp_link_identifier" value="wp'.get_current_user_id().$post_id.'" />';
					}
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="paykickstart_aff_link_div" style="display:none;">';
				$html .= '<input type="text" name="pks_campaign_id" placeholder="Campaign ID" style="width:100%;" />';
				$html .= '<div class="wl_instructions">';
					$html .= '<p>This is the Campaign ID of the product you\'re promoting</p>';
				$html .= '</div>';
				$html .= '<input type="hidden" name="pks_link_identifier" value="pks'.get_current_user_id().$post_id.'" />';
				$html .= '<input type="text" name="notes_pks" placeholder="Notes" style="width:100%;" />';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="external_aff_link_div" style="display:none;">';
				$user = wp_get_current_user();
				if(!in_array( 'funnelmates_pro', (array) $user->roles ) || !in_array( 'administrator', (array) $user->roles )){
					$html .= '<div class="wl_instructions" style="padding: 5px;border: 1px solid #856404;background-color: #fff3cd;color: #856404;border-radius: 5px;">';
						$html .= '<h5>Warning!</h5>';
						$html .= '<p>External links can only be used on:</p>';
						$html .= '<ul>';
							$html .= '<li>Funnels you publish for personal use only</li>';
							$html .= '<li>Funnels you publish in marketplace with Whitelabel or Exclusive rights permissions.  Free and Premium funnels cannot contain external links</li>';
						$html .= '</ul>';
						$html .= '<p>Please note: Whitelabel and exclusive funnels can only be sold if you\'re a pro FunnelMates member. <a href="/add-funds/">Click here to view available membership upgrade options.</a></p>';
					$html .= '</div>';
				}
				$html .= '<input type="text" name="external_affiliate_link" placeholder="Add Your Link" style="width:100%;" />';
				$html .= '<input type="hidden" name="external_link_identifier" value="ex'.get_current_user_id().$post_id.'" />';
				/*$html .= '<div class="wl_instructions">';
					$html .= '<p>All lower caps, no spaces or special characters</p>';
				$html .= '</div>';*/
				$html .= '<input type="text" name="notes" placeholder="Notes" style="width:100%;" />';
				$html .= '<div class="wl_instructions">';
					$html .= '<p>Optional.  Can be used to provide information to funnel customer about why you\'ve included this link, links to external affiliate program or why they should keep the link as it is.</p>';
				$html .= '</div>';
			$html .= '</div>';
		$html .= '</div>';
	$html .= '</div>';
	$html .= '<div class="dropdown_div" style="display:none;">';
		/*$html .= '<input type="text" name="download_name" placeholder="Download Name (For Your Reference)" style="width:100%;" value="'.$name.'"/>';
		$html .= '<input type="text" name="download_link" placeholder="Download link" style="width:100%;" value="'.$link.'"/>';*/
		$html .= '<input type="text" name="download_name" placeholder="Download Name (For Your Reference)" style="width:100%;" value=""/>';
		$html .= '<input type="text" name="download_link" placeholder="Download link" style="width:100%;" value=""/>';
	$html .= '</div>';
	$html .= '<p class="identifier_error" style="display:none;color:#FF0000;font-size:12px;">Identifier Already in Use.</p>';
	$html .= '<input type="hidden" name="link_type" value="download_link" />';
	$html .= '<input type="hidden" name="affiliate_network_type" value="" />';
	$html .= '<button class="process_aff_link button" data-postid="'.$post_id.'" style="display:none;border: 0;background:#279dbc;color: #FFF;padding: 10px 50px;border-radius: 5px;font-weight: 600;margin-top: 10px;" type="button">Process Link</button>';
	/*$html .= '<div class="linkplace_div" style="display:none;">';
		$html .= '<div class="aff_network_link" style="text-align: center;">';
			$html .= '<input type="text" name="internal_name" placeholder="Product/Link Name" style="width:100%;" />';
		$html .= '</div>';
		$html .= '<div class="aff_network_container" style="text-align: center;min-height: 50px;line-height: 50px;">';
			$html .= '<p style="margin: 0;text-align: center;color: #BBB;font-size: 11px;font-style: italic;position: relative;margin-bottom: -18px;">Select your Link Type</p>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="jvzoo" id="jvzoo" /><label class="halfopacity jvzoo" for="jvzoo">JVZoo</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="clickbank" id="clickbank" /><label class="halfopacity clickbank" for="clickbank">ClickBank</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="warriorplus" id="warriorplus" /><label class="halfopacity warriorplus" for="warriorplus">Warrior Plus</label>';
			$html .= '<input style="visibility:hidden;" type="radio" name="affiliate_network" value="external" id="external" /><label class="halfopacity external" for="external">External</label>';
		$html .= '</div>';
		$html .= '<div class="aff_network_link" style="text-align: center;display:none;">';
			$html .= '<input type="text" name="affiliate_link" placeholder="Affiliate Link" style="width:100%;" />';
			$html .= '<input type="text" name="link_identifier" placeholder="Link Identifier" style="width:100%;" />';
		$html .= '</div>';
	$html .= '</div>';
	$html .= '<div class="dropdown_div">';
		$html .= '<input type="text" name="download_name" placeholder="Download Name (For Your Reference)" style="width:100%;" value="'.$link.'"/>';
		$html .= '<input type="text" name="download_link" placeholder="Download link" style="width:100%;" value="'.$link.'"/>';
	$html .= '</div>';
	$html .= '<button class="process_aff_link button" data-postid="'.$post_id.'" style="border: 0;background:#279dbc;color: #FFF;padding: 10px 50px;border-radius: 5px;font-weight: 600;margin-top: 10px;" type="button">Process Link</button>';
	//$html .= '</div>';*/
	echo $html;
	exit;
}
function unlock_funnel(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$getrec = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items` WHERE post_id=".$post_id);
	// print_r($getrec);
	$html .= '<div>';
		$html .= '<h1 style="text-align:center;text-decoration:underline;">'.get_the_title($getrec->post_id).'</h1>';
		$html .= '<p style="font-size: 20px;text-align: center;">'.$getrec->description.'</p>';
		$html .= '<div style="width:100%;padding: 0 1px;display:inline-block;">';
			$html .= '<div style="width:50%;float:left;padding: 0 1px;text-align:center;">';
				$html .= '<i class="fa fa-tag" style="margin-right: 10px;"></i>';
				$category = get_the_category($getrec->post_id);
				$html .= '<a href="'.get_category_link($category[0]->term_id).'" rel="category tag">'.$category[0]->name.'</a>';
			$html .= '</div>';
			$html .= '<div style="width:50%;float:left;padding: 0 1px;text-align:center;">';
				$html .= '<i class="fa fa-user" style="margin-right: 10px;"></i>';
				$author_id = get_post_field ('post_author', $getrec->post_id);
				$display_name = get_the_author_meta( 'display_name' , $author_id ); 
				$category = get_the_category($getrec->post_id);
				$html .= '<a href="'.get_permalink($author_id).'" rel="category tag">'.$display_name.'</a>';
			$html .= '</div>';
		$html .= '</div>';
		$a=array("https://fnl1.com", "https://fnl2.com", "https://fnl3.com", "https://fnl5.com", "https://fnl7.com");
		$random_keys=array_rand($a,1);
		$root_url = $a[$random_keys];
		$html .= '<p style="margin: 10px 0;"><a class="btn btn-info" target="_BLANK" style="width: 100%;font-weight: 600;box-shadow: 0px 0px 15px 1px #999;font-size: 20px;" href="'.$root_url.'/f/'.$getrec->post_id.'/5/">Preview Funnel<br/><span style="font-size: 12px;font-weight: normal;">(You can check funnel before purchasing)</span></a></p>';
		$coupon = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_coupon_code` WHERE funnel_id='".$getrec->post_id."'");
		if($coupon->id){
			$html .= '<p id="coupon_code_p" style="text-align:center;margin: 0;"><a href="javascript:void(0);" id="enter_coupon_code" style="font-size: 12px;color: #999999;text-decoration: none;">Have A Coupon Code?</a></p>';
			$html .= '<table class="table tbl_coupon">';
				$html .= '<tr>';
					$html .= '<td><input type="text" name="insert_coupon_code" /></td>';
					$html .= '<td><input type="hidden" value="'.$coupon->id.'" name="apply_coupon_id" /><input type="hidden" value="'.$getrec->post_id.'" name="coupon_funnel_id" /><button id="apply_coupon" class="btn btn-success">Apply</button></td>';
				$html .= '</tr>';
			$html .= '</table>';
		}
		if($author_id == get_current_user_id()){
			$html .= '<h3 style="text-align:center;margin:50px 0;">You\'re Creator of this Funnel! :)</h3>';
			echo $html;
			exit;
		}
		$pricing = maybe_unserialize($getrec->pricing);
		$activat_funnels = $wpdb->get_results("SELECT type FROM `wp_setc_activated_funnels` WHERE funnel_id='".$post_id."' AND user_id='".get_current_user_id()."' ");
		$html .= '<div id="price_div_unlock_funnel">';
		foreach($activat_funnels as $funnel){
			$funnel_type = $funnel->type;
			if(array_key_exists($funnel_type,$pricing)){
				$btn_disable = "disabled";
			}else{
				$btn_disable = "";
			}
		}
		foreach($pricing as $k=>$p){
			$i = 0;
			if($funnel_type == $k){
				$i++;
				if($i > 0){
					$btn_disable = "";
				}
			}
			$html .= '<div style="width:100%;padding: 0 1px;display:inline-block;margin:10px 0;">';
			$html .= '<div style="width:25%;float:left;padding: 0 1px;">';
				$html .= '<div>';
					$html .= '<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">'.ucfirst($k).'</p>';
					$html .= '<div id="price_'.$post_id.'_'.$k.'" style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;">$'.$p.'</div>';
				$html .= '</div>';
			$html .= '</div>';
			$html .= '<div style="width:75%;float:left;padding: 0 1px;line-height:65px;">';
				$html .= '<div style="margin-left: 20px;">';
					$already = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id='".$post_id."' AND type='".strtolower($k)."' AND user_id='".get_current_user_id()."' ");
					//print_r($already);
					//$already->type;
					if(count($already) == 0){
						$current_balance = get_funnelmates_credit(get_current_user_id());
						if($current_balance >= $p){
							$btn_label = 'Activate '.ucfirst($k).' Funnel';
							$btn_class = 'success';
							$activated_free_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_activated_funnels` WHERE user_id='".get_current_user_id()."' AND type='free'");
							$funnel_limit = -1;
							$user_meta = get_userdata(get_current_user_id());
							if(in_array('funnelmates_access',$user_meta->roles)){
								$funnel_limit = 2;
							}else if(in_array('funnelmates_deluxe',$user_meta->roles)){
								$funnel_limit = 2;
							}
							if(strtolower($k) == 'free' && ($activated_free_count >= $funnel_limit && $funnel_limit != -1)){
								$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$post_id.' limit" onclick="funnel_active_limit();" class="btn btn-'.$btn_class.'" '.$btn_disable.'>'.$btn_label.'</button>';
							}else{
								$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$post_id.'" onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$post_id.','."'".strtolower($p)."'".');" class="btn btn-'.$btn_class.'" '.$btn_disable.'>'.$btn_label.'</button>';
							}
						}else{
							$btn_label = 'Insufficient Balance';
							$btn_class = 'danger';
							$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$post_id.'" class="btn btn-'.$btn_class.'" disabled>'.$btn_label.'</button>'; //onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$post_id.');"
						}
					}else{
						$html .= '<button class="btn btn-warning" disabled>Already Activated! '.ucfirst($k).' Funnel</button>'; //onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$post_id.');"
					}
				$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		$html .= '</div>';
	$html .= '</div>';
	echo $html;
	exit;
}
function unlock_funnel_finalize(){
	global $wpdb;
	$type = $_POST['type'];
	$post_id = $_POST['post_id'];
	$price = $_POST['price'];
	$is_coupon = $_POST['is_coupon'];
	$date = date('Y-m-d H:i:s');
	
	// exit;
	// to get current user name
	global $current_user;
	wp_get_current_user() ;
	$c_user_name = $current_user->user_login; // current user name
	
	if($type && $post_id){
		//if($type == 'free'){
			$getrec = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items` WHERE post_id=".$post_id);
			$pricing = maybe_unserialize($getrec->pricing);
			if($is_coupon){
				$array = array();
				$get_coupon = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_coupon_code` WHERE id='$is_coupon'");
				// print_R($get_coupon);exit;
				foreach($pricing as $k=>$p){
					if($get_coupon->coupon_type == 'percentage'){
						if($p > 0){
							$final_p = ($p * $get_coupon->coupon_value) / 100;
							$final_p = $p - $final_p;
						}else{
							$final_p = $p;
						}
					}else if($get_coupon->coupon_type == 'price'){
						if($p > 0){
							$final_p = $p - $get_coupon->coupon_value;
						}else{
							$final_p = $p;
						}
					}
					if($final_p < 0){
						$final_p = 0;
					}
					$array[$k] = number_format($final_p,1);
				}
				$pricing = $array;
			}
			// if(get_current_user_id() == '7'){
				// print_r($pricing);exit;
			// }
			if(array_key_exists($type,$pricing)){
				/*$balance = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='C' AND user_id='".get_current_user_id()."' ");
				if(!$balance){
					$balance = 0;
				}*/
				$balance = get_funnelmates_credit(get_current_user_id());
				if($balance >= $pricing[$type]){
					$settle = settle_balance($pricing[$type],$type,$post_id);
					if($settle == 1){
						$already = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id='".$post_id."' AND user_id='".get_current_user_id()."'  AND active_status = 0");
						if(count($already) == 0){
							$activated = $wpdb->query("INSERT INTO `wp_setc_activated_funnels` (funnel_id,user_id,type) VALUES ('".$post_id."','".get_current_user_id()."','".$type."')");
						}else{
							$activated = $wpdb->query("UPDATE `wp_setc_activated_funnels` SET `type`= '".$type."',`time`= '".$date."' WHERE user_id = '".get_current_user_id()."' AND funnel_id = '".$post_id."' AND active_status = 0");
							
						}
							$purchase = $wpdb->query("INSERT INTO `wp_setc_pks_balance` (`value`, `user_id`, `type`, `time`, `funnel_id`) VALUES ('".$price."','".get_current_user_id()."','D','".$date."','".$post_id."')");
						
						//////////************type***************//////////
						/*if($type == 'exclusive'){
							/*if($type == 'exclusive'){
								$dlt_funnel_marketplace = $wpdb->query("DELETE FROM `wp_fm_marketplace_items` WHERE post_id = $post_id");
							}*
						}else*/ 
						if($type == 'whitelabel' || $type == 'exclusive'){
							$postarr = array(
								'post_author' => get_current_user_id(),
								'post_title' => get_the_title($post_id).'-'.$c_user_name,
								'post_status' => 'publish',
								'post_type' => 'landing',
								'meta_input' => array(
									'funnel_type' => $type
								)
							);
							$newpost = wp_insert_post($postarr);
							if($newpost){
								update_post_meta($newpost,'bb_preset_template',$post_id);
								
								$args = array(
									'name'      	 => 'thank-you',
									'post_type'      => 'landing',
									'posts_per_page' => -1,
									'post_parent'    => $post_id,
									'order'          => 'ASC',
									'orderby'        => 'menu_order'
								);
								$children_t = get_posts( $args );
								$funnel_thanks = $children_t[0]->ID;
								$postarr_thanks = array(
									'post_author' => get_current_user_id(),
									'post_title' => 'Thank You',
									'post_status' => 'publish',
									'post_parent' => $newpost,
									'post_type' => 'landing',
								);
								$thanks_post = wp_insert_post($postarr_thanks);
								update_post_meta($thanks_post,'bb_preset_template',$funnel_thanks);
								
								
								$args = array(
									'name'      	 => 'confirm',
									'post_type'      => 'landing',
									'posts_per_page' => -1,
									'post_parent'    => $post_id,
									'order'          => 'ASC',
									'orderby'        => 'menu_order'
								);
								$children_c = get_posts( $args );
								$funnel_confirm = $children_c[0]->ID;
								$postarr_confirm = array(
									'post_author' => get_current_user_id(),
									'post_title' => 'Confirm',
									'post_status' => 'publish',
									'post_parent' => $newpost,
									'post_type' => 'landing',
								);
								$confirm_post = wp_insert_post($postarr_confirm);
								update_post_meta($confirm_post,'bb_preset_template',$funnel_confirm);
								
								$args = array(
									'name'      	 => 'download',
									'post_type'      => 'landing',
									'posts_per_page' => -1,
									'post_parent'    => $post_id,
									'order'          => 'ASC',
									'orderby'        => 'menu_order'
								);
								$children_d = get_posts( $args );
								$funnel_download = $children_d[0]->ID;
								$postarr_download = array(
									'post_author' => get_current_user_id(),
									'post_title' => 'Download',
									'post_status' => 'publish',
									'post_parent' => $newpost,
									'post_type' => 'landing',
								);
								$download_post = wp_insert_post($postarr_download);
								update_post_meta($download_post,'bb_preset_template',$funnel_download);
								
								update_post_meta($newpost,'is_whitelabel','1');
							}
							
							$promotional_tools = $wpdb->get_results("SELECT * FROM `wp_fm_promotional_tools` WHERE post_id = $post_id ");
							foreach($promotional_tools as $pt){
								$item = $pt->item_type;
								if($item == 'banner'){
									$content = addslashes($pt->content);
								}else{
									$content = maybe_unserialize(base64_decode($pt->content));
									$content_data = str_replace($post_id,$newpost,$content['data']);
									$content_array = array();
									if($content['url']){
										$content_array['url'] = $content['url'];
									}
									if($content['data']){
										$content_array['data'] = $content_data;
									}
									if($content['image']){
										$content_array['image'] =$content['image'];
									}
									if($content['desc']){
										$content_array['desc'] =$content['desc'];
									}
									if($content['subject']){
										$content_array['subject'] =$content['subject'];
									}
									$content  = addslashes(base64_encode(maybe_serialize($content_array)));
								}
								// $content = addslashes($pt->content);
								$ins_pro_tool = $wpdb->query("INSERT INTO `wp_fm_promotional_tools` (item_type,post_id,content) VALUES ('{$item}','{$newpost}','{$content}')");
							}
							
							$followupemail = $wpdb->get_results("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id = $post_id ");			
							foreach($followupemail as $f_e){
								$subject = addslashes($f_e->subject);
								$content = addslashes($f_e->content);
								$content = str_replace($post_id,$newpost,$content);
								$content = str_replace($funnel_confirm,$confirm_post,$content);
								$content = str_replace($funnel_download,$download_post,$content);
								$content = str_replace($funnel_thanks,$thanks_post,$content);
								$femail_type = $f_e->type;
								$funnel_id = $newpost;	
								$ins_foll_email = $wpdb->query("INSERT INTO `wp_setc_followup_emails`(subject, content, funnel_id, type) VALUES ('$subject','$content','$funnel_id','$femail_type')");
							}
							
							$sql_query = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = '$post_id'");
							foreach($sql_query as $sq){
								$funnel_id = $newpost;
								$internal_name = $sq->name;
								$affiliate_network = $sq->aff_network;
								$affiliate_link = $sq->aff_link;
								$link_identifier = $sq->link_identifier;
								if($type == 'whitelabel'){
									$link_identifier = unique_identifier($link_identifier);
								}
								$notes = addslashes($sq->notes);
								if($sql_query){
								   $query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link, link_identifier, notes) VALUES ('".$funnel_id."', '".$internal_name."', '".$affiliate_network."', '".$affiliate_link."', '".$link_identifier."', '".$notes."')");
								}
							}
							
							if($type == 'exclusive'){
								$dlt_funnel_marketplace = $wpdb->query("DELETE FROM `wp_fm_marketplace_items` WHERE post_id = $post_id");
								$wpdb->query("UPDATE `wp_setc_activated_funnels` SET new_funnel_id = $newpost WHERE user_id = '".get_current_user_id()."' AND funnel_id = '".$post_id."' AND active_status = 0");
								// delete_funnel($post_id);
							}else{
								$wpdb->query("UPDATE `wp_setc_activated_funnels` SET new_funnel_id = $newpost WHERE user_id = '".get_current_user_id()."' AND funnel_id = '".$post_id."' AND active_status = 0");
							} 
							
						}
						//////////************type***************//////////
						
						if($activated && $purchase){
							$u_id = get_current_user_id();
							$check_purchase_info = $wpdb->get_row("SELECT *  FROM `wp_postmeta` WHERE meta_key = '_purchase_info_$u_id' and post_id = $post_id");
							if($check_purchase_info){
								update_post_meta( $post_id, '_purchase_info_'.$u_id, $date);
								if($newpost){
									delete_post_meta( $post_id, '_purchase_info_'.$u_id);
									add_post_meta( $newpost, '_created_info_'.$u_id, $date);
								}
							}else{
								if($type == 'exclusive' || $type == 'whitelabel'){
									add_post_meta( $newpost, '_created_info_'.$u_id, $date);
								}else{
									add_post_meta( $post_id, '_purchase_info_'.$u_id, $date);
								}
							}
							echo 'Activated..!';
							if($type == 'exclusive'){
								delete_funnel($post_id);
							}
						}
					}
				}else{
					echo 'Insufficient Balance';
				}
			}else{
				echo 'No '.$type.' Type Funnel Exists';
			}
		//}
	}else{
		echo "Data Missing";
	}
	exit;
}
function paypal_withdrwal_request(){
	global $wpdb;
	$user_id = get_current_user_id();
	$email = $_POST['paypal_email'];
	$amount = $_POST['paypal_amount'];
	$insert_query = $wpdb->query("INSERT INTO wp_withdrawal (user_id, email, request_amount, type) VALUES ('$user_id', '$email', '$amount', '1')");
	if($insert_query){
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function payment_withdrawal(){
	global $wpdb;
	$user_id = $_POST['user_id'];
	$amount = $_POST['amount'];
	
	$email = $_POST['email'];
	$name = $_POST['name'];
	$address = $_POST['address'];
	$city = $_POST['city'];
	$postal_code = $_POST['postal_code'];
	$bank_name = $_POST['bank_name'];
	$bank_address = $_POST['bank_address'];
	$account_name = $_POST['account_name'];
	$account_number = $_POST['account_number'];
	$swift_code = $_POST['swift_code'];
	$ifsc_code = $_POST['ifsc_code'];
	$currency = $_POST['currency'];
	
	//echo "INSERT INTO wp_withdrawal(user_id, user_name, user_email, user_address,  request_amount) VALUES ('$user_id', '$name', '$email', '$address', '$amount')"; exit;
	// echo "INSERT INTO wp_withdrawal (user_id, full_name, email, home_address, city, postal_code, bank_name, bank_address, account_name, account_number, ifsc_code, currency, request_amount) VALUES ('$user_id', '$name', '$email', '$address', '$city', '$postal_code', '$bank_name', '$bank_address', '$account_name', '$account_number', '$ifsc_code', '$currency', '$amount')";exit;
	$insert_query = $wpdb->query("INSERT INTO wp_withdrawal (user_id, full_name, email, home_address, city, postal_code, bank_name, bank_address, account_name, account_number, swift_code, ifsc_code, currency, request_amount) VALUES ('$user_id', '$name', '$email', '$address', '$city', '$postal_code', '$bank_name', '$bank_address', '$account_name', '$account_number', '$swift_code', '$ifsc_code', '$currency', '$amount')");
	
	if($insert_query){
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function settle_balance($amount,$type = 'free',$funnel_id){
	global $wpdb;
	$author = get_post_field('post_author', $funnel_id);
	$admin_amount = (ADMIN_SHARE / 100) * $amount;
	$creator_amount = $amount - $admin_amount;
	$wpdb->query("INSERT INTO `wp_setc_pks_main_balance` (amount,user,type,funnel_id,funnel_type) VALUES ('".$amount."','".get_current_user_id()."','D','".$funnel_id."','".$type."')");
	$lastid = $wpdb->insert_id;
	$wpdb->query("INSERT INTO `wp_setc_pks_main_balance` (amount,user,type,funnel_id,funnel_type,linkto) VALUES ('".$creator_amount."','".$author."','C','".$funnel_id."','".$type."','$lastid')");
	$wpdb->query("INSERT INTO `wp_setc_pks_main_balance` (amount,user,type,funnel_id,funnel_type,admin_share,linkto) VALUES ('".$admin_amount."','".ADMIN_ACCOUNT."','C','".$funnel_id."','".$type."','1','$lastid')");
	return 1;
}
function get_user_main_fm_balance($user){
	global $wpdb;
	//$balance = $wpdb->get_var("SELECT SUM(amount) FROM `wp_setc_pks_main_balance` WHERE type='C' AND user='".$user."' ");
	$c_balance = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='C' AND user_id ='".$user."' ");
	$d_balance = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='D' AND user_id ='".$user."' ");
	$balance = $c_balance - $d_balance;
	return $balance;
}
function withdrawal_search_email(){
	global $wpdb;
	$html = '';
	$email = $_POST['search'];
	$current_tab = $_POST['c_tab'];
	if($current_tab == "pending_tab"){
		$status_id = 0;
	}else if($current_tab == "accept_tab"){
		$status_id = 1;
	}else{
		$status_id = 2;
	}
	$query = $wpdb->get_results("SELECT * FROM `wp_withdrawal` where user_email LIKE '%$email%' and request_process = $status_id");
	$html .='
	<tr class="head_title">
		<th>user ID</th>
		<th>user name</th>
		<th>user email</th>
		<th>user address</th>
		<th>amount</th>
		<th>action</th>
	</tr>';
	foreach($query as $q){
		$w_id = $q->id;
		$id = $q->user_id;
		$email = $q->user_email;
		$name = $q->user_name;
		$address = $q->user_address;
		$amount = $q->request_amount;
		$amount_process = $q->request_process;
		if($amount_process == 0){
			$accept = "<button id='accept_text$w_id' class='withdrawal_accept btn_accept_withdrawal' value=".$w_id.">Accept</button>";
			$reject = "<button id='reject_text$w_id' class='withdrawal_reject btn_reject_withdrawal' value=".$w_id.">Reject</button>";
		}elseif($amount_process == 1){
			$accept = "<button class='btn_accept_withdrawal'>Accepted</button>";
			$reject  = "";
		}else{
			$accept = "";
			$reject = "<button class='btn_reject_withdrawal'>Rejected</button>";
		}
		$html .= '<tr>
			<td>'.$id.'</td>
			<td>'.$name.'</td>
			<td>'.$email.'</td>
			<td>'.$address.'</td>
			<td>'.$amount.'</td>
			<td style="display:flex;">
				<span>'.$accept.'</span>
				<span>'.$reject.'</span>
			</td>
		</tr>';
	}
	echo $html;
	exit;
}
function withdrawal_accept_request(){
	$id = $_POST['id'];
	global $wpdb;
	//echo "UPDATE `wp_withdrawal` SET `request_process`= '1' WHERE $accept";
	$withdrawal_query = $wpdb->get_row("SELECT * FROM `wp_withdrawal` WHERE id = $id");
	if($withdrawal_query->id){
		$transaction_history = $wpdb->query("INSERT INTO wp_setc_pks_main_balance (amount,user,type,funnel_id,funnel_type) VALUES ('".$withdrawal_query->request_amount."','".$withdrawal_query->user_id."','W','0','0')");
		if($transaction_history){
			$withdrawal_query = $wpdb->query("UPDATE `wp_withdrawal` SET request_process='1' WHERE id = $id");
			echo '1';
		}
	}
	exit;
}
function withdrawal_reject_request(){
	$reject = $_POST['reject'];
	global $wpdb;
	//echo "UPDATE `wp_withdrawal` SET `request_process`= '1' WHERE $reject";
	$withdrawal_query = $wpdb->query("UPDATE `wp_withdrawal` SET `request_process`= '2' WHERE id = $reject");
	exit;
}

function withdrawal_status_tab(){
	global $wpdb;
	$withdrwal_statue = $_POST['status'];
	$withdrwal_offset = $_POST['offset'];
	$withdrwal_ppp = $_POST['ppp'];
	if($withdrwal_statue == 'pending'){
		$withdrwal_id = 0;		
		$htmlpending = '';
		if($withdrwal_offset == ''){
		$htmlpending .='<table  id="customers" align="center" border="1" class="withdrawal_data">
		<tr class="head_title">
			<th>user ID</th>
			<th>user name</th>
			<th>user email</th>
			<th>user address</th>
			<th>amount</th>
			<th>action</th>
		</tr>';
		}
		if($withdrwal_offset != ''){
			//echo "SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $wtthdrwal_offset, $wtthdrwal_ppp";
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $withdrwal_offset, $withdrwal_ppp");
		}else{
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT 2");
		}
		foreach($withdrwal_status_query as $q){
			$w_id = $q->id;
			$id = $q->user_id;
			$email = $q->user_email;
			$name = $q->user_name;
			$address = $q->user_address;
			$amount = $q->request_amount;
			$amount_process = $q->request_process;
			if($amount_process == 0){
				$accept = "<button id='accept_text$w_id' class='withdrawal_accept btn_accept_withdrawal' value=".$w_id.">Accept</button>";
				$reject = "<button id='reject_text$w_id' class='withdrawal_reject btn_reject_withdrawal' value=".$w_id.">Reject</button>";
			}elseif($amount_process == 1){
				$accept = "<button class='btn_accept_withdrawal'>Accepted</button>";
				$reject  = "";
			}else{
				$accept = "";
				$reject = "<button class='btn_reject_withdrawal'>Rejected</button>";
			}
			$htmlpending .= '<tr>
				<td>'.$id.'</td>
				<td>'.$name.'</td>
				<td>'.$email.'</td>
				<td>'.$address.'</td>
				<td>'.$amount.'</td>
				<td>
					<span>'.$accept.'</span>
					<span>'.$reject.'</span>
				</td>
			</tr>';
		}		
		if($withdrwal_offset == ''){
		$htmlpending .= '</table>';
		$htmlpending .= '<button id="withdrawal_more" value="pending">Load More</button>';
		$htmlpending .= '<input type="hidden" value="1" id="page_value">';
		$htmlpending .= '<input type="hidden" value="pending_tab" id="active_tab">';
		}
		//$pagenav .= 'jquery('.pagenav').value('1uuu')';
		//$pagenav = '1';
		//$pagenav = json_decode($page_nav);
	}elseif($withdrwal_statue == 'accepted'){
		$withdrwal_id = 1;
		
		$htmlaccepted = '';
		if($withdrwal_offset == ''){
		$htmlaccepted .='<table  id="customers" align="center" border="1" class="withdrawal_data">
		<tr class="head_title">
			<th>user ID</th>
			<th>user name</th>
			<th>user email</th>
			<th>user address</th>
			<th>amount</th>
			<th>action</th>
		</tr>';
		}
		
		if($withdrwal_offset != ''){
			//echo "SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $withdrwal_offset, $withdrwal_ppp";
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $withdrwal_offset, $withdrwal_ppp");
		}else{
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id  ORDER BY `id` DESC LIMIT 2");
		}
		
		foreach($withdrwal_status_query as $q){
			$w_id = $q->id;
			$id = $q->user_id;
			$email = $q->user_email;
			$name = $q->user_name;
			$address = $q->user_address;
			$amount = $q->request_amount;
			$amount_process = $q->request_process;
			if($amount_process == 0){
				$accept = "<button id='accept_text$w_id' class='withdrawal_accept btn_accept_withdrawal' value=".$w_id.">Accept</button>";
				$reject = "<button id='reject_text$w_id' class='withdrawal_reject btn_reject_withdrawal' value=".$w_id.">Reject</button>";
			}elseif($amount_process == 1){
				$accept = "<button class='btn_accept_withdrawal'>Accepted</button>";
				$reject  = "";
			}else{
				$accept = "";
				$reject = "<button class='btn_reject_withdrawal'>Rejected</button>";
			}
			$htmlaccepted .= '<tr>
				<td>'.$id.'</td>
				<td>'.$name.'</td>
				<td>'.$email.'</td>
				<td>'.$address.'</td>
				<td>'.$amount.'</td>
				<td>
					<span>'.$accept.'</span>
					<span>'.$reject.'</span>
				</td>
			</tr>';
		}
		if((empty($withdrwal_status_query)) && $withdrwal_offset == ''){
			$htmlaccepted .= '<h1>Record not found</h1>';
		}
		if($withdrwal_offset == '' && (!empty($withdrwal_status_query))){
			$htmlaccepted .= '</table>';
			$htmlaccepted .= '<button id="withdrawal_more" value="accepted">Load More</button>';
			$htmlaccepted .= '<input type="hidden" value="1" id="page_value">';
			$htmlaccepted .= '<input type="hidden" value="accept_tab" id="active_tab">';
		}
		//$pagenav = '2';
	}else{
		$withdrwal_id = 2;
		
		$htmlrejected = '';
		
		if($withdrwal_offset == ''){
		$htmlrejected .='<table  id="customers" align="center" border="1" class="withdrawal_data">
		<tr class="head_title">
			<th>user ID</th>
			<th>user name</th>
			<th>user email</th>
			<th>user address</th>
			<th>amount</th>
			<th>action</th>
		</tr>';
		}
		
		if($withdrwal_offset != ''){
			//echo "SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $withdrwal_offset, $withdrwal_ppp";
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process = $withdrwal_id ORDER BY `id` DESC LIMIT $withdrwal_offset, $withdrwal_ppp");
		}else{
			$withdrwal_status_query = $wpdb->get_results("SELECT * FROM `wp_withdrawal`where request_process	= $withdrwal_id ORDER BY `id` DESC LIMIT 2");
		}
		foreach($withdrwal_status_query as $q){
			$w_id = $q->id;
			$id = $q->user_id;
			$email = $q->user_email;
			$name = $q->user_name;
			$address = $q->user_address;
			$amount = $q->request_amount;
			$amount_process = $q->request_process;
			if($amount_process == 0){
				$accept = "<button id='accept_text$w_id' class='withdrawal_accept btn_accept_withdrawal' value=".$w_id.">Accept</button>";
				$reject = "<button id='reject_text$w_id' class='withdrawal_reject btn_reject_withdrawal' value=".$w_id.">Reject</button>";
			}elseif($amount_process == 1){
				$accept = "<button class='btn_accept_withdrawal'>Accepted</button>";
				$reject  = "";
			}else{
				$accept = "";
				$reject = "<button class='btn_reject_withdrawal'>Rejected</button>";
			}
			$htmlrejected .= '<tr>
				<td>'.$id.'</td>
				<td>'.$name.'</td>
				<td>'.$email.'</td>
				<td>'.$address.'</td>
				<td>'.$amount.'</td>
				<td>
					<span>'.$accept.'</span>
					<span>'.$reject.'</span>
				</td>
			</tr>';
		}
		if((empty($withdrwal_status_query)) && $withdrwal_offset == ''){
			$htmlrejected .= '<h1>Record not found</h1>';
		}
		if($withdrwal_offset == '' && (!empty($withdrwal_status_query))){
			$htmlrejected .= '</table>';
			$htmlrejected .= '<button id="withdrawal_more" value="Rejected">Load More</button>';
			$htmlrejected .= '<input type="hidden" value="1" id="page_value">';
			$htmlrejected .= '<input type="hidden" value="reject_tab" id="active_tab">';
		}
		//$pagenav = '3';
	}
	$array = array('pending_data'=>$htmlpending, 'accepted_data'=>$htmlaccepted, 'rejected_data'=>$htmlrejected, 'pagenav'=>$pagenav);
	echo json_encode($array);
	exit;
}
function funnel_review_modal(){
	global $wpdb;
	$id = $_POST['post_id'];
	//echo "SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".$id."' AND user_id='".get_current_user_id()."'   AND date < now() - interval 1 DAY";
	$check_last_update = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".$id."' AND user_id='".get_current_user_id()."'   AND date < now() - interval 1 DAY");  // this query get last date of review update

	// echo "SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".$id."', user_id='".get_current_user_id()."' ";exit;
	$review_exists = $wpdb->get_row("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".$id."' AND user_id='".get_current_user_id()."' ");
	// print_r($review_exists);
	if($review_exists->id != ''){
		$valueformoney = $review_exists->valueformoney;
		$welldesigned = $review_exists->welldesigned;
		$qualityofcontent = $review_exists->qualityofcontent;
		$text = stripslashes($review_exists->text);
		$type = 'Update';
	}else{
		$valueformoney = 0;
		$welldesigned = 0;
		$qualityofcontent = 0;
		$text = '';
		$type = 'Submit';
	}
	
	if($check_last_update !='' || $review_exists->id == ''){
		$disabled = '';
		$msg = '';
	}else{
		$disabled = 'disabled';
		$msg = '<p>You can update review after 24 hours of last updated time</p>';
	}
	if($id){
		$html .= '<input type="hidden" name="funnel_id_review" value="'.$id.'" />';
		$html .= '<table class="table">';
			$html .= '<tr>';
				$html .= '<th>Value for Money</th>';
				$html .= '<td>';
					$html .= '<div id="rating_div">';
						$html .= '<div class="star-rating">';
							for($vfm=1;$vfm<=5;$vfm++){
								if($vfm <= $valueformoney){
									$classvfm = 'fas selected';
								}else{
									$classvfm = '';
								}
								$html .= '<span class="far fa-star valueformoney '.$classvfm.'" data-rating="'.$vfm.'"></span>';
							}
							$html .= '<input type="hidden" name="valueformoney_val" class="rating-value" value="'.$valueformoney.'">';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<th>Well designed</th>';
				$html .= '<td>';
					$html .= '<div id="rating_div">';
						$html .= '<div class="star-rating">';
							/*$html .= '<span class="far fa-star welldesigned" data-rating="1"></span>';
							$html .= '<span class="far fa-star welldesigned" data-rating="2"></span>';
							$html .= '<span class="far fa-star welldesigned" data-rating="3"></span>';
							$html .= '<span class="far fa-star welldesigned" data-rating="4"></span>';
							$html .= '<span class="far fa-star welldesigned" data-rating="5"></span>';*/
							for($wd=1;$wd<=5;$wd++){
								if($wd <= $welldesigned){
									$classwd = 'fas selected';
								}else{
									$classwd = '';
								}
								$html .= '<span class="far fa-star welldesigned '.$classwd.'" data-rating="'.$wd.'"></span>';
							}
							$html .= '<input type="hidden" name="welldesigned_val" class="rating-value" value="'.$welldesigned.'">';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<th>Quality of content</th>';
				$html .= '<td>';
					$html .= '<div id="rating_div">';
						$html .= '<div class="star-rating">';
							/*$html .= '<span class="far fa-star qualityofcontent" data-rating="1"></span>';
							$html .= '<span class="far fa-star qualityofcontent" data-rating="2"></span>';
							$html .= '<span class="far fa-star qualityofcontent" data-rating="3"></span>';
							$html .= '<span class="far fa-star qualityofcontent" data-rating="4"></span>';
							$html .= '<span class="far fa-star qualityofcontent" data-rating="5"></span>';*/
							for($qoc=1;$qoc<=5;$qoc++){
								if($qoc <= $qualityofcontent){
									$classqoc = 'fas selected';
								}else{
									$classqoc = '';
								}
								$html .= '<span class="far fa-star qualityofcontent '.$classqoc.'" data-rating="'.$qoc.'"></span>';
							}
							$html .= '<input type="hidden" name="qualityofcontent_val" class="rating-value" value="'.$qualityofcontent.'">';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td colspan="2">';
					$html .= '<textarea name="funnelreview_text" placeholder="Add Review..!">'.$text.'</textarea>';
				$html .= '</td>';
			$html .= '</tr>';
			$html .= '<tr>';
				$html .= '<td colspan="2" align="center">';
					$html .= '<p class="review_error"></p>';
					$html .= '<button class="btn btn-info submit_review" '.$disabled.'>'.$type.' Review</button>';
					$html .= '<div>'.$msg.'</div>';
				$html .= '</td>';
			$html .= '</tr>';
		$html .= '</table>';
	}
	echo $html;
	exit;
}

function active_funnel_status(){
	global $wpdb;
	$f_status = $_POST['funnel_status'];
	$f_id = $_POST['funnel_id'];
	$c_user_id = $_POST['c_user_id'];
	if($f_status == 'active'){
		$u_status = 0; 
		$active_status = $wpdb->query("UPDATE `wp_setc_activated_funnels` SET `active_status`= $u_status WHERE user_id = $c_user_id and funnel_id = $f_id");
		echo "1";
	}else{
		$u_status = 1;
		$active_status = $wpdb->query("UPDATE `wp_setc_activated_funnels` SET `active_status`= $u_status WHERE user_id = $c_user_id and funnel_id = $f_id");
		echo "2";
	}
	//echo "UPDATE `wp_setc_activated_funnels` SET `active_status`= $u_status WHERE user_id = $c_user_id and funnel_id = $f_id";
	exit;
}

function delete_inactive_funnel(){
	global $wpdb;
	$f_id = $_POST['funnel_id'];
	$c_user_id = $_POST['c_user_id'];
	//echo "DELETE FROM `wp_setc_activated_funnels` WHERE user_id = $c_user_id and funnel_id = $f_id and active_status = 1";
	$delete_inactive_funnel = $wpdb->query("DELETE FROM `wp_setc_activated_funnels` WHERE user_id = $c_user_id and funnel_id = $f_id and active_status = 1");
	if($delete_inactive_funnel){
		echo "1";
	}
	exit;
}
function save_followup_email(){
	global $wpdb;
	$content = $_POST['content'];
	$subject = $_POST['subject'];
	$post_id = $_POST['post_id'];
	$type = $_POST['type'];
	$isthere = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='$post_id' AND type='$type'");
	if($isthere->id){
		$query = $wpdb->query("UPDATE `wp_setc_followup_emails` SET subject='$subject', content='$content' WHERE funnel_id='".$isthere->funnel_id."' AND type='".$isthere->type."' ");
		record_notice('followup_email',$post_id,'update',$isthere->id);
	}else{
		$query = $wpdb->query("INSERT INTO `wp_setc_followup_emails` (subject,content,funnel_id,type) VALUES ('$subject','$content','$post_id','$type')");
		record_notice('followup_email',$post_id,'insert',$wpdb->insert_id);
	}
	echo '1';
	exit;
}
function record_notice($type,$funnel_id,$update,$insert_id){
	global $wpdb;
	$isthere = $wpdb->get_row("SELECT * FROM `wp_fm_funneledit_data` WHERE funnel_id='$funnel_id' AND type='$type' AND relation_id='$insert_id'");
	$date = date('Y-m-d H:i:s');
	if($isthere->id){
		$query = $wpdb->query("UPDATE `wp_fm_funneledit_data` SET user_id='".get_current_user_id()."', process='$update', date='$date' WHERE funnel_id='".$isthere->funnel_id."' AND type='".$isthere->type."' ");
	}else{
		$query = $wpdb->query("INSERT INTO `wp_fm_funneledit_data` (user_id,relation_id,funnel_id,type,date,process) VALUES ('".get_current_user_id()."','$insert_id','$funnel_id','$type','$date','$update')");
	}
	return '1';
}
function get_followup_email(){
	global $wpdb;
	$id = $_POST['id'];
	$isthere = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE id='$id'");
	if($isthere->id){
		if($isthere->type == '0'){
			$html .= '<h4 style="text-align:center;">Immideate</h4>';
		}else{
			$html .= '<h4 style="text-align:center;">After '.$isthere->type.' Days</h4>';
		}
		$html .= '<h5>Subject:</h5>';
		$html .= '<div class="subject" style="background: white;padding: 5px;border: 1px solid #DDD;border-radius: 5px;margin-bottom: 10px;">';
			$html .= $isthere->subject;
		$html .= '</div>';
		$html .= '<h5>Email Body:</h5>';
		$html .= '<div class="data" style="background: white;padding: 5px;border: 1px solid #DDD;border-radius: 5px;min-height: 200px;">';
			$html .= $isthere->content;
		$html .= '</div>';
	}
	echo $html;
	exit;
}
function validate_link_identifier($linkid = ''){
	global $wpdb;
	if(empty($linkid)){
		$link_identifier = $_POST['link_identifier'];
	}else{
		$link_identifier = $linkid;
	}
	$random_number = rand(0,999);
	$final_identifier = $link_identifier.$random_number;
	$identifier_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_website_links` where link_identifier = '".$final_identifier."' ");
	if($identifier_count > 0){
		validate_link_identifier($link_identifier);
	}else{
		if(empty($linkid)){
			echo $final_identifier;
		}else{
			return $final_identifier;
		}
	}
	if(empty($linkid)){
		exit;
	}
}
function submit_website_link(){
	global $wpdb;
	$funnel_id = $_POST['funnel_id'];
	$link_type = $_POST['link_type'];
	if($link_type == 'download_link'){
		$download_link = $_POST['download_link'];
		$download_name = $_POST['download_name'];
		$query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link) VALUES ('".$funnel_id."','".$download_name."','creator','".$download_link."')");
		/*$sql_query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$funnel_id." and aff_network = 'creator'");
		$id = $sql_query->id;
		if($id){
			$query = $wpdb->query("UPDATE `".$wpdb->prefix."setc_website_links` SET name='".$download_name."', aff_link='".$download_link."' WHERE id=".$id);
		}else{
			$query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link) VALUES ('".$funnel_id."','".$download_name."','creator','".$download_link."')");
		} Date 22/1/2021*/
	}else if($link_type == 'affiliate_link'){
		$internal_name = $_POST['internal_name'];
		$affiliate_network = $_POST['affiliate_network'];
		$affiliate_link = $_POST['affiliate_link'];
		$link_identifier = $_POST['link_identifier'];
		$notes = $_POST['notes'];
		$query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link, link_identifier, notes) VALUES ('".$funnel_id."', '".$internal_name."', '".$affiliate_network."', '".$affiliate_link."', '".$link_identifier."', '".$notes."')");
	}
	if($query){
		echo '1';
	}
	exit;
}
function email_link_modal(){
	global $wpdb;
	$funnel_id = $_POST['id'];
	$email_rs = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_website_links` WHERE funnel_id='$funnel_id' AND link_place='email' ");
	$html .= '<table class="table">';
		$html .= '<tr>';
			$html .= '<th>Internal Name</th>';
			$html .= '<th>Affiliate Network</th>';
			$html .= '<th>Affiliate Link</th>';
			$html .= '<th>Link Identifier</th>';
			$html .= '<th>Link Place</th>';
			$html .= '<th>Insert</th>';
		$html .= '</tr>';
	foreach($email_rs as $rs){
		$html .= '<tr>';
			$html .= '<td>'.$rs->name.'</td>';
			$html .= '<td>'.$rs->aff_network.'</td>';
			$html .= '<td>'.$rs->aff_link.'</td>';
			$html .= '<td>'.$rs->link_identifier.'</td>';
			$html .= '<td>'.$rs->link_place.'</td>';
			$html .= '<td><a href="javascript:void(0);" onclick="add_link_to_followup_email('.$rs->id.','.$funnel_id.');">Insert</td>';
		$html .= '</tr>';
	}
	$html .= '</table>';
	echo $html;
	exit;
}

function upload_screenshot(){
	$img = $_POST['dataURL'];
	$s_id = $_POST['id'];
	$poster_img = upload_image_url_to_wp($img,$s_id);
	exit;
} 
function funnel_viewascustomer(){
	$funnel_id = $_POST['funnel_id'];
	$checked = $_POST['checked'];
	if($checked == '1'){
		$html .= '<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">';
			/*$html .= '<a href="javascript:void(0);" class="funnel_links" data="'.$funnel_id.'">My Funnel Link</a>';
			$html .= '<a href="javascript:void(0);" class="download_resource" data="'.$funnel_id.'">Download Resource</a>';
			$html .= '<a href="javascript:void(0);" class="affiliate_requests" data="'.$funnel_id.'">Affiliate Requests</a>';*/
			$html .= '<a href="javascript:void(0);" class="affiliate_requests" data="'.$funnel_id.'">Promotional Links</a>';
			$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="add_promotional_tools">Funnel Link & Traffic Kit</a>';
			$html .= '<a href="javascript:void(0);" class="download_resource" data="'.$funnel_id.'">Download Resources</a>';
		$html .= '</div>';
		$html .= '<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">';
			/*$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="add_promotional_tools_creator">Promotional Tools</a>';
			$html .= '<a href="#">Statistics</a>';
			$html .= '<a href="javascript:void();" class="funnel_review" data="'.$funnel_id.'">Review</a>';*/
			$html .= '<a href="javascript:void(0);" class="f_integrations_activator" data="'.$funnel_id.'">Integrations</a>';
			$html .= '<a href="javascript:void(0);" class="clicks_and_coversions" data="'.$funnel_id.'">Clicks & Conversions</a>';
			$html .= '<a href="javascript:void();" class="funnel_review" data="'.$funnel_id.'">Rate & Review</a>';
		$html .= '</div>';
	}else if($checked == '0'){
		$html .= '<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">';
			/*$html .= '<a href="javascript:void(0);" class="edit_funnel_pages" data="'.$funnel_id.'">Edit Pages</a>';
			$html .= '<a href="javascript:void(0);" class="funnel_website_links" data="'.$funnel_id.'">Website Links</a>';
			$html .= '<a href="javascript:void(0);" class="followup_emails" data="'.$funnel_id.'">Followup Emails</a>'; */
			
			$html .= '<a href="javascript:void(0);" class="edit_funnel_pages" data="'.$funnel_id.'">Edit Pages</a>';
			$html .= '<a href="javascript:void(0);" class="funnel_website_links" data="'.$funnel_id.'">Website Links</a>';
			$html .= '<a href="javascript:void(0);" class="followup_emails" data="'.$funnel_id.'">Followup Emails</a>';
			$html .= '<a href="javascript:void(0);" class="f_integrations_activator" data="'.$funnel_id.'">Integrations</a>';
		$html .= '</div>';
		$html .= '<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">';
			/*$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="add_promotional_tools">Promotional Tools</a>';
			$html .= '<a href="#">Statistics</a>';
			$html .= '<a href="#">Marketplace</a>'; */
			
			$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="add_promotional_tools">Promotional Tools</a>';
			$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="btn_marketplace">Marketplace</a>';
			$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="funnel_statistics">Statistics</a>';
			if(current_user_can('funnelmates_pro')){ 
				$html .= '<a href="javascript:void(0);" data="'.$funnel_id.'" class="clone_funnel">Clone Funnel</a>';
			} 
		$html .= '</div>';
	}	
	echo $html;
	exit;
}

function search_funnel(){
	global $wpdb;
	$html = array();
	$funnel_name = $_POST['funnel_q'];
	$c_id = get_current_user_id();
	
	$items = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id where post_author not in($c_id) and wp_posts.post_title LIKE '%$funnel_name%'");
	
	/*if($items){
		$items .= "and wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium'";
	} */
	
	$created_funnel = $wpdb->get_results("SELECT ID FROM `wp_posts` WHERE post_author = $c_id AND post_type = 'landing' AND post_parent = 0 AND post_status = 'publish' AND post_title LIKE '%$funnel_name%'");
		
	$mearge_id = array_merge($created_funnel,$items);
	foreach($mearge_id as $i){
		$html[] = array('id'=>$i->ID); 
	}
	/*
	$items = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id where post_author not in($c_id) and wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' and wp_posts.post_title LIKE '%$funnel_name%'");
	foreach($items as $i){
		$funnel_id = $i->ID; 
	}
	
	$wpb_all_query1 = new WP_Query(array('author'=>get_current_user_id(),'s'=> $funnel_name,'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'fields' => 'ids'));
	$ids = $wpb_all_query1->posts;
	array_push($ids,$funnel_id);
	$myarray = $ids;
	$wpb_all_query = new WP_Query(array(
		'post__in'=> $myarray,'post_type'=>'landing','post_status'=>'publish','post_parent' => 0)
	);
	
	if ( $wpb_all_query->have_posts() ) :
	while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); 
		$html[] = array('id'=>get_the_ID());
	endwhile;
	endif; 
	*/
	echo json_encode($html);
	exit;
}

function get_category_post(){
	global $wpdb;
	$limit = $_POST['limit'];
	$cur_cat = $_POST['cur_cat']; 
	$page = $_POST['page'];
	if($page == ''){
		$page = 1;
	}else if($page == 'lastpage'){
		$page = 'lastpage';
	}else{
		$page = $page + 1;
	}
	$newpage = (($page - 1)*($limit));
	$html = '';
	//$items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items ORDER BY id DESC LIMIT  $limit");
	$items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items INNER JOIN wp_term_relationships ON wp_fm_marketplace_items.post_id = wp_term_relationships.object_id and term_taxonomy_id = $cur_cat ORDER BY wp_fm_marketplace_items.id DESC LIMIT $newpage, $limit");
	if(count($items) > 0){
		foreach($items as $i){
			$post_id = $i->post_id;
			$pricing = maybe_unserialize($i->pricing);
			$funnel = get_post($post_id);
			$count_price = count($pricing);
			$html .= '<div class="marketplace_item '.$post_id.'" >
				<div class="category_item_inner">
					<div class="intern-thumbnail" style="">
						<a href="">
							<picture>
								<source srcset="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m.jpg" media="(min-width: 1920px)"><img src="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m-800x600.jpg">
							</picture>
						</a>
					</div>';
					$freecolor = "background-color:#f37021;color:#fff;border-radius: 4px 0 0 0;";
					$premiumcolor = "background-color:#446095;color:#fff;";
					$whitelabelcolor = "background-color:#a8519f;color:#fff;";
					$exclusivecolor = "background-color:#f4ea00;color:#000;border-radius: 0 4px 0 0;";
					foreach($pricing as $k => $p){
						if($count_price == 4){
							$width = "25%";
						}else if($count_price == 3){
							$width = "33.33%";
						}else if($count_price == 2){
							$width = "50%";
						}else{
							$width = "100%";
						}
						if($k == 'free'){
							$html.=  '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'premium'){
							$html.= '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'whitelabel'){
							$html.= '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'exclusive'){
							$html.= '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
						}
					}
					$html .= '<div class="funnel_details">
						<h5 style="text-align: left;">'.$funnel->post_title.'</h5>';		
						$html .= '<div class="funnel_details_inner">';
						$html .= '<div style="width:100%;float:left;font-size: 12px;">';
						$html .= '<table>';
							$html .= '<tr>';
								$html .= '<td>Created By: <a href="'.get_author_posts_url($funnel->post_author).'">'.get_the_author_meta('first_name',$funnel->post_author).' '.get_the_author_meta('last_name',$funnel->post_author).'</a></td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$cat = get_the_category($post_id);
								$html .= '<td>Category: <a href="'.get_category_link($cat[0]).'">'.$cat[0]->name.'</a></td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Funnel Rating:</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Activate Users:0</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$html .= '<td>Affiliate Products:0</td>';
							$html .= '</tr>';
							$html .= '<tr>';
								$pm_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$post_id);
								if($pm_count == 0){
									$pt = 'No';
								}else{
									$pt = 'Yes';
								}
								$html .= '<td>Promo Tools: '.$pt.'</td>';
							$html .= '</tr>';
						$html .= '</table>';
						$html .='</div>';
						$html .= '</div>';
						$html .= '<div class="marketplace_item_desc">';
								$html .= '<p style="text-align: justify;margin: 10px 0;font-size: 15px;">'.$i->description.'</p>';
								$html .= '<button onclick="unlock_funnel('.$i->post_id.');" id="unlock_funnel_'.$i->post_id.'" type="button" style="/*background-color:#82b440;padding: 5px 30px;border-radius: 20px;*/" class="btn btn-default"><i class="fa fa-arrow-right"></i>Unlock this Funnel</button>';
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
			$html .= '</div>';	
			$html .= '</div>';
			if($i%3==0){
				$html .=  '<div style="clear:both;"></div>';
			}$i++;
			$pagenav = '<button id="page" value="'.$page.'" style="display:none;"></button>';
		}
	}else if($page != 'lastpage'){
		$html .= "";
		$pagenav = '<button id="page" value="lastpage" style="display:none;"></button>';
	}else{
		$html .= '<article id="post-" class="box content">
						<div class="intern-padding">
							<div class="special-404 text-center">
								<i class="fa fa-meh text-light"></i>
							</div>
							<div class="heading text-center">
								<h2>
									Nothing Found				</h2>
							</div>
						</div>
						<div class="intern-padding">
							<p class="blog-sum-up text-center">
								Apologies, but no results were found. Perhaps searching will help find a related post.			</p>

							<div class="blog-button text-center">
								<a href="https://funnelmates.com" class="btn btn-default"><i class="fa fa-arrow-left"></i> Back on the home page</a>
							</div>
						</div>
					</article>';
	}
	$array = array('html'=>$html, 'pagenav'=>$pagenav);
	$json = json_encode($array);
	echo $json;
	exit;
}

function get_author_post(){
	global $wpdb;
	$html = '';
	$limit = $_POST['limit'];
	$author = $_POST['author']; 
	$page = $_POST['page'];
	if($page == ''){
		$page = 1;
	}else if($page == 'lastpage'){
		$page = 'lastpage';
	}else{
		$page = $page + 1;
	}
	$newpage = (($page - 1)*($limit));
	$author_post = $wpdb->get_results("SELECT * FROM `wp_posts` WHERE `post_author` = $author AND `post_status` LIKE 'publish' AND `post_parent` = 0 AND `post_type` LIKE 'landing' ORDER BY ID DESC LIMIT $newpage, $limit" );
	$i_c = 0;
	if(count($author_post) > 0){
		foreach($author_post as $auth){
			$id = $auth->ID;
			$title = $auth->post_title;
			$description = $auth->post_content;
			$html .= '<div class="author_item">';
			$html .= '<div class="intern-thumbnail " style="">';		
													
							$funnel_img = $wpdb->get_row("SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".$id."'");
							$path = wp_get_upload_dir();
							$img_path = $path['baseurl'].'/screenshot';
							$f_img = $img_path .'/'. $funnel_img->image;
							if($funnel_img){
							
							$html .=' <a href="'.get_permalink($id).'">
								<picture>
									<source srcset="'.$f_img.'" media="(min-width: 1920px)"><img src="'.$f_img.'">
								</picture>
							</a>';
							}else{								
							$html .= '<a href="'.get_permalink($id).'">
									<picture>
										<source srcset="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m.jpg" media="(min-width: 1920px)"><img src="https://funnelmates.com/wp-content/uploads/2015/06/photodune-2191223-university-of-tennessee-hill-m-800x600.jpg">
									</picture>
								</a>';
							}
				$html .= '</div>';
				$html .= '<div style="display:inline-block;width: 100%;">';
								$pricing = maybe_unserialize($get_funnel_details->pricing);
								$count_price = count($pricing);
								$freecolor = "background-color:#f37021;color:#fff;";
								$premiumcolor = "background-color:#446095;color:#fff;";
								$whitelabelcolor = "background-color:#a8519f;color:#fff;";
								$exclusivecolor = "background-color:#f4ea00;color:#000;";
								foreach($pricing as $k => $p){
									if($count_price == 4){
										$width = "25%";
									}else if($count_price == 3){
										$width = "33.33%";
									}else if($count_price == 2){
										$width = "50%";
									}else{
										$width = "100%";
									}
									if($k == 'free'){
										$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
									}else if($k == 'premium'){
										$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
									}else if($k == 'whitelabel'){
										$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
									}else if($k == 'exclusive'){
										$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
									}
								}
					$html .= '</div>';
					$html .= '<div class="intern-padding">
									<div class="intern-box box-title">
										<h3>
											<a href="'.get_permalink($id).'">'.$title.'</a>
										</h3>
									</div>
									<div>'.$description.'<br/>';
										
										$html .='Funnel Rating:<br/>';
										$html .= 'Activate Users:0<br/>';
										$html .= 'Affiliate Products:0<br/>';
										if(count($promotionaltools) > 0){
											$html .= 'Promo Tools: Yes';
										}else{
											$html .= 'Promo Tools: No';
										}
									$html .= '</div>
									<span class="directory-category">
										<i class="fa fa-tag"></i>';
										$category = get_the_category($id);
										$html .= '<a href="'.get_category_link($category[0]->term_id).'" rel="category tag">'.$category[0]->name.'</a>
									</span>
									<span class="directory-comments">
										<i class="fa fa-comments"></i>
										<a href="https://funnelmates.com/demo-post-5/#respond">0</a>
									</span>
									<span class="post-date directory-comments">
										<i class="fa fa-clock"></i>
										June 3, 2015
									</span>
								</div>';
								$html .='<div class="intern-box text-center">
									<a href="javascript:void(0);" onclick="unlock_funnel('.$id.');" id="unlock_funnel_'.$id.'" class="btn btn-default mb-0">
										<i class="fa fa-arrow-right"></i> Unlock This Funnel
									</a>
								</div>';
			$html .= '</div>';
			$i_c++;
			if($i_c%3==0){$html.= '<div style="clear:both;"></div>';}
		}
		$pagenav = '<button id="page" value="'.$page.'" style="display:none;"></button>';
	}else if($page != 'lastpage'){
		$html .= "";
		$pagenav = '<button id="page" value="lastpage" style="display:none;"></button>';
	}else{
		
	}
	$array = array('html'=>$html, 'pagenav'=>$pagenav);
	$json = json_encode($array);
	echo $json;
	exit;
}

function get_store_items(){
	global $wpdb;
	$html = '';
	$limit = $_POST['limit'];
	$page = $_POST['page'];
	if($page == ''){
		$page = 1;
	}else if($page == 'lastpage'){
		$page = 'lastpage';
	}else{
		$page = $page + 1;
	}
	$newpage = (($page - 1)*($limit));
	//$store_items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items ORDER BY id DESC LIMIT $newpage, $limit");
	if(get_current_user_id() == '7'){
		$store_items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items INNER JOIN wp_posts ON wp_fm_marketplace_items.post_id = wp_posts.ID AND wp_posts.post_status = 'publish' ORDER BY wp_fm_marketplace_items.id DESC LIMIT $newpage, $limit");
	}else{
		$store_items = $wpdb->get_results("SELECT * FROM wp_fm_marketplace_items INNER JOIN wp_posts ON wp_fm_marketplace_items.post_id = wp_posts.ID AND wp_posts.post_status = 'publish' AND wp_posts.post_author != '1' ORDER BY wp_fm_marketplace_items.id DESC LIMIT $newpage, $limit");
	}
	//echo count($items);
	//$i_c = 0;
	if($store_items){
			foreach($store_items as $i){
			$pricing = maybe_unserialize($i->pricing);
			$post_id = $i->post_id;
			$funnel = get_post($post_id);
			$count_price = count($pricing);
			$funnel_image_id = $i->image_id;
			$funnel_image = wp_get_attachment_url( $funnel_image_id );
			if(get_post_meta($post_id,'is_funnel_published',true) == 1){ 
			
			$html .= '<div class="marketplace_item '.$post_id.'" >';
				$html .= '<div class="marketplace_item_inner">';
					$freecolor = "background-color:#f37021;color:#fff;border-radius: 4px 0 0 0;";
					$premiumcolor = "background-color:#446095;color:#fff;";
					$whitelabelcolor = "background-color:#a8519f;color:#fff;";
					$exclusivecolor = "background-color:#f4ea00;color:#000;border-radius: 0 4px 0 0;";
					foreach($pricing as $k => $p){
						if($count_price == 4){
							$width = "25%";
						}else if($count_price == 3){
							$width = "33.33%";
						}else if($count_price == 2){
							$width = "50%";
						}else{
							$width = "100%";
						}
						if($k == 'free'){
							$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$freecolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'premium'){
							$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$premiumcolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'whitelabel'){
							$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$whitelabelcolor.'">'.ucfirst($k).'</div>';
						}else if($k == 'exclusive'){
							$html .= '<div style="width:'.$width.';float:left;text-align:center;'.$exclusivecolor.'">'.ucfirst($k).'</div>';
						}
					}
					$html .= '<div class="funnel_details">';
						$html .= '<h5 style="text-align: center; color:#9EC73A;">'.$funnel->post_title.'</h5>';
						$html .= '<div class="funnel_details_inner">';
							if($funnel_image){
								$html .= '<div style="width:30%;float:left;"><img src="'.$funnel_image.'" data="'.$funnel->ID.'"/></div>';						
							}else{
								$html .= '<div style="width:30%;float:left;"><img src="http://placehold.it/100x100" /></div>';
							}
							$html .= '<div style="width:70%;float:left;padding: 0 10px;font-size: 12px;">';
								$html .= '<table>';
									$html .= '<tr>';
										$html .= '<td>Created By: <a href="'.get_author_posts_url($funnel->post_author).'">'.get_the_author_meta('first_name',$funnel->post_author).' '.get_the_author_meta('last_name',$funnel->post_author).'</a></td>';
									$html .= '</tr>';
									$html .= '<tr>';
										$cat = get_the_category($post_id);
										$html .= '<td>Category: <a href="'.get_category_link($cat[0]).'" style="color:#9EC73A;">'.$cat[0]->name.'</a></td>';
									$html .= '</tr>';
									$html .= '<tr>';
										$rating = $wpdb->get_row("SELECT COUNT(*) as COUNTER, AVG(valueformoney) as valueformoney, AVG(welldesigned) as welldesigned, AVG(qualityofcontent) as qualityofcontent FROM `wp_setc_funnel_review` WHERE funnel_id='".$funnel->ID."' ");
											$valueformoney_average = $rating->valueformoney;
											$welldesigned_average = $rating->welldesigned;
											$qualityofcontent_average = $rating->qualityofcontent;
											$final_average = ($valueformoney_average + $welldesigned_average + $qualityofcontent_average) / 3;
											$html .= '<td>Funnel Rating: '.number_format($final_average,1).' Based on '.$rating->COUNTER.' Reviews</td>';
										// $rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
										// $review_count = count($rating);
										// $totalreview = 0;
										// foreach($rating as $r){
											// $totalreview = $totalreview + $r->valueformoney;
										// }
										// if($review_count == 0){
											// $html .= '<td>Funnel Rating: 0 Based on '.$review_count.' Reviews</td>';
										// }else{
											// $html .= '<td>Funnel Rating: '.$totalreview/$review_count.' Based on '.$review_count.' Reviews</td>';
										// }
									$html .= '</tr>';
									$html .= '<tr>';
										$f_activations = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_activated_funnels` WHERE funnel_id='$post_id'");
										$html .= '<td>Funnel Activations: '.$f_activations.'</td>';
									$html .= '</tr>';
									$html .= '<tr>';
										$aff_pro = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_website_links` WHERE (aff_network='external' OR aff_network='jvzoo' OR aff_network='clickbank' OR aff_network='warriorplus') AND funnel_id='$post_id'");
										//$html .= '<td>Affiliate Products: '.$aff_pro.'</td>';
										$html .= '<td>Affiliate Products: <span onclick="affiliate_products('.$post_id.');" style="cursor:pointer; color:#9EC73A;">'.$aff_pro.'</span></td>';
									$html .= '</tr>';
									$html .= '<tr>';
										$pm_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_fm_promotional_tools` WHERE post_id=".$post_id);
										if($pm_count == 0){
											$pt = 'No';
										}else{
											$pt = 'Yes';
										}
										$html .= '<td>Promo Tools: '.$pt.'</td>';
									$html .= '</tr>';
								$html .= '</table>';
							$html .= '</div>';
						$html .= '</div>';
						$html .= '<div class="ratings_div" style="display: inline-block;width: 100%;">';
							$rating = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id=".$funnel->ID);
							$review_count = count($rating);
							if($review_count > 0){
								$html .= '<p style="margin: 0;font-size: 13px;font-weight: 600;">Based on <span style="font-size: 20px;">'.$review_count.'</span> Reviews</p>';
							$valueformoney = 0;
							$welldesigned = 0;
							$qualityofcontent = 0;
							foreach($rating as $r){
								$valueformoney = $valueformoney + $r->valueformoney;
								$welldesigned = $welldesigned + $r->welldesigned;
								$qualityofcontent = $qualityofcontent + $r->qualityofcontent;
							}
							$average_valueformoney = $valueformoney / $review_count;
							$average_welldesigned = $welldesigned / $review_count;
							$average_qualityofcontent = $qualityofcontent / $review_count;
							
							if($average_valueformoney > 0 && $average_valueformoney <= 2){
								//red
								$background_valueformoney = 'ED1C24';
							}else if($average_valueformoney > 2 && $average_valueformoney <= 3.5 ){
								//yellow
								$background_valueformoney = 'ff9e28';
							}else if($average_valueformoney > 3.5 ){
								//green
								$background_valueformoney = '9ec73b';
							}
							
							if($average_welldesigned > 0 && $average_welldesigned <= 2){
								//red
								$background_welldesigned = 'ED1C24';
							}else if($average_welldesigned > 2 && $average_welldesigned <= 3.5 ){
								//yellow
								$background_welldesigned = 'ff9e28';
							}else if($average_welldesigned > 3.5 ){
								//green
								$background_welldesigned = '9ec73b';
							}
							
							if($average_qualityofcontent > 0 && $average_qualityofcontent <= 2){
								//red
								$background_qualityofcontent = 'ED1C24';
							}else if($average_qualityofcontent > 2 && $average_qualityofcontent <= 3.5 ){
								//yellow
								$background_qualityofcontent = 'ff9e28';
							}else if($average_qualityofcontent > 3.5 ){
								//green
								$background_qualityofcontent = '9ec73b';
							}
							
							$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_valueformoney.';color: white;">'.number_format($average_valueformoney,1).'</span><p style="font-size: 12px;">Value For Money</p></div>';
							$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_welldesigned.';color: white;">'.number_format($average_welldesigned,1).'</span><p style="font-size: 12px;">Well Designed</p></div>';
							$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 14px;display: inline-block;border-radius:70px;width: 67px;text-align:center;font-size: 26px;background: #'.$background_qualityofcontent.';color: white;">'.number_format($average_qualityofcontent,1).'</span><p style="font-size: 12px;">Quality of Content</p></div>';
							}else{
								$html .= 'No Review Found..';
							}
						$html .= '</div>';
						$html .= '<div class="marketplace_item_desc">';
							$html .= '<p style="text-align: justify;margin: 10px 0;font-size: 15px; word-break: break-all;">'.stripcslashes(stripcslashes(stripcslashes($i->description))).'</p>';
							$html .= '<button onclick="unlock_funnel('.$i->post_id.');" id="unlock_funnel_'.$i->post_id.'" type="button" style="background-color:#82b440;padding: 5px 30px;border-radius: 20px;">Unlock this Funnel</button>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
			}
			/*$i_c++;
			if($i_c%3==0){$html.= '<div style="clear:both;"></div>';}*/
		}
		$pagenav = '<button id="page" value="'.$page.'" style="display:none;"></button>';
	}else if($page != 'lastpage'){
		$html .= '';
		$pagenav = '<button id="page" value="lastpage" style="display:none;"></button>';		
	}else{
		
	}
	$array = array('html'=>$html, 'pagenav'=>$pagenav);
	$json = json_encode($array);
	echo $json;
	exit;
}

function get_funnel_items(){
	global $wpdb;
	//$html = '';
	$html = array();
	$limit = $_POST['limit'];
	$c_id = $_POST['current_id'];
	$page = $_POST['page'];
	if($page == ''){
		$page = 1;
	}else if($page == 'lastpage'){
		$page = 'lastpage';
	}else{
		$page = $page + 1;
	}
	$newpage = (($page - 1)*($limit));
	/*$purchase_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_postmeta.meta_key = '_purchase_info_$c_id' AND wp_posts.post_author not in($c_id) and (wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' OR wp_setc_activated_funnels.type = 'whitelabel') GROUP BY(wp_posts.ID) ORDER by wp_postmeta.meta_value ASC");*/
	$purchase_funnels = $wpdb->get_results("SELECT * FROM `wp_posts` INNER JOIN wp_setc_activated_funnels on wp_posts.ID = wp_setc_activated_funnels.funnel_id INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_postmeta.meta_key = '_purchase_info_$c_id' AND wp_posts.post_author not in($c_id) and (wp_setc_activated_funnels.type = 'free' OR wp_setc_activated_funnels.type = 'premium' OR wp_setc_activated_funnels.type = 'whitelabel') GROUP BY(wp_posts.ID) ORDER by wp_postmeta.meta_value ASC");
	
	$funnel_id = array();
	foreach($purchase_funnels as $i){
		$funnel_id[] = $i->ID; 
		$funnel_statue = $i->active_status; 
	}
	
	$created_funnels = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page'=>-1, 'fields' => 'ids'));
	$ids = $created_funnels->posts;
	$myarray = array_merge($funnel_id,$ids);
	$all_funnel_id = implode(', ', $myarray); 
	$sort_funnel_id = $wpdb->get_results("SELECT * FROM `wp_posts`INNER JOIN wp_postmeta on wp_posts.ID = wp_postmeta.post_id where wp_posts.ID in($all_funnel_id) AND (wp_postmeta.meta_key = '_purchase_info_$c_id' or wp_postmeta.meta_key = '_created_info_$c_id') ORDER by wp_postmeta.meta_value DESC");
	$new_funnel_id = array();
	foreach($sort_funnel_id as $it){
		$new_funnel_id[] = $it->ID; 
	}
	
	if($new_funnel_id){
		$wpb_all_query = new WP_Query(array('orderby' => 'post__in', 'post__in'=> $new_funnel_id,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page' => $limit, 'offset' => $newpage, 'paged' => $page));
		//$wpb_all_query = new WP_Query(array('post__in'=> $myarray,'post_type' => 'landing','post_parent' => 0, 'post_status'=>'publish',  'posts_per_page' => $limit, 'offset' => $newpage, 'paged' => $page));
	}else{
		$wpb_all_query = new WP_Query(array('author'=>get_current_user_id(),'post_type'=>'landing','post_parent' => 0, 'post_status'=>'publish', 'posts_per_page' => $limit, 'offset' => $newpage, 'paged' => $page, 'fields' => 'ids'));
	}
	
	if($wpb_all_query->have_posts()){
		while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post();
		$html[] = get_the_ID();
			/*$html .= '<div class="funnel_parent funnel_search_'.get_the_ID().'" style="-webkit-box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);-moz-box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);box-shadow: 0px 0px 10px 1px rgba(153,153,153,1);min-height: 100px;border-radius:5px;padding: 15px;margin:30px 0;">';
			
			//echo "SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id = '".get_the_ID()."' and active_status = 0";
			//$funnel_current_user = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id = '".get_the_ID()."' and active_status = 0");
			$funnel_current_user = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id = '".get_the_ID()."'");
			$funnel_type = $funnel_current_user->type;
			$c_user_id = $funnel_current_user->user_id;
			$funnel_user_info = get_userdata($c_user_id);
			$username = $funnel_user_info->user_login;
			$funnel_active_status = $funnel_current_user->active_status;
			
			if($funnel_type == 'exclusive' || $funnel_active_status == 2){
				$parent_link = get_the_permalink();
				$parent_title = get_the_title();
				global $post;
				
				$html .='<div class="funnel_title">
					<h3 style="float:left;"><a href="'.$parent_link.'">'.$parent_title.'</a></h3>
					<label class="switch unpublish_funnel_switch"></label>
					<h6>';
						$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
						if($funnel_type == 'lead_magnet_funnel'){
							$html .= 'Lead Magnet Funnel';
						}else if($funnel_type == 'video_lead_funnel'){
							$html .= 'Video Lead Funnel';
						}else if($funnel_type == 'sales_funnel'){
							$html .= 'Sales Funnel';
						}
						$category = get_the_category( get_the_ID() ); 
						if($category[0]->cat_name){
							$html .= '&nbsp;-&nbsp;';
							$html .= $category[0]->cat_name;
						}
					$html .= '</h6>
					<hr/>';
					$html .= '<div class="funnel_content" style="width: 100%;display: inline-block;">
						<div class="funnel_img" style="width:20%;float:left;">';
							//echo "SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'";
							$funnel_img = $wpdb->get_row("SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'");
							$path = wp_get_upload_dir();
							$img_path = $path['baseurl'].'/screenshot';
							$f_img = $img_path .'/'. $funnel_img->image;
							if($funnel_img){
							$html .= '<img src="'.$f_img.'" data="'.get_the_ID().'" class="change_funnel_image" style="width:130px;" />';
							}else{
								$html .= '<img src="http://placehold.it/200x200" data="'.get_the_ID().'" class="change_funnel_image" style="width:130px;" />';
							}
						$html .= '</div>';
						$html .= '<div class="funnel_btns" id="funnel_btns_'.get_the_ID().'" style="width:80%;float:left;">
							<h3>This funnel purchased by '.$username.'</h3>
						</div>
					</div>
				</div>';
			}else{
				$parent_link = get_the_permalink();
				$parent_title = get_the_title();
				global $post;
				$author_id=$post->post_author;
				if($author_id == $c_id){
					$html .= '<div class="funnel_title">';
						$html .= '<h3 style="float:left;"><a href="'.$parent_link.'">'.$parent_title.'</a></h3>';
						if(get_post_meta(get_the_ID(),'is_funnel_published',true) == 1){
							$html .= '<label class="switch unpublish_funnel_switch">
								<input data="'.get_the_ID().'" type="checkbox" name="publish_funnel" onchange="unpublish_funnel('.get_the_ID().');" checked>
								<span class="slider round"></span>
							</label>';
						}else{
							$html .= '<label class="switch publish_funnel_switch">
								<input data="'.get_the_ID().'" type="checkbox" name="publish_funnel" onchange="publish_funnel('.get_the_ID().');" >
								<span class="slider round"></span>
							</label>';
						}
						$html .= '<input type="checkbox" style="margin-left: 40px;" name="viewascustomer" onchange="funnel_viewascustomer('.get_the_ID().');" data-funnel_id="'.get_the_ID().'" value="1" id="viewascustomer'.get_the_ID().'" /> <label style="cursor:pointer;" for="viewascustomer'.get_the_ID().'">View as Customer</label>';
						$html .= '<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/floader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />';
						$html .= '<div id="funnel_delete_button_container" style="float:right;">';
						if(get_post_meta(get_the_ID(),'is_funnel_published',true) != 1){ 
							$html .= '<a data="'.get_the_ID().'" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;" class="delete_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
						}else{ 
							/*<p id="hide_after_unpublish" style="float:right;">You can't delete published Funnel.</p>*//*
							$html .= '<a id="dlt_after_unpublish" data="'.get_the_ID().'" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;display:none;" class="delete_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
						}
						$html .= '</div>';
						$html .= '<h6>';
							$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
							if($funnel_type == 'lead_magnet_funnel'){
								$html .= 'Lead Magnet Funnel';
							}else if($funnel_type == 'video_lead_funnel'){
								$html .= 'Video Lead Funnel';
							}else if($funnel_type == 'sales_funnel'){
								$html .= 'Sales Funnel';
							}
							$category = get_the_category( get_the_ID() ); 
							if($category[0]->cat_name){
								$html .= '&nbsp;-&nbsp;';
								$html .= $category[0]->cat_name;
							}
						$html .= '</h6>';
					$html .= '</div>';
				}else{
					$html .= '<div class="funnel_title">
						<h3 style="float:left;"><a href="'.$parent_link.'">'.$parent_title.'</a></h3>
						<input type="hidden" name="current_user_id" value="'.$c_id.'">';
						if($funnel_statue == 0){
						$html .= '<label class="switch">
									<input data="'.get_the_ID().'" type="checkbox" name="active_funnel" checked>
									<span class="slider round"></span>
							</label>';						
						}else{
						$html .= '<label class="switch">
								<input data="'.get_the_ID().'" type="checkbox" name="active_funnel">
								<span class="slider round"></span>
							</label>';
						}
						$html .= '<img class="switch funnel_loader" src="'.get_stylesheet_directory_uri().'/images/floader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />';
						if($funnel_statue != 0){
						$html .= '<a id="dlt_active_funnel"data="'.get_the_ID.'" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right;" class="delete_inactive_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
						}else{
						$html .= '<a id="dlt_inactive_funnel" data="'.get_the_ID().'" style="background:#FF6347;padding:5px 10px;color:white;font-weight:600;border-radius:5px;text-decoration:none;margin:0 10px;float:right; display:none;" class="delete_inactive_funnel" href="javascript:void(0);" >Delete <i class="fa fa-trash" aria-hidden="true"></i></a>';
						} 
						$html .= '<h6>';
							$funnel_type = get_post_meta(get_the_ID(),'funnel_type',true); 
							if($funnel_type == 'lead_magnet_funnel'){
								$html .= 'Lead Magnet Funnel';
							}else if($funnel_type == 'video_lead_funnel'){
								$html .= 'Video Lead Funnel';
							}else if($funnel_type == 'sales_funnel'){
								$html .= 'Sales Funnel';
							}
							$category = get_the_category( get_the_ID() ); 
							if($category[0]->cat_name){
								$html .= '&nbsp;-&nbsp;';
								$html .= $category[0]->cat_name;
							}
						$html .= '</h6>';
					$html .= '</div>';
				}
				$html .= '</hr>';
				$html .= '<div class="funnel_content" style="width: 100%;display: inline-block;">
						<div class="funnel_img" style="width:20%;float:left;">';
								//echo "SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'";
								$funnel_img = $wpdb->get_row("SELECT * FROM `wp_funnel_screenshot` WHERE funnel_id = '".get_the_ID()."'");
								$path = wp_get_upload_dir();
								$img_path = $path['baseurl'].'/screenshot';
								$f_img = $img_path .'/'. $funnel_img->image;
								if($funnel_img){
							$html .= '<img src="'.$f_img.'" data="'.get_the_ID().'" class="change_funnel_image" style="width:130px;" />';
							}else{
							$html .= '<img src="http://placehold.it/200x200" data="'.get_the_ID().'" class="change_funnel_image" style="width:130px;" />';
							}							
						$html .= '</div>';
						$html .= '<div class="funnel_btns" id="funnel_btns_'.get_the_ID().'" style="width:80%;float:left;">
							<div class="funnel_btns_left" style="width:50%;float:left;padding-right: 5px;">';
								if(get_post_meta(get_the_ID(),'is_funnel_published',true) != 1){ 
									$html .= '<a href="javascript:void(0);" class="edit_funnel_pages" data="'.get_the_ID().'">Edit Pages</a>
									<a href="javascript:void(0);" class="funnel_website_links" data="'.get_the_ID().'">Website Links</a>
									<a href="javascript:void(0);" class="followup_emails" data="'.get_the_ID().'">Followup Emails</a>';
								}else{ 
									$html .= '<a href="javascript:void(0);" class="" data="'.get_the_ID().'">My Funnel Link</a>
									<a href="#">Download Resource</a>
									<a href="#">Affiliate Requests</a>';
								}
							$html .= '</div>';
							$html .= '<div class="funnel_btns_right" style="width:50%;float:left;padding-left: 5px;">
								<a href="javascript:void(0);" data="'.get_the_ID().'" class="add_promotional_tools">Promotional Tools</a>
								<a href="#">Statistics</a>';
								if(get_post_meta(get_the_ID(),'is_funnel_published',true) != 1){
								$html .= '<a href="#">Marketplace</a>';
								}else{
								$html .= '<a href="javascript:void();" class="funnel_review" data="'.get_the_ID().'">Review</a>';
								}
							$html .= '</div>';
						$html .= '</div>';
					$html .= '</div>';
					$html .= '<div id="promotional_tools_div_'.get_the_ID().'" style="display:none;">';
					 if($author_id == $c_id){
						 $html .= '<div style="display:inline-block;width: 100%;">';
							$html .= '<p style="margin:0;font-size: 20px;font-weight: 600;text-align: center;">Drag and Drop Each Promotional Tool Element Below To Create and Edit</p>
							<p style="margin:0;font-size: 14px;text-align: center;">You can drag more than one of each type, to provide multiple alternatives</p>
							<ul class="droptrue" id="sortable1" style="width:100%;margin-bottom:10px;">
								<li class="ui-state-default before_drop" post_id="'.get_the_ID().'" data="email_swipe">Email Swipes</li>
								<li class="ui-state-default before_drop" post_id="'.get_the_ID().'" data="banner">Banners</li>
								<li class="ui-state-default before_drop" post_id="'.get_the_ID().'" data="fb_ads">Facebook Ads</li>
								<li class="ui-state-default before_drop" post_id="'.get_the_ID().'" data="yt_vid">YouTube Videos</li>
							</ul>';
							
							$html .= '<ul class="dropfalse dropfalse_'.get_the_ID().'" id="sortable2" style="width:100%;border:1px dashed;background:#FFFFDD;">';
								$html .= '<p>Promotional Tools</p>';
								$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ");
								//print_r($data);
								foreach($get_pt as $gpt){
									$data = $gpt->content;
									$ter =  maybe_unserialize($data);
									foreach($ter as $k=>$p){
										if($k == "subject"){
											$subject = $p;
										}else if($k == "data"){
											$fb_data = $p;
										}else if($k == "image"){
											$fb_image = $p;
										}else if($k == "url"){
											$yt_url = $p;
										}else if($k == "desc"){
											$video_des = $p;
										}else{
											$content = $p;
										}
									}
									
									if($gpt->item_type == 'email_swipe'){ 	
									$f_text = "Insert member's first name";
									$l_text = "Insert member's last name";
									$p_text = "Insert squeeze page link";
									$aff_fname = "add_macro_to_mail('AFFFIRSTNAME',".$gpt->id.")";
									$aff_lname = "add_macro_to_mail('AFFLASTNAME',".$gpt->id.")";
									$aff_squeeze_id = "add_macro_to_mail('SQUEEZE_identifier',".$gpt->id.")";
									
									$html .= '<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="'.$gpt->post_id.'" data="email_swipe" style="display: list-item;" draggable="false" item_id="'.$gpt->id.'">
										<div id="email_swipe_'.get_the_ID().'">
											<label>Email Content</label>
											<input type="text" name="email_swipe_content_subject_'.$gpt->id.'" placeholder="Email Subject" value="'.$subject.'"/>
											<div>
												<ul class="macros">
													<li onclick="'.$aff_fname.'">'.$f_text.'</li>
													<li onclick="'.$aff_lname.'">'.$l_text.'</li>
													<li onclick="'.$aff_squeeze_id.'">'.$p_text.'</li>
												<ul>
											</div>
											<textarea name="email_swipe_content_'.$gpt->id.'">'.$content.'</textarea>
											<div class="action_btns">
											<button class="button btn-update update_'.$gpt->item_type.'" type="button">Update</button>
											<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
											<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
											</div>
										</div>
									</li>';
									}else if($gpt->item_type == 'banner'){ 
										$html .= '<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="'.$gpt->post_id.'" data="banner" style="display: list-item;" draggable="false" item_id="'.$gpt->id.'">';
											$html .= '<div id="banner_'.$gpt->post_id.'">
												<label>Banners</label>
												<input type="hidden" value="" class="regular-text" id="process_banner_image_'.$gpt->id.'" data="'.$gpt->id.'" name="process_banner_image_'.$gpt->id.'">
												<button style="width:30%;background:#82b440;" type="button" class="banner_image button">Choose Banner</button>';
												$image_url = wp_get_attachment_url( $gpt->content );
												$html .= '<img src="'.$image_url.'" class="preview banner_image_disp_'.$gpt->id.'" style="height:100px;width:100px;" />
												<div class="action_btns">
												<button class="button btn-update update_'.$gpt->item_type.'" type="button">Update</button>
												<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
												<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
												</div>
											</div>
										</li>';
										}else if($gpt->item_type == 'fb_ads'){ 
										$html .= '<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="'.$gpt->post_id.'" data="fb_ads" style="display: list-item;" draggable="false" item_id="'.$gpt->id.'">';
											$html .= '<div id="fb_ads_'.$gpt->post_id.'">
												<label>Facebook Ads</label>
												<input type="hidden" value="" class="regular-text" id="process_fb_image_'.$gpt->id.'" data="'.$gpt->id.'" name="process_fb_image_'.$gpt->id.'">
												<button style="width:30%;background:#82b440;" type="button" class="fb_image button">Choose Banner</button>';
												$image_url = wp_get_attachment_url( $fb_image );
												$html .= '<img src="'.$image_url.'" class="preview fb_image_disp_'.$gpt->id.'" style="height:100px;width:100px;" />
												<textarea name="facebook_content_'.$gpt->id.'">'.$fb_data.'</textarea>
												<div class="action_btns">
												<button class="button btn-update update_'.$gpt->item_type.'" type="button">Update</button>
												<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
												<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
												</div>
											</div>
										</li>';
										}else if($gpt->item_type == 'yt_vid'){
										$html .= '<li class="ui-sortable-handle ui-state-highlight after_drop" post_id="'.$gpt->post_id.'" data="yt_vid" style="display: list-item;" draggable="false" item_id="'.$gpt->id.'">
											<div id="yt_vid_'.$gpt->post_id.'">
												<label>YouTube Videos</label>
												<input type="text" class="yt_vid_content" name="yt_vid_content_'.$gpt->id.'" placeholder="Youtube Video URL" value="'.$yt_url.'"/>
												<input type="text" class="yt_vid_content_desc" name="yt_vid_content_desc_'.$gpt->id.'" placeholder="Video Description" value="'.$video_des.'"/>
												<div class="action_btns">
												<button class="button btn-update update_'.$gpt->item_type.'" type="button">Update</button>
												<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
												<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
												</div>
											</div>
										</li>';
										} 
									$html .= '</li>';
								}
							$html .= '</ul>';
						 $html .= '</div>';
     					 $f_text = "Insert member's first name";
						 $l_text = "Insert member's last name";
						 $p_text = "Insert squeeze page link";
						 $aff_fname = "add_macro_to_mail('AFFFIRSTNAME','')";
						 $aff_lname = "add_macro_to_mail('AFFLASTNAME','')";
						 $aff_squeeze_id = "add_macro_to_mail('SQUEEZE_identifier','')";
						 $html .= '<div id="email_swipe_'.get_the_ID().'" style="display:none;">
								<label>Email Content</label>
								<input type="text" name="email_swipe_content_subject_" placeholder="Email Subject" />
								<ul class="macros">
									<input type="hidden" name="hidden_post_id" value="" />
									<li onclick="'.$aff_fname.'">'.$f_text.'</li>
									<li onclick="'.$aff_lname.'">'.$l_text.'</li>
									<li onclick="'.$aff_squeeze_id.'">'.$p_text.'</li>
								<ul>
								<textarea name="email_swipe_content_"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_email_swipe" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>';
						 $html .= '<div id="banner_'.get_the_ID().'" style="display:none;">
								<label>Banners</label>								
								<input type="hidden" value="" class="regular-text" id="process_banner_image_" name="process_banner_image_">
								<button style="width:30%;background:#82b440;" type="button" class="banner_image button">Choose Banner</button>
								<img src="" class="preview" style="height:100px;width:100px;display:none;" />
							
								<div class="action_btns">
								<button class="button btn-update update_banner" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="'.get_stylesheet_directory_uri.'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="fb_ads_'.get_the_ID().'" style="display:none;">
								<label>Facebook Ads</label>
								<input type="hidden" value="" class="regular-text" id="process_fb_image_" name="process_fb_image_">
								<button style="width:30%;background:#82b440;" type="button" class="fb_image button">Choose Banner</button>
								<img src="" class="fb_preview" style="height:100px;width:100px;display:none;" />
								<textarea name="facebook_content_" class="facebook_content"></textarea>
								<div class="action_btns">
								<button class="button btn-update update_fb_ads" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>
							<div id="yt_vid_'.get_the_ID().'" style="display:none;">
								<label>YouTube Videos</label>
								<input type="text" class="yt_vid_content" name="yt_vid_content_" placeholder="Youtube Video URL" />
								<input type="text" class="yt_vid_content_desc" name="yt_vid_content_desc_" placeholder="Video Description" />
								<div class="action_btns">
								<button class="button btn-update update_yt_vid" type="button">Update</button>
								<button class="button btn-remove remove_promotional_tool" type="button">Remove</button>
								<img class="funnel_loader" src="'.get_stylesheet_directory_uri().'/images/funnel_loader.gif" style="width:30px;margin-left:20px;margin-bottom:5px;display:none;" />
								</div>
							</div>';
					 }else{
						 $html .= '<div>					';	
							$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".get_the_ID()."' ORDER BY position ASC ");
							foreach($get_pt as $item){
								$html .= '<div style="padding: 15px 10px; margin: 10px 0; background-color: #e5d4d4; font-size:16px; font-weight:700;"><?php echo $item->item_type; ?> </div>';
							}
						$html .= '</div>';
					 }
					$html .= '</div>';
					$html .= '<div id="followup_emails_div_'.get_the_ID().'" style="display:none;">';
					$email_function = "openEmail";
						$html .= '<div class="tab_email">
						  <button class="tablinks_email" onclick="openEmail(event, '."'immediate_".get_the_ID()."_tab'".')">Immediately</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'one_day_".get_the_ID()."_tab'".')">1 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'two_day_".get_term_ID."_tab'".')">2 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'three_day_".get_the_ID()."_tab'".')">3 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'four_day_".get_the_ID()."_tab'".')">4 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'five_day_".get_the_ID()."_tab'".')">5 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'six_day_".get_the_ID()."_tab'".')">6 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'seven_day_".get_the_ID()."_tab'".')">7 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'fourteen_day_".get_the_ID()."_tab'".')">14 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'twentyone_day_".get_the_ID()."_tab'".')">21 Day</button>
						  <button class="tablinks_email" onclick="openEmail(event, '."'twentyeight_day_".get_the_ID()."_tab'".')">28 Day</button>
						</div>';
						$html .= '<div class="followup_email_container" style="display:inline-block;width:100%;">'; //followup_email_container div start
							$html .= '<div style="width:100%;padding:0 2px;" id="immediate_'.get_the_ID().'_tab" class="tabcontent_email">
									<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;">Immediately</p>';
									$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='0' ");
									$html .= '<input type="text" placeholder="Enter Subject Line" name="immediate_subject_'.get_the_ID().'" style="border: none !important;box-shadow: none !important;background:#f5f5f5;font-weight:600;margin-top: 0;" value="'.$data->subject.'" />';						
									$editor_id_immediate = 'immediate_'.get_the_ID();
									$settings_immediate =   array(
										'wpautop' => true, // use wpautop?
										'media_buttons' => false, // show insert/upload button(s)
										'textarea_name' => $editor_id_immediate, // set the textarea name to something different, square brackets [] can be used here
										'textarea_rows' => get_option('default_post_edit_rows', 10), // rows="..."
										'tabindex' => '',
										'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
										'editor_class' => '', // add extra class(es) to the editor textarea
										'teeny' => false, // output the minimal editor config used in Press This
										'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
										'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
										'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
									);
									ob_start(); 
									wp_editor( $data->content, $editor_id_immediate, $settings_immediate );
									$html .= ob_get_clean();
									$html .= '<button class="btn btn-info" id="btn_immidiate_save_email_'.get_the_ID().'" onclick="save_followup_email('."'".$editor_id_immediate."'".','."'immediate_subject_".get_the_ID()."'".','."'btn_immidiate_save_email_".get_the_ID()."'".','.get_the_ID().',0);">Save Email</button>
									<button class="btn btn-primary" onclick="add_website_email_link('.get_the_ID().')">Add Link1</button>';
							$html .= '</div>';
							$html .= '<div style="width:100%;padding:0 2px;" id="one_day_'.get_the_ID().'_tab" class="tabcontent_email">
									<p style="background:#f5f5f5;font-weight: 800;margin: 0;text-align: center;font-size: 20px;">After 1 Days</p>';
									
									$data = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id='".get_the_ID()."' AND type='1' ");
									
									$html .= '<input type="text" placeholder="Enter Subject Line" name="after1days_subject_'.get_the_ID().'" style="border: none !important;box-shadow: none !important;background:#f5f5f5;font-weight:600;margin-top: 0;" value="'.$data->subject.'" />';
									$html .= '<div>'.add_editor().'</div>';
									$html .= '<button class="btn btn-info" id="btn_after1days_save_email_'.get_the_ID().'" onclick="save_followup_email('."'".$editor_id_after1days."'".','."'after1days_subject_".get_the_ID()."'".','."'btn_after1days_save_email_".get_the_ID()."'".','.get_the_ID().',1);">Save Email</button>
									<button class="btn btn-primary">Add Link1</button>
							</div>';
						$html .= '<div>';//followup_email_container div over
					$html .= '</div>';
			}
			$html .= '</div>';*/
		endwhile;
		$pagenav = '<button id="page" value="'.$page.'" style="display:none;"></button>';	
	}else if($page != 'lastpage'){
		$html[] = '';
		$pagenav = '<button id="page" value="lastpage" style="display:none;"></button>';		
	}else{
		
	}
	
	$array = array('html'=>$html, 'pagenav'=>$pagenav);
	$json = json_encode($array);
	echo $json;
	exit;
}

function get_funnel_buy_purchase_data(){
	global $wpdb;
	$html .= '';
	$page = $_POST['id'];
	$limit = $_POST['limit'];
	$newpage = (($page - 1)*($limit));
	$transaction_history = $wpdb->get_results("SELECT * FROM wp_setc_pks_main_balance WHERE user=".get_current_user_id()." AND admin_share='0' ORDER BY id DESC LIMIT $newpage, $limit");		
	usort($transaction_history);
	$html .= '<style>tr.admin_share_class{background-color:#e0ebff;}</style>';
	$html .= '<table id="table_transaction">
				<tr>
					<th>ID</th>
					<th>Type</th>
					<th>Description</th>
					<th>Price</th>
					<th>Time</th>
				</tr>';
				foreach($transaction_history as $th){
					if($th->admin_share == 1){
						$admin_share_class = "admin_share_class";
					}else{
						$admin_share_class = "";
					}
					$html .= '<tr class="'.$admin_share_class.'">
						<td>#'.$th->id.'</td>';
						if($th->funnel_id == '0'){
							if($th->type == 'R'){
								$html .= '<td style="color:#28d094;">Refunded</td>';
							}else{
								$html .= '<td style="color:#28d094;">Credited</td>';
							}
							if($th->type == 'W'){
								$html .= '<td>Withdrawal Credited</td>';
							}else if($th->type == 'R'){
								$html .= '<td>Refund</td>';
							}else{
								$html .= '<td>Credits Added</td>';
							}
						}else{
							if($th->type == 'D'){ 
								$html .= '<td style="color:#28d094;">Activated</td>
								<td><a href="'.get_permalink($th->funnel_id).'">Activated '.ucfirst($th->funnel_type).' Funnel ('.get_the_title($th->funnel_id).')</a> from <a href="'.get_author_posts_url(get_post_field('post_author', $th->funnel_id)).'">'.get_the_author_meta('first_name',get_post_field('post_author', $th->funnel_id)).' '.get_the_author_meta('last_name',get_post_field('post_author', $th->funnel_id)).'</a></td>';
							}else if($th->type == 'C'){
								$row = $wpdb->get_row("SELECT * FROM `wp_setc_pks_main_balance` WHERE id='".$th->linkto."'");
								$html .= '<td style="color:#ff4961;">Sold</td>
								<td><a href="'.get_permalink($th->funnel_id).'">Sold '.ucfirst($th->funnel_type).' Funnel ('.get_the_title($th->funnel_id).')</a> to <a href="'.get_author_posts_url($row->user).'">'.get_the_author_meta('first_name',$row->user).' '.get_the_author_meta('last_name',$row->user).'</a></td>';
							}else if($th->type == 'R'){
								$row = $wpdb->get_row("SELECT * FROM `wp_setc_pks_main_balance` WHERE id='".$th->linkto."'");
								$html .= '<td style="color:#ff4961;">Debited</td><td>
								<a href="'.get_permalink($th->funnel_id).'">Refund Funnel ('.get_the_title($th->funnel_id).')</a></td>';
							}
						}	
						$html .= '<td>$'.$th->amount.'</td>
						<td>'.$th->time.'</td>
					</tr>';
				}
			$html .= '</table>';
	echo $html;
	exit;
}

function delete_external_link_data(){
	$post_id = $_POST['post_id'];
	global $wpdb;
	$query = $wpdb->query("DELETE FROM `wp_setc_website_links` where id = ".$post_id);
	echo '1';
	exit;
}
function edit_external_link_data(){
	$post_id = $_POST['post_id'];
	global $wpdb;
	$linktype = ($query->aff_network == 'creator')?'Download':ucfirst($query->aff_network);
	$html = '';
	$html .= '<style>.affiliate_link_error{background-color: #FF000055 !important;}</style>';
	$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` where id = ".$post_id);
	if($query->aff_network == 'creator'){
		$link_label = 'Download Link';
	}else if($query->aff_network == 'external'){
		$link_label = 'External Link';
	}else if($query->aff_network == 'jvzoo'){
		$link_label = 'JVZoo Affiliate Link';
	}else if($query->aff_network == 'clickbank'){
		$link_label = 'ClickBank ID';
	}else if($query->aff_network == 'warriorplus'){
		$link_label = 'WarriorPlus Product';
	}
	$html .= '<input type="hidden" name="linktype" value="'.$query->aff_network.'" /><label>Product/Link Name</label><input type="text" name="affname" value="'.$query->name.'" style="margin:0 !important;"><br><br>';
	if($query->aff_network == 'warriorplus'){
		$html .= '<label>'.$link_label.'</label>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=50");
		$result = curl_exec($ch);

		$json_decode_result = json_decode($result);
		$html .= '<select name="wp_offer_update" style="width:100%;">';
		foreach($json_decode_result->data as $data){
			if($query->aff_link == $data->offer->id){
				$selected = "selected";
			}else{
				$selected = "";
			}
			$html .= '<option value="'.$data->offer->id.'" '.$selected.'>'.$data->offer->name.'</option>';
		}
		$html .= '</select>';
	}else{
		$html .= '<label>'.$link_label.'</label><input type="text" name="afflink" value="'.$query->aff_link.'" style="margin:0 !important;">';
	}
	if($query->aff_network == 'external'){
		$html .= '<label>Notes</label><input type="text" name="edit_notes" value="'.$query->notes.'" style="margin:0 !important;">';
	}else if($query->aff_network == 'clickbank'){
		$html .= '<label>alt page</label><input type="text" name="edit_notes" value="'.$query->notes.'" style="margin:0 !important;">';
	}
	
	$html .= '<p id="error_msg_aff"></p>';
	$html .= '<button class="update_aff_external_link button" data-postid="'.$post_id.'" style="border: 0;background:#279dbc;color: #FFF;padding: 10px 50px;border-radius: 5px;font-weight: 600;margin-top: 10px;" type="button">Update '.$linktype.' Link</button>';
	echo $html;
	exit;
}

function update_aff_external_link(){
	global $wpdb;
	$update_id = $_POST['post_id'];
	$update_link = $_POST['link'];
	$update_name = $_POST['link_name'];
	$edit_notes = $_POST['edit_notes'];
	
	if($check_identifier == ''){
		if($update_id){
			$query = $wpdb->query("UPDATE `".$wpdb->prefix."setc_website_links` SET name='".$update_name."', aff_link='".$update_link."', notes='".$edit_notes."'  WHERE id=".$update_id);
			if($query){
				echo "1";
			}
		}
	}else{
		echo "2";
	}
	exit;
}
function generate_funnel_links(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$a=array("https://fnl1.com", "https://fnl2.com", "https://fnl3.com", "https://fnl5.com", "https://fnl7.com");
	$random_keys=array_rand($a,1);
	$root_url = $a[$random_keys];
	
	// $is_custom_url = '1';
	
	$is_custom_url = get_user_meta(get_current_user_id(),'fm_custom_url',true);
	
	if($is_custom_url){
		$shorten_url = $is_custom_url.'/'.$post_id.'/';
	}else{
		$shorten_url = $root_url.'/f/'.$post_id.'/'.get_current_user_id().'/';
	}
	
	$slug = get_post_field( 'post_name', $post_id );
	$html = "<h5>Squeeze Page</h5>";
	$html .= "<p>Here is your squeeze page link. Send traffic to this link and everyone who signs up will start receiving emails containing your affiliate links for recommended products.</p>";
	$html .= "<p>Shortened link: (Preferred for social sharing, email marketing or most forms of sharing. You must use this link to place it in the Description of your video if you are advertising via Video Discovery with YouTube Ads)</p>";
	$html .= '<p style="text-align:center;font-size: 18px;font-weight: 600;"><a href="'.$shorten_url.'" target="_BLANK">'.$shorten_url.'</a></p>';
	$html .= "<p>Raw link: (Use this link if you advertise on platforms that doesn't allow URL Redirection. For example, YouTube doesn't allow URL redirects, that's why for some type of ads, for example Skippable in-stream Ads, and Bumper Ads, we need to use the Raw Link version to advertise your missions via YouTube Ads)</p>";
	$html .= '<p style="text-align:center;font-size: 18px;font-weight: 600;"><a href="https://funnelmates.com/f/'.$post_id.'/'.get_current_user_id().'/" target="_BLANK">https://funnelmates.com/f/'.$post_id.'/'.get_current_user_id().'/</a></p>';
	$html .= '<h5>Tracking</h5>';
	$html .= '<p>You can also track the hits and conversion rate of your squeeze pages. All you need to do is append a tracking ID (where it says XXXX)></p>';
	$html .= '<p style="text-align:center;font-size: 18px;font-weight: 600;">https://funnelmates.com/f/'.$post_id.'/'.get_current_user_id().'/XXXX</p>';
	$html .= '<p>The tracking id can be up to 20 characters long but it has to be alphanumeric (letters and numbers only) For example if you were advertising this squeeze page on your blog you could make a tracking link like this:</p>';
	$html .= '<p style="text-align:center;font-size: 18px;font-weight: 600;">https://funnelmates.com/f/'.$post_id.'/'.get_current_user_id().'/blogpost1</p>';
	$html .= '<p>Please note: If you are using YouTube ads they are currently not permitting the use of tracking URLS, so you\'ll need to use the raw link as mentioned above and track using Google\'s internal ad tracking system.</p>';
	echo $html;
	exit;
}
/*function save_custom_url(){
	global $wpdb;
	$user_id = get_current_user_id();
	$custom_domain = $_POST['custom_url'];
	$query = "SELECT $wpdb->users.ID  FROM $wpdb->users  INNER JOIN $wpdb->usermeta ON ( $wpdb->users.ID = $wpdb->usermeta.user_id )  INNER JOIN $wpdb->usermeta AS mt1 ON ( $wpdb->users.ID = mt1.user_id ) WHERE 1=1  AND 
          ( $wpdb->usermeta.meta_key = 'fm_custom_url' AND $wpdb->usermeta.meta_value = '$custom_domain' ) ";
	$result = $wpdb->get_results($query);
	if($result && $user_id != $result[0]->ID){
		echo '0';
	}else if($result && $user_id == $result[0]->ID){
		echo '2';
	}else{
		update_user_meta( $user_id, 'fm_custom_url', $custom_domain);
		echo '1';
	}
	exit;
}*/
function remove_custom_url(){
	$user_id = get_current_user_id();
	delete_user_meta( $user_id, 'fm_custom_url');
	echo '1';
	exit;
}
function search_funnels_template(){
	$user_id = get_current_user_id();
	$user_meta = get_userdata($user_id);
	// echo '<pre>';print_r($user_meta->roles);exit;
	$time_start = microtime(true);
	$interval = new DateInterval('P1M');
	$interval->invert = 1;
	$date = new DateTime(date('Y-m-d'));
	$date->add($interval);	
	$funnel_limit = -1;
	if(in_array('funnelmates_access',$user_meta->roles)){
		$funnel_limit = 3;
	}else if(in_array('funnelmates_deluxe',$user_meta->roles)){
		$funnel_limit = 3;
	}
	$args = array(
		'nopaging'          => true,
		'posts_per_page'    => -1,
		'post_type'         => 'landing',
		'post_status'       => 'publish',
		'order_by'          => 'date',
		'post_parent' 		=> 0,
		'date_query'        => array(
			'after' => $date->format('Y-m-d')
		),
		'author__in'		=> array($user_id)
	);
	$month_query = new WP_Query($args);
	// echo $month_query->post_count;exit;
	if($month_query->post_count >= $funnel_limit && $funnel_limit != -1){
		// $html .= '<p style="text-align:center;font-size:30px;font-weight:600;">Your membership level only allows you to activate 2 free funnels a month, to unlock unlimited free funnels <a href="https://wildfireconcepts.com/funnelmatesplatinum" target="_BLANK">click here to upgrade to platinum</a>.</p>';
		$html .= '<p style="text-align:center;font-size:30px;font-weight:600;">Your membership level only allows you to create 3 funnels a month, to publish unlimited funnels <a href="https://wildfireconcepts.com/funnelmatesplatinum" target="_BLANK">click here to upgrade to platinum</a>.</p>';
		// $html .= '<h4  style="text-align:center;">You\'ve created '.$month_query->post_count.' Funnels after '.$date->format('Y-m-d').'</h4>';
		wp_reset_query();
		echo $html;exit;
	}
	
	
	$funneltype = str_replace('_','-',$_POST['funnel_type']);
	$funneltype_display = str_replace('_',' ',$_POST['funnel_type']);
	$terms = get_terms( 'fl-builder-template-category', array(
		'hide_empty' => false,
	));
	/*$html .= '<div id="template_search_container">';
	$html .= '<input style="float: left;width: 95%;margin: 0;" type="text" name="template_search" placeholder="Search Template" />';
	$html .= '<input type="hidden" name="funnel_type" value="'.$funneltype.'" />';
	$html .= '<button id="search_template_clicked" style="padding: 11px;float: left;color: white;" type="button" class="btn btn_green"><i class="fa fa-search"></i></button>';
	$html .= '</div>';*/
	$t_id_array = array();
	foreach($terms as $t){
		$args = array(
			'post_type' => 'fl-builder-template',
			'tax_query' => array(
				array(
					'taxonomy' => 'fl-builder-template-category',
					'field' => 'term_id',
					'terms' => $t->term_id
				),
				array(
					'taxonomy' => 'funnel_type',
					'field' => 'slug',
					'terms' => $funneltype
				)
			),
			
		);
		$query = new WP_Query( $args );
		
		if($query->have_posts()){
			while ( $query->have_posts() ) : $query->the_post();
				if(get_the_title() == $t->name){
					array_push($t_id_array,get_the_ID());
				}
			endwhile;
			wp_reset_postdata();
		}
	}
	$html .= '<div class="template_sel_parent" style="margin-top:15px;display: inline-block;width: 100%;">';
	$ppp = 5;
	if(count($t_id_array) > 0){
		$args = array(
			'post_type' => array( 'fl-builder-template' ),
			'orderby' => 'post__in',
			'post__in' => $t_id_array,
			'posts_per_page' => $ppp
		);
		$query = new WP_Query( $args );
				
		if($query->have_posts()){
			while ( $query->have_posts() ) : $query->the_post();
				$cat = get_the_terms( get_the_ID(), 'fl-builder-template-category' );
				// print_r($cat[]);
				$html .= '<div class="count_templates" style="width: 20%;float: left;text-align: center;">';
				$html .= '<h4 style="font-size:17px;">'.get_the_title().'</h4>';
				$html .= '<div>'.get_the_post_thumbnail(get_the_ID(),'thumbnail').'</div>';
				$html .= '<button style="margin-top: 0.5rem;color:#FFF;" data="'.$cat[0]->term_id.'" type="button" class="btn btn_green select_funnel_template">Select Template</button>';
				$html .= '</div>';
			endwhile;
			wp_reset_postdata();
		}
	}else{
		$html .= '<p style="text-align: center;font-size: 25px;">There is no templates for "'.ucfirst($funneltype_display).'"</p>';
	}
	$html .= '</div>';
	echo $html;
	exit;
}
function open_download_resource(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$html = '';
	$html .= '<h4 align="center">Download Resource</h4>';
	$author_id = get_post_field ('post_author', $post_id);
	$display_name = get_the_author_meta( 'display_name' , $author_id ); 
	$html .= '<p><strong>'.$display_name.'</strong> has provided the following downloads as incentives for people to sign up on your <strong>'.get_the_title($post_id).'</strong> lead capture funnel.</p>';
	$html .= '<p>Click below to access these gifts.</p>';
	$query_download = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'creator'  ");
	if(count($query_download) > 0){
		foreach($query_download as $qd){
			$html .= '<p style="text-align:center;"><a href="'.$qd->aff_link.'" target="_BLANK">Click to download <strong>'.$qd->name.'</strong></a>';
		}
	}
	$html .= '<p>Understanding your funnel, and knowing what people will receive can help you share your funnel link more confidently. So if you\'re online and someone\'s asking for a good resource to solving their problem, you can share your funnel link. Help them out, and get a new subscriber! Win, win!</p>';
	echo $html;
	exit;
}
function open_affiliate_requests_links(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$html = '';
	$html .= '<h4>\''.get_the_title($post_id).'\' FUNNEL DESCRIPTION</h4>';
	$funnel = $wpdb->get_row("SELECT description FROM `wp_fm_marketplace_items` WHERE post_id='$post_id'");
	$html .= '<p>'.$funnel->description.'</p>';
	$html .= '<h4 align="center">Affiliate Requests</h4>';
	$html .= '<p>Below you\'ll find the details for all of products promoted in the <strong>'.get_the_title($post_id).'</strong> funnel.</p>';
	$html .= '<p>This includes products recommended on the funnel pages and in the email sequence.</p>';
	$html .= '<p>Please click each link and where required, request affiliate approval.</p>';
	$html .= '<p>Add codeword \'<strong>FunnelMates</strong>\' to increase your chances of being approved.  Some vendors are more likely to approve you if you let them know you\'re connected with us.</p>';
	$html .= '<p>NOTE: <strong>Please Allow 3-5 Days For Affiliate Approval</strong></p>';
	$html .= '<p>Affiliate approval is a manual task done by the vendor/product creator of each product. Please allow up to 5 days for approval on the products before contacting support.</p>';
	/*$html .= '<div style="background: #f9d8d8;padding: 5px;border: 1px solid #9d5555;border-radius: 5px;margin: 10px 0;color: #9d5555;">';
	$html .= '<p><strong>Your links below will change when you\'re approved!</strong></p>';
	$html .= '<ol><li>First they\'ll send you to request permission to promote.</li><li>Once approved your links will send you to the product sales page, so you can see the products you\'re automatically recommending in your funnel.</li></ol>';
	$html .= '</div>';*/
	$query_jvzoo = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'jvzoo'  ");
	if(count($query_jvzoo) > 0){
		$html .= '<h5>JVZoo Affiliate Products</h5>';
		foreach($query_jvzoo as $qjvz){
			$html .= '<div class="jvzoo_affiliate_links">';
				/*$product_id = substr($qjvz->aff_link, strrpos($qjvz->aff_link, '/') + 1);
				$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
				$affiliate_id = $general_settings['jvzoo_id'];
				$ch = curl_init(); 
				curl_setopt($ch, CURLOPT_USERPWD,'1602d00c13ea17c0c6b2d6432a2196bc2e1bd9200645de5e5a7255318ccfa3d8');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_URL, "https://api.jvzoo.com/v2.0/products/{$product_id}/affiliates/{$affiliate_id}");
				$result = curl_exec($ch);

				if($result === false)
					$approved = false;

				$result = json_decode($result);
				
				if($result->meta->status->http_status_code == '200' && $result->meta->status->message == 'Success'){
					$approved = $result->meta->status->http_status_code == '200' ? true : false;
				}*/
				$afflink_request = 'https://funnelmates.com/jvz/'.get_current_user_id().'/'.$qjvz->link_identifier.'/request';
				$afflink_sales = 'https://funnelmates.com/jvz/'.get_current_user_id().'/'.$qjvz->link_identifier.'/sales';
				$html .= '<p style="text-align:center;"><a href="'.$afflink_request.'" target="_BLANK">Click to request to promote <strong>'.$qjvz->name.'</strong></a> (<a href="'.$afflink_sales.'" target="_BLANK">Sales Page Preview</a>)';
				/*if($approved == true){
					$html .= '<span style="margin-left: 10px;background: #82b440;color: white;padding: 1px 5px;border-radius: 5px;font-weight: 600;"><i class="fas fa-check" style="margin-right: 5px;"></i>Approved</span>';
				}*/
				$html .= '</p>';
			$html .= '</div>';
		}
	}
	$query_wp = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'warriorplus'  ");
	if(count($query_wp) > 0){
		$html .= '<h5>WarriorPlus Affiliate Products</h5>';
		foreach($query_wp as $qwp){
			$html .= '<div class="warriorplus_affiliate_links">';
				// $afflink = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/';
				$afflink_request = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/request';
				$afflink_sales = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/sales';
				$html .= '<p style="text-align:center;"><a href="'.$afflink_request.'" target="_BLANK">Click to request to promote <strong>'.$qwp->name.'</strong></a> (<a href="'.$afflink_sales.'" target="_BLANK">Sales Page Preview</a>)</p>';
			$html .= '</div>';
		}
	}
	$query_cb = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'clickbank'  ");
	if(count($query_cb) > 0){
		$html .= '<h5>ClickBank Affiliate Products</h5>';
		$html .= '<p>The following Clickbank offers are included in this funnel, which do not require you to approval to promote</p>';
		$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
		$clickbank_id = $general_settings['clickbank_id'];
		if(empty($clickbank_id)){
			$clickbank_id = 'funnelmate';
		}
		foreach($query_cb as $qcb){
			$html .= '<div class="clickbank_affiliate_links">';
				// $html .= '<p style="text-align:center;"><a href="http://funnelmate.'.$qcb->aff_link.'.hop.clickbank.net/" target="_BLANK">https://'.$clickbank_id.'.'.$qcb->aff_link.'.hop.clickbank.net/</a></p>';
				$html .= '<p style="text-align:center;"><a href="http://'.$clickbank_id.'.'.$qcb->aff_link.'.hop.clickbank.net/" target="_BLANK">http://'.$clickbank_id.'.'.$qcb->aff_link.'.hop.clickbank.net/</a></p>';
			$html .= '</div>';
		}
	}
	$query_pks = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'paykickstart'  ");
	if(count($query_pks) > 0){
		$html .= '<h5>PayKickStart Affiliate Products</h5>';
		foreach($query_pks as $qpks){
			$html .= '<div class="paykickstart_affiliate_links">';
				$afflink = 'https://funnelmates.com/pks/'.get_current_user_id().'/'.$qpks->link_identifier.'/';
				$html .= '<p style="text-align:center;"><a href="'.$afflink.'" target="_BLANK">'.$afflink.'</a></p>';
				if($qex->notes){
					$html .= '<p style="margin:0;">Description: '.$qpks->notes.'</p>';
				}
			$html .= '</div>';
		}
	}
	$query_ex = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'external'  ");
	if(count($query_ex) > 0){
		// external
		$html .= '<h5>External Link</h5>';
		$html .= 'Your <strong>'.get_the_title($post_id).'</strong> funnel contains the following external links. You can customise these links by clicking the \'Website Links\' button and choosing \'Edit My Links\' in the dropdown menu.';
		foreach($query_ex as $qex){
			$html .= '<div class="external_affiliate_links" style="margin:10px;">';
				$html .= '<p style="margin:0;">Link Name: '.$qex->name.'</p>';
				if($qex->notes){
					$html .= '<p style="margin:0;">Description: '.$qex->notes.'</p>';
				}
				$html .= '<p>URL: <a target="_BLANK" href="'.$qex->aff_link.'">'.$qex->aff_link.'</a></p>';
			$html .= '</div>';
		}
	}
	$html .= '<p>Once your subscribers finish the first email sequence, they are automatically segmented onto lists that promote products as they go live. See the calendar in your dashboard regularly, to see what\'s coming up - request your links before we mail so you can get your commissions.</p>';
	echo $html;
	exit;
}
/*
function download_vid(){
	global $wpdb;
	$vid_url = $_POST['v_url'];
	$yt  = new YouTubeDownloader();
	$downloadLinks ='';
	$error='';
	if(!empty($vid_url)) {
		$vid = $yt->getYouTubeCode($vid_url);
        if($vid) {
            $result = $yt->processVideo($vid);
            print_r($result);
			exit;
            if($result) {
                //print_r($result);
                $info = $result['videos']['info'];
                $formats = $result['videos']['formats'];
                $adapativeFormats = $result['videos']['adapativeFormats'];

                

                $videoInfo = json_decode($info['player_response']);

                $title = $videoInfo->videoDetails->title;
                $thumbnail = $videoInfo->videoDetails->thumbnail->thumbnails{0}->url;
            }
            else {
                $error = "Something went wrong";
            }

        }
    }
    else {
		echo "else";
        $error = "Please enter a YouTube video URL";
    }
	exit;
}*/
function funnel_integrations_activator(){
	global $wpdb;
	// echo 'here';
	$html = '';	
	$html .= '<h5 style="margin-bottom: 20px;">Autoresponder Integrations</h5>';
	$html .= '<input type="hidden" name="int_post_id" value="'.$_POST['post_id'].'" />';
	///////////////// Aweber /////////////////
	/*$aw_accessToken = get_user_meta(get_current_user_id(),'fm_aweber_access_token',true);
	// $aw_accessTokenSecret = get_user_meta(get_current_user_id(),'fm_aweber_token_secret',true);
	if($aw_accessToken && $aw_accessTokenSecret){
		require_once(get_stylesheet_directory().'/includes/AWeber-API-PHP-Library-master/aweber_api/aweber.php');
		$html .= '<h6>Aweber</h6>';
		$consumerKey    = "AkdjgEyHLfpgvSrcrH32P98h";
		$consumerSecret = "4lYVMsP6s7uttOYpFDdXI9Wb5vVbcFWf6fQJrVgk";
		$aweber = new AWeberAPI($consumerKey, $consumerSecret);
		$account = $aweber->getAccount($aw_accessToken, $aw_accessTokenSecret);
		if($account->lists){
			// $list_selected = get_post_meta($_POST['post_id'],'aw_ar_integration',true);
			$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='aweber'");
			$list_selected = $list_selected->list;
			if($list_selected){
				$disable_drop = 'disabled';
			}else{
				$disable_drop = '';
			}
			$html .= '<select style="width: 100%;margin: 0;" name="aweber_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
			foreach($account->lists as $list){
				if($list_selected == $list->id){
					$aw_selected = 'selected';
				}else{
					$aw_selected = '';
				}
				$html .= '<option value="'.$list->id.'" '.$aw_selected.'>'.$list->name.'</option>';
			}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
				if($list_selected){
					$html .= '<button id="aweber_remove_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
				}else{
					$html .= '<button id="aweber_save_list" class="btn_green button btn" style="color:#FFFFFF;">Save Aweber List</button>';
				}
			$html .= '</div>';
		}else{
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-aweber/">Aweber Account not connected!</a></p>';
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-aweber/">Aweber Account not connected!</a></p>';
		$html .= '</div>';
	}*/
	$accessToken = get_user_meta(get_current_user_id(),'fm_aweber_access_token',true);
	$accessTokenRefresh = get_user_meta(get_current_user_id(),'fm_aweber_token_refresh',true);
	$scopes = array(
		'account.read',
		'list.read',
		'list.write',
		'subscriber.read',
		'subscriber.write',
		// 'email.read',
		// 'email.write',
		// 'subscriber.read-extended',
		// 'landing-page.read'
	);
	if($accessToken){
		require_once(get_stylesheet_directory().'/includes/oauth2/vendor/autoload.php');
		$client = new GuzzleHttp\Client();
		$provider = new League\OAuth2\Client\Provider\GenericProvider([
			'clientId'              	=> 'CGPdJJYONkMmvDYO4P6JCDnNzJtvoYUq',    // The client ID assigned to you by the provider
			'clientSecret'          	=> 'np8YZI3zpI6MjAYQxbZTuKPS0mi2mv5F',    // The client password assigned to you by the provider
			'redirectUri'           	=> 'https://funnelmates.com/settings/',
			'scopes' 					=> $scopes,
			'scopeSeparator' 			=> ' ',
			'urlAuthorize'          	=> 'https://auth.aweber.com/oauth2/authorize',
			'urlAccessToken'        	=> 'https://auth.aweber.com/oauth2/token',
			'urlResourceOwnerDetails' 	=> 'https://api.aweber.com/1.0/accounts'
		]);
		$existingAccessToken = get_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', true);
		// $html .= print_r($accesstoken1,true);
		if ($existingAccessToken->hasExpired()) {
			$newAccessToken = $provider->getAccessToken('refresh_token', [
				'refresh_token' => $existingAccessToken->getRefreshToken()
			]);
			update_user_meta(get_current_user_id(), 'fm_aweber_access_token', $newAccessToken->getToken());
			update_user_meta(get_current_user_id(), 'fm_aweber_token_refresh', $newAccessToken->getRefreshToken());
			update_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', $newAccessToken);
		}
		$finalAccessToken = get_user_meta(get_current_user_id(), 'fm_aweber_accesstoken_parent', true);
		$resourceowner = get_user_meta(get_current_user_id(), 'fm_aweber_resourceowner', true);
		$accounts = getCollection($client, $finalAccessToken->getToken(), 'https://api.aweber.com/1.0/accounts');
		$accountUrl = $accounts[0]['self_link'];
		$listsUrl = $accounts[0]['lists_collection_link'];
		$lists = getCollection($client, $accessToken, $listsUrl);
		// $html .= '<pre>';
		// $html .= print_r($lists,true);
		// $html .= '</pre>';
		if(count($lists) > 0){
			$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='aweber'");
			$list_selected = $list_selected->list;
			if($list_selected){
				$disable_drop = 'disabled';
			}else{
				$disable_drop = '';
			}
			$html .= '<select style="width: 100%;margin: 0;" name="aweber_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
			foreach($lists as $list){
				if($list_selected == $list['id']){
					$aw_selected = 'selected';
				}else{
					$aw_selected = '';
				}
				$html .= '<pre>';
				$html .= print_r($list,true);
				$html .= '</pre>';
				$html .= '<option value="'.$list['id'].'" '.$aw_selected.'>'.$list['name'].'</option>';
			}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
				if($list_selected){
					$html .= '<button id="aweber_remove_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
				}else{
					$html .= '<button id="aweber_save_list" class="btn_green button btn" style="color:#FFFFFF;">Save Aweber List</button>';
				}
			$html .= '</div>';
		}
		
	}
	///////////////// Aweber /////////////////
	///////////////// GetResponse /////////////////
	$gr_ifconnected = get_user_meta(get_current_user_id(),'_ccv3_getresponse_apikey',true);
	if($gr_ifconnected){
		$html .= '<h6>GetResponse</h6>';
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.getresponse.com/v3/accounts');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


		$headers = array();
		$headers[] = 'X-Auth-Token: api-key '.$gr_ifconnected;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		
		$response = json_decode($result);
		if($response->accountId){
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.getresponse.com/v3/campaigns');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


			$headers = array();
			$headers[] = 'X-Auth-Token: api-key '.$gr_ifconnected;
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$result = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			if(count(json_decode($result)) > 0){
				// $list_selected = get_post_meta($_POST['post_id'],'gr_ar_integration',true);
				$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='get_response'");
				$list_selected = $list_selected->list;
				if($list_selected){
					$disable_drop = 'disabled';
				}else{
					$disable_drop = '';
				}
				$html .= '<select style="width: 100%;margin: 0;" name="gr_listid" '.$disable_drop.'>';
					$html .= "<option value='0'>Choose List</option>";
				foreach(json_decode($result) as $campaigns){
					if($list_selected == $campaigns->campaignId){
						$gr_selected = 'selected';
					}else{
						$gr_selected = '';
					}
					$html .= '<option value="'.$campaigns->campaignId.'" '.$gr_selected.'>'.$campaigns->name.'</option>';
				}
				$html .= '</select>';
				$html .= '<div style="text-align: right;margin: 10px 0;">';
					if($list_selected){
						$html .= '<button id="getresponse_remove_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
					}else{
						$html .= '<button id="getresponse_save_list" class="btn_green button btn" style="color:#FFFFFF;">Save GetResponse List</button>';
					}
				$html .= '</div>';
			}else{
				$html .= '<div style="text-align: center;margin: 10px 0;">';
					$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-getresponse/">GetResponse Account not connected!</a></p>';
				$html .= '</div>';
			}
		}
		
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-getresponse/">GetResponse Account not connected!</a></p>';
		$html .= '</div>';
	}
	///////////////// GetResponse /////////////////
	///////////////// MailChimp /////////////////
	$mc_ifconnected = get_user_meta(get_current_user_id(),'_ccv3_mailchimp_apikey',true);
	if($mc_ifconnected){
		$html .= '<h6>MailChimp</h6>';
		$api_key = $mc_ifconnected;
		if($api_key){
			$data = array('fields' => 'lists',);
			$url = 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/';
			$result = json_decode( mailchimp_curl_connect( $url, 'GET', $api_key, $data) );
			if( !empty($result->lists) ) {
				// $list_selected = get_post_meta($_POST['post_id'],'mc_ar_integration',true);
				$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='mailchimp'");
				$list_selected = $list_selected->list;
				if($list_selected){
					$disable_drop = 'disabled';
				}else{
					$disable_drop = '';
				}
				$html .= '<select style="width: 100%;margin: 0;" name="mc_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach( $result->lists as $list ){
					if($list_selected == $list->id){
						$mc_selected = 'selected';
					}else{
						$mc_selected = '';
					}
					$html .= '<option value="' . $list->id . '"  '.$mc_selected.'>' . $list->name . ' (' . $list->stats->member_count . ')</option>';
				}
				$html .= '</select>';
				$html .= '<div style="text-align: right;margin: 10px 0;">';
					if($list_selected){
						$html .= '<button id="mailchimp_remove_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
					}else{
						$html .= '<button id="mailchimp_save_list" class="btn_green button btn" style="color:#FFFFFF;">Save MailChimp List</button>';
					}
				$html .= '</div>';
			}elseif ( is_int( $result->status ) ) {
				$html .= '<div style="text-align: center;margin: 10px 0;">';
					$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-mailchimp/">MailChimp Account not connected!</a></p>';
				$html .= '</div>';
			}else{
				$html .= '<div style="text-align: center;margin: 10px 0;">';
					$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-mailchimp/">MailChimp Account not connected!</a></p>';
				$html .= '</div>';
			}
		}else{
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-mailchimp/">MailChimp Account not connected!</a></p>';
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-mailchimp/">MailChimp Account not connected!</a></p>';
		$html .= '</div>';
	}
	///////////////// MailChimp /////////////////
	///////////////// SendLane /////////////////
	$apikey = get_user_meta(get_current_user_id(),'_ccv3_sendlane_apikey',true);
	$apihashkey = get_user_meta(get_current_user_id(),'_ccv3_sendlane_apihashkey',true);
	if($apikey && $apihashkey){
		$curl = curl_init();
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://sendlane.com/api/v1/lists",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('api' => $apikey,'hash' => $appihashkey),
		));

		$response = curl_exec($curl);

		curl_close($curl);
		$response = json_decode($response);
		//print_r($response);
		if(count($response->error) <= 0){
			$html .= '<h6>SendLane</h6>';
			// $list_selected = get_post_meta($_POST['post_id'],'sl_ar_integration',true);
			$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='sendlane'");
			$list_selected = $list_selected->list;
			if($list_selected){
				$disable_drop = 'disabled';
			}else{
				$disable_drop = '';
			}
			$html .= '<select style="width: 100%;margin: 0;" name="sendlane_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
			foreach($response as $rs){
				if($list_selected == $rs->list_id){
					$sl_selected = 'selected';
				}else{
					$sl_selected = '';
				}
				$html .= '<option data="'.$rs->list_key.'" value="'.$rs->list_id.'" '.$sl_selected.'>'.$rs->list_name.'</option>';
			}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
				if($list_selected){
					$html .= '<button id="sendlane_remove_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
				}else{
					$html .= '<button id="sendlane_save_list" class="btn_green button btn" style="color:#FFFFFF;">Save SendLane List</button>';
				}
			$html .= '</div>';
		}else{
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-sendlane/">SendLane Account not connected!</a></p>';
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-sendlane/">SendLane Account not connected!</a></p>';
		$html .= '</div>';
	}
	///////////////// SendLane /////////////////
	///////////////// Convertkit /////////////////
	$ck_ifconnected = get_user_meta(get_current_user_id(),'_ccv3_convertkit_apikey',true);
	if($ck_ifconnected){
		$html .= '<h6>ConvertKit</h6>';
		$api_key = $ck_ifconnected;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.convertkit.com/v3/tags?api_key='.$api_key);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result = curl_exec($ch);
		$response = json_decode($result);
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='convertKit'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
		if($response->tags){
			$html .= '<select style="width: 100%;margin: 0;" name="convertkit_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach($response as $key=>$value){
					foreach ($value as $k => $v){
						if($list_selected == $v->id){
							$sl_selected = 'selected';
						}else{
							$sl_selected = '';
						}	
						$html .= '<option data="'.$v->id.'" value="'.$v->id.'" '.$sl_selected.'>'.$v->name.'</option>';
					}
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
			if($list_selected){
				$html .= '<button id="remove_convertkit_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
			}else{
				$html .= '<button id="save_convertkit_list" class="btn_green button btn" style="color:#FFFFFF;">Save ConvertKit List</button>';
			}
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p style="text-align">ConvertKit Account not connected!</p>';
		$html .= '</div>';		
	}
	///////////////// Convertkit /////////////////
	///////////////// Drip /////////////////
	
	$dp_ifconnected = get_user_meta(get_current_user_id(),'_ccv3_drip_apikey',true);
	if($dp_ifconnected){
		$html .= '<h6>Drip</h6>';
		$api_key = $dp_ifconnected;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.getdrip.com/v2/accounts');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

		curl_setopt($ch, CURLOPT_USERPWD, $api_key . ':' . '');

		$headers = array();
		$headers[] = 'User-Agent: Your App Name (www.yourapp.com)';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		
		$response = json_decode($result);
		if($response->accounts){
			$auth_id = $response->accounts[0]->id;
		}
		
		$ch1 = curl_init();

		curl_setopt($ch1, CURLOPT_URL, 'https://api.getdrip.com/v2/'.$auth_id.'/campaigns');
		curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, 'GET');

		curl_setopt($ch1, CURLOPT_USERPWD, $api_key . ':' . '');

		$headers1 = array();
		$headers1[] = 'User-Agent: Your App Name (www.yourapp.com)';
		curl_setopt($ch1, CURLOPT_HTTPHEADER, $headers1);

		$result1 = curl_exec($ch1);
		if (curl_errno($ch1)) {
			echo 'Error:' . curl_error($ch1);
		}
		curl_close($ch1);
		$response1 = json_decode($result1);
		
		
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='drip'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
		if($response){
			$html .= '<select style="width: 100%;margin: 0;" name="drip_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach($response1 as $drip_key => $drip_value){
					foreach ($drip_value as $k => $v1){
						if($v1->id){
							if($list_selected == $v1->id && $v1->id != ''){
								$sl_selected = 'selected';
							}else{
								$sl_selected = '';
							}	
							$html .= '<p>'.$list_selected.'</p>';
							$html .= '<option data="'.$v1->id.'" value="'.$v1->id.'" '.$sl_selected.'>'.$v1->name.'</option>';
						}
					}
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
			if($list_selected){
				$html .= '<button id="remove_drip_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
			}else{
				$html .= '<button id="save_drip_list" class="btn_green button btn" style="color:#FFFFFF;">Save Drip List</button>';
			}
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p style="text-align">Drip Account not connected!</p>';
		$html .= '</div>';		
	}
	///////////////// Drip /////////////////
	
	///////////////// Mailerlite /////////////////
	$ml_ifconnected = get_user_meta(get_current_user_id(),'_ccv3_mailerlite_apikey',true);
	if($ml_ifconnected){
		$html .= '<h6>Mailerlite</h6>';
		$api_key = $ml_ifconnected;
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'https://api.mailerlite.com/api/v2/groups/');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');


		$headers = array();
		$headers[] = 'X-Mailerlite-Apikey: '.$api_key.'';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		curl_close($ch);
		$response = json_decode($result);
		
		
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='mailerlite'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
		if($response){
			$html .= '<select style="width: 100%;margin: 0;" name="mailerlite_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach($response as $ml_key => $v1){
					if($v1->id){
						if($list_selected == $v1->id && $v1->id != ''){
							$sl_selected = 'selected';
						}else{
							$sl_selected = '';
						}	
						$html .= '<p>'.$list_selected.'</p>';
						$html .= '<option data="'.$v1->id.'" value="'.$v1->id.'" '.$sl_selected.'>'.$v1->name.'</option>';
					}					
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
			if($list_selected){
				$html .= '<button id="remove_mailerlite_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
			}else{
				$html .= '<button id="save_mailerlite_list" class="btn_green button btn" style="color:#FFFFFF;">Save Mailerlite List</button>';
			}
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p style="text-align">Mailerlite Account not connected!</p>';
		$html .= '</div>';		
	}
	///////////////// Mailerlite /////////////////
	
	///////////////// Sendiio /////////////////
	$se_sendiio_api = get_user_meta(get_current_user_id(),'_ccv3_sendiio_apikey',true);	
	$se_sendiio_token = get_user_meta(get_current_user_id(),'_ccv3_sendiio_apitoken',true);	
	$se_sendiio_user = get_user_meta(get_current_user_id(),'_ccv3_sendiio_user_id',true);	
	if($se_sendiio_api && $se_sendiio_token && $se_sendiio_user){
		$html .= '<h6>Sendiio</h6>';
		$api_token = $se_sendiio_token;
		$api_key = $se_sendiio_api;
		$curl = curl_init();

		curl_setopt_array($curl, array(
		 CURLOPT_URL => 'https://sendiio.com/api/v1/lists/email',
		 CURLOPT_RETURNTRANSFER => true,
		 CURLOPT_ENCODING => '',
		 CURLOPT_MAXREDIRS => 10,
		 CURLOPT_TIMEOUT => 0,
		 CURLOPT_FOLLOWLOCATION => true,
		 CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		 CURLOPT_CUSTOMREQUEST => 'GET',
		 CURLOPT_HTTPHEADER => array(
		   'token: '.$api_token,
		   'secret: '.$api_key 
		 ),
		));
		
		$result = curl_exec($curl);
		$response = json_decode($result);
		
		
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='sendiio'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
		if($response){
			$html .= '<select style="width: 100%;margin: 0;" name="sendiio_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach($response as $se_key => $v1){
					foreach($v1 as $key => $v){
						foreach($v as $k => $sv){
						if($sv->id){
							if($list_selected == $sv->id && $sv->id != ''){
								$sl_selected = 'selected';
							}else{
								$sl_selected = '';
							}	
							$html .= '<p>'.$list_selected.'</p>';
							$html .= '<option data="'.$sv->id.'" value="'.$sv->id.'" '.$sl_selected.'>'.$sv->name.'</option>';
						}					
					}
				  }
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
			if($list_selected){
				$html .= '<button id="remove_sendiio_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
			}else{
				$html .= '<button id="save_sendiio_list" class="btn_green button btn" style="color:#FFFFFF;">Save Sendiio List</button>';
			}
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p style="text-align">Sendiio Account not connected!</p>';
		$html .= '</div>';		
	} 
	///////////////// Sendiio /////////////////
	
	///////////////// MailSqaud /////////////////
	$mailsqaud_apitoken = get_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apitoken',true);	
	$mailsqaud_apilogin = get_user_meta(get_current_user_id(),'_ccv3_mailsqaud_apilogin',true);	
	if($mailsqaud_apitoken && $mailsqaud_apilogin){
		$html .= '<h6>MailSqaud</h6>';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,'https://mailsqaud.com/API/getLists');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array( 'APIToken: '.$mailsqaud_apitoken, 'Login: '.$mailsqaud_apilogin));
		$data = curl_exec($ch);
		if (curl_errno($ch)) { 
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p style="text-align">MailSqaud Account not connected!</p>';
			$html .= '</div>';
			exit;
		} else {
		   $response = json_decode($data);
		}
		curl_close($ch);
		
		
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='mailsqaud'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
		if($response->response){
			$html .= '<select style="width: 100%;margin: 0;" name="mailsqaud_listid" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose List</option>";
				foreach($response->response as $res){
					if($list_selected == $res->list_id){
						$ms_selected = 'selected';
					}else{
						$ms_selected = '';
					}
					$html .= "<option value='".$res->list_id."' ".$ms_selected.">".$res->list_name."</option>";
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
			if($list_selected){
				$html .= '<button id="remove_mailsqaud_list" class="btn_red button btn" style="color:#FFFFFF;">Remove Selection</button>';
			}else{
				$html .= '<button id="save_mailsqaud_list" class="btn_green button btn" style="color:#FFFFFF;">Save MailSqaud List</button>';
			}
			$html .= '</div>';
		}
	}else{
		$html .= '<div style="text-align: center;margin: 10px 0;">';
			$html .= '<p style="text-align">Sendiio Account not connected!</p>';
		$html .= '</div>';		
	} 
	///////////////// MailSqaud /////////////////
	
	///////////////// Zapier /////////////////
	$html .= '<h6>Zapier Integration <span style="font-size: 12px;"><a target="_BLANK" href="'.site_url().'/connecting-your-autoresponder-using-zapier/">(Setup Docs)</a></span></h6>';
		// $list_selected = get_post_meta($_POST['post_id'],'zapier_ar_integration',true);
		$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='zapier'");
		$list_selected = $list_selected->list;
		if($list_selected){
			$disable_drop = 'disabled';
		}else{
			$disable_drop = '';
		}
	$html .= '<input type="text" name="zapier_webhook" style="width: 100%;margin: 0;" value="'.$list_selected.'" placeholder="Zapier Webhook URL" '.$disable_drop.' />';
	$html .= '<div style="text-align: right;margin: 10px 0;">';
		if($list_selected){
			$html .= '<button id="remove_zapier_webhook" class="btn_red button btn" style="color:#FFFFFF;">Remove Zapier Webhook</button>';
		}else{
			$html .= '<button id="save_zapier_webhook" class="btn_green button btn" style="color:#FFFFFF;">Save Zapier Webhook</button>';
		}
	$html .= '</div>';
	///////////////// Zapier /////////////////
	
	$author_id = get_post_field ('post_author', $_POST['post_id']);
	if($author_id == get_current_user_id()){
		///////////////// Go To Webinar /////////////////
		$html .= '<div style="text-align: center;margin: 20px 0 0 0;border-top: 3px solid #AAA;padding-top: 20px;">';
		$html .= '<h5 style="text-align:left;">WEBINAR SOFTWARE</h5>';
		$gotowebinar = get_user_meta(get_current_user_id(),'fm_gotowebinar',true);
		if($gotowebinar->access_token){
			$gotowebinar = go_to_webinar_refresh_token(get_current_user_id());
			// $list_selected = get_post_meta($_POST['post_id'],'webinar_ar_integration',true);
			$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='webinar'");
			$list_selected = $list_selected->list;
			$html .= '<h6>GoToWebinar</h6>';
			$curl = curl_init();
			$account_key = $gotowebinar->account_key;
			$start_date = str_replace('+00:00', 'Z', gmdate('c', gmmktime()));
			$end_year_5 = date('Y', strtotime('+5 years'));
			$url = "https://api.getgo.com/G2W/rest/v2/accounts/{$account_key}/webinars?page=0&size=199&fromTime=$start_date&toTime=$end_year_5-01-01T23:59:59Z";
			/*$url_new = "https://api.getgo.com/G2W/rest/v2/recordingassets/search";
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL,$url_new);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,
						"accountKey={$account_key}");
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				"Authorization: Bearer ".$gotowebinar->access_token,
				"Content-Type: application/x-www-form-urlencoded",
				"Accept: application/json"
			  ));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec($ch);

			curl_close ($ch);

			$html .= print_r($server_output,true);*/
			
			
			curl_setopt_array($curl, array(
			  CURLOPT_URL => $url,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 0,
			  CURLOPT_FOLLOWLOCATION => true,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_HTTPHEADER => array(
				"Authorization: Bearer ".$gotowebinar->access_token,
				"Content-Type: application/x-www-form-urlencoded",
				"Accept: application/json"
			  ),
			));
			$response = curl_exec($curl);
			curl_close($curl);
			$res = json_decode($response);
			if($list_selected){
				$disable_drop = 'disabled';
			}else{
				$disable_drop = '';
			}
			$html .= '<select style="width: 100%;margin: 0;" name="webinar_id" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose Webinar</option>";
			foreach($res->_embedded->webinars as $webinar){
				if($list_selected == $webinar->webinarKey){
					$webinar_selected = 'selected';
				}else{
					$webinar_selected = '';
				}
				$html .= '<option value="'.$webinar->webinarKey.'" '.$webinar_selected.'>'.$webinar->subject.'</option>';
			}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
				if($list_selected){
					$html .= '<button id="gotowebinar_remove_webinar" class="btn_red button btn" style="color:#FFFFFF;">Remove Webinar</button>';
				}else{
					$html .= '<button id="gotowebinar_save_webinar" class="btn_green button btn" style="color:#FFFFFF;">Save Webinar</button>';
				}
			$html .= '</div>';
		}else{
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-webinar/">GoToWebinar Account not connected!</a></p>';
			$html .= '</div>';
		}
		$html .= '</div>';
		$everwebinar_key = get_user_meta(get_current_user_id(),'fm_everwebinar_apikey',true);
		if($everwebinar_key){
			// $everwebinarid = get_post_meta($_POST['post_id'],'everwebinar_ar_integration',true);
			$list_selected = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_integrations` WHERE funnel_id='".$_POST['post_id']."' AND user_id='".get_current_user_id()."' AND type='everwebinar'");
			$everwebinarid = $list_selected->list;
			if($everwebinarid){
				$disable_drop = 'disabled';
			}else{
				$disable_drop = '';
			}
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, 'https://api.webinarjam.com/everwebinar/webinars');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, "api_key=$everwebinar_key");

			$headers = array();
			$headers[] = 'Content-Type: application/x-www-form-urlencoded';
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			$resultw = curl_exec($ch);
			if (curl_errno($ch)) {
				echo 'Error:' . curl_error($ch);
			}
			curl_close($ch);
			// echo '<pre>';
			$ever_webinars = json_decode($resultw);
			// echo '</pre>';
			$html .= '<select style="width: 100%;margin: 0;" name="everwebinar_id" '.$disable_drop.'>';
				$html .= "<option value='0'>Choose Webinar</option>";
				foreach($ever_webinars->webinars as $webinar){
					// print_r($webinar);
					if($everwebinarid == $webinar->webinar_id){
						$webinar_selected = 'selected';
					}else{
						$webinar_selected = '';
					}
					$html .= '<option value="'.$webinar->webinar_id.'" '.$webinar_selected.'>'.$webinar->name.'</option>';
				}
			$html .= '</select>';
			$html .= '<div style="text-align: right;margin: 10px 0;">';
				if($everwebinarid){
					$html .= '<button id="everwebinar_remove_webinar" class="btn_red button btn" style="color:#FFFFFF;">Remove Webinar</button>';
				}else{
					$html .= '<button id="everwebinar_save_webinar" class="btn_green button btn" style="color:#FFFFFF;">Save Webinar</button>';
				}
			$html .= '</div>';
		}else{
			$html .= '<div style="text-align: center;margin: 10px 0;">';
				$html .= '<p><a target="_BLANK" href="'.site_url().'/wiki/connecting-webinar/">EverWebinar Account not connected!</a></p>';
			$html .= '</div>';
		}
		///////////////// Go To Webinar /////////////////
	}
	$html .= '<div style="text-align: center;margin: 20px 0 0 0;border-top: 3px solid #AAA;padding-top: 20px;">';
		$html .= '<h5 style="text-align:left;">Third Party Scripts</h5>';
		$html .= '<a href="javascript:void(0);" data="'.$_POST['post_id'].'" style="color: white;text-decoration: none;font-weight: 600;" class="btn btn_add_scripts btn_green">Add/Edit Scripts</a>';
	$html .= '</div>';
	echo $html;
	exit;
}
function save_funnel_integration(){
	global $wpdb;
	$type = $_POST['type'];
	$list = $_POST['list'];
	$post_id = $_POST['post_id'];
	$user_id = get_current_user_id();
	/*if($type == 'aweber'){
		$key = 'aw_ar_integration';
	}else if($type == 'get_response'){
		$key = 'gr_ar_integration';
	}else if($type == 'mailchimp'){
		$key = 'mc_ar_integration';
	}else if($type == 'sendlane'){
		$key = 'sl_ar_integration';
	}else if($type == 'zapier'){
		$key = 'zapier_ar_integration';
	}else if($type == 'webinar'){
		$key = 'webinar_ar_integration';
	}else if($type == 'everwebinar'){
		$key = 'everwebinar_ar_integration';
	}*/
	if($list && $post_id){
		if($wpdb->query("INSERT INTO `wp_setc_funnel_integrations` (funnel_id,user_id,list,type) VALUES ('$post_id','$user_id','$list','$type')")){
			echo '1';
		}
	}	
	exit;
}
function remove_funnel_integration(){
	global $wpdb;
	$type = $_POST['type'];
	$post_id = $_POST['post_id'];
	$user_id = get_current_user_id();
	/*if($type == 'aweber'){
		$key = 'aw_ar_integration';
	}else if($type == 'get_response'){
		$key = 'gr_ar_integration';
	}else if($type == 'mailchimp'){
		$key = 'mc_ar_integration';
	}else if($type == 'sendlane'){
		$key = 'sl_ar_integration';
	}else if($type == 'zapier'){
		$key = 'zapier_ar_integration';
	}else if($type == 'webinar'){
		$key = 'webinar_ar_integration';
	}else if($type == 'everwebinar'){
		$key = 'everwebinar_ar_integration';
	}*/
	if($post_id){
		if($wpdb->query("DELETE FROM `wp_setc_funnel_integrations` WHERE funnel_id='$post_id' AND user_id='$user_id' AND type='$type'")){
			echo '1';
		}
		/*if(delete_post_meta($post_id,$key)){
			echo '1';
		}*/
	}
	exit;
}
function show_followup_email(){
	$content = $_POST['content'];
	$html = apply_filters( 'the_content', $content );
	echo stripslashes($html);
	exit;
}
/*add_action('wp_ajax_My_Action_Name', function(){
    wp_editor($_POST['default_text'], 'My_TextAreaID_22',$settings = array( 'tinymce'=>true, 'textarea_name'=>'name77', 'wpautop' =>false,   'media_buttons' => true ,   'teeny' => false, 'quicktags'=>true,));    exit;
});*/
function add_followup_emails_to_mailserver(){
	global $wpdb, $automation_array;
	$post_id = $_POST['post_id'];
	$emails = $wpdb->get_results("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id=$post_id ORDER BY type ASC");
	$automation = get_post_meta($post_id,'acelle_automation_uid',true);
	$automation = maybe_unserialize($automation);
	$list = get_post_meta($post_id,'acelle_list_uid',true);
	
	// $maildb = new wpdb("funnelma_mailu","RX.N1d~jwveA","funnelma_mailserver","localhost");
	$maildb = new wpdb("root","Secutv643!!sf##~~6rfa","mail_server","localhost");
	$ms_automation = $maildb->get_row("SELECT * FROM `fmmsqautomation2s` WHERE uid='".$automation->uid."' ");
	$ms_automation_id = $ms_automation->id;
	$ms_list = $maildb->get_row("SELECT * FROM `fmmsqmail_lists` WHERE uid='".$list."' ");
	
	$default_plain = " Is this email not displayed properly? Open the web viewPaste email content here Copyright © {CONTACT_NAME}, All rights reserved.{LIST_NAME}{CONTACT_ADDRESS_1},&nbsp;{CONTACT_ADDRESS_2},&nbsp;{CONTACT_CITY},&nbsp;{CONTACT_STATE}{CONTACT_COUNTRY}Want to change how you receive these emails?You can update your preferences or unsubscribe from this list ";

	$default_content_start = '<html lang="en" data-bubblesppbfhhgcdehhbjigifndnfmcihegokbbprevent-events-conflict-installed="true"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><meta name="description" content=""><meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors"><meta name="generator" content="AcelleSystemLayouts"><!-- Bootstrap core CSS --><link href="css/bootstrap.min.css" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link rel="stylesheet" type="text/css" id="u0" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/skin.min.css"><link rel="stylesheet" type="text/css" id="u1" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/content.inline.min.css"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><link href="https://fonts.googleapis.com/icon?family=Material+Icons+Outlined" rel="stylesheet"><link rel="stylesheet" type="text/css" id="u0" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/skin.min.css"><link rel="stylesheet" type="text/css" id="u1" href="https://mail.funnelmates.com/builder/iframe/tinymce/skins/ui/oxide-dark/content.inline.min.css"></head><body class="vsc-initialized" spellcheck="false"><main role="main"><div class="py-5 bg-light" id="mce_15" style="position: relative; line-height: 1;" spellcheck="false"><div class="container">';
	$default_content_end = '</div></div><div class="py-5 bg-light" id="mce_15" style="position: relative; line-height: 1;" spellcheck="false"><div class="container"><!-- <div class="text-center mb-4"><div><img src="image/logo.png" width="350px" /></div></div> --><p class="" id="mce_16" style="position: relative; line-height: 1;" spellcheck="false"><em class="">Copyright © {CONTACT_NAME}, All rights reserved.<br><br><br>{LIST_NAME}<br>{CONTACT_ADDRESS_1},&nbsp;{CONTACT_ADDRESS_2},&nbsp;{CONTACT_CITY},&nbsp;{CONTACT_STATE}<br>{CONTACT_COUNTRY}</em><br><br>Want to change how you receive these emails?<br>You can <a href="{UPDATE_PROFILE_URL}">update your preferences</a> or <a href="{UNSUBSCRIBE_URL}">unsubscribe from this list</a></p></div></div></main><footer class="text-muted py-5" id="mce_8" style="position: relative;" spellcheck="false"><div class="container">&nbsp;</div></footer></body></html>';
	
	$random_first = rand(0,999999999);
	$random_first = $random_first.$post_id;
	$first = new stdClass();
	$first->title = "New contact subscribes to mail list";
	$first->id = "trigger";
	$first->type = "ElementTrigger";
	$first->options = new stdClass();
	$first->options->key = "welcome-new-subscriber";
	$first->options->type = "list-subscription";
	$first->options->init = "true";
	$first->child = $random_first;
	$automation_array[] = $first;
	
	$child_wait = $random_first;
	$previousValue = 0;
	$maildb->query("DELETE FROM `fmmsqemails` WHERE automation2_id=$ms_automation_id");
	foreach($emails as $key=>$e){
		$content = addslashes($default_content_start.$e->content.$default_content_end);
		$content = apply_filters( 'the_content', $content );
		$email_uid = rand(0,999999999);
		$email_uid = $email_uid.$post_id;
		//".$ms_list->from_email."
		$maildb->query("INSERT INTO fmmsqemails (`uid`,`automation2_id`,`subject`,`from`,`from_name`,`reply_to`,`content`,`created_at`,`updated_at`,`sign_dkim`,`track_open`,`track_click`,`action_id`,`plain`) VALUES ('".$email_uid."','".$ms_automation_id."','".addslashes($e->subject)."','support@funnelmates.com','".$ms_list->from_name."','support@funnelmates.com','".$content."',CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,1,1,0,'".$random_first."','".$default_plain."');");
		if($previousValue != 0){
			$wait_days = $e->type - $previousValue;
			$child_wait = insert_wait_mail($child_wait, $wait_days, $post_id);
		}
		$child_wait = insert_email_mail($email_uid, $child_wait, $post_id);
		$previousValue = $e->type;
	}
	$end_automation_array = end($automation_array);
	$end_automation_array->child = null;
	// print_r($automation_array);exit;
	$data = json_encode($automation_array);
	if($maildb->query("UPDATE `fmmsqautomation2s` SET status='active', data='".$data."', updated_at=CURRENT_TIMESTAMP WHERE id='".$ms_automation_id."' ")){
		echo '1';
	}
	exit;
}
function get_review_data_admin(){
	global $wpdb;
	$id = $_GET['data'];
	if($id){
		$review_exists = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_funnel_review` WHERE funnel_id='".$id."'");
		// print_r($review_exists);
		if(count($review_exists) > 0){
			echo '<table class="widefat striped">';
				echo '<tr>';
					echo '<td>User</td>';
					echo '<td>Value For Money</td>';
					echo '<td>Well Designed</td>';
					echo '<td>Quality of Content</td>';
					echo '<td>Review</td>';
				echo '</tr>';
			foreach($review_exists as $review){
				echo '<tr>';
					echo '<td>'.$review->user_id.'</td>';
					echo '<td>'.$review->valueformoney.'</td>';
					echo '<td>'.$review->welldesigned.'</td>';
					echo '<td>'.$review->qualityofcontent.'</td>';
					echo '<td>'.$review->text.'</td>';
				echo '</tr>';
			}
			echo '</table>';
		}else{
			echo 'No Review Found..';
		}
	}else{
		echo 'Somehting went wrong..';
	}
	exit;
}
function get_affiliaterequests_admin(){
	global $wpdb;
	$post_id = $_GET['data'];
	if($post_id){
		$html = '<div style="text-align:left;padding:0 15px;">';
		$html .= '<h4>\''.get_the_title($post_id).'\' FUNNEL DESCRIPTION</h4>';
		$funnel = $wpdb->get_row("SELECT description FROM `wp_fm_marketplace_items` WHERE post_id='$post_id'");
		$html .= '<h4 align="center">Affiliate Requests</h4>';
		$html .= '<p>Below you\'ll find the details for all of products promoted in the <strong>'.get_the_title($post_id).'</strong> funnel.</p>';
		$query_jvzoo = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'jvzoo'  ");
		if(count($query_jvzoo) > 0){
			$html .= '<h5>JVZoo Affiliate Products</h5>';
			foreach($query_jvzoo as $qjvz){
				$html .= '<div class="jvzoo_affiliate_links">';
					$afflink_request = 'https://funnelmates.com/jvz/'.get_current_user_id().'/'.$qjvz->link_identifier.'/request';
					$afflink_sales = 'https://funnelmates.com/jvz/'.get_current_user_id().'/'.$qjvz->link_identifier.'/sales';
					$html .= '<p style="text-align:center;"><a href="'.$afflink_request.'" target="_BLANK">Click to request to promote <strong>'.$qjvz->name.'</strong></a> (<a href="'.$afflink_sales.'" target="_BLANK">Sales Page Preview</a>)';
					$html .= '</p>';
				$html .= '</div>';
			}
		}
		$query_wp = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'warriorplus'  ");
		if(count($query_wp) > 0){
			$html .= '<h5>WarriorPlus Affiliate Products</h5>';
			foreach($query_wp as $qwp){
				$html .= '<div class="warriorplus_affiliate_links">';
					$afflink_request = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/request';
					$afflink_sales = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/sales';
					$html .= '<p style="text-align:center;"><a href="'.$afflink_request.'" target="_BLANK">Click to request to promote <strong>'.$qwp->name.'</strong></a> (<a href="'.$afflink_sales.'" target="_BLANK">Sales Page Preview</a>)</p>';
				$html .= '</div>';
			}
		}
		$query_cb = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'clickbank'  ");
		if(count($query_cb) > 0){
			$html .= '<h5>ClickBank Affiliate Products</h5>';
			$html .= '<p>The following Clickbank offers are included in this funnel, which do not require you to approval to promote</p>';
			$general_settings = get_user_meta(get_current_user_id(),'_ccv3_general_settings',true);
			$clickbank_id = $general_settings['clickbank_id'];
			if(empty($clickbank_id)){
				$clickbank_id = 'funnelmate';
			}
			foreach($query_cb as $qcb){
				$html .= '<div class="clickbank_affiliate_links">';
					$html .= '<p style="text-align:center;"><a href="http://funnelmate.'.$qcb->aff_link.'.hop.clickbank.net/" target="_BLANK">https://'.$clickbank_id.'.'.$qcb->aff_link.'.hop.clickbank.net/</a></p>';
				$html .= '</div>';
			}
		}
		$query_pks = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'paykickstart'  ");
		if(count($query_pks) > 0){
			$html .= '<h5>PayKickStart Affiliate Products</h5>';
			foreach($query_pks as $qpks){
				$html .= '<div class="paykickstart_affiliate_links">';
					$afflink = 'https://funnelmates.com/pks/'.get_current_user_id().'/'.$qpks->link_identifier.'/';
					$html .= '<p style="text-align:center;"><a href="'.$afflink.'" target="_BLANK">'.$afflink.'</a></p>';
					if($qex->notes){
						$html .= '<p style="margin:0;">Description: '.$qpks->notes.'</p>';
					}
				$html .= '</div>';
			}
		}
		$query_ex = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$post_id." AND aff_network = 'external'  ");
		if(count($query_ex) > 0){
			// external
			$html .= '<h5>External Link</h5>';
			$html .= 'Your <strong>'.get_the_title($post_id).'</strong> funnel contains the following external links. You can customise these links by clicking the \'Website Links\' button and choosing \'Edit My Links\' in the dropdown menu.';
			foreach($query_ex as $qex){
				$html .= '<div class="external_affiliate_links" style="margin:10px;">';
					$html .= '<p style="margin:0;">Link Name: '.$qex->name.'</p>';
					if($qex->notes){
						$html .= '<p style="margin:0;">Description: '.$qex->notes.'</p>';
					}
					$html .= '<p>URL: <a target="_BLANK" href="'.$qex->aff_link.'">'.$qex->aff_link.'</a></p>';
				$html .= '</div>';
			}
		}
		$html .= '</div>';
		echo $html;
		exit;
	}
}
function get_feedback_data_admin(){
	global $wpdb;
	$id = $_GET['data'];
	if($id){
		$requested_feedback = get_post_meta($id,'requested_feedback',true);
		$requested_feedback = json_decode($requested_feedback);
		if($requested_feedback == '' || $requested_feedback == '0'){
			$feedback = '';
		}else{
			$feedback = $requested_feedback;
		}
		echo '<div class="error_boxes" style="text-align: left;padding: 10px;">';
			if(in_array('incomplete_email_sequence',$feedback)){
				$checked_incomplete_email_sequence = 'checked';
			}else{
				$checked_incomplete_email_sequence = '';
			}
			echo '<div><input type="checkbox" name="incomplete_email_sequence" id="incomplete_email_sequence" value="1" '.$checked_incomplete_email_sequence.' /><label for="incomplete_email_sequence">Incomplete Email Sequence</label></div>';
			
			if(in_array('no_download_link',$feedback)){
				$checked_no_download_link = 'checked';
			}else{
				$checked_no_download_link = '';
			}
			echo '<div><input type="checkbox" name="no_download_link" id="no_download_link" value="1" '.$checked_no_download_link.' /><label for="no_download_link">No Download Link</label></div>';
			
			if(in_array('missing_optin_form',$feedback)){
				$checked_missing_optin_form = 'checked';
			}else{
				$checked_missing_optin_form = '';
			}
			echo '<div><input type="checkbox" name="missing_optin_form" id="missing_optin_form" value="1" '.$checked_missing_optin_form.' /><label for="missing_optin_form">Missing Optin Form</label></div>';
			
			if(in_array('no_affiliate_links',$feedback)){
				$checked_no_affiliate_links = 'checked';
			}else{
				$checked_no_affiliate_links = '';
			}
			echo '<div><input type="checkbox" name="no_affiliate_links" id="no_affiliate_links" value="1" '.$checked_no_affiliate_links.' /><label for="no_affiliate_links">No Affiliate Links</label></div>';
			
			if(in_array('broken_affiliate_links',$feedback)){
				$checked_broken_affiliate_links = 'checked';
			}else{
				$checked_broken_affiliate_links = '';
			}
			echo '<div><input type="checkbox" name="broken_affiliate_links" id="broken_affiliate_links" value="1" '.$checked_broken_affiliate_links.' /><label for="broken_affiliate_links">Broken Affiliate Link</label></div>';
			
			if(in_array('page_error',$feedback)){
				$checked_page_error = 'checked';
			}else{
				$checked_page_error = '';
			}
			echo '<div><input type="checkbox" name="page_error" id="page_error" value="1" '.$checked_page_error.' /><label for="page_error">Page Error</label></div>';
			
			if(in_array('add_a_note',$feedback)){
				$checked_add_a_note = 'checked';
				$display_textarea = 'block';
				$content_textarea = $feedback[7];
			}else{
				$display_textarea = 'none';
				$checked_add_a_note = '';
				$content_textarea = '';
			}
			echo '<div><input type="checkbox" name="add_a_note" id="add_a_note" value="1" '.$checked_add_a_note.' /><label for="add_a_note">Add a Note</label></div>';
		echo '</div>';
		echo '<div class="add_a_note_div" style="display:'.$display_textarea.';">';
			echo '<h5>Insert Feedback Text</h5>';
			echo '<textarea id="feedback_textarea_'.$id.'" name="feedback_textarea_'.$id.'" style="width:90%;height:200px;">'.$content_textarea.'</textarea>';
		echo '</div>';
		if($feedback){
			echo '<button type="button" style="width:50%;" class="button button-primary" id="feedback_text_submit_'.$id.'" disabled>Feedback Requested</button>';
		}else{
			echo '<button type="button" style="width:50%;" class="button button-primary" id="feedback_text_submit_'.$id.'" onclick="feedback_text_submit('.$id.')">Request Feedback</button>';
		}
	}else{
		echo 'Something went wrong..';
	}
	exit;
}
function save_requested_feedback(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$user_id = get_post_field ('post_author', $post_id);
	$content = $_POST['content'];
	// print_r(json_decode());exit;
	if($post_id){
		// echo bp_is_active( 'notifications' );exit;
		update_post_meta($post_id,'requested_feedback',maybe_serialize($content));
		// $wpdb->query("INSERT INTO `wp_setc_notices` (content,funnel_id,user_id) VALUES ('requested_feedback','$post_id','$user_id')");
		/*if ( bp_is_active( 'notifications' ) ) {
			$args = array(
				'user_id' => $user_id,
				'item_id' => $post_id,
				'component_name' => 'custom_fm',
				'component_action' => 'requested_feedback',
				'date_notified' => bp_core_current_time(),
				'is_new' => 1, );
			$notification_id = bp_notifications_add_notification( $args );
		}*/
		$array = json_decode(stripslashes($content));
		unset($array[7]);
		// print_r($array);exit;
		foreach($array as $a){
			if($a){
				fm_bp_custom_add_notification($post_id,$a);
			}
		}
		update_post_meta($post_id,'is_funnel_published','feedback_requested');
		echo '1';
	}else{
		echo '0';
	}
	exit;
}
function user_improvising_funnel(){
	$post_id = $_POST['post_id'];
	if($post_id){
		update_post_meta($post_id,'is_funnel_published',0);
		update_post_meta($post_id,'is_user_improvising',1);
		echo '1';
	}
	exit;
}
function cancel_funnel_review(){
	$post_id = $_POST['post_id'];
	if($post_id){
		update_post_meta($post_id,'is_funnel_published',0);
		echo '1';
	}
	exit;
}
function get_promotionaltools_admin(){
	global $wpdb;
	$post_id = $_GET['data'];
	$html .= '<div style="border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;">';
	$get_pt = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."fm_promotional_tools` WHERE post_id='".$post_id."' ORDER BY position ASC ");
	if(count($get_pt) > 0){
		foreach($get_pt as $item){
			if($item->content){
				$html .= '<div style="padding: 15px 10px; margin: 10px; font-size:16px; font-weight:700;box-shadow:0px 0px 5px 0px #425f94;">';
					$html .= '<div class="title" style="text-align:center;">';
						if($item->item_type == 'yt_vid'){
							$html .= 'Video';
						}else{
							$html .= ucfirst(str_replace('_',' ',$item->item_type));
						}
					$html .= '</div>';
					$html .= '<div class="promo_content">';
						if($item->item_type == 'banner'){
							if($item->content){
								$image_url = wp_get_attachment_url( $item->content );
								$preview_html = '<a href="'.site_url().'/f/'.$item->post_id.'/'.get_current_user_id().'/"><img src="'.$image_url.'" /></a>';
								$html .= '<img style="width:100%;" src="'.$image_url.'" class="preview banner_image_disp_'.$item->id.'" style="text-align:center;"/>';
								$html .= '<div class="preview_html_'.$item->id.'">';
									$html .= '<textarea disabled>'.$preview_html.'</textarea>';
								$html .= '</div>';
							}
						}else if($item->item_type == 'email_swipe'){
							$email = maybe_unserialize(base64_decode($item->content));
							$content = stripslashes($email['content']);
							$html .= 'Subject : '.$email['subject'];
							$html .= '<hr>';
							$html .= apply_filters('the_content', $content);
						}else if($item->item_type == 'article'){
							$article = maybe_unserialize(base64_decode($item->content));
							$content = stripslashes($article['content']);
							$html .= apply_filters('the_content', $content);
						}else if($item->item_type == 'misc'){
							$misc = maybe_unserialize(base64_decode($item->content));
							$content = stripslashes($misc['content']);
							$html .= apply_filters('the_content', $content);
						}else if($item->item_type == 'yt_vid'){
							$video = maybe_unserialize(base64_decode($item->content));
							$video_url = generateVideoEmbedUrl($video['url']);
							$desc = stripslashes($video['desc']);
							$html .= '<div style="text-align:center;"><iframe src="'.$video_url.'" width="540" height="310"></iframe></div>';
							$html .= "<br>";
							$html .= '<div style="text-align:center; padding:5px;"><a href="'.$video['url'].'" target="_blank" style="text-decoration:none; cursor:pointer;">'.$video_url.'</a></div>';
							$html .= '<p class="vc_message_box vc_color-warning">Would you like to download this video to use in your marketing? 
							<a href="https://funnelmates.com/wiki/how-to-download-a-youtube-video/"> Click here to see how to do it.</a></p>';
							$html .= "<br>";
							$html .= apply_filters('the_content', $desc);
						}else if($item->item_type == 'twitter'){
							$data = maybe_unserialize(base64_decode($item->content));
							$url = '[landing_page_url id='.$post_id.']';			
							$url = apply_filters('the_content', $url);
							$data1 = strip_shortcodes($data['data']);
							$image1 = $data['image'];
							$image_url = wp_get_attachment_url( $image1 );
							$html .= '<div style="display:flex;">';
								$html .= '<div style="width:25%; padding:10px;">';
									$html .= '<a href="'.$url.'" target="_BLANK"><img style="width:100%;" src="'.$image_url.'" class="preview social_image_disp_'.$item->id.'" /></a>';
								$html .= '</div>';
								$html .= '<div style="width:75%; padding:10px;">';
									$html .= '<div>'.apply_filters('the_content', $data1).'</div>';
									$html .= '<p style="margin-top:10px; text-align:center;"><a style="background-color:#1b95e0; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://twitter.com/intent/tweet?text='.stripslashes($data1).$url.'" target="_blank"><i class="fab fa-twitter"></i> Click Here To Tweet On Twitter</a></p>';
								$html .= '</div>';
							$html .= '</div>';
						}else if($item->item_type == 'fb_ads'){
							$data = maybe_unserialize(base64_decode($item->content));
							$url = '[landing_page_url id='.$post_id.']';			
							$url = apply_filters('the_content', $url);
							$data1 = stripslashes($data['data']);
							$image1 = $data['image'];
							$image_url = wp_get_attachment_url( $image1 ); 
							$author_id = get_post_field( 'post_author',$post_id );
							$fb_url = 'https://funnelmates.com/fb/'. $post_id.'/' . $author_id;
							$html .= '<div style="display:flex;">';
								$html .= '<div style="width:25%; padding:10px;">';
									$html .= '<a href="'.$url.'" target="_BLANK"><img style="width:100%;" src="'.$image_url.'" class="preview social_image_disp_'.$item->id.'" /></a>';
								$html .= '</div>';
								$html .= '<div style="width:75%; padding:10px;">';
									$html .= '<div>'.apply_filters('the_content', $data1).'</div>';
									$html .= '<p style="margin-top:10px; text-align:center;"><a style="background-color:#29487d; color:#FFFFFF; padding:5px 15px; border-radius:10px;" href="https://www.facebook.com/sharer.php?u='.$fb_url.'/" target="_blank"><i class="fab fa-facebook"></i> Click Here To Post On Facebook</a></p>';
								$html .= '</div>';
							$html .= '</div>';
						}else if($item->item_type == 'linkedin'){
							$data = maybe_unserialize(base64_decode($item->content));
							$url = '[landing_page_url id='.$post_id.']';			
							$url = apply_filters('the_content', $url);		
							$data1 = stripslashes($data['data']);
							$image1 = $data['image'];
							$image_url = wp_get_attachment_url( $image1 ); 									
							$author_id = get_post_field( 'post_author',$post_id );
							$linkedin_url = 'https://funnelmates.com/in/'. $post_id.'/' . $author_id;
							$html .= '<div style="display:flex;">';
								$html .= '<div style="width:25%; padding:10px;">';
									$html .= '<a href="'.$url.'" target="_BLANK"><img style="width:100%;" src="'.$image_url.'" class="preview social_image_disp_'.$item->id.'" /></a>';
								$html .= '</div>';
								$html .= '<div style="width:75%; padding:10px;">';
									$html .= '<div>'.apply_filters('the_content', $data1).'</div>';
									$html .= '<p style="margin-top:10px; text-align:center;"><a style="background-color:#0077b5; color:#FFFFFF; padding:5px 15px; border-radius:10px; text-decoration:none;" href="https://www.linkedin.com/shareArticle?mini=true&url='.$linkedin_url.'/" target="_blank"><i class="fab fa-linkedin"></i> Click Here To Post On LinkedIn</a></p>';
								$html .= '</div>';
							$html .= '</div>';
						}
					$html .= '</div>';
				$html .= '</div>';
			}
		}
	}else{
		$html .= '<div style="text-align: center;border: 1px solid #425f94;background-color:#e8f0ff;box-shadow: 0px 0px 10px 0px #000 inset;min-height: 100px;line-height:100px;font-size: 35px;font-weight: 600;text-transform: uppercase;opacity: .5;">No Promotional Tools Available</div>';
	}
	$html .= '</div>';
	echo $html;
	exit;
}
function get_followupemails_admin(){
	global $wpdb;
	$post_id = $_GET['data'];
	$emails = $wpdb->get_results("SELECT * FROM `wp_setc_followup_emails` WHERE funnel_id=$post_id ORDER BY type ASC");
	if(count($emails) > 0){
		$automation = get_post_meta($post_id,'acelle_automation_uid',true);
		$automation = maybe_unserialize($automation);
		$list = get_post_meta($post_id,'acelle_list_uid',true);
		$automation = '<p style="margin-bottom:0;"><span style="background: #CDCDCD;padding: 5px;border-radius: 5px;font-weight: 600;">New contact subscribes to mail list</span><br><i style="border: solid black;border-width: 0 4px 4px 0;display: inline-block;padding: 4px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"></i></p>';
		$child_wait = $random_first;
		$previousValue = 0;
		foreach($emails as $key=>$e){
			$content = addslashes($e->content);
			$email_uid = rand(0,999999999);
			$email_uid = $email_uid.$post_id;
			if($previousValue != 0){
				$wait_days = $e->type - $previousValue;
				$automation .= '<p style="margin:0;"><span style="background: green;color: white;padding: 5px;border-radius: 5px;font-weight: 600;">Wait '.$wait_days.'</span><br><i style="border: solid black;border-width: 0 4px 4px 0;display: inline-block;padding: 4px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"></i></p>';
			}
			//
			/* $automation .= '<p style="margin:0;"><span onclick="view_followupemail_admin('.$e->id.')" style="background: rgb(101, 117, 138);color: white;padding: 5px;border-radius: 5px;cursor: pointer;font-weight: 600;">Email "'.$e->subject.'"</span><br><i style="border: solid black;border-width: 0 4px 4px 0;display: inline-block;padding: 4px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"></i></p>';
			$html = apply_filters( 'the_content', $e->content );
			$automation .= '<div style="display:none;text-align: left;width: 70%;margin: 0 auto;border: 1px solid;padding: 10px;background: #FDFDFD;" id="view_followupemail_'.$e->id.'" style="text-align:center;padding:10px;">'.stripslashes($html).'</div>'; */
			
			$automation .= '<p style="margin:0;"><span onclick="view_followupemail_admin('.$e->id.')" style="background: rgb(101, 117, 138);color: white;padding: 5px;border-radius: 5px;cursor: pointer;font-weight: 600;">Email "'.$e->subject.'"</span><br><i style="border: solid black;border-width: 0 4px 4px 0;display: inline-block;padding: 4px;transform: rotate(45deg);-webkit-transform: rotate(45deg);"></i></p>';
			$html = apply_filters( 'the_content', $e->content );
			$automation .= '<div style="display:none;text-align: left;width: 70%;margin: 0 auto;border: 1px solid;padding: 10px;background: #FDFDFD;" id="view_followupemail_'.$e->id.'" style="text-align:center;padding:10px;">'.stripslashes($html).'</div>';
			$previousValue = $e->type;  
		}
		$automation .= '<button onclick="copy_emails_to_mailfm('.$post_id.')" style="width: 70%;font-size: 18px;font-weight: 600;" class="button button-primary">Add Email Sequence to Mail Server</button>';
		echo $automation;
	}else{
		echo 'No Followup Emails';
	}
	exit;
}
/*function get_single_followupemail(){
	global $wpdb;
	$email_id = $_GET['email_id'];
	$email = $wpdb->get_row("SELECT * FROM `wp_setc_followup_emails` WHERE id=$email_id");
	$html = apply_filters( 'the_content', $email->content );
	echo '<div >'.stripslashes($html).'</div>';
	exit;
}*/

/**********BUDDYPRESS NOTIFICATION*************/
function register_custom_fm_notification( $component_names = array() ) {
    // Force $component_names to be an array
    if ( ! is_array( $component_names ) ) {
        $component_names = array();
    }
    // Add 'custom' component to registered components array
    array_push( $component_names, 'custom_fm' );
    // Return component's with 'custom' appended
    return $component_names;
}
add_filter( 'bp_notifications_get_registered_components', 'register_custom_fm_notification' );
// this gets the saved item id, compiles some data and then displays the notification
function fm_custom_bp_notifications( $action, $item_id, $secondary_item_id, $total_items, $format = 'string' ) {
	if('review_approved' === $action){
		$custom_title = 'Congratulations! Your funnel "'.get_the_title($item_id).'" is now live! Want people to buy it? Read this.';
		$custom_link = site_url().'/wiki/getting-more-funnel-activations/';
		$custom_text = 'Congratulations! Your funnel "'.get_the_title($item_id).'" is now live! Want people to buy it? Read this.';
	}else if('triggered_review' === $action){
		$custom_title = 'Your funnel "'.get_the_title($item_id).'" is under review';
		$custom_link = site_url().'/wiki/fix-your-funnel-is-under-review/';
		$custom_text = 'Your funnel "['.get_the_title($item_id).']" is under review';
	}else if('incomplete_email_sequence' === $action){
		$custom_title = 'Incomplete Email Sequence [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fix-incomplete-email-sequence/';
		$custom_text = 'Incomplete Email Sequence [FIX] ['.get_the_title($item_id).']';
	}else if('no_download_link' === $action){
		$custom_title = 'No Download Link [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fix-no-download-link/';
		$custom_text = 'No Download Link [FIX] ['.get_the_title($item_id).']';
	}else if('missing_optin_form' === $action){
		$custom_title = 'Missing Optin Form [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fix-missing-optin-form/';
		$custom_text = 'Missing Optin Form [FIX] ['.get_the_title($item_id).']';
	}else if('no_affiliate_links' === $action){
		$custom_title = 'No Affiliate Links [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fix-no-affiliate-links/';
		$custom_text = 'No Affiliate Links [FIX] ['.get_the_title($item_id).']';
	}else if('broken_affiliate_links' === $action){
		$custom_title = 'Broken Affiliate Link [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fix-broken-affiliate-links/';
		$custom_text = 'Broken Affiliate Link [FIX] ['.get_the_title($item_id).']';
	}else if('page_error' === $action){
		$custom_title = 'Page Error [FIX] ['.get_the_title($item_id).']';
		$custom_link = site_url().'/wiki/fixing-a-page-error/';
		$custom_text = 'Page Error [FIX] ['.get_the_title($item_id).']';
	}else if('add_a_note' === $action){
		$custom_title = 'You\'ve got feedback from reviewers.. ['.get_the_title($item_id).']';
		$custom_link = site_url().'/funnels/';
		$custom_text = 'You\'ve got feedback from reviewers.. ['.get_the_title($item_id).']';
	}
	if ( 'string' === $format ) {
		$return = apply_filters( 'custom_filter', '<a target="_BLANK" href="' . esc_url( $custom_link ) . '" title="' . esc_attr( $custom_title ) . '">' . esc_html( $custom_text ) . '</a>', $custom_text, $custom_link );
	} else {
		$return = apply_filters( 'custom_filter', array(
			'text' => $custom_text,
			'link' => $custom_link
		), $custom_link, (int) $total_items, $custom_text, $custom_title );
	}
	return $return;
}
add_filter( 'bp_notifications_get_notifications_for_user', 'fm_custom_bp_notifications', 10, 5 );
function fm_bp_custom_add_notification( $funnel_id, $type ) {
    $user_id = get_post_field ('post_author', $funnel_id);
    bp_notifications_add_notification( array(
        'user_id'           => $user_id,
        'item_id'           => $funnel_id,
        'component_name'    => 'custom_fm',
        'component_action'  => $type,
        'date_notified'     => bp_core_current_time(),
        'is_new'            => 1,
    ) );
}
/**********BUDDYPRESS NOTIFICATION*************/
function approve_funnel_final(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	delete_post_meta($post_id,'requested_feedback');
	delete_post_meta($post_id,'review_date');
	delete_post_meta($post_id,'is_user_improvising');
	update_post_meta($post_id,'is_funnel_published','1');
	fm_bp_custom_add_notification($post_id,'review_approved');
	echo '1';
	exit;
}
function show_funnel_clicks_and_coversions(){
	global $wpdb;
	$post_id = $_POST['post_id'];
	$c_id = get_current_user_id();
	//$tracking_count = $wpdb->get_results("SELECT DISTINCT(trackingid) FROM `wp_setc_view_counter` WHERE pageid='{$post_id}' and refresh_delete not in(1)");
	$tracking_count = $wpdb->get_results("SELECT DISTINCT(trackingid) FROM `wp_setc_view_counter` WHERE pageid='{$post_id}' and (refresh_delete = '0' OR refresh_delete = '2') and ccid = $c_id");
	$html .= '<table class="table">';
		$html .= '<tr>';
			$html .= '<th>Tracking</th>';
			$html .= '<th>Hits</th>';
			$html .= '<th>Emails</th>';
			$html .= '<th>Conversion</th>';
			$html .= '<th>Action</th>';
		$html .= '</tr>';
	$html .= '<tr>';
	foreach($tracking_count as $t){
		if($t->refresh_delete != '1'){
			$rs_views = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_view_counter` WHERE trackingid='{$t->trackingid}' AND pageid='{$post_id}' and ccid = '{$c_id}' and refresh_delete not in(2)");
			$rs_emails = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_email_counter` WHERE trackingid='{$t->trackingid}' AND pageid='{$post_id}' and ccid = '{$c_id}' and refresh_delete not in(2)");
			if($rs_views == 0 || $rs_emails == 0){
				$percentage = 0;
			}else{				
				$percentage = number_format(($rs_emails/$rs_views)*100,2);
			}
			$html .= '<tr>';
				if($t->trackingid){
					$html .= '<td>'.$t->trackingid.'</td>';	
				}else{
					$html .= '<td>No Tracking ID</td>';
				}
				$html .= '<td class="rs_view_'.$t->trackingid.'">'.$rs_views.'</td>';
				$html .= '<td class="rs_email_'.$t->trackingid.'">'.$rs_emails.'</td>';
				$html .= '<td class="rs_per_'.$t->trackingid.'">'.$percentage.'%</td>';
				$html .= '<td>
							<span style="cursor:pointer;" class="dlt_tracking" data="'.$t->trackingid.'"><i class="dlt_track_'.$t->trackingid.' fa fa-trash"></i></span> 
							<span style="cursor:pointer;" class="refresh_tracking" data="'.$t->trackingid.'"><i class="refresh_track_'.$t->trackingid.' fas fa-sync-alt"></i></span></td>';
			$html .= '</tr>';		
		}
	}
	$html .= '<button style="display:none;" class="track_post_id" value="'.$post_id.'"></button>';
	$html .= '</table>';	
	$total_views = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_view_counter` WHERE pageid='{$post_id}' and ccid = '{$c_id}'  and (refresh_delete = '0' OR refresh_delete = '2')");
	$total_emails = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_email_counter` WHERE pageid='{$post_id}' and ccid = '{$c_id}'  and (refresh_delete = '0' OR refresh_delete = '2')");
	if($total_views == 0 || $total_emails == 0){
		$percentage1 = 0;
	}else{				
		$percentage1 = number_format(($total_emails/$total_views)*100,2);
	}
	$html .= '<div class="ratings_div" style="display: inline-block;width: 100%;">';
	$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 0;display: inline-block;border-radius: 50%;width: 100px;text-align:center;font-size: 50px;background: #279dbc;color: white;height: 100px;line-height: 100px;" class="rs_views_div">'.$total_views.'</span><p style="font-size: 12px;">Total Views</p></div>';
	$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 0;display: inline-block;border-radius: 50%;width: 100px;text-align:center;font-size: 50px;background: #279dbc;color: white;height: 100px;line-height: 100px;" class="rs_email_div">'.$total_emails.'</span><p style="font-size: 12px;">Emails Added</p></div>';
	$html .= '<div style="width: 33.33%;float: left;text-align: center;font-size: 12px;font-weight: 600;"><span style="margin: 10px;padding: 0;display: inline-block;border-radius: 50%;width: 100px;text-align:center;font-size: 20px;background: #279dbc;color: white;height: 100px;line-height: 100px;" class="percentage_div">'.$percentage1.'%</span><p style="font-size: 12px;">Conversion</p></div>';
	$html .=  '</div>';
	
	// $html .= '<canvas id="conversion_chart"></canvas>';
	/*$tracking_count = $wpdb->get_results("SELECT DISTINCT(trackingid) FROM `wp_setc_view_counter` WHERE pageid='{$post_id}'");
	$emails = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_email_counter` WHERE pageid='{$post_id}'");
	if($emails > 0){
		$conversion = ($emails/COUNT($views))*100;
	}else{
		$conversion = '0';
	}
	foreach($tracking_count as $t){
		$html .= '<table class="table">';
			$html .= '<tr>';
				$html .= '<th>Tracking</th>';
				$html .= '<th>Hits</th>';
				$html .= '<th>Emails</th>';
				$html .= '<th>Conversion</th>';
			$html .= '</tr>';
		$html .= '<tr>';
		$rs = $wpdb->get_results("SELECT * FROM `wp_setc_view_counter` WHERE trackingid='{$t->trackingid}' pageid='{$post_id}'");
		/*foreach($rs as $v){
			
		}*/
	//}
	// $html .= print_r($array,true);
	/*$html .= '<table class="table">';
		$html .= '<tr>';
			$html .= '<th>Tracking</th>';
			$html .= '<th>Hits</th>';
			$html .= '<th>Emails</th>';
			$html .= '<th>Conversion</th>';
		$html .= '</tr>';
	$html .= '<tr>';
		$html .= '<td></td>';
		$html .= '<td>'.COUNT($views).'</td>';
		$html .= '<td>'.$emails.'</td>';
		$html .= '<td>'.$conversion.'%</td>';
	$html .= '</tr>';
	$html .= '</table>';*/
	/*$html .= '<table class="table">';
		$html .= '<tr>';
			$html .= '<th>Tracking</th>';
			$html .= '<th>Hits</th>';
			$html .= '<th>Emails</th>';
			$html .= '<th>Conversion</th>';
		$html .= '</tr>';
	foreach($views as $v){
		$html .= '<tr>';
			$html .= '<td>'.$v->trackingid.'</td>';
			$html .= '<td>'.COUNT($views).'</td>';
			
			$html .= '<td>'.$emails.'</td>';
			$html .= '<td>'.$conversion.'%</td>';
		$html .= '</tr>';
	}
	$html .= '</table>';*/
	echo $html;
	exit;
}
/*function clone_box(){
	$post_id = $_POST['post_id'];
	$html = '';
	$html .= '<div id="clone_error_msg"></div>';
	$html .= '<label>Funnel Name</label>';
	$html .= '<input type="hidden" name="main_funnel_Id" id="main_funnel_Id" value="'.$post_id.'">';
	$html .= '<input type="text" name="clone_funnel_name" id="clone_funnel_name">';
	$html .= '<span style="color:#FFFFFF; margin-top:10px;" class="btn_green button btn create_clone_funnel" data-funnelid="'.$post_id.'">Clone Funnel</span>';
	echo $html;
	exit;
}

function clone_funnel(){	
	$main_fid = $_POST['main_funnel'];
	$post_title = $_POST['funnel_name'];
	$date = date('Y-m-d H:i:s');
	$args = array(
	  'numberposts' => 10
	);
	 
	$latest_posts = get_posts( $args );
	$funnel_type = get_post_meta($main_fid, 'funnel_type', true );
	$postarr = array(
		'post_author' => get_current_user_id(),
		'post_title' => $post_title,
		'post_status' => 'publish',
		'post_type' => 'landing',
		'meta_input' => array(
			'funnel_type' => $funnel_type
		)
	);
	$newpost = wp_insert_post($postarr);
	$c_id = get_current_user_id();
	add_post_meta( $newpost, '_created_info_'.$c_id, $date);

	if($newpost){
		update_post_meta($newpost,'bb_preset_template',$main_fid);
		$postarr_thanks = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Thank You',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$thanks_post = wp_insert_post($postarr_thanks);
		update_post_meta($thanks_post,'bb_preset_template',$main_fid);
		$postarr_confirm = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Confirm',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$confirm_post = wp_insert_post($postarr_confirm);
		update_post_meta($confirm_post,'bb_preset_template',$main_fid);
		$postarr_download = array(
			'post_author' => get_current_user_id(),
			'post_title' => 'Download',
			'post_status' => 'publish',
			'post_parent' => $newpost,
			'post_type' => 'landing',
		);
		$download_post = wp_insert_post($postarr_download);
		update_post_meta($download_post,'bb_preset_template',$main_fid);
		
		$promotional_tools = $wpdb->get_results("SELECT * FROM `wp_fm_promotional_tools` WHERE post_id = $post_id ");
		foreach($promotional_tools as $pt){
			$item = $pt->item_type;
			$content = addslashes($pt->content);
			$ins_pro_tool = $wpdb->query("INSERT INTO `wp_fm_promotional_tools` (item_type,post_id,content) VALUES ('{$item}','{$newpost}','{$content}')");
		}
	}
	exit;
}*/
function PayKickStart_affiliate_approval($affiliate,$campaign_id){
	$base_url = "https://app.paykickstart.com/api/";
	$route = "affiliate";
	$url = $base_url . $route;
	$data = http_build_query([
		'auth_token' => 'hN6MYScTOVCD',
		'campaign_id' => $campaign_id,
		'affiliate' => $affiliate
	]);
	
	$output = $url . "?" . $data;
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,$output);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	$output = curl_exec($ch);
	curl_close($ch);
	$output = json_decode($output);
	
	return $output;
}
/*add_action( 'funnel_trigger_review', 'funnel_trigger_review_cron' );
function funnel_trigger_review_cron(){
	send_mail_fm('setc.avesh@gmail.com','wordpress@funnelmates.com','FunnelMates CRON WP','MAILINNER','Here');
}
function send_mail_fm($to_email,$from_email,$from_name,$subject,$message){
	$to=$to_email;
	$from=$from_email;
	$body_user=$message;
	$headers = "MIME-Version: 1.0\n"; 
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n"; 
	$headers .= "From: ".$from_name."<".$from.">\n"; 
	$headers .= "X-Priority: 1\n"; 
	$headers .= "X-MSMail-Priority: High\n"; 
	$headers .= "X-Mailer: PHP/".phpversion()."\n";
	
	if(mail($to, $subject, $body_user, $headers))
		return 1;
	else
		return 0;
}*/

/*
function trigger_reviewed_cron() {
	send_mail_fm('setc.avesh@gmail.com','wordpress@funnelmates.com','FunnelMates','MAILINNER','Here');
	/*global $wpdb;
	$args = array(
		'meta_key'   => 'is_funnel_published',
		'meta_value' => '1',
		'numberposts' => -1,
		'post_type' => 'landing',
		'post_status' => 'publish',
	);
	$posts = get_posts($args);
	foreach($posts as $p){
		$count_review = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_funnel_review` WHERE funnel_id='".$p->ID."'");
		if($count_review >= 5){
			// echo $count_review;exit;
			// echo $p->ID;
			$getsum = $wpdb->get_row("SELECT AVG(valueformoney) as valueformoney, AVG(welldesigned) as welldesigned, AVG(qualityofcontent) as qualityofcontent FROM `wp_setc_funnel_review` WHERE funnel_id='".$p->ID."' ");
			$valueformoney_average = $getsum->valueformoney;
			$welldesigned_average = $getsum->welldesigned;
			$qualityofcontent_average = $getsum->qualityofcontent;
			$final_average = ($valueformoney_average + $welldesigned_average + $qualityofcontent_average) / 3;
			if($final_average <= 2){
				update_post_meta($p->ID,'is_funnel_published','triggered_review');
				//Your funnel is under review
				fm_bp_custom_add_notification($p->ID,'triggered_review');
				// echo $final_average;
				//send_mail('setc.avesh@gmail.com','wordpress@funnelmates.com','FunnelMates','MAILINNER',$final_average);
			}
		}
	}*
}
function send_mail_fm($to_email,$from_email,$from_name,$subject,$message){
	$to=$to_email;
	$from=$from_email;
	$body_user=$message;
	$headers = "MIME-Version: 1.0\n"; 
	$headers .= "Content-type: text/html; charset=iso-8859-1\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n"; 
	$headers .= "From: ".$from_name."<".$from.">\n"; 
	$headers .= "X-Priority: 1\n"; 
	$headers .= "X-MSMail-Priority: High\n"; 
	$headers .= "X-Mailer: PHP/".phpversion()."\n";
	
	if(mail($to, $subject, $body_user, $headers))
		return 1;
	else
		return 0;
}*/
add_filter( 'posts_where', 'setc_posts_where', 10, 2 );
function setc_posts_where( $where, $wp_query ){
    global $wpdb;
    if ( $setc_title = $wp_query->get( 'setc_title' ) ) {
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( $setc_title ) ) . '%\'';
    }
    return $where;
}
function fm_total_leads(){
	global $wpdb;
	$user_id = get_current_user_id();
	$args = array(
		'nopaging'          => true,
		'posts_per_page'    => -1,
		'post_type'         => 'landing',
		'post_status'       => 'publish',
		'post_parent' 		=> 0,
		'author__in'		=> array($user_id)
	);
	$post_ids = array();
	$the_query = new WP_Query($args);
	if ( $the_query->have_posts() ) {
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
			array_push($post_ids,get_the_ID());
		}
	}
	wp_reset_postdata();
	$total_e = $wpdb->get_results("SELECT COUNT(*) FROM `wp_setc_email_counter` WHERE pageid IN (".join(',',$post_ids).") GROUP BY email");
	return count($total_e);
	// print_R($post_ids);
	// return $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_email_counter` WHERE pageid IN (".join(',',$post_ids).") GROUP BY email");
}

function delete_tracking_id(){
	global $wpdb;
	$trackID = $_POST['tracking'];
	$t_post_id = $_POST['tracking_post_id'];
	$user_id = get_current_user_id();
	
	$check_data = $wpdb->get_row("SELECT * FROM `wp_setc_view_counter` WHERE `ccid` = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	
	if($check_data->trackingid){
		//echo "1111"; exit;
		$update_track_view = $wpdb->query("UPDATE `wp_setc_view_counter` SET `refresh_delete`= 1 WHERE ccid = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	}else{
		//echo "UPDATE `wp_setc_view_counter` SET `refresh_delete`= 1 WHERE ccid = $user_id and trackingid = '' and pageid = $t_post_id";
		//echo "11112"; exit;
		$update_track_view = $wpdb->query("UPDATE `wp_setc_view_counter` SET `refresh_delete`= 1 WHERE ccid = $user_id and trackingid = '' and pageid = $t_post_id");
	}
	
	$check_data_email = $wpdb->get_row("SELECT * FROM `wp_setc_email_counter` WHERE `ccid` = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	
	if($check_data_email->trackingid){
		//echo "11113"; exit;
		$update_track_email = $wpdb->query("UPDATE `wp_setc_email_counter` SET `refresh_delete`= 1 WHERE ccid = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	}else{		
		//echo "11114"; exit;
		$update_track_email = $wpdb->query("UPDATE `wp_setc_email_counter` SET `refresh_delete`= 1 WHERE ccid = $user_id and trackingid = '' and pageid = $t_post_id");
	}
	
	if($update_track_view || $update_track_email){
		echo "1";
	}else{
		echo "2";
	}
	exit;
}

function refresh_tracking_id(){
	global $wpdb;	
	$trackID = $_POST['tracking'];
	$t_post_id = $_POST['tracking_post_id'];
	$user_id = get_current_user_id();
	
	$check_data = $wpdb->get_row("SELECT * FROM `wp_setc_view_counter` WHERE `ccid` = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	if($check_data->trackingid){
		$update_track_view = $wpdb->query("UPDATE `wp_setc_view_counter` SET `refresh_delete`= 2 WHERE ccid = $user_id and trackingid = '$trackID' and pageid = $t_post_id");	
	}else{
		$update_track_view = $wpdb->query("UPDATE `wp_setc_view_counter` SET `refresh_delete`= 2 WHERE ccid = $user_id and trackingid = '' and pageid = $t_post_id");	
	}
	
	$check_data_email = $wpdb->get_row("SELECT * FROM `wp_setc_email_counter` WHERE `ccid` = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	if($check_data_email->trackingid){
		$update_track_email = $wpdb->query("UPDATE `wp_setc_email_counter` SET `refresh_delete`= 2 WHERE ccid = $user_id and trackingid = '$trackID' and pageid = $t_post_id");
	}else{
		$update_track_email = $wpdb->query("UPDATE `wp_setc_email_counter` SET `refresh_delete`= 2 WHERE ccid = $user_id and trackingid = '' and pageid = $t_post_id");
	}
	
	if($update_track_view || $update_track_email){
		echo "1";
	}else{
		echo "2";
	}
	exit;
}

function affiliate_products(){
	global $wpdb;
	$postid = $_POST['post_id'];
	$funnel_name = get_the_title($postid);
	$html = '';
	$html .= '<div style="font-size:20px; font-weight:700;">The '.$funnel_name.' funnel contains the following affiliate offers and links: </div><br><br>';
	$query_jvzoo = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$postid." AND aff_network = 'jvzoo'  ");
	if(count($query_jvzoo) > 0){
		$html .= '<div style="text-align:center;"><img src="https://funnelmates.com/wp-content/uploads/2021/05/jvzoologo.png" style="width:10%; height:auto;"></div>';
		foreach($query_jvzoo as $qjvz){
			$afflink_sales = 'https://funnelmates.com/jvz/'.get_current_user_id().'/'.$qjvz->link_identifier.'/sales';
			$html .= '<div class="jvzoo_affiliate_links">';
				$html .= '<p style="text-align:center;"><a href="'.$afflink_sales.'" target="_BLANK">'.$qjvz->name.'</a></p>';
			$html .= '</div>';
		}
	}
	$query_wp = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$postid." AND aff_network = 'warriorplus'  ");
	if(count($query_wp) > 0){
		$html .= '<div style="text-align:center;"><img src="https://funnelmates.com/wp-content/uploads/2021/05/WarriorPlus.png" style="width:20%; height:auto;"></div>';
		foreach($query_wp as $qwp){
			$afflink_sales = 'https://funnelmates.com/wp/'.get_current_user_id().'/'.$qwp->link_identifier.'/sales';
			$html .= '<div class="warriorplus_affiliate_links">';
				$html .= '<p style="text-align:center;"><a href="'.$afflink_sales.'" target="_BLANK">'.$qwp->name.'</a></p>';
			$html .= '</div>';
		}
	}
	$query_cb = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$postid." AND aff_network = 'clickbank'  ");
	if(count($query_cb) > 0){
		$html .= '<div style="text-align:center;"><img src="https://funnelmates.com/wp-content/uploads/2021/05/clickbank.png" style="width:30%; height:auto;"></div>';
		foreach($query_cb as $qcb){
			$html .= '<div class="clickbank_affiliate_links">';
				$html .= '<p style="text-align:center;"><a href="http://funnelmate.'.$qcb->aff_link.'.hop.clickbank.net/" target="_BLANK">'.$qcb->name.'</a></p>';
			$html .= '</div>';
		}
	}
	$query_pks = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$postid." AND aff_network = 'paykickstart'  ");
	if(count($query_pks) > 0){
		$html .= '<div style="text-align:center;"><img src="https://funnelmates.com/wp-content/uploads/2021/05/paykick.png" style="width:30%; height:auto;"></div>';
		foreach($query_pks as $qpks){
			$afflink = 'https://funnelmates.com/pks/'.get_current_user_id().'/'.$qpks->link_identifier.'/';
			$html .= '<div class="paykickstart_affiliate_links">';
				$html .= '<p style="text-align:center;"><a href="'.$afflink.'" target="_BLANK">'.$qpks->name.'</a></p>';
			$html .= '</div>';
		}
	}
	$query_ex = $wpdb->get_results("SELECT * FROM `wp_setc_website_links` where funnel_id = ".$postid." AND aff_network = 'external'  ");
	if(count($query_ex) > 0){
		$html .= '<div style="text-align:center;"><span style="font-size:18px; font-weight:600;">[External] </span></div>';
		foreach($query_ex as $qex){
			$html .= '<div class="external_affiliate_links">';
				$html .= '<p style="text-align:center;"><a target="_BLANK" href="'.$qex->aff_link.'">'.$qex->name.'</a></p>';
			$html .= '</div>';
		}
	}
	echo $html;
	exit;
}

function edit_whitelabel_external_link_data(){
	$post_id = $_POST['post_id'];
	global $wpdb;
	$linktype = ($query->aff_network == 'creator')?'Download':ucfirst($query->aff_network);
	$html = '';
	$html .= '<style>.affiliate_link_error{background-color: #FF000055 !important;}</style>';
	$query = $wpdb->get_row("SELECT * FROM `wp_setc_website_links` where id = ".$post_id." and aff_network = 'external'");
	$link_label = 'External Link';
	//$html .= '<p>Current URL :  '.$query->aff_link.'</p>'	;
	$html .= '<label>'.$link_label.'</label><input type="text" name="external" value="" style="margin:0 !important;">';
	
	$html .= '<p id="error_msg_aff"></p>';
	$html .= '<button class="update_whitelabel_external_link button" data-postid="'.$post_id.'" style="border: 0;background:#279dbc;color: #FFF;padding: 10px 50px;border-radius: 5px;font-weight: 600;margin-top: 10px;" type="button">Update '.$linktype.' Link</button>';
	echo $html;
	exit;
}
function update_whitelabel_external_link(){
	global $wpdb;
	$update_id = $_POST['post_id'];
	$update_link = $_POST['link'];
	
	if($update_id){
		$query = $wpdb->query("UPDATE `".$wpdb->prefix."setc_website_links` SET aff_link='".$update_link."', whitelabel_updated='1' WHERE id=".$update_id);
		if($query){
			echo "1";
		}else{
			echo "2";
		}
	}
	exit;
}
function save_free_funnel(){
	global $wpdb;
	$month = $_POST['month_free_funnel'];
	$funnel_id = $_POST['funnel_free_funnel'];
	if($month && $funnel_id){
		$wpdb->query("INSERT INTO `wp_setc_free_funnels` (month,funnel_id) VALUES ('".$month."', '".$funnel_id."') ");
	}
	echo '1';
	exit;
}
function delete_free_funnel(){
	global $wpdb;
	$id = $_POST['id'];
	if($id){
		$wpdb->query("DELETE FROM `wp_setc_free_funnels` WHERE id=$id");
	}
	echo '1';
	exit;
}
function coupon_array_filter($var){
    return ($var !== NULL && $var !== FALSE && $var !== "" && $var !== "0");
}
function apply_coupon(){
	global $wpdb;
	$coupon_id = $_POST['coupon_id'];
	$coupon_code = $_POST['coupon_code'];
	$funnel_id = $_POST['funnel_id'];
	if($coupon_id && $coupon_code){
		$get_coupon = $wpdb->get_row("SELECT * FROM `wp_setc_funnel_coupon_code` WHERE id='$coupon_id'");
		if($get_coupon->coupon_code == $coupon_code){
			// echo 'valid';
			// echo "SELECT * FROM `wp_fm_marketplace_items` WHERE post_id=".$funnel_id;exit;
			$valid_for = maybe_unserialize($get_coupon->valid_for);
			// print_r($valid_for);
			$valid_for = array_filter($valid_for, "coupon_array_filter");
			$getrec = $wpdb->get_row("SELECT * FROM `wp_fm_marketplace_items` WHERE post_id=".$funnel_id);
			$pricing = maybe_unserialize($getrec->pricing);
			$array = array();
			// print_r($valid_for);exit;
			foreach($pricing as $k=>$p){
				if(array_key_exists($k,$valid_for)){
					if($get_coupon->coupon_type == 'percentage'){
						if($p > 0){
							$final_p = ($p * $get_coupon->coupon_value) / 100;
							$final_p = $p - $final_p;
						}else{
							$final_p = $p;
						}
					}else if($get_coupon->coupon_type == 'price'){
						if($p > 0){
							$final_p = $p - $get_coupon->coupon_value;
						}else{
							$final_p = $p;
						}
					}
					if($final_p < 0){
						$final_p = 0;
					}
				}else{
					$final_p = $p;
				}
				$final_p = number_format($final_p,1);
				$final_p = $final_p + 0;
				// $array[$k] = $array[$k] = number_format($final_p,1);
				$html .= '<div style="width:100%;padding: 0 1px;display:inline-block;margin:10px 0;">';
				$html .= '<div style="width:25%;float:left;padding: 0 1px;">';
					$html .= '<div>';
						$html .= '<p style="margin: 0;text-align: center;background: #414d6f;color: white;font-weight: 600;font-size: 12px;">'.ucfirst($k).'</p>';
						$html .= '<div id="price_'.$funnel_id.'_'.$k.'" style="text-align: center;background: #82b440;height: 50px;line-height: 50px;color: white;font-size: 35px;font-weight: 600;">$'.$final_p.'</div>';
					$html .= '</div>';
				$html .= '</div>';
				$html .= '<div style="width:75%;float:left;padding: 0 1px;line-height:65px;">';
					$html .= '<div style="margin-left: 20px;">';
						$already = $wpdb->get_row("SELECT * FROM `wp_setc_activated_funnels` WHERE funnel_id='".$funnel_id."' AND type='".strtolower($k)."' AND user_id='".get_current_user_id()."' ");
						//print_r($already);
						//$already->type;
						if(count($already) == 0){
							$current_balance = get_funnelmates_credit(get_current_user_id());
							if($current_balance >= $final_p){
								$btn_label = 'Activate '.ucfirst($k).' Funnel';
								$btn_class = 'success';
								$activated_free_count = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_activated_funnels` WHERE user_id='".get_current_user_id()."' AND type='free'");
								$funnel_limit = -1;
								$user_meta = get_userdata(get_current_user_id());
								if(in_array('funnelmates_access',$user_meta->roles)){
									$funnel_limit = 2;
								}else if(in_array('funnelmates_deluxe',$user_meta->roles)){
									$funnel_limit = 2;
								}
								if(strtolower($k) == 'free' && ($activated_free_count >= $funnel_limit && $funnel_limit != -1)){
									$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$funnel_id.' limit" onclick="funnel_active_limit();" class="btn btn-'.$btn_class.'" '.$btn_disable.'>'.$btn_label.'</button>';
								}else{
									$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$funnel_id.'" onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$funnel_id.','."'".strtolower($final_p)."'".','."'".$coupon_id."'".');" class="btn btn-'.$btn_class.'" '.$btn_disable.'>'.$btn_label.'</button>';
								}
							}else{
								$btn_label = 'Insufficient Balance';
								$btn_class = 'danger';
								$html .= '<button id="unlock_funnel_'.strtolower($k).'_'.$funnel_id.'" class="btn btn-'.$btn_class.'" disabled>'.$btn_label.'</button>'; //onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$funnel_id.');"
							}
						}else{
							$html .= '<button class="btn btn-warning" disabled>Already Activated! '.ucfirst($k).' Funnel</button>'; //onclick="unlock_funnel_finalize('."'".strtolower($k)."'".','.$funnel_id.');"
						}
					$html .= '</div>';
				$html .= '</div>';
				$html .= '</div>';
			}
			// print_r($array);
		}else{
			echo '0';
			exit;
		}
	}
	// echo json_encode($array);
	echo $html; exit;
	exit;
}