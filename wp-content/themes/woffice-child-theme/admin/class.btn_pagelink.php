<?php
//class start
class add_new_tinymce_btn {
	public $btn_arr;
	public $js_file;
	/*
	* call the constructor and set class variables
	* From the constructor call the functions via wordpress action/filter
	*/
	function __construct($seperator, $btn_name,$javascrip_location){
		$this->btn_arr = array("Seperator"=>$seperator,"Name"=>$btn_name);
		$this->js_file = $javascrip_location;
		add_action('init', array(&$this,'add_tinymce_button'));
		add_filter( 'tiny_mce_version', array(&$this,'refresh_mce_version'));
		add_filter( 'wp_ajax_get_pagelink_values', array(&$this,'get_pagelink_values'));
		add_filter( 'wp_ajax_get_downloadlink_values', array(&$this,'get_downloadlink_values'));
		add_filter( 'wp_ajax_get_affiliatelink_values', array(&$this,'get_affiliatelink_values'));
		add_filter( 'wp_ajax_get_externallink_values', array(&$this,'get_externallink_values'));
		add_filter( 'wp_ajax_register_download_link', array(&$this,'register_download_link'));
		add_filter( 'wp_ajax_register_affiliate_link', array(&$this,'register_affiliate_link'));
		add_filter( 'wp_ajax_register_external_link', array(&$this,'register_external_link'));
		add_action('wp_ajax_get_email_template', array(&$this,'get_email_template'));
		add_action('wp_ajax_get_warriorplus_products', array(&$this,'get_warriorplus_products'));
	}
	function get_warriorplus_products(){
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=50");
		$result = curl_exec($ch);

		$json_decode_result = json_decode($result);
		$json_encode_result = json_encode($json_decode_result->data);*/
		$json_decode_result = get_user_meta(get_current_user_id(),'warrior_plus_products',true);
		$json_decode_result = maybe_unserialize($json_decode_result);
		// print_r($json_decode_result);
		// $json_decode_result = json_decode($json_decode_result);
		$json_encode_result = json_encode($json_decode_result->data);
		echo $json_encode_result;
		exit;
	}
	function get_email_template(){
		global $wpdb;
		$template_id = $_POST['template_id'];
		$query = $wpdb->get_row("SELECT * FROM `wp_setc_followup_email_templates` WHERE id='$template_id'");
		echo json_encode($query);
		exit;
	}
	function get_pagelink_values(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		// $query = $wpdb->get_results("SELECT id,name,aff_link FROM `wp_setc_website_links` where funnel_id = ".$post_id);
		$array = array();
		$object = new stdClass();
		$object->name = 'Landing Page';
		$object->aff_link = $post_id;
		$array[] = $object;
		
		$args = array(
			'name'      	 => 'confirm',
			'post_type'      => 'landing',
			'posts_per_page' => -1,
			'post_parent'    => $post_id,
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		);
		$dl_post = get_posts($args);
		$object = new stdClass();
		$object->name = 'Confirm Page';
		$object->aff_link = $dl_post[0]->ID;
		$array[] = $object;
		
		$args = array(
			'name'      	 => 'download',
			'post_type'      => 'landing',
			'posts_per_page' => -1,
			'post_parent'    => $post_id,
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		);
		$dl_post = get_posts($args);
		$object = new stdClass();
		$object->name = 'Download Page';
		$object->aff_link = $dl_post[0]->ID;
		$array[] = $object;
		
		$args = array(
			'name'      	 => 'thank-you',
			'post_type'      => 'landing',
			'posts_per_page' => -1,
			'post_parent'    => $post_id,
			'order'          => 'ASC',
			'orderby'        => 'menu_order'
		);
		$dl_post = get_posts($args);
		$object = new stdClass();
		$object->name = 'Thank You Page';
		$object->aff_link = $dl_post[0]->ID;
		$array[] = $object;
		
		echo json_encode($array);
		exit;
	}
	function get_downloadlink_values(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$query = $wpdb->get_results("SELECT id,name,aff_link FROM `wp_setc_website_links` where funnel_id = '".$post_id."' AND aff_network='creator'");
		echo json_encode($query);
		exit;
	}
	function get_affiliatelink_values(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$query = $wpdb->get_results("SELECT id,name,aff_link FROM `wp_setc_website_links` where funnel_id = '".$post_id."' AND (aff_network='jvzoo' OR aff_network='clickbank' OR aff_network='warriorplus' OR aff_network='paykickstart')");
		echo json_encode($query);
		exit;
	}
	function get_externallink_values(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$query = $wpdb->get_results("SELECT id,name,aff_link FROM `wp_setc_website_links` where funnel_id = '".$post_id."' AND aff_network='external'");
		echo json_encode($query);
		exit;
	}
	function register_download_link(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$name = $_POST['name'];
		$link = $_POST['link'];
		$identifier = 'dl'.get_current_user_id().$post_id;//$_POST['identifier'];
		$identifier = validate_link_identifier($identifier);
		$query = $wpdb->query("INSERT INTO `wp_setc_website_links` (funnel_id,name,aff_network,aff_link,link_identifier) VALUES ('".$post_id."', '".$name."', 'creator', '".$link."', '".$identifier."')");
		if($query){
			echo $wpdb->insert_id;
		}
		exit;
	}
	function register_affiliate_link(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$name = $_POST['name'];
		$linktype = $_POST['linktype'];
		$link = $_POST['link'];
		// $identifier = $_POST['identifier'];
		if($linktype == 'warriorplus'){
			$identifier = 'wp'.get_current_user_id().$post_id;
		}else if($linktype == 'jvzoo'){
			$identifier = 'jvz'.get_current_user_id().$post_id;
		}else if($linktype == 'clickbank'){
			$identifier = 'cb'.get_current_user_id().$post_id;
		}else if($linktype == 'paykickstart'){
			$identifier = 'pks'.get_current_user_id().$post_id;
		}
		$identifier = validate_link_identifier($identifier);
		$notes = $_POST['notes'];
		$count_identifier = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_website_links` where link_identifier = '".$identifier."' ");
		if($count_identifier > 0){
			echo '0';
		}else{
			$query = $wpdb->query("INSERT INTO `wp_setc_website_links` (funnel_id,name,aff_network,aff_link,link_identifier,notes) VALUES ('".$post_id."', '".$name."', '".$linktype."', '".$link."', '".$identifier."','".$notes."')");
			if($query){
				echo $wpdb->insert_id;
			}
		}
		exit;
	}
	function register_external_link(){
		global $wpdb;
		$post_id = $_POST['post_id'];
		$name = $_POST['name'];
		$link = $_POST['link'];
		// $identifier = $_POST['identifier'];
		$identifier = 'ex'.get_current_user_id().$post_id;
		$identifier = validate_link_identifier($identifier);
		$notes = $_POST['notes'];
		$count_identifier = $wpdb->get_var("SELECT COUNT(*) FROM `wp_setc_website_links` where link_identifier = '".$identifier."' ");
		if($count_identifier > 0){
			echo '0';
		}else{
			$query = $wpdb->query("INSERT INTO `wp_setc_website_links` (funnel_id,name,aff_network,aff_link,link_identifier,notes) VALUES ('".$post_id."', '".$name."', 'external', '".$link."', '".$identifier."','".$notes."')");
			if($query){
				echo $wpdb->insert_id;
			}
		}
		exit;
	}
	
	/*
	* create the buttons only if the user has editing privs.
	* If so we create the button and add it to the tinymce button array
	*/
	function add_tinymce_button() {
		// if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
			// return;
		// if ( get_user_option('rich_editing') == 'true') {
			//the function that adds the javascript
			add_filter('mce_external_plugins', array(&$this,'add_new_tinymce_plugin'));
			//adds the button to the tinymce button array
			add_filter('mce_buttons', array(&$this,'register_new_button')); 
		// }
	}
	/*
	* add the new button to the tinymce array
	*/
	function register_new_button($buttons) {
		array_push($buttons, $this->btn_arr["Seperator"],$this->btn_arr["Name"]);
		return $buttons;
	}
	/*
	* Call the javascript file that loads the 
	* instructions for the new button
	*/
	function add_new_tinymce_plugin($plugin_array) {
		$plugin_array[$this->btn_arr['Name']] = $this->js_file;
		return $plugin_array;
	}
	/*
	* This function tricks tinymce in thinking 
	* it needs to refresh the buttons
	*/
	function refresh_mce_version($ver) {
		$ver += 3;
		return $ver;
	}
}//class end

//create an instance of the class
$t = new add_new_tinymce_btn('|','pagelink',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
$u = new add_new_tinymce_btn('|','downloadlink',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
$v = new add_new_tinymce_btn('|','affiliatelink',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
$w = new add_new_tinymce_btn('|','externallink',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
$x = new add_new_tinymce_btn('|','personalisation',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
$y = new add_new_tinymce_btn('|','load_template',get_stylesheet_directory_uri().'/adminjs/buttons/pagelink.js');
?>