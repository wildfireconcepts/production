<?php

/**
 * @class FLButtonModule
 */
class FLFMAffButtonModule extends FLBuilderModule {

	/**
	 * @method __construct
	 */
	public function __construct() {
		parent::__construct(array(
			'name'            => __( 'Affiliate Button', 'fl-builder' ),
			'description'     => __( 'A simple call to action button.', 'fl-builder' ),
			'category'        => __( 'Email Forms and Links', 'fl-builder' ),
			'partial_refresh' => true,
			'icon'            => 'button.svg',
		));
	}

	/**
	 * Ensure backwards compatibility with old settings.
	 *
	 * @since 2.2
	 * @param object $settings A module settings object.
	 * @param object $helper A settings compatibility helper.
	 * @return object
	 */
	public function filter_settings( $settings, $helper ) {

		// Handle old responsive button align.
		if ( isset( $settings->mobile_align ) ) {
			$settings->align_responsive = $settings->mobile_align;
			unset( $settings->mobile_align );
		}

		// Handle old font size setting.
		if ( isset( $settings->font_size ) ) {
			$settings->typography                = array();
			$settings->typography['font_size']   = array(
				'length' => $settings->font_size,
				'unit'   => isset( $settings->font_size_unit ) ? $settings->font_size_unit : 'px',
			);
			$settings->typography['line_height'] = array(
				'length' => $settings->font_size,
				'unit'   => isset( $settings->font_size_unit ) ? $settings->font_size_unit : 'px',
			);
			unset( $settings->font_size );
			unset( $settings->font_size_unit );
		}

		// Handle old padding setting.
		if ( isset( $settings->padding ) && is_numeric( $settings->padding ) ) {
			$settings->padding_top    = $settings->padding;
			$settings->padding_bottom = $settings->padding;
			$settings->padding_left   = $settings->padding * 2;
			$settings->padding_right  = $settings->padding * 2;
			unset( $settings->padding );
		}

		// Handle old gradient style setting.
		if ( isset( $settings->three_d ) && $settings->three_d ) {
			$settings->style = 'gradient';
		}

		// Handle old border settings.
		if ( ! empty( $settings->bg_color ) && ( ! isset( $settings->border ) || empty( $settings->border ) ) ) {
			$settings->border = array();

			// Border style, color, and width
			if ( isset( $settings->border_size ) && isset( $settings->style ) && 'transparent' === $settings->style ) {
				$settings->border['style'] = 'solid';
				$settings->border['color'] = FLBuilderColor::adjust_brightness( $settings->bg_color, 12, 'darken' );
				$settings->border['width'] = array(
					'top'    => $settings->border_size,
					'right'  => $settings->border_size,
					'bottom' => $settings->border_size,
					'left'   => $settings->border_size,
				);
				unset( $settings->border_size );
				if ( ! empty( $settings->bg_hover_color ) ) {
					$settings->border_hover_color = FLBuilderColor::adjust_brightness( $settings->bg_hover_color, 12, 'darken' );
				}
			}

			// Border radius
			if ( isset( $settings->border_radius ) ) {
				$settings->border['radius'] = array(
					'top_left'     => $settings->border_radius,
					'top_right'    => $settings->border_radius,
					'bottom_left'  => $settings->border_radius,
					'bottom_right' => $settings->border_radius,
				);
				unset( $settings->border_radius );
			}
		}

		// Handle old transparent background style.
		if ( isset( $settings->style ) && 'transparent' === $settings->style ) {
			$settings->style = 'flat';
			$helper->handle_opacity_inputs( $settings, 'bg_opacity', 'bg_color' );
			$helper->handle_opacity_inputs( $settings, 'bg_hover_opacity', 'bg_hover_color' );
		}

		// Return the filtered settings.
		return $settings;
	}

	/**
	 * @method enqueue_scripts
	 */
	public function enqueue_scripts() {
		if ( $this->settings && 'lightbox' == $this->settings->click_action ) {
			$this->add_js( 'jquery-magnificpopup' );
			$this->add_css( 'font-awesome-5' );
			$this->add_css( 'jquery-magnificpopup' );
		}
	}

	/**
	 * @method update
	 */
	public function update( $settings ) {
		// print_r($settings);exit;
		// Remove the old three_d setting.
		if ( isset( $settings->three_d ) ) {
			unset( $settings->three_d );
		}
		
		if( isset($settings->click_action) ){
			if($settings->click_action == 'add_new_affiliate_link'){
				global $wpdb;
				$internal_name = $settings->affiliate_name;
				if($settings->affiliate_link_type == 'jvzoo'){
					$link = $settings->jvz_affiliate_link;
					// $identifier = $settings->jvz_link_identifier;
					$identifier_prefix = 'jvz';
					$notes = "";
				}else if($settings->affiliate_link_type == 'clickbank'){
					$link = $settings->cb_vendor_id;
					// $identifier = $settings->cb_link_identifier;
					$identifier_prefix = 'cb';
					$notes = $settings->cb_notes;
				}else if($settings->affiliate_link_type == 'warriorplus'){
					$link = $settings->wp_product_id;
					$identifier_prefix = 'wp';
					// $identifier = $settings->wp_link_identifier;
				}else if($settings->affiliate_link_type == 'paykickstart'){
					$link = $settings->pks_campaign_id;
					// $identifier = $settings->cb_link_identifier;
					$identifier_prefix = 'pks';
					$notes = $settings->pks_notes;
				}else if($settings->affiliate_link_type == 'external'){
					$link = $settings->external_affiliate_link;
					// $identifier = $settings->external_link_identifier;
					$identifier_prefix = 'ex';
					$notes = $settings->external_notes;
				}
				$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				$post_id = url_to_postid($actual_link);
				if(wp_get_post_parent_id($post_id) == 0 || wp_get_post_parent_id($post_id) == false){
					$post_id = $post_id;
				}else{
					$post_id = wp_get_post_parent_id($post_id);
				}
				$identifier = $identifier_prefix.get_current_user_id().$post_id;
				$identifier = validate_link_identifier($identifier);
				if($link && $identifier && $internal_name){
					$query = $wpdb->query("INSERT INTO `".$wpdb->prefix."setc_website_links` (funnel_id, name, aff_network, aff_link, link_identifier, notes) VALUES ('".$post_id."', '".$internal_name."', '".$settings->affiliate_link_type."', '".$link."', '".$identifier."', '".$notes."')");
					if($query){
						$settings->link = $identifier;
						$settings->affiliate_url = $identifier;
						$settings->click_action = "affiliate_link";
					}
				}
			}
		}
		return $settings;
	}
	
	// public function get_linktype() {
		// return $this->settings->click_action;
	// }
	public function get_affiliate_network(){
		return $this->settings->affiliate_link_type;
	}

	/**
	 * @method get_classname
	 */
	public function get_classname() {
		$classname = 'fl-button-wrap';

		if ( ! empty( $this->settings->width ) ) {
			$classname .= ' fl-button-width-' . $this->settings->width;
		}
		if ( ! empty( $this->settings->align ) ) {
			$classname .= ' fl-button-' . $this->settings->align;
		}
		if ( ! empty( $this->settings->icon ) ) {
			$classname .= ' fl-button-has-icon';
		}

		return $classname;
	}

	/**
	 * Returns button link rel based on settings
	 * @since 1.10.9
	 */
	public function get_rel() {
		$rel = array();
		if ( '_blank' == $this->settings->link_target ) {
			$rel[] = 'noopener';
		}
		if ( isset( $this->settings->link_nofollow ) && 'yes' == $this->settings->link_nofollow ) {
			$rel[] = 'nofollow';
		}
		$rel = implode( ' ', $rel );
		if ( $rel ) {
			$rel = ' rel="' . $rel . '" ';
		}
		return $rel;
	}
	/**
	 * @method _get_download_url
	 * @protected
	 */
	public function _get_affiliate_url() {
		global $wpdb;
		$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		$post_id = url_to_postid($actual_link);
		if(wp_get_post_parent_id($post_id) == 0 || wp_get_post_parent_id($post_id) == false){
			$post_id = $post_id;
		}else{
			$post_id = wp_get_post_parent_id($post_id);
		}
		$get_aff = $wpdb->get_results("SELECT * FROM `".$wpdb->prefix."setc_website_links` WHERE funnel_id='".$post_id."' AND aff_network!='creator'");
		$options_array = array();
		$options_array[''] = _x('None', 'Affiliate Link.', 'fl-builder');
		foreach($get_aff as $gaff){
			$options_array[$gaff->link_identifier] = __($gaff->name.' ['.$gaff->aff_network.']', 'fl-builder');
		}
		return $options_array;
	}
	public function _get_wp_products(){
		global $wpdb;
		/*$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_URL, "https://warriorplus.com/api/v2/affiliate_requests?as=affiliate&apiKey=932cb1277c5faf9aadfb613c52050e6&limit=50");
		$result = curl_exec($ch);

		$json_decode_result = json_decode($result);
		$options_array = array();
		$options_array[''] = _x('None', 'WarriorPlus Product.', 'fl-builder');
		foreach($json_decode_result->data as $data){
			$options_array[$data->offer->id] = __($data->offer->name, 'fl-builder');
		}*/
		$json_decode_result = get_user_meta(get_current_user_id(),'warrior_plus_products',true);
		$json_decode_result = maybe_unserialize($json_decode_result);
		if(is_array($json_decode_result)){
		if(count($json_decode_result) > 0){
			$options_array = array();
			$options_array[''] = _x('None', 'WarriorPlus Product.', 'fl-builder');
			foreach($json_decode_result->data as $data){
				$options_array[$data->offer->id] = __($data->offer->name, 'fl-builder');
			}
		}
		}
		return $options_array;
	}
}

/**
 * Register the module and its form settings.
 */
FLBuilder::register_module('FLFMAffButtonModule', array(
	'general' => array(
		'title'    => __( 'General', 'fl-builder' ),
		'sections' => array(
			'general'  => array(
				'title'  => '',
				'fields' => array(
					'text'           => array(
						'type'        => 'text',
						'label'       => __( 'Text', 'fl-builder' ),
						'default'     => __( 'Click Here', 'fl-builder' ),
						'preview'     => array(
							'type'     => 'text',
							'selector' => '.fl-button-text',
						),
						'connections' => array( 'string' ),
					),
					'icon'           => array(
						'type'        => 'icon',
						'label'       => __( 'Icon', 'fl-builder' ),
						'show_remove' => true,
						'show'        => array(
							'fields' => array( 'icon_position', 'icon_animation' ),
						),
						'preview'     => array(
							'type' => 'none',
						),
					),
					'icon_position'  => array(
						'type'    => 'select',
						'label'   => __( 'Icon Position', 'fl-builder' ),
						'default' => 'before',
						'options' => array(
							'before' => __( 'Before Text', 'fl-builder' ),
							'after'  => __( 'After Text', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'icon_animation' => array(
						'type'    => 'select',
						'label'   => __( 'Icon Visibility', 'fl-builder' ),
						'default' => 'disable',
						'options' => array(
							'disable' => __( 'Always Visible', 'fl-builder' ),
							'enable'  => __( 'Fade In On Hover', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'click_action'   => array(
						'type'    => 'select',
						'label'   => __( 'Click Action', 'fl-builder' ),
						'default' => 'link',
						'options' => array(
							'add_new_affiliate_link'     => __( 'Add New Affiliate Link', 'fl-builder' ),
							// 'link'     => __( 'Link', 'fl-builder' ),
							'affiliate_link'      => __( 'Affiliate Link', 'fl-builder' ),
							// 'lightbox' => __( 'Lightbox', 'fl-builder' ),
						),
						'toggle'  => array(
							'add_new_affiliate_link'     => array(
								'sections' => array( 'add_new_affiliate_link' ),
							),
							'link'     => array(
								'fields' => array( 'link' ),
							),
							/*'lightbox' => array(
								'sections' => array( 'lightbox' ),
							),*/
							'affiliate_link'  => array(
								'fields' => array( 'affiliate_url' ),
							),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
					'link'           => array(
						'type'          => 'link',
						'label'         => __( 'Link', 'fl-builder' ),
						'placeholder'   => __( 'http://www.example.com', 'fl-builder' ),
						'show_target'   => true,
						'show_nofollow' => true,
						'show_download' => true,
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					'affiliate_url'  => array(
						'type'    => 'select',
						'label'   => __( 'Affiliate URL', 'fl-builder' ),
						'default' => 'none',
						'options' => FLFMAffButtonModule::_get_affiliate_url(),
					),
				),
			),
			'add_new_affiliate_link' => array(
				'title'  => __( 'New Affiliate Link', 'fl-builder' ),
				'fields' => array(
					'affiliate_name' => array(
						'type'        => 'text',
						'label'       => __( 'Product/Link Name', 'fl-builder' ),
						'placeholder' => 'Name to Identify the Link',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'affiliate_link_type' => array(
						'type'    => 'select',
						'label'   => __( 'Link Type', 'fl-builder' ),
						'default' => 'JVZoo',
						'options' => array(
							'jvzoo'  => __( 'JVZoo', 'fl-builder' ),
							'clickbank' => __( 'ClickBank', 'fl-builder' ),
							'warriorplus' => __( 'Warrior Plus', 'fl-builder' ),
							'paykickstart' => __( 'PayKickStart', 'fl-builder' ),
							'external' => __( 'External', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
						'toggle'  => array(
							'jvzoo'  => array(
								'fields' => array( 'jvz_affiliate_link'/*, 'jvz_link_identifier'*/ ),
							),
							'clickbank' => array(
								'fields' => array( 'cb_vendor_id'/*, 'cb_link_identifier'*/, 'cb_notes' ),
							),
							'warriorplus' => array(
								'fields' => array( 'wp_product_id'/*, 'wp_link_identifier'*/ ),
							),
							'paykickstart' => array(
								'fields' => array( 'pks_campaign_id', /*'external_link_identifier',*/ 'pks_notes' ),
							),
							'external' => array(
								'fields' => array( 'external_affiliate_link'/*, 'external_link_identifier'*/, 'external_notes' ),
							),
						),
					),
					'jvz_affiliate_link' => array(
						'type'          => 'text',
						'label'         => __( 'JVZoo Affiliate Request Link', 'fl-builder' ),
						'placeholder'   => __( 'https://www.jvzoo.com/affiliate/affiliateinfo/index/349507', 'fl-builder' ),
						'help'    => __( 'The link users visit to request to promote the offer eg. https://www.jvzoo.com/affiliate/affiliateinfo/index/349507', 'fl-builder' ),
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					/*'jvz_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'JVZoo Link Identifier', 'fl-builder' ),
						'placeholder' => 'linkidentifier',
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'cb_vendor_id' => array(
						'type'        => 'text',
						'label'       => __( 'ClickBank Vendor ID', 'fl-builder' ),
						'placeholder' => '352425',
						'help'    => __( 'This is the account nickname of the product you\'re promoting', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					/*'cb_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'ClickBank Link Identifier', 'fl-builder' ),
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'cb_notes' => array(
						'type'        => 'text',
						'label'       => __( 'alt page', 'fl-builder' ),
						'placeholder' => '?cbpage=',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'wp_product_id' => array(
						'type'    => 'select',
						'label'   => __( 'WarriorPlus Product', 'fl-builder' ),
						'default' => 'none',
						'options' => FLFMAffButtonModule::_get_wp_products(),
					),
					/*'wp_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'WarriorPlus Identifier', 'fl-builder' ),
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'pks_campaign_id' => array(
						'type'        => 'text',
						'label'       => __( 'Campaign ID', 'fl-builder' ),
						'placeholder' => '123',
						'help'    => __( 'This is the Campaign ID of the product you\'re promoting', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'pks_notes' => array(
						'type'        => 'text',
						'label'       => __( 'Notes', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'external_affiliate_link' => array(
						'type'          => 'text',
						'label'         => __( 'External Affiliate Request Link', 'fl-builder' ),
						'placeholder'   => __( 'Add Your Link', 'fl-builder' ),
						'help'    => __( 'External links can only be used on: Funnels you publish for personal use only, Funnels you publish in marketplace with Whitelabel or Exclusive rights permissions. Free and Premium funnels cannot contain external links Please note: Whitelabel and exclusive funnels can only be sold if you\'re a pro FunnelMates member.', 'fl-builder' ),
						'preview'       => array(
							'type' => 'none',
						),
						'connections'   => array( 'url' ),
					),
					/*'external_link_identifier' => array(
						'type'        => 'text',
						'label'       => __( 'External Link Identifier', 'fl-builder' ),
						'help'    => __( 'All lower caps, no spaces or special characters', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),*/
					'external_notes' => array(
						'type'        => 'text',
						'label'       => __( 'External Notes', 'fl-builder' ),
						'placeholder' => 'Notes..',
						'help'    => __( 'Optional. Can be used to provide information to funnel customer about why you\'ve included this link, links to external affiliate program or why they should keep the link as it is.', 'fl-builder' ),
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					//
				),
			),
			/*'lightbox' => array(
				'title'  => __( 'Lightbox Content', 'fl-builder' ),
				'fields' => array(
					'lightbox_content_type' => array(
						'type'    => 'select',
						'label'   => __( 'Content Type', 'fl-builder' ),
						'default' => 'html',
						'options' => array(
							'html'  => __( 'HTML', 'fl-builder' ),
							'video' => __( 'Video', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
						'toggle'  => array(
							'html'  => array(
								'fields' => array( 'lightbox_content_html' ),
							),
							'video' => array(
								'fields' => array( 'lightbox_video_link' ),
							),
						),
					),
					'lightbox_content_html' => array(
						'type'        => 'code',
						'editor'      => 'html',
						'label'       => '',
						'rows'        => '19',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'string' ),
					),
					'lightbox_video_link'   => array(
						'type'        => 'text',
						'label'       => __( 'Video Link', 'fl-builder' ),
						'placeholder' => 'https://vimeo.com/122546221',
						'preview'     => array(
							'type' => 'none',
						),
						'connections' => array( 'custom_field' ),
					),
				),
			),*/
		),
	),
	'style'   => array(
		'title'    => __( 'Style', 'fl-builder' ),
		'sections' => array(
			'style'  => array(
				'title'  => '',
				'fields' => array(
					'width'        => array(
						'type'    => 'select',
						'label'   => __( 'Width', 'fl-builder' ),
						'default' => 'auto',
						'options' => array(
							'auto'   => _x( 'Auto', 'Width.', 'fl-builder' ),
							'full'   => __( 'Full Width', 'fl-builder' ),
							'custom' => __( 'Custom', 'fl-builder' ),
						),
						'toggle'  => array(
							'auto'   => array(
								'fields' => array( 'align' ),
							),
							'full'   => array(),
							'custom' => array(
								'fields' => array( 'align', 'custom_width' ),
							),
						),
					),
					'custom_width' => array(
						'type'    => 'unit',
						'label'   => __( 'Custom Width', 'fl-builder' ),
						'default' => '200',
						'slider'  => array(
							'px' => array(
								'min'  => 0,
								'max'  => 1000,
								'step' => 10,
							),
						),
						'units'   => array(
							'px',
							'vw',
							'%',
						),
						'preview' => array(
							'type'     => 'css',
							'selector' => 'a.fl-button',
							'property' => 'width',
						),
					),
					'align'        => array(
						'type'       => 'align',
						'label'      => __( 'Align', 'fl-builder' ),
						'default'    => 'left',
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.fl-button-wrap',
							'property' => 'text-align',
						),
					),
					'padding'      => array(
						'type'       => 'dimension',
						'label'      => __( 'Padding', 'fl-builder' ),
						'responsive' => true,
						'slider'     => true,
						'units'      => array( 'px' ),
						'preview'    => array(
							'type'     => 'css',
							'selector' => 'a.fl-button',
							'property' => 'padding',
						),
					),
				),
			),
			'text'   => array(
				'title'  => __( 'Text', 'fl-builder' ),
				'fields' => array(
					'text_color'       => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Text Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button, a.fl-button *',
							'property'  => 'color',
							'important' => true,
						),
					),
					'text_hover_color' => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Text Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button:hover, a.fl-button:hover *, a.fl-button:focus, a.fl-button:focus *',
							'property'  => 'color',
							'important' => true,
						),
					),
					'typography'       => array(
						'type'       => 'typography',
						'label'      => __( 'Typography', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => 'a.fl-button',
						),
					),
				),
			),
			'icons'  => array(
				'title'  => __( 'Icon', 'fl-builder' ),
				'fields' => array(
					'duo_color1' => array(
						'label'      => __( 'DuoTone Icon Primary Color', 'fl-builder' ),
						'type'       => 'color',
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => 'i.fl-button-icon.fad:before',
							'property'  => 'color',
							'important' => true,
						),
					),
					'duo_color2' => array(
						'label'      => __( 'DuoTone Icon Secondary Color', 'fl-builder' ),
						'type'       => 'color',
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => 'i.fl-button-icon.fad:after',
							'property'  => 'color',
							'important' => true,
						),
					),
				),
			),
			'colors' => array(
				'title'  => __( 'Background', 'fl-builder' ),
				'fields' => array(
					'bg_color'          => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Background Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
					'bg_hover_color'    => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Background Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
					'style'             => array(
						'type'    => 'select',
						'label'   => __( 'Background Style', 'fl-builder' ),
						'default' => 'flat',
						'options' => array(
							'flat'     => __( 'Flat', 'fl-builder' ),
							'gradient' => __( 'Gradient', 'fl-builder' ),
						),
					),
					'button_transition' => array(
						'type'    => 'select',
						'label'   => __( 'Background Animation', 'fl-builder' ),
						'default' => 'disable',
						'options' => array(
							'disable' => __( 'Disabled', 'fl-builder' ),
							'enable'  => __( 'Enabled', 'fl-builder' ),
						),
						'preview' => array(
							'type' => 'none',
						),
					),
				),
			),
			'border' => array(
				'title'  => __( 'Border', 'fl-builder' ),
				'fields' => array(
					'border'             => array(
						'type'       => 'border',
						'label'      => __( 'Border', 'fl-builder' ),
						'responsive' => true,
						'preview'    => array(
							'type'      => 'css',
							'selector'  => 'a.fl-button',
							'important' => true,
						),
					),
					'border_hover_color' => array(
						'type'        => 'color',
						'connections' => array( 'color' ),
						'label'       => __( 'Border Hover Color', 'fl-builder' ),
						'default'     => '',
						'show_reset'  => true,
						'show_alpha'  => true,
						'preview'     => array(
							'type' => 'none',
						),
					),
				),
			),
		),
	),
));
