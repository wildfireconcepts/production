<?php

	$button_node_id = "fl-node-$id";

if ( isset( $settings->id ) && ! empty( $settings->id ) ) {
	$button_node_id = $settings->id;
}
?>
<div class="<?php echo $module->get_classname(); ?>">
	<?php if ( isset( $settings->click_action ) && 'lightbox' == $settings->click_action ) { ?>
		<a href="<?php echo 'video' == $settings->lightbox_content_type ? $settings->lightbox_video_link : '#'; ?>" class="fl-button <?php echo $button_node_id; ?> fl-button-lightbox<?php echo ( 'enable' == $settings->icon_animation ) ? ' fl-button-icon-animation' : ''; ?>" role="button" target="_blank">
	<?php }elseif ( isset( $settings->click_action ) && ('affiliate_link' == $settings->click_action) ) {
		$aff_network = $settings->affiliate_link_type;
		// if($aff_network == ''){
			global $wpdb;
			$gettype = $wpdb->get_row('SELECT aff_network FROM `wp_setc_website_links` WHERE link_identifier="'.$settings->affiliate_url.'" ');
			// print_r($gettype);
			if($gettype->aff_network == 'jvzoo'){
				$aff_network = 'jvz';
			}else if($gettype->aff_network == 'clickbank'){
				$aff_network = 'cb';
			}else if($gettype->aff_network == 'warriorplus'){
				$aff_network = 'wp';
			}else if($gettype->aff_network == 'paykickstart'){
				$aff_network = 'pks';
			}else if($gettype->aff_network == 'external'){
				$aff_network = 'ex';
			}
		/*}else{
			if($aff_network == 'jvzoo'){
				$aff_network = 'jvz';
			}else if($aff_network == 'clickbank'){
				$aff_network = 'cb';
			}else if($aff_network == 'warriorplus'){
				$aff_network = 'wp';
			}else if($aff_network == 'external'){
				$aff_network = 'ex';
			}
		}*/
		if($_COOKIE['CCID']){
			$ccid = $_COOKIE['CCID'];
		}else{
			$ccid = 5;
		}
		?>
		<a href="<?php echo site_url().'/'.$aff_network.'/'.$ccid.'/'.$settings->affiliate_url; ?>" target="_blank" class="fl-button<?php echo ( 'enable' == $settings->icon_animation ) ? ' fl-button-icon-animation' : ''; ?>" role="button"<?php echo $module->get_rel(); ?>>
	<?php }else{ ?>
		<a href="<?php echo $settings->link; ?>"<?php echo ( isset( $settings->link_download ) && 'yes' === $settings->link_download ) ? ' download' : ''; ?> target="_blank" class="fl-button<?php echo ( 'enable' == $settings->icon_animation ) ? ' fl-button-icon-animation' : ''; ?>" role="button"<?php echo $module->get_rel(); ?>>
	<?php } ?>
		<?php
		if ( ! empty( $settings->icon ) && ( 'before' == $settings->icon_position || ! isset( $settings->icon_position ) ) ) :
			?>
		<i class="fl-button-icon fl-button-icon-before <?php echo $settings->icon; ?>" aria-hidden="true"></i>
		<?php endif; ?>
		<?php if ( ! empty( $settings->text ) ) : ?>
		<span class="fl-button-text"><?php echo $settings->text; ?></span>
		<?php endif; ?>
		<?php
		if ( ! empty( $settings->icon ) && 'after' == $settings->icon_position ) :
			?>
		<i class="fl-button-icon fl-button-icon-after <?php echo $settings->icon; ?>" aria-hidden="true"></i>
		<?php endif; ?>
	</a>
</div>
<?php if ( 'lightbox' == $settings->click_action && 'html' == $settings->lightbox_content_type && isset( $settings->lightbox_content_html ) ) : ?>
	<div class="<?php echo $button_node_id; ?> fl-button-lightbox-content mfp-hide">
		<?php echo $settings->lightbox_content_html; ?>
	</div>
<?php endif; ?>
