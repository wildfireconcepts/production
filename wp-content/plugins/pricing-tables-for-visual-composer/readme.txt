=== Pricing Tables For WPBakery Page Builder (formerly Visual Composer) ===
Contributors: Labibahmed42
Tags: pricing tables for wpbakery,pricing tables for wpbakery page builder,pricing tables for wordpress,pricing tables,pricing plans,pricing tables,wpbakery addon
Donate link: https://www.paypal.me/labibahmed/5
Requires at least: 3.5
Tested up to: 5.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Add simple,unique and fully customizable pricing tables to your site that suit your needs.

== Description ==
Pricing Tables For WPBakery is a WPBakery Page Builder Addon that will allow you to add simplest pricing tables to your site with few clicks and customizable options.You don't need to use any HTML type thing to deal with this plugin.It is giving you full control to customize the things according to your need.
It’s super easy to create pricing or compare tables with this addon.

Note: This plugin requires <a href="http://codecanyon.net/item/visual-composer-page-builder-for-wordpress/242431?ref=labibahmed" target="_blank">WPBakery Page Builder</a> Plugin.

<a href="http://thecodude.com/demo/codecanyon/pricing-pro/">See DEMO Here</a><br>

<h3>Features</h3>

<ul>
	<li>100% Responsive</li> 
	<li>Custom Column Layouts </li> 
	<li>Custom Content</li> 
	<li>Extenal Linking on Buttons</li> 
	<li>Custom Headings</li> 
	<li>Customizable Options</li> 
	<li>Animated</li>
	<li>24/7 Support</li>
</ul>


== Screenshots ==
1. Admin Settings
2. Frontend

== Changelog ==
= 1.0 =
* Initial Release

= 1.1 =
* Added new features

