<?php
require_once(WPBLAZER_PLUGIN_URL . '/vendor/autoload.php');

use Aws\S3;

class Back_dir extends WPB_Backup {

    /**
     * @static
     * 
     * @access private
     */
    private static $instance;

    /**
     * @static
     * 
     * @return self
     */
    public static function get_instance() {

        if (empty(self::$instance))
            self::$instance = new Back_dir();

        return self::$instance;
    }

    /**
     * 
     */
    private function dir() {

        $dir = get_option('wpb_backup_dir');

        if (!$dir)
            $dir = $this->get_default_dir();

        if (!is_dir($dir) && is_writable(dirname($dir)))
            mkdir($dir, 0755);

        if (get_option('wpb_backup_dir') !== $dir)
            update_option('wpb_backup_dir', $dir);

        $index = $dir . '/index.html';

        if (!file_exists($index) && wp_is_writable($dir))
            file_put_contents($index, '');

        return parent::clear_path($dir);
    }

    private function get_default_dir() {

        $dirname = 'WPB-' . substr($this->generate_key(), 0, 10) . '-backups';
        $dir = parent::clear_path(trailingslashit(WP_CONTENT_DIR) . $dirname);

        $upload_dir = wp_upload_dir();

        if ((!is_dir($dir) && !is_writable(dirname($dir)) ) || ( is_dir($dir) && !is_writable($dir) ))
            $dir = parent::clear_path(trailingslashit($upload_dir['basedir']) . $dirname);

        return $dir;
    }

    /**
     * @access private
     * 
     * @return string $generate_key
     */
    private function generate_key() {

        if (!empty($this->generate_key))
            return $this->generate_key;

        $generate_key = array(ABSPATH, time());

        shuffle($generate_key);

        return md5(serialize($generate_key));
    }

    public function __construct() {
        
        parent::__construct();

        $this->set_backup_dir($this->dir());
    }

    public function do_backup() {
        $restart = get_option('wpb_zip_restart');
        
        if ($restart != '1') {        
            $this->set_status('Running backup...');        
            set_log('Running backup...', TRUE);
        }
        
        $this->clean_data(TRUE);
        
        $archive_filepath = $this->runbackup();
        
        $backup = array_reverse(glob($this->get_backup_dir() . '/*.zip')); 
        
        return $archive_filepath;
    }

    /**
     * Get the backup once it has run, will return status running as a WP Error
     *
     * @return WP_Error|string
     */
    public function get_backup() {

        global $is_apache;
        
        $status = $this->get_status();

        if($status == 'Dumping Database...') {
            return array( 'status' => 'error', 'error' => $status );
        }
        if($status == 'Creating zip file...') {
            return array( 'status' => 'error', 'error' => $status );
        }
        if(file_exists($this->get_backup_dir() . '/' . get_option( 'wpb_backname' ))) {
            if ($status = $this->get_status()){
                $this->do_action('backup_complete');
            }
        } elseif(empty($status) && !file_exists($this->get_backup_dir() . '/' . get_option( 'wpb_backname' ))) {
            return array( 'status' => 'error', 'error' => 'Backup failed or killed' );
        } elseif($status == 'Backup Process Failed...') { 
            return array( 'status' => 'error', 'error' => 'Backup failed or killed' );
        } else {
            return array( 'status' => 'error', 'error' => $status );
        }
        
        $backups = array();
        foreach (glob($this->get_backup_dir() . '/*.zip') as $backup) {
            $backups[filemtime($backup)] = $backup;
        }
        krsort($backups);

        if(count($backups) > 3)
            unlink( end($backups) );
        
        $sqlfile = array_reverse(glob($this->get_backup_dir() . '/*.sql'));  
                
        if($sqlfile)
            unlink( end($sqlfile) );
                     
        $backup = reset($backups);

        if (file_exists($backup)) {
            return str_replace(parent::clear_path(WP_CONTENT_DIR), WP_CONTENT_URL, $backup);
        }

        return array( 'status' => 'error', 'error' => 'No backup was found' );
    }

    /**
     * Get the backup once it has run, will return status running as a WP Error
     *
     * @return WP_Error|string
     */
    public function get_backups() {

        $backups = array_reverse(glob($this->get_backup_dir() . '/*.zip'));  
        
        $sqlfile = array_reverse(glob($this->get_backup_dir() . '/*.sql'));  
                
        if($sqlfile)
            unlink( end($sqlfile) );

        if (!empty($backups)) {
            $result = array();
            foreach($backups as $key => $backup){
                $result[$key]['backupsize'] = filesize($backup);
                $result[$key]['backupurl'] = str_replace(parent::clear_path(WP_CONTENT_DIR), WP_CONTENT_URL, $backup);
                $result[$key]['backuploc'] = str_replace(parent::clear_path(WPBLAZER_ROOT), '', $this->get_backup_dir());
            }
            return $result;
        }

        return array( 'status' => 'error', 'error' => 'No backup was found' );
    }

    public function clear() {
        
        $file = WPB_API_Authenticate::get_arg('backup_filename');
    
        $dir = get_option('wpb_backup_dir');
        
        if(!empty($file)){
            if ( is_writeable( $dir . '/' . $file ) ) {
                unlink( $dir . '/' . $file );
            }
            return TRUE;
        }

        $back_dir = trailingslashit( $this->get_backup_dir() );

        if ( is_writable( $back_dir ) && $dir = opendir( $back_dir ) ) {
            while ( false !== ( $file = readdir( $dir ) ) ) {
                if ( is_writeable( $back_dir . $file ) ) {
                    unlink( $back_dir . $file );
                }
            }
            closedir( $dir );
        }
        
        $this->clean_data(TRUE);
        delete_option('wpb_dumbed_table');
        delete_option('wpb_dumb_start');
        delete_option('wpb_dumb_restart');

        delete_option('wpb_zip_files');
        delete_option('wpb_zip_file');
        delete_option('wpb_zip_folders');
        delete_option('wpb_zip_folder');
        delete_option('wpb_zip_restart');

        rmdir($this->get_backup_dir());

        delete_option('wpb_backup_dir');
    }
    
    /*
     * Clean the cloud uploaded data if it's set 
     */
    public function clean_data($type = FALSE) {
        if ($type) {
            delete_option('wpb_az_uid');
            delete_option('wpb_az_offset');
            delete_option('wpb_az_restart');
            delete_option('wpb_gd_uid');
            delete_option('wpb_gd_offset');
            delete_option('wpb_gd_restart');
            delete_option('wpb_s3_uid');
            delete_option('wpb_s3_parts');
            delete_option('wpb_s3_part');
            delete_option('wpb_s3_stepsdone');
            delete_option('wpb_s3_restart');
            delete_option('wpb_db_uid');
            delete_option('wpb_db_offset');            
            delete_option('wpb_db_restart');
        }
    }

    /**
     * 
     */
    protected function do_action($action) {

        switch ($action) :

            case 'mysqldump_started' :

                $restart = get_option('wpb_zip_restart');
        
                if ($restart != '1') { 

                    $this->set_status('Dumping Database...');
                
                    set_log('Dumping Database...');

                }

                break;

            case 'archive_started' :

                $this->set_status('Creating zip archive...');
                
                set_log('Creating zip archive...');

                $this->set_pid();

                break;

            case 'ziparchive_started' :

                $restart = get_option('wpb_zip_restart');
        
                if ($restart != '1') { 

                    $this->set_status('Creating zip file...');
                
                    set_log('Creating zip file...');

                }

                break;

            case 'backup_complete' :
                
                set_log('Backup Completed...<br> FileName: ' . get_option( 'wpb_backname' ));

                if (file_exists($this->default_status_dir()))
                    unlink($this->default_status_dir());

                unlink($this->dir() . '/.backup_pid');

                break;
                    
            case 'backup_warning' :
                
                $this->set_error('shell failed');

	    	break;

        endswitch;
    }
    
    private function set_pid() {

        $file = $this->dir() . '/.backup_pid';

        if (file_exists($file))
            unlink($file);

        if (!$handle = @fopen($file, 'w'))
            return;

        fwrite($handle, getmypid());

        fclose($handle);
    }
    
    private function set_error($string) {

        $file = $this->dir() . '/.backup_warnings';

        if (file_exists($file))
            unlink($file);

        if (!$handle = @fopen($file, 'w'))
            return;

        fwrite($handle, json_encode($err));

        fclose($handle);
    }

    
    private function get_pid() {

        $file = $this->dir() . '/.backup_pid';

	if ( file_exists( $file ) )
            return (int) trim( file_get_contents( $file ) );
	else
            return false;

    }

    private function get_status() {

        if (!file_exists($this->default_status_dir()))
            return '';

        $status = file_get_contents($this->default_status_dir());

        return $status;
    }

    private function default_status_dir() {

        return $this->dir() . '/.backup-status';
    }

    public function set_status($string) {

        if (!$file = fopen($this->default_status_dir(), 'w'))
            return;

        fwrite($file, $string);

        fclose($file);
    }

    public function dropbox_url(){

        $backup = array_reverse(glob($this->get_backup_dir() . '/*.zip'));
        
        return reset($backup);
        
    }

}
    
function default_log_dir() {
    
    $dir = get_option('wpb_backup_dir');

    return $dir . '/.backup-log';
}

function wpb_get_dump() {

    $startoption = get_option('wpb_dumb_start');

    return $startoption;
}


function wpb_get_zip() {

    $zip_folders = get_option('wpb_zip_folders');
    $zip_files = get_option('wpb_zip_files');
    $startoption = array_merge($zip_folders,$zip_files);

    return $startoption;
}

function wpb_delete_dump() {
    global $wpdb;
    require_once( ABSPATH . 'wp-admin/includes/file.php' );
    $home_path = get_home_path();

    $filename = 'database_' . DB_NAME . '.sql';
    $sqlfilename = strtolower(sanitize_file_name(remove_accents($filename)));
    $sqlfile = trailingslashit($home_path) . $sqlfilename;

    // Delete the database dump
    if (file_exists($sqlfile))
        unlink($sqlfile);

    $sqlfile = trailingslashit(Back_dir::get_instance()->get_backup_dir()) . $sqlfilename;
    // Delete the database dump
    if (file_exists($sqlfile))
        unlink($sqlfile);

    delete_option('wpb_dumbed_table');
    delete_option('wpb_dumb_start');
    delete_option('wpb_dumb_restart');

    return DB_NAME;
}

function wpb_get_log() {

    if (!file_exists(default_log_dir()))
        return '';

    $log = file_get_contents(default_log_dir());

    return $log;
}

function set_log($string, $type = false) {

    if (!$type) {
        if (!$file = fopen(default_log_dir(), 'a'))
            return;
    } else {
        if (!$file = fopen(default_log_dir(), 'w+'))
            return;
    }

    if (is_array($string) || is_object($string)) {
        $string = json_encode($string);
    }

    fwrite($file, $string . '<br />' . PHP_EOL);

    fclose($file);
}

function wpb_upload_ftp() {

    set_log('Starting FTP upload...');
    $ftphost = WPB_API_Authenticate::get_arg('ftphost');
    $ftpuname = WPB_API_Authenticate::get_arg('ftpuname');
    $ftppass = WPB_API_Authenticate::get_arg('ftppass');
    $ftplocation = WPB_API_Authenticate::get_arg('ftpdir');
    $ftpdirectory = sanitize_file_name( get_bloginfo( 'name' ) );
    $ftp_max_backups = WPB_API_Authenticate::get_arg( 'max_backups' );
    $ftp_max_backups = !empty($ftp_max_backups) ? $ftp_max_backups : 3;
    @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
    @set_time_limit( 0 );

    $file = backup_url();
    $ftp_server = $ftphost;
    $conn_id = ftp_connect($ftp_server) or ( $ftpmsg = "Could not connect");

    if (!ftp_login($conn_id, $ftpuname, $ftppass)) {
        set_log('Error Connecting Ftp Host. Please check the username/password or ftp host....');
        $result .= "Error Connecting Ftp Host. Please check the username/password or ftp host.";
    } else {
        if ($ftplocation == '') {
            $create_dir = "WpBlazer3BackUp";//sanitize_file_name( get_bloginfo( 'name' ) )
            ftp_mkdir($conn_id, $create_dir);
            $des_dir = $create_dir;
        } else {
            $des_dir = $ftplocation;
        }
        ftp_chdir($conn_id, $des_dir);
        
        if ( ! @ftp_chdir( $conn_id, $ftpdirectory ) ) {
            if ( @ftp_mkdir( $conn_id, $ftpdirectory ) ) {
                ftp_chdir($conn_id, $ftpdirectory);
            }            
        }        
        
        set_log('FTP upload started...');
        ftp_chdir($conn_id, $ftpdirectory);
        $fp = fopen($file, 'r');

        if (ftp_fput($conn_id, basename($file), $fp, FTP_BINARY)) {
            $result .= "FTP file sent successfully";
            $filelist = array();
            if ( $files = ftp_nlist( $conn_id, '.' ) ) {
                foreach ( $files as $file ) {
                    if (basename($file) != '.' && basename($file) != '..') {
                        $extension = '.zip';
                        $filename = basename($file);
                        $timestamp = ftp_mdtm( $conn_id, $file );
                        if (substr($filename, ( strlen($extension) * - 1)) === $extension)
                            $filelist[$timestamp] = $filename;
                    }
                }
            }            
            if ($ftp_max_backups > 0) { //Delete old backups in FTP
                if (count($filelist) > $ftp_max_backups) {
                    ksort($filelist);
                    $i = 0;
                    while ($file = array_shift($filelist)) {
                        if (count($filelist) < $ftp_max_backups)
                            break;
                        //delete files on FTP
                        if (ftp_delete( $conn_id, $file )) {
                            $i++;
                        }
                    }
                    if ($i > 0)
                        set_log($i . ' file deleted on FTP');
                }
            }
            ftp_close($conn_id);
            fclose($fp);
            set_log('Uploading backup to FTP completed...<br> FileName: ' . basename($file));
        }
    }
    return $result;
}

function backup_url(){
    
    $file = WPB_API_Authenticate::get_arg('backup_filename');
    
    $dir = get_option('wpb_backup_dir');
    
    if(!empty($file))
        return $dir . '/' . $file;
    
    $backups = array();
    foreach (glob($dir . '/*.zip') as $backup) {
        $backups[filemtime($backup)] = $backup;
    }
    krsort($backups);
        
    return reset($backups);
    
}

function wpb_upload_db() {

    if (get_option('wpb_db_restart') != '') {
        set_log('Starting Dropbox upload...');
    }
    $upload_start = microtime(TRUE);
    require_once(WPBLAZER_PLUGIN_URL . '/WPB-DB.php');
    $consumer_key = WPB_API_Authenticate::get_arg('consumer_key'); // Default Key
    $consumer_secret = WPB_API_Authenticate::get_arg('consumer_secret'); // Default Secret
    $db_max_backups = WPB_API_Authenticate::get_arg('max_backups');
    $db_max_backups = !empty($db_max_backups) ? $db_max_backups : 3;

    try {
        $dropbox = new Dropbox($consumer_key, $consumer_secret, TRUE, $upload_start);
        $dropboxtoken = WPB_API_Authenticate::get_arg('db_token'); // User Token
        $dropbox->setOAuthTokens($dropboxtoken);
        $file = backup_url();
        if (get_option('wpb_db_restart') != '') {
            set_log('Dropbox upload started...');
        }
        $dropbox_directory = trailingslashit(sanitize_file_name(get_bloginfo('name')));  // folder name
        $dropbox_destination = $dropbox_directory . basename($file);
        $response = $dropbox->upload($file, $dropbox_destination, true);

        if ($response == 'restart') {
            return $response;
        } elseif (!empty($response['error'])) {
            set_log('Cannot upload backup to Dropbox<br> Error: ' . $response['error']);
            return FALSE;
        } else {
            $filelist = array();
            $files = $dropbox->listFolder($dropbox_directory);

            if (is_array($files)) {
                foreach ($files as $file) {
                    $extension = '.zip';
                    if ($file['.tag'] == 'file') {
                        $filename = basename($file['path_lower']);
                        $timestamp = strtotime($file['server_modified']);
                        if (substr($filename, ( strlen($extension) * - 1)) === $extension)
                            $filelist[$timestamp] = $filename;
                    }
                }
            }
            if ($db_max_backups > 0 && is_object($dropbox)) { //Delete old backups in Dropbox
                if (count($filelist) > $db_max_backups) {
                    ksort($filelist);
                    $i = 0;
                    while ($file = array_shift($filelist)) {
                        if (count($filelist) < $db_max_backups)
                            break;
                        //delete files on Dropbox
                        $result = $dropbox->filesDelete(array('path' => $dropbox_directory . $file));
                        $i++;
                    }
                    set_log($i . ' file deleted on Dropbox');
                }
            }
            set_log('Uploading backup to dropbox completed...<br> FileName: ' . str_replace('/', '', $response['path_lower']));
            return TRUE;
        }
    } catch (Exception $e) {
        set_log('Dropbox API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        return FALSE;
    }
}

function wpb_upload_myvault() {   
    
    if(get_option('wpb_s3_restart') != ''){
        set_log('Starting MyVault upload...');
    }
    $upload_start = microtime( TRUE );
    $steps_done = get_option('wpb_s3_stepsdone');
    $steps_done = !empty($steps_done) ? $steps_done : "0";
    $amazon_endpoint = WPB_API_Authenticate::get_arg( 'amazon_endpoint' );
    $max_execution_time = (int)@ini_get('max_execution_time');

    if($max_execution_time == 0){
        $max_execution_time = 30;
        @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
        @set_time_limit( 0 );
    }
    $file = backup_url();
    
    $s3_access_key = WPB_API_Authenticate::get_arg( 'as3_access_key' ); // Default Access Key
    $s3_secure_key = WPB_API_Authenticate::get_arg( 'as3_secure_key' ); // Default Secure Key
    $endpoint = !empty($amazon_endpoint) ? $amazon_endpoint : "s3.amazonaws.com";
    $s3_bucket = WPB_API_Authenticate::get_arg( 'as3_bucket' );
    $s3_directory = "WpBlazer3/";  // folder name
    $backup_file = basename($file);  // backup.zip
    $backup_folder = str_replace(basename($file),'',$file);  // backup folder
    
    //Prepare Upload
    if ($file_handle = fopen($backup_folder . $backup_file, 'rb')) {
        fseek($file_handle, $steps_done);
        
        try {
            
            $s3 = Aws\S3\S3Client::factory(array('key' => $s3_access_key,
                        'secret' => $s3_secure_key,
                        'region' => $endpoint,
                        'base_url' => 'https://'.$endpoint,
                        'scheme' => 'https',
                        'ssl.certificate_authority' => true ) );
            
            $s3uploadId = get_option('wpb_s3_uid');

            if (empty($s3uploadId)) {
                $args = array('ACL' => 'private',
                    'Bucket' => $s3_bucket,
                    'ContentType' => 'application/octet-stream',
                    'Key' => $s3_directory . $backup_file);
                if (!empty($s3ssencrypt)) {
                    $args['ServerSideEncryption'] = $s3ssencrypt;
                }
                if (!empty($s3storageclass)) {
                    $args['StorageClass'] = empty($s3storageclass) ? '' : $s3storageclass;
                }

                $upload = $s3->createMultipartUpload($args);

                $s3uploadId = $upload->get('UploadId');
                $s3Parts = array();
                $s3Part = 1;                
            } else {                
                $s3uploadId = get_option('wpb_s3_uid');
                $s3Parts = get_option('wpb_s3_parts');
                $s3Part = get_option('wpb_s3_part');
            }
            
            if(get_option('wpb_s3_restart') != ''){
                set_log('MyVault upload started...');
            }
            while (!feof($file_handle)) {
                $part_data = fread($file_handle, 1048576 * 5); //5MB Minimum part size
                $part = $s3->uploadPart(array('Bucket' => $s3_bucket,
                    'UploadId' => $s3uploadId,
                    'Key' => $s3_directory . $backup_file,
                    'PartNumber' => $s3Part,
                    'Body' => $part_data));
                $steps_done = $steps_done + strlen($part_data);
                $s3Parts[] = array('ETag' => $part->get('ETag'),
                    'PartNumber' => $s3Part);
                $s3Part ++;
                
                add_option('wpb_s3_uid', $s3uploadId) or update_option('wpb_s3_uid', $s3uploadId);
                add_option('wpb_s3_parts', $s3Parts) or update_option('wpb_s3_parts', $s3Parts);
                add_option('wpb_s3_part', $s3Part) or update_option('wpb_s3_part', $s3Part);
                add_option('wpb_s3_stepsdone', $steps_done) or update_option('wpb_s3_stepsdone', $steps_done);
                
                $time_remaining = microtime(TRUE) - $upload_start;
                if ($time_remaining >= ( $max_execution_time - 3 )){
                    add_option('wpb_s3_restart', 1) or update_option('wpb_s3_restart', 1);
                    return 'restart';
                }
            }

            $s3->completeMultipartUpload(array('Bucket' => $s3_bucket,
                'UploadId' => $s3uploadId,
                'Key' => $s3_directory . $backup_file,
                'Parts' => $s3Parts));
        } catch (Exception $e) {
            set_log('MyVault API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
            if (!empty($s3uploadId))
                $s3->abortMultipartUpload(array('Bucket' => $s3_bucket,
                    'UploadId' => $s3uploadId,
                    'Key' => $s3_directory . $backup_file));
            delete_option('wpb_s3_uid');
            delete_option('wpb_s3_parts');
            delete_option('wpb_s3_part');
            delete_option('wpb_s3_stepsdone');
            delete_option('wpb_s3_restart');
            $steps_done = 0;
            if (is_resource($file_handle))
                fclose($file_handle);
            return FALSE;
        }
        fclose($file_handle);
    } else {
        set_log('Cannot open backup file to upload.');
        return FALSE;
    }
    
    delete_option('wpb_s3_uid');
    delete_option('wpb_s3_parts');
    delete_option('wpb_s3_part');
    delete_option('wpb_s3_stepsdone');
    delete_option('wpb_s3_restart');
    set_log('Uploading backup to MyVault completed...<br> FileName: ' . $backup_file);

    return TRUE;
}

function wpb_upload_s3() {   
    
    if(get_option('wpb_s3_restart') != ''){
        set_log('Starting Amazon S3 upload...');
    }
    $upload_start = microtime( TRUE );
    $steps_done = get_option('wpb_s3_stepsdone');
    $steps_done = !empty($steps_done) ? $steps_done : "0";
    $amazon_endpoint = WPB_API_Authenticate::get_arg( 'amazon_endpoint' );
    $max_execution_time = (int)@ini_get('max_execution_time');

    if($max_execution_time == 0){
        $max_execution_time = 30;
        @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
        @set_time_limit( 0 );
    }
    $file = backup_url();
    
    $s3_access_key = WPB_API_Authenticate::get_arg( 'as3_access_key' ); // Default Access Key
    $s3_secure_key = WPB_API_Authenticate::get_arg( 'as3_secure_key' ); // Default Secure Key
    $endpoint = !empty($amazon_endpoint) ? $amazon_endpoint : "s3.amazonaws.com";
    $s3_bucket = WPB_API_Authenticate::get_arg( 'as3_bucket' );
    $s3_directory = trailingslashit( sanitize_file_name( get_bloginfo( 'name' ) ) );  // folder name
    $s3_max_backups = WPB_API_Authenticate::get_arg( 'max_backups' );
    $s3_max_backups = !empty($s3_max_backups) ? $s3_max_backups : 3;
    $backup_file = basename($file);  // backup.zip
    $backup_folder = str_replace(basename($file),'',$file);  // backup folder
    
    //Prepare Upload
    if ($file_handle = fopen($backup_folder . $backup_file, 'rb')) {
        fseek($file_handle, $steps_done);
        
        try {
            
            $s3 = Aws\S3\S3Client::factory(array('key' => $s3_access_key,
                        'secret' => $s3_secure_key,
                        'region' => $endpoint,
                        'base_url' => 'https://'.$endpoint,
                        'scheme' => 'https',
                        'ssl.certificate_authority' => true ) );
            
            $s3uploadId = get_option('wpb_s3_uid');

            if (empty($s3uploadId)) {
                $args = array('ACL' => 'private',
                    'Bucket' => $s3_bucket,
                    'ContentType' => 'application/octet-stream',
                    'Key' => $s3_directory . $backup_file);
                if (!empty($s3ssencrypt)) {
                    $args['ServerSideEncryption'] = $s3ssencrypt;
                }
                if (!empty($s3storageclass)) {
                    $args['StorageClass'] = empty($s3storageclass) ? '' : $s3storageclass;
                }

                $upload = $s3->createMultipartUpload($args);

                $s3uploadId = $upload->get('UploadId');
                $s3Parts = array();
                $s3Part = 1;                
            } else {                
                $s3uploadId = get_option('wpb_s3_uid');
                $s3Parts = get_option('wpb_s3_parts');
                $s3Part = get_option('wpb_s3_part');
            }
            
            if(get_option('wpb_s3_restart') != ''){
                set_log('Amazon S3 upload started...');
            }
            while (!feof($file_handle)) {
                $part_data = fread($file_handle, 1048576 * 5); //5MB Minimum part size
                $part = $s3->uploadPart(array('Bucket' => $s3_bucket,
                    'UploadId' => $s3uploadId,
                    'Key' => $s3_directory . $backup_file,
                    'PartNumber' => $s3Part,
                    'Body' => $part_data));
                $steps_done = $steps_done + strlen($part_data);
                $s3Parts[] = array('ETag' => $part->get('ETag'),
                    'PartNumber' => $s3Part);
                $s3Part ++;
                
                add_option('wpb_s3_uid', $s3uploadId) or update_option('wpb_s3_uid', $s3uploadId);
                add_option('wpb_s3_parts', $s3Parts) or update_option('wpb_s3_parts', $s3Parts);
                add_option('wpb_s3_part', $s3Part) or update_option('wpb_s3_part', $s3Part);
                add_option('wpb_s3_stepsdone', $steps_done) or update_option('wpb_s3_stepsdone', $steps_done);
                
                $time_remaining = microtime(TRUE) - $upload_start;
                if ($time_remaining >= ( $max_execution_time - 3 )){
                    add_option('wpb_s3_restart', 1) or update_option('wpb_s3_restart', 1);
                    return 'restart';
                }
            }

            $s3->completeMultipartUpload(array('Bucket' => $s3_bucket,
                'UploadId' => $s3uploadId,
                'Key' => $s3_directory . $backup_file,
                'Parts' => $s3Parts));
        } catch (Exception $e) {
            set_log('Amazon S3 API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
            if (!empty($s3uploadId))
                $s3->abortMultipartUpload(array('Bucket' => $s3_bucket,
                    'UploadId' => $s3uploadId,
                    'Key' => $s3_directory . $backup_file));
            delete_option('wpb_s3_uid');
            delete_option('wpb_s3_parts');
            delete_option('wpb_s3_part');
            delete_option('wpb_s3_stepsdone');
            delete_option('wpb_s3_restart');
            $steps_done = 0;
            if (is_resource($file_handle))
                fclose($file_handle);
            return FALSE;
        }
        fclose($file_handle);
        
        try {
            $dirfilelist = array();
            $objects = $s3->getIterator('ListObjects', array('Bucket' => $s3_bucket,'Prefix' => $s3_directory));
            if (is_object($objects)) {
                foreach ($objects as $object) {
                    $extension = '.zip';
                    $file = basename($object['Key']);
                    $timestamp = strtotime($object['LastModified']);
                    if ( substr( $file, ( strlen( $extension ) * - 1 ) ) === $extension )
                        $dirfilelist[$timestamp] = $file;
                }
            }
            if ($s3_max_backups > 0 && is_object($s3)) { //Delete old backups in s3
                if (count($dirfilelist) > $s3_max_backups) {
                    ksort($dirfilelist);
                    $i = 0;
                    while ($file = array_shift($dirfilelist)) {
                        if (count($dirfilelist) < $s3_max_backups)
                            break;
                        //delete files on S3
                        $result = $s3->deleteObject(array(
                            'Bucket' => $s3_bucket,
                            'Key' => $s3_directory . $file
                        ));
                        $i++;
                    }
                    if ($i > 0)
                        set_log($i. ' file deleted on Amazon  S3 Bucket');
                }
            }            
        } catch (Exception $e) {
            set_log('S3 Service API: '.$e->getMessage());

            return FALSE;
        }
    } else {
        set_log('Cannot open backup file to upload.');
        return FALSE;
    }
    
    delete_option('wpb_s3_uid');
    delete_option('wpb_s3_parts');
    delete_option('wpb_s3_part');
    delete_option('wpb_s3_stepsdone');
    delete_option('wpb_s3_restart');
    set_log('Uploading backup to Amazon S3 completed...<br> FileName: ' . $backup_file);

    return TRUE;
}

function wpb_upload_gdrive() {

    if(get_option('wpb_gd_restart') != ''){
        set_log('Starting Google Drive upload...');
    }
    $upload_start = microtime( TRUE );
    $backup_folder = sanitize_file_name( get_bloginfo( 'name' ) );
    $backup_file = backup_url();
    $clientKey = WPB_API_Authenticate::get_arg( 'client_key' );
    $clientSecret = WPB_API_Authenticate::get_arg( 'client_secret' );
    $refreshToken = WPB_API_Authenticate::get_arg( 'refresh_token' );
    $max_execution_time = (int)@ini_get('max_execution_time');
    $gdrive_max_backups = WPB_API_Authenticate::get_arg( 'max_backups' );
    $gdrive_max_backups = !empty($gdrive_max_backups) ? $gdrive_max_backups : 3;
    if($max_execution_time == 0){
        $max_execution_time = 30;
        @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
        @set_time_limit( 0 );
    }

    $client = new \Google_Client();
    $client->setClientId($clientKey);
    $client->setClientSecret($clientSecret);
    $client->setScopes(array(
		  'https://www.googleapis.com/auth/drive',
		  'https://www.googleapis.com/auth/userinfo.email',
		  'https://www.googleapis.com/auth/userinfo.profile'));
    
    try {
        $client->refreshToken($refreshToken);
    } catch (Exception $e) {
        set_log('Google Drive API Connection Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        return FALSE;
    }
    $gdrive_service = new \Google_Service_Drive($client);

    if ($backup_folder != null) {
        try {
            $parameters = array();
            $parameters['q'] = "title = '$backup_folder' and trashed = false and 'root' in parents and 'me' in owners and mimeType= 'application/vnd.google-apps.folder'";
            $files = $gdrive_service->files->listFiles($parameters);
            $list_result = array();
            $list_result = array_merge($list_result, $files->getItems());
            $list_result = (array) $list_result;

            if (empty($list_result)) {
                $file = new Google_Service_Drive_DriveFile();
                $file->setTitle($backup_folder);
                $file->setMimeType('application/vnd.google-apps.folder');

                $createdFolder = $gdrive_service->files->insert($file, array(
                    'mimeType' => 'application/vnd.google-apps.folder',
                ));
                if ($createdFolder) {
                    $createdFolder = (array) $createdFolder;
                    $folder_id = $createdFolder['id'];
                }
            } else {
                foreach ($list_result as $v => $f) {
                    $folder_id = $f->id;
                }
            }
        } catch (Exception $e) {
            set_log('Google Drive API Folder Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
            return FALSE;
        }
    }

    $new_file = new \Google_Service_Drive_DriveFile();
    $new_file->setTitle(basename($backup_file));
    $new_file->setDescription('Backup file of site: ' . home_url() . '.');
    $new_file->setMimeType('binary/octet-stream');
    
    $parent = new \Google_Service_Drive_ParentReference();
    if(!empty($folder_id)){
        $parent->setId($folder_id);
    }
    $new_file->setParents(array($parent));
    if(get_option('wpb_gd_restart') != ''){
        set_log('Google Drive upload started...');
    }
    try {
        $client->setDefer(true);
        $request = $gdrive_service->files->insert($new_file);
        $chunkSize = 1024 * 1024 * 4;
        $resume = get_option('wpb_gd_uid');
        $offset = get_option('wpb_gd_offset');
        $resume = !empty($resume) ? $resume : false;
        $offset = !empty($offset) ? $offset : 0;
        
        // Create a media file upload to represent our upload process.
        $media = new \Google_Http_MediaFileUpload($client, $request, 'application/zip', null, true, $chunkSize);
        $media->setFileSize(filesize($backup_file));

        $handle = fopen($backup_file, "rb");
        fseek($handle, $offset);

        while (!feof($handle)) {
            $chunk = fread($handle, $chunkSize);
            $status = $media->nextChunk($chunk, $resume, $offset);
            $resume = $media->wpblazer_getResumeUri();
            $offset = $media->getProgress();
            
            add_option('wpb_gd_uid', $resume) or update_option('wpb_gd_uid', $resume);
            add_option('wpb_gd_offset', $offset) or update_option('wpb_gd_offset', $offset);
            
            $time_remaining = microtime(TRUE) - $upload_start;
            if ($time_remaining >= ( $max_execution_time - 3 )) {
                add_option('wpb_gd_restart', 1) or update_option('wpb_gd_restart', 1);
                return 'restart';
            }
        }

        fclose($handle);
        $client->setDefer(false);    
        
        try {
            $parameters = array();
            $parameters['orderBy'] = 'createdDate desc';
            $parameters['q'] = "trashed = false and mimeType != 'application/vnd.google-apps.folder'";
            $files = $gdrive_service->children->listChildren($folder_id, $parameters);
            $filelist = array();
            foreach ($files->getItems() as $file) {
                $getfile = $gdrive_service->files->get($file->getId());
                $extension = 'zip';
                if ( $getfile->getFileExtension() == $extension )
                    $filelist[$getfile->getCreatedDate()] = $file->getId();
            }

            if ($gdrive_max_backups > 0 && is_array($filelist)) { //Delete old backups in Google Drive
                if (count($filelist) > $gdrive_max_backups) {
                    ksort($filelist);
                    $i = 0;
                    while ($fileId = array_shift($filelist)) {
                        if (count($filelist) < $gdrive_max_backups)
                            break;
                        //delete files on Google Drive
                        $result = $gdrive_service->children->delete($folder_id, $fileId);
                        $i++;
                    }
                    if ($i > 0)
                        set_log($i. ' file deleted on Google Drive');
                }
            }
        } catch (Exception $e) {
            set_log('Google Drive API Error - '.$e->getMessage());
            return FALSE;
        }         
    } catch (Exception $e) {
        set_log('Google Drive API Upload Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        delete_option('wpb_gd_uid');
        delete_option('wpb_gd_offset');
        delete_option('wpb_gd_restart');
        return FALSE;
    }
    
    delete_option('wpb_gd_uid');
    delete_option('wpb_gd_offset');
    delete_option('wpb_gd_restart');
    set_log('Uploading backup to Google Drive completed...<br> FileName: ' . basename($backup_file));
    
    return TRUE;
}

function wpb_upload_msazure() {

    if(get_option('wpb_az_restart') != ''){
        set_log('Starting Windows Azure upload...');
    }
    $upload_start = microtime( TRUE );
    $backup_folder = strtolower(sanitize_file_name( get_bloginfo( 'name' ) ));
    $backup_file = backup_url();
    $azureKey = WPB_API_Authenticate::get_arg( 'azure_key' );
    $azureName = WPB_API_Authenticate::get_arg( 'azure_name' );    
    $azure_max_backups = WPB_API_Authenticate::get_arg( 'max_backups' );
    $azure_max_backups = !empty($azure_max_backups) ? $azure_max_backups : 3;
    $max_execution_time = (int)@ini_get('max_execution_time');
    if($max_execution_time == 0){
        $max_execution_time = 30;
        @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
        @set_time_limit( 0 );
    }

    $offset = get_option('wpb_az_offset');
    $blockIds = get_option('wpb_az_uid');
    $offset = !empty($offset) ? $offset : 0;
    $blockIds = !empty($blockIds) ? $blockIds : array();
    $blobName = basename($backup_file);
    set_include_path( get_include_path() . PATH_SEPARATOR . WPBLAZER_PLUGIN_URL .'/vendor/pear/http_request2/');
    set_include_path( get_include_path() . PATH_SEPARATOR . WPBLAZER_PLUGIN_URL .'/vendor/pear/net_url2/');
    set_include_path( get_include_path() . PATH_SEPARATOR . WPBLAZER_PLUGIN_URL .'/vendor/pear/pear_exception/');

    try {
        $connectionString = "DefaultEndpointsProtocol=http;AccountName=$azureName;AccountKey=$azureKey";
        $connection = WindowsAzure\Common\ServicesBuilder::getInstance()->createBlobService($connectionString);
        
        $containers = $connection->listContainers()->getContainers();
        
        foreach ($containers as $container) {
            if ($container->getName() == $backup_folder) {
                $container_url = $container->getUrl();
                break;
            }
        }

        if (empty($container_url)) {
            $createContainerOptions = new WindowsAzure\Blob\Models\CreateContainerOptions();
            $createContainerOptions->setPublicAccess(WindowsAzure\Blob\Models\PublicAccessType::NONE);
            $connection->createContainer($backup_folder, $createContainerOptions);
        }
        if(get_option('wpb_az_restart') != ''){
            set_log('Windows Azure upload started...');
        }
        //Prepare Upload
        if ($file_handle = fopen($backup_file, 'rb')) {
            fseek($file_handle, $offset);

            while (!feof($file_handle)) {
                $data = fread($file_handle, 1048576 * 4); //4MB
                if (strlen($data) == 0) {
                    continue;
                }

                $counter = count($blockIds) + 1;
                $blockId = md5($data) . str_pad($counter, 6, "0", STR_PAD_LEFT);
                $connection->createBlobBlock($backup_folder, $blobName, $blockId, $data);

                $blockIds[] = $blockId;
                $offset = $offset + strlen($data);
                add_option('wpb_az_uid', $blockIds) or update_option('wpb_az_uid', $blockIds);
                add_option('wpb_az_offset', $offset) or update_option('wpb_az_offset', $offset);

                $time_remaining = microtime(TRUE) - $upload_start;
                if ($time_remaining >= ( $max_execution_time - 3 )) {
                    add_option('wpb_az_restart', $offset) or update_option('wpb_az_restart', $offset);
                    return 'restart';
                }
            }
            fclose($file_handle);            
        }
        
        $blocklist = new WindowsAzure\Blob\Models\BlockList();
        foreach ($blockIds as $block_id) {
            $blocklist->addUncommittedEntry($block_id);
        }
        $connection->commitBlobBlocks($backup_folder, $blobName, $blocklist->getEntries());
        
        try {
            $filelist = array();
            $list_blob_options = new WindowsAzure\Blob\Models\ListBlobsOptions();
            $files = $connection->listBlobs( $backup_folder, $list_blob_options )->getBlobs();
            foreach ($files as $file) {
                $filename = basename($file->getName());
                $extension = '.zip';
                $timestamp = $file->getProperties()->getLastModified()->getTimestamp();
                if (substr($filename, ( strlen($extension) * - 1)) === $extension)
                    $filelist[$timestamp] = $filename;
            }

            if ($azure_max_backups > 0 && is_array($filelist)) { //Delete old backups in Windows Azure
                if (count($filelist) > $azure_max_backups) {
                    ksort($filelist);
                    $i = 0;
                    while ($file = array_shift($filelist)) {
                        if (count($filelist) < $azure_max_backups)
                            break;
                        //delete files on Windows Azure
                        $connection->deleteBlob( $backup_folder, $file );
                        $i++;
                    }
                    if ($i > 0)
                        set_log($i . ' file deleted on Windows Azure');
                }
            }
        } catch (Exception $e) {
            set_log('Windows Azure API Error - ' . $e->getMessage());
            return FALSE;
        }
    } catch (Exception $e) {
        set_log('Windows Azure API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        delete_option('wpb_az_uid');
        delete_option('wpb_az_offset');
        delete_option('wpb_az_restart');
        return FALSE;
    }
    
    delete_option('wpb_az_uid');
    delete_option('wpb_az_offset');
    delete_option('wpb_az_restart');
    set_log('Uploading backup to Windows Azure completed...<br> FileName: ' . $blobName);
    
    return TRUE;
}

function wpb_upload_rackspace() {

    set_log('Starting RackSapce upload...');
    $upload_start = microtime( TRUE );
    $backup_folder = sanitize_file_name( get_bloginfo( 'name' ) );
    $backup_file = backup_url();
    $rackspaceKey = WPB_API_Authenticate::get_arg( 'rackspace_key' );
    $rackspaceName = WPB_API_Authenticate::get_arg( 'rackspace_name' );
    $rackspaceRegion = WPB_API_Authenticate::get_arg( 'rackspace_region' );
    $rackspace_max_backups = WPB_API_Authenticate::get_arg( 'max_backups' );
    $rackspace_max_backups = !empty($rackspace_max_backups) ? $rackspace_max_backups : 3;
    $max_execution_time = (int)@ini_get('max_execution_time');
    if($max_execution_time == 0){
        $max_execution_time = 30;
    }
    @ini_set( 'memory_limit', apply_filters( 'admin_memory_limit', WP_MAX_MEMORY_LIMIT ) );
    @set_time_limit( 0 );

    if($rackspaceRegion == 'LON'){
        $regionUrl = 'https://lon.identity.api.rackspacecloud.com/v2.0/';
    } else {
        $regionUrl = 'https://identity.api.rackspacecloud.com/v2.0/';
    }
    
    try {
        $client = new \OpenCloud\Rackspace($regionUrl,array('username' => $rackspaceName,'apiKey' => $rackspaceKey ));
        $objectStoreService = $client->objectStoreService( 'cloudFiles' , $rackspaceRegion, 'publicURL' );
        $objectStoreService->createContainer( $backup_folder );
        $container = $objectStoreService->getContainer( $backup_folder );        
    } catch (Exception $e) {
        set_log('RackSapce API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        return FALSE;
    }    

    $offset = get_option('wpb_rs_offset');
    $offset = !empty($offset) ? $offset : 0;
    try {
        set_log('RackSapce upload started...'); 
        if ($file_handle = fopen($backup_file, 'rb')) {
            fseek($file_handle, $offset);
            
            $uploaded = $container->uploadObject(basename($backup_file), $file_handle);
            
            add_option('wpb_rs_offset', $offset) or update_option('wpb_rs_offset', $offset);
            fclose($file_handle);
        } else {
            set_log('Cannot open backup file to upload.');
            return FALSE;
        }
        
        if ($uploaded) {
            try {
                $files = $container->objectList();
                $filelist = array();
                foreach ( $files as $file ) {
                    $filename = basename( $file->getName() );
                    $extension = '.zip';
                    $timestamp = strtotime($file->getLastModified());
                    if ( substr( $filename, ( strlen( $extension ) * - 1 ) ) === $extension )
                            $filelist[$timestamp] = $file;
                }

                if ($rackspace_max_backups > 0 && is_array($filelist)) { //Delete old backups in RackSapce
                    if (count($filelist) > $rackspace_max_backups) {
                        ksort($filelist);
                        $i = 0;
                        while ($file = array_shift($filelist)) {
                            if (count($filelist) < $rackspace_max_backups)
                                break;
                            //delete files on RackSapce
                            $file->delete();
                            $i++;
                        }
                        if ($i > 0)
                            set_log($i . ' file deleted on RackSapce');
                    }
                }
            } catch (Exception $e) {
                set_log('RackSapce API Error - ' . $e->getMessage());
                return FALSE;
            }
            set_log('Uploading backup to RackSapce completed...<br> FileName: ' . basename($backup_file));
            return TRUE;
        }
    } catch (Exception $e) {
        set_log('RackSapce API Error - ' . $e->getMessage() . $e->getFile() . $e->getLine());
        return FALSE;
    }
}