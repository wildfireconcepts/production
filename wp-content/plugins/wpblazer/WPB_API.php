<?php
// Disable error_reporting so they don't break the json request
if ( ! defined( 'WP_DEBUG' ) || ! WP_DEBUG )
	error_reporting( 0 );

// Log in as admin
wp_set_current_user( 1 );

$actions = array();

foreach( WPB_API_Authenticate::get_actions() as $action ) {

	switch( $action ) {            

		case 'do_backup' :
                    
			if ( in_array( WPB_API_Authenticate::get_arg( 'backup_type' ), array( 'complete', 'database', 'file' ) ) )
				Back_dir::get_instance()->set_backup_type( WPB_API_Authenticate::get_arg( 'backup_type' ) );

                        $actions[$action] = Back_dir::get_instance()->do_backup();
                        
                break;
                        
		case 'delete_backup' :
                        $actions[$action] = Back_dir::get_instance()->clear();
                    
                break;
                    
		case 'get_backup' :
			$actions[$action] = Back_dir::get_instance()->get_backup();
                    
		break;
            
                case 'get_backups' :
			$actions[$action] = Back_dir::get_instance()->get_backups();
                    
		break;

		default :
		
			// Function Calling
			$func_name = "wpb_".$action;
			
			$actions[$action] = $func_name();

		break;

	}

}

echo json_encode( $actions );