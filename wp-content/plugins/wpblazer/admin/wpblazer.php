<?php
/**
 * Add API Key to form
 */
function wpblazer_add_api_key() { ?>

<div class="wrap">

	<h2><?php _e( 'WP Blazer', 'wp-blazer' ); ?></h2>

	<div id="wpblazer-message" class="manage-menus">

		<form method="post" action="options.php">

			<p>

				<label style="vertical-align: baseline;" for="wpblazer_api_key"><strong><?php _e( 'Enter your Blog API key', 'wp-blazer' ); ?></strong></label>

				<input type="text" style="margin-left: 5px; margin-right: 5px;" class="code regular-text" id="wpblazer_api_key" name="wpblazer_api_key" value="<?php echo get_option( 'wpblazer_api_key' ); ?>"  />

				<input name="wpb-submit" id="wpb-submit" type="submit" value="<?php _e( 'Save Settings','wp-blazer' ); ?>" class="button button-primary" />

			</p>

			<?php settings_fields( 'wpblazer-settings' );

			// Output any sections defined for page sl-settings
			do_settings_sections( 'wpblazer-settings' ); ?>

		</form>

	</div>

</div>

<?php }