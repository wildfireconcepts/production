<?php

/*
 * 
 */
function wpb_activation(){
    wpb_create_table();
}

/*
 * Create database
 * it will call when plugin activate
 */
function wpb_create_table(){
    
    global $wpdb;
    $table_name = $wpdb->prefix.'wpbmeta';
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name';") != $table_name) {
        $sql = "CREATE TABLE `".$wpdb->prefix."wpbmeta` (
                `post_id` INT( 20 ) NOT NULL ,
                `meta_key` VARCHAR ( 255 ) NOT NULL,
    		`meta_value` VARCHAR ( 255 ) NOT NULL,
    		`meta_count` INT( 20 ) NOT NULL DEFAULT '1',
    		`last_modified` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,	
    		PRIMARY KEY ( `post_id` , `meta_value` )		
    		) CHARACTER SET utf8
			DEFAULT CHARACTER SET utf8
			COLLATE utf8_general_ci
			DEFAULT COLLATE utf8_general_ci;";	
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta( $sql );
   }

}

/*
 * 
 */
function wpb_head_hook(){
    
    $referer = wpb_get_referer();
    if (!$referer) return FALSE;
    $query = wpb_get_query($referer);
    if($query){
        $keyword = wpb_get_keyword($query);
        if($keyword)
            wpb_save_data($referer,$keyword);
    }
    if(!$query){
        if (!strpos('ref:'.site_url(),$referer))
            wpb_save_data($referer);
    }
    
}

add_action('wp_head', 'wpb_head_hook');

/*
 * 
 */
function wpb_get_referer(){
    
    if(!isset($_SERVER['HTTP_REFERER']) || ($_SERVER['HTTP_REFERER'] == '')) return false;
    $parsed_url = parse_url($_SERVER['HTTP_REFERER']);
    $referer = $parsed_url['host'];
    $referer = preg_replace('#^www\.(.+\.)#i', '$1', $referer);
    return $referer; 
    
}

function wpb_get_query($ref){
    
    $search_engines = array('google.com' => 'q',
			'search.yahoo.com' => 'p',
			'search.msn.com' => 'q',
			'bing.com' => 'q',
			'msxml.excite.com' => 'qkw',
			'search.lycos.com' => 'query',
			'alltheweb.com' => 'q',
			'search.aol.com' => 'query',
			'search.iwon.com' => 'searchfor',
			'ask.com' => 'q',
			'ask.co.uk' => 'ask',
			'search.cometsystems.com' => 'qry',
			'hotbot.com' => 'query',
			'overture.com' => 'Keywords',
			'metacrawler.com' => 'qkw',
			'search.netscape.com' => 'query',
			'looksmart.com' => 'key',
			'dpxml.webcrawler.com' => 'qkw',
			'search.earthlink.net' => 'q',
			'search.viewpoint.com' => 'k',
			'yandex.kz' => 'text',
			'yandex.ru' => 'text',
			'baidu.com' => 'wd',			
			'mamma.com' => 'query');
    
    $query = false;
    if (isset($search_engines[$ref])) {
        $query = $search_engines[$ref];
    } else {
        if (strpos('ref:'.$ref,'google'))
            $query = "q";
		elseif (strpos('ref:'.$ref,'search.atomz.'))
            $query = "sp-q";
		elseif (strpos('ref:'.$ref,'search.msn.'))
            $query = "q";
		elseif (strpos('ref:'.$ref,'search.yahoo.'))
            $query = "p";
		elseif (strpos('ref:'.$ref,'yandex'))
            $query = "text";
		elseif (strpos('ref:'.$ref,'baidu'))
            $query = "wd";	
        elseif (preg_match('/home\.bellsouth\.net\/s\/s\.dll/i', $ref))
            $query = "bellsouth";
    }
    return $query;
    
}

function wpb_get_keyword($q){
    $query_array = array();
    $query = explode($q.'=', $_SERVER['HTTP_REFERER']);
    $query = explode('&', $query[1]);
    $query = urldecode($query[0]);
    $query = str_replace("'", '', $query);
    $query = str_replace('"', '', $query);
    $query_array = preg_split('/[\s,\+\.]+/',$query);
    $query_terms = implode(' ', $query_array);
    $terms = htmlspecialchars(urldecode(trim($query_terms)));
    return $terms;
}

function wpb_save_data($ref,$key = NULL){
    if( is_home() ){
        $ID = 0;
    } else {
        global $post;
        $ID = $post->ID;
    }
    global $wpdb;
    $result = $wpdb->query( $wpdb->prepare( "INSERT INTO ".$wpdb->prefix."wpbmeta ( `post_id`,`meta_value`,`meta_key`,`meta_count` ) VALUES ( %s, %s, %s, 1 )
			ON DUPLICATE KEY UPDATE `meta_count` = `meta_count` + 1", $ID, $key, $ref ) );	
}


function wpb_get_popular_terms(){
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT * FROM `".$wpdb->prefix."wpbmeta` WHERE `post_id` != 0 ORDER BY `meta_count` DESC LIMIT ".$count.";" ); 
    return $result;

}

function wpb_get_recent_terms(){
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT * FROM `".$wpdb->prefix."wpbmeta` WHERE `post_id` != 0 ORDER BY `last_modified` DESC LIMIT ".$count.";" );			
    return $result;
        
}


function wpb_get_search_engine_traffic(){
    
    $search_engines = array('google','search.yahoo','search.msn','bing.com','msxml.excite','search.lycos',
        'alltheweb','search.aol','search.iwon','ask.com','ask.co.uk','search.cometsystems','hotbot',
        'overture','metacrawler','search.netscape','looksmart','dpxml.webcrawler','search.earthlink',
        'search.viewpoint','yandex.kz','yandex.ru','yandex','baidu.com','mamma.com','bellsouth','baidu');
    $search = implode("%' OR meta_key LIKE '%", $search_engines);
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT * FROM `".$wpdb->prefix."wpbmeta` WHERE `meta_key` LIKE '%".$search."%' ORDER BY `meta_count` DESC LIMIT ".$count.";");
    return $result;
    
}

function wpb_get_social_media_traffic(){
    
    /*$social_media = array('facebook','twitter','plus.google','pinterest','feedburner','linkedin','youtube',
        'vimeo','dailymotion','vine','flickr','500px','instagram','wordpress.','tumblr','blogger','reddit',
        'dribbble','stumbleupon','digg','envato','behance','delicious','forrst','playstore','zerply','wikipedia',
        'apple','flattr','github','chimein','friendfeed','newsvine','identica','steam','xbox','windows',
        'outlook','coderwall','tripadvisor','appnet','goodreads','slideshare','buffer','rss','disqus',
        'patreon','paypal','playstation','smugmug','triplej','yammer','stackoverflow','drupal','android',
        'meetup','persona','t.co'); */

    $social_media = array('facebook','twitter','plus.google','pinterest','feedburner','linkedin','youtube',
        'vimeo','dailymotion','vine','flickr','500px','instagram','wordpress.','tumblr','blogger','reddit',
        'dribbble','stumbleupon','digg','envato','behance','delicious','forrst','playstore','zerply','wikipedia’,’t.co');   
    $social = implode("%' OR meta_key LIKE '%", $social_media);
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT `meta_key`,sum(`meta_count`),`post_id`,`meta_value` FROM `".$wpdb->prefix."wpbmeta` WHERE `meta_key` LIKE '%".$social."%' GROUP BY `meta_key` ORDER BY sum(`meta_count`) DESC LIMIT ".$count.";");
    return $result;
    
}

function wpb_get_backlink_traffic(){
    
    $search_engines = array('google','search.yahoo','search.msn','bing.com','msxml.excite','search.lycos',
        'alltheweb','search.aol','search.iwon','ask.com','ask.co.uk','search.cometsystems','hotbot',
        'overture','metacrawler','search.netscape','looksmart','dpxml.webcrawler','search.earthlink',
        'search.viewpoint','yandex.kz','yandex.ru','yandex','baidu.com','mamma.com','bellsouth','baidu');
    $social_media = array('facebook','twitter','plus.google','pinterest','feedburner','linkedin','youtube',
        'vimeo','dailymotion','vine','flickr','500px','instagram','wordpress.','tumblr','blogger','reddit',
        'dribbble','stumbleupon','digg','envato','behance','delicious','forrst','playstore','zerply','wikipedia’,’t.co');  
    $result = array_merge($search_engines, $social_media);
    $site = implode("%' AND `meta_key` NOT LIKE '%", $result);
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT `meta_key`,sum(`meta_count`),`post_id`,`meta_value` FROM `".$wpdb->prefix."wpbmeta` WHERE `meta_key` NOT LIKE '%".$site."%' GROUP BY `meta_key` ORDER BY sum(`meta_count`) DESC LIMIT ".$count.";");
    return $result;
    
}

function wpb_get_page_traffic(){
    
    $search_engines = array('google','search.yahoo','search.msn','bing.com','msxml.excite','search.lycos',
        'alltheweb','search.aol','search.iwon','ask.com','ask.co.uk','search.cometsystems','hotbot',
        'overture','metacrawler','search.netscape','looksmart','dpxml.webcrawler','search.earthlink',
        'search.viewpoint','yandex.kz','yandex.ru','yandex','baidu.com','mamma.com','bellsouth','baidu');
    $search = implode("%' OR s.meta_key LIKE '%", $search_engines);
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT s.post_id, s.meta_key, s.meta_value, sum(s.meta_count) as meta_count, p.ID, p.guid, p.post_title FROM ".$wpdb->prefix."wpbmeta s LEFT JOIN ".$wpdb->prefix."posts p on p.ID = s.post_id WHERE s.meta_key LIKE '%".$search."%' GROUP BY s.post_id ORDER BY meta_count DESC LIMIT ".$count.";");
    
    return $result;
    
}

function wpb_get_socail_page_traffic(){
    
    $social_media = array('facebook','twitter','plus.google','pinterest','feedburner','linkedin','youtube',
        'vimeo','dailymotion','vine','flickr','500px','instagram','wordpress.','tumblr','blogger','reddit',
        'dribbble','stumbleupon','digg','envato','behance','delicious','forrst','playstore','zerply','wikipedia’,’t.co'); 
    $social = implode("%' OR meta_key LIKE '%", $social_media);
    
    global $wpdb;
    $get_count = (int)WPB_API_Authenticate::get_arg( 'count' );
    $count = !empty($get_count) ? $get_count : 50;
    $result = $wpdb->get_results( "SELECT s.post_id, s.meta_key, s.meta_value, sum(s.meta_count) as meta_count, p.ID, p.guid, p.post_title FROM ".$wpdb->prefix."wpbmeta s LEFT JOIN ".$wpdb->prefix."posts p on p.ID = s.post_id WHERE s.meta_key LIKE '%".$social."%' GROUP BY s.post_id ORDER BY meta_count DESC LIMIT ".$count.";");
    
    return $result;
    
}
