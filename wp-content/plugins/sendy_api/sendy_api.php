<?php
/*
Plugin Name: Sendy API
Description: ...
Version: 0.0.1
Author: Wildfire Concepts
Author URI: http://wildfireconcepts.com/
*/
defined('ABSPATH') or die();
define('S_API_VERSION', '0.0.1');
define('S_API_FOLDER', 'sendy_api');
define('S_API_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
define('S_API_PLUGINS_URL', plugins_url( '', __FILE__ ));
define('S_API_PLUGINS_BASENAME', plugin_basename(__FILE__));
define('S_API_PLUGIN_FILE', __FILE__);
add_action('admin_menu', 'sendy_admin_menu');
add_filter('wp_enqueue_styles', 's_api_load_admin_styles');
add_filter('wp_enqueue_scripts', 's_api_load_admin_scripts');
function sendy_admin_menu() {
	// Add the menu page
	$hook_name = add_menu_page( 'Sendy API',
		'Sendy API',
		'manage_options',
		S_API_FOLDER . '-admin-menu',
		's_api_load_main_page',
		'',
		383);
	add_action( 'admin_print_styles-' . $hook_name, 's_api_load_admin_styles');
	add_action( 'admin_print_scripts-' . $hook_name, 's_api_load_admin_scripts');
}
function s_api_load_main_page(){require "settings.php";}
function s_api_load_admin_styles() {
	$rand = rand(0,9);
	wp_register_style(S_API_FOLDER.'-page-stylesheet', S_API_PLUGINS_URL.'/includes/admin_style.css',array(), '1.'.$rand);
	wp_enqueue_style(S_API_FOLDER . '-page-stylesheet');
}
function s_api_load_admin_scripts() {
	$rand = rand(0,9);
	// wp_localize_script( WPSW_FOLDER . '-script', 'frontendajax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));
	wp_register_script(S_API_FOLDER.'-script', S_API_PLUGINS_URL.'/includes/admin_script.js', array('jquery'), '1.'.$rand);
	wp_enqueue_script('jquery');
	wp_enqueue_script(S_API_FOLDER . '-script');
}
add_action ('sendy_subscribe', 'sendy_add_subscriber_func', 10, 4 );
function sendy_add_subscriber_func($fullname, $email, $userid,$affid){
	$options = get_option('s_api-options');
	$list_id = $options['list_id'];
	$sendy_url = $options['url'];
	if($fullname && $email && $list_id && $sendy_url){
		$postdata = http_build_query(
	    array(
			'name' => $fullname,
			'email' => $email,
			'list' => $list_id,
			'UserID' => $userid,
			'AffID' => $affid,
			'boolean' => 'true'
			)
		);
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents($sendy_url.'/subscribe', false, $context);
		/*if($result == 1){
			echo 'Subscriber Added';
		}else{
			echo $result;
		}*/
	}
}
add_action ('sendy_unsubscribe', 'sendy_unsubscriber_func', 10, 4 );
function sendy_unsubscriber_func($email){
	$options = get_option('s_api-options');
	$list_id = $options['list_id'];
	$sendy_url = $options['url'];
	if($email && $list_id && $sendy_url){
		$postdata = http_build_query(
			array(
			'email' => $email,
			'list' => $list_id,
			'boolean' => 'true'
			)
		);
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents($sendy_url.'/unsubscribe', false, $context);
		/*if($result == 1){
			echo 'Subscriber Removed';
		}else{
			echo $result;
		}*/
	}
}
add_action ('sendy_delete_subscriber', 'sendy_delete_subscriber_func', 10, 4 );
function sendy_delete_subscriber_func($email){
	$options = get_option('s_api-options');
	$list_id = $options['list_id'];
	$sendy_url = $options['url'];
	$api_key = $options['api_key'];
	if($email && $list_id && $sendy_url && $api_key){
		$postdata = http_build_query(
			array(
			'api_key' => $api_key,
			'email' => $email,
			'list_id' => $list_id
			)
		);
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents($sendy_url.'/api/subscribers/delete.php', false, $context);
		/*if($result == 1){
			echo 'Subscriber Deleted';
		}else{
			echo $result;
		}*/
	}
}
add_action ('sendy_subscription_status', 'sendy_subscription_status_func', 10, 4 );
function sendy_subscription_status_func($email){
	$options = get_option('s_api-options');
	$list_id = $options['list_id'];
	$sendy_url = $options['url'];
	$api_key = $options['api_key'];
	if($email && $list_id && $sendy_url && $api_key){
		$postdata = http_build_query(
			array(
			'api_key' => $api_key,
			'email' => $email,
			'list_id' => $list_id
			)
		);
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents($sendy_url.'/api/subscribers/subscription-status.php', false, $context);
		// echo $result;
	}
}
add_action ('sendy_active_subscriber_count', 'sendy_active_subscriber_count_func', 10, 4 );
function sendy_active_subscriber_count_func(){
	$options = get_option('s_api-options');
	$list_id = $options['list_id'];
	$sendy_url = $options['url'];
	$api_key = $options['api_key'];
	if($list_id && $sendy_url && $api_key){
		$postdata = http_build_query(
			array(
			'api_key' => $api_key,
			'list_id' => $list_id
			)
		);
		$opts = array('http' => array('method'  => 'POST', 'header'  => 'Content-type: application/x-www-form-urlencoded', 'content' => $postdata));
		$context  = stream_context_create($opts);
		$result = file_get_contents($sendy_url.'/api/subscribers/active-subscriber-count.php', false, $context);
		// echo $result;
	}
}