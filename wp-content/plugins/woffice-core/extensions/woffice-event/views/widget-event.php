<?php if ( ! defined( 'ABSPATH' ) ) die( 'Direct access forbidden.' );

global $bp;

echo $before_widget;
echo $title;

$current_user = wp_get_current_user();

if ($current_user) {
	echo do_shortcode("[woffice_calendar widget='true' visibility='personal' id='{$current_user->id}']");
}

echo $after_widget;
