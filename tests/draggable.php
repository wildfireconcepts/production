<!doctype html> 
<html lang="en"> 
<head> 
<title>jQuery UI Droppable</title> 
<style type="text/css"> 
#sortable1, #sortable2, #sortable3 { list-style-type: none; margin: 0; float: left; margin-right: 10px; background: #eee; padding: 5px; width: 143px;}
#sortable1 li, #sortable2 li, #sortable3 li { margin: 5px; padding: 5px; font-size: 1.2em; width: 120px;cursor:pointer; }
#sortable2 li.ui-state-highlight{padding: 21px;width: 100%;}
</style> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
</script> 
<script> 
$(function() {
    $("ul.droptrue").sortable({
        connectWith: "ul",
        revert: false,
		helper: function (e, li) {
			copyHelper = li.clone().insertAfter(li);
			return li.clone();
		},
        /*receive: function(event, ui) {
            var formula = [];
            ui.item.attr('draggable', "false").removeClass('ui-state-highlight').addClass('ui-state-default');
            $('#sortable2 li').each(function(index) {
                formula.push('{' + $(this).html() + '}');
            });
            $('#basedatadrag').val(formula.join('+'));
        }*/
    });

    $("ul.dropfalse").sortable({
        connectWith: "ul",
        revert: true,
        receive: function(event, ui) {
            var formula = [];
            ui.item.attr('draggable', "false").removeClass('ui-state-default').addClass('ui-state-highlight');
			var div_id = ui.item.attr("data");
			ui.item.html($('#'+div_id).html());
            // $('#basedatadrag').val(formula.join('+'));
        }
    }).draggable(false);
    $("#sortable1, #sortable2").disableSelection();
}); 
</script> 
</head> 
<body>         
        <ul id="sortable1" class="droptrue">
            Base Data
            <li class="ui-state-default" data="item_1">Email Swipes</li>
            <li class="ui-state-default">Item 2</li>
            <li class="ui-state-default">Item 3</li>
            <li class="ui-state-default">Item 4</li>
            <li class="ui-state-default">Item 5</li>
            <li class="ui-state-default">Item 6</li>
        </ul>

        <ul id="sortable2" class="dropfalse">
            Hierarchy
        </ul>
	<input id="basedatadrag" type="hidden" style="width: 100%">
	<div id="item_1" style="display:none;">
		Email Swipes
		<input type="text" />
	</div>
</body> 
</html>					 
