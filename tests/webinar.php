<?php
if(isset($_GET['code'])){
	$ch = curl_init();
	//?grant_type=authorization_code&code=eyJraWQiOiJvYXV0aHYyLmxtaS5jb20uMDIxOSIsImFsZyI6IlJTNTEyIn0.eyJscyI6ImYyMzgwOTBiLWEzMjUtNDMyZC1hNThmLWFhNzI3ZTBjMGE2OCIsIm9nbiI6InB3ZCIsInVyaSI6Imh0dHBzOi8vZnVubmVsbWF0ZXMuY29tL3Rlc3RzL3dlYmluYXIucGhwIiwic2MiOiJjb2xsYWI6IiwiYXVkIjoiZDkyYjMwNGEtOTM0NC00Y2I0LWEwMTItNTY5NDdjODE1MWZmIiwic3ViIjoiMTk3NzUwMDY1NzM4MzgzOTc1NiIsImp0aSI6ImViZGZkNzU0LTU0MWYtNDM3Mi1iODYwLTU3ODM4Y2IyZWY2MCIsImV4cCI6MTYxNDY2Nzg5OSwiaWF0IjoxNjE0NjY3Mjk5LCJ0eXAiOiJjIn0.XMsulMS_33_I6FTHCECnuY2-EYOXa065BG9GXO-HDhkjWpgoAmXZAIkuf2ex-WDKYJYhBg0w9wagtQT2wU0aG3ekJGwNYGX035CG4Tzlv329YlFJzlhWjulhCYmIb5o1EI8IgnZD0XtUPQOs79kwsYmau61qN_jfwE6lQR8H4uoMcc1bSAK__0w9j-DXWdFiARhdXbeHKmVhDaKgaeEM9kt4RTI6udr5QafeuhlDRpLLLqN8RBfxhW4f99GxYCBI_7bL_fvDrZ3se8gtLHmHO1iSZlSDYPeVKHKhtTAVJhCAS98tcqz1NqtghCPYpnvLaWxiHXWpmha4KTk7zCf1Pw&r
	curl_setopt($ch, CURLOPT_URL, 'https://api.getgo.com/oauth/v2/token');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=authorization_code&code=".$_GET['code']);

	$headers = array();
	$headers[] = 'Authorization: Basic '.base64_encode('d92b304a-9344-4cb4-a012-56947c8151ff:UcRhLAxv81il8dp2s8mxHrSN');
	$headers[] = 'Accept: application/json';
	$headers[] = 'Content-Type: application/x-www-form-urlencoded';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);
	// echo '<pre>';
	$result = json_decode($result);
	echo '<p><strong>'.$result->email.'</strong> is Connected<p>';
}else{
	?>
	<form action="https://api.getgo.com/oauth/v2/authorize" method="GET">
		<input type="hidden" name="redirect_uri" value="https://funnelmates.com/tests/webinar.php" />
		<input type="hidden" name="client_id" value="d92b304a-9344-4cb4-a012-56947c8151ff" />
		<input type="hidden" name="response_type" value="code" />
		<input type="submit" name="submit" value="Authorize" />
	</form>
	<?php 
}