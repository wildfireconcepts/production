<?php
chdir(__DIR__);
require_once(__DIR__ . "/wp-load.php");

$manual_users = get_users(array(
    'meta_key' => 'is_annual_buyer',
    'meta_value' => '1'
));
foreach($manual_users as $mu){
	$rebilled = get_user_meta($mu->ID,'last_annual_credit',true);
	$stop = true;
	if($rebilled != ''){
		$rebilled = str_replace('/', '-', $rebilled);
		$dateDiff = dateDiffInDays($rebilled, date('d-m-Y'));
		if($dateDiff >= 30){
			$stop = false;
		}
	}else{
		$buydate = get_user_meta($mu->ID,'annual_buy_date',true);
		$buydate = str_replace('/', '-', $buydate);
		$dateDiff = dateDiffInDays($buydate, date('d-m-Y'));
		if($dateDiff >= 30){
			$stop = false;
		}
	}
	if($stop == false){
		$prod = get_user_meta($mu->ID,'annual_product',true);
		$added_value = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='C' AND user_id='".$mu->ID."' ");
		$used_value = $wpdb->get_var("SELECT SUM(value) FROM `wp_setc_pks_balance` WHERE type='D' AND user_id='".$mu->ID."' ");
		if(!$added_value){
			$added_value = 0;
		}
		if(!$used_value){
			$used_value = 0;
		}
		$nowithdrawal = $added_value - $used_value;
		if($nowithdrawal < 30){
			$credit = 30-$nowithdrawal;
		}else{
			$credit = 0;
		}
		if($prod == 364466 || $prod == 364468){ // 364466 - FunnelMates Platinum Annual, 364468 - FunnelMates Platinum One Time
			$wpdb->query("INSERT INTO `wp_setc_pks_balance` (value,user_id,type) VALUES ('{$credit}','{$mu->ID}','C')");
			if($credit < 30){
				$month_f = $wpdb->get_row("SELECT * FROM `wp_setc_free_funnels` WHERE `month`='".date('m', strtotime("now"))."'");
				update_user_meta($mu->ID,'last_annual_credit',date('d/m/Y'));
				if($month_f->funnel_id){
					free_rebill_funnel($month_f->funnel_id,$mu->ID);
				}
			}
		}
	}
}
function dateDiffInDays($date1, $date2){
    $diff = strtotime($date2) - strtotime($date1);
    return abs(round($diff / 86400));
}